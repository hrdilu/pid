# TCP metro state_position

-   Data chodí na TCP-IG

```xml
<m linka="A" tm="2022-07-20T13:45:34Z" gvd="GD20a">
    <vlak csp=" 3" csr=" 3" cv="279" ko="1809" odch="25" />
    <!--...-->
</m>
```

-   Transport (TCP) Input Gateway
    -   Raw data se uloží na blob storage (1MB buffer)
    -   knihovnou `xml2js` transformují na JSON
    -   pošlou na Rabbita do fronty `saveMetroRunsToDB`
-   saveMetroRunsToDB
    -   zprávy se uloží do DB
    -   následně se pošlou do fronty `processMetroRunMessages`,
-   processMetroRunMessages
    -   data se napasují na struktury tabulek `vehiclepositions_trips` a `vehiclepositions_positions` (stav je `unknown`, `tracking` je vždy 2)
    -   Rovněž se podle kmenové linky a oběhu vytvoří následující spoje (stav je `unknown`, `tracking` je vždy 0)
    -   navíc se předchozímu spoji vytvoří nová pozice s `tracking 0`
    -   všechny spoje se odešlou na `updateDelay`
-   updateDelay
    -   upsert asociovaných pozic
    -   opětovné načtení asociovaných pozic z DB
    -   určení trasy spoje podle GTFS shapes a stop times
    -   processing pozic, uložení do databáze
        -   Not tracking (`tracking` je `0`)
            -   pokud je poslední pozice neznámá, nebo je známá a ve stavu `on_track` nebo `at_stop`, je aktuální stav `before_track`
            -   pokud je poslední známá pozice s `tracking 2`
                -   pokud je pozice duplicitní (existuje pozice se stejným `origin_timestamp`), stav je `duplicate`
                -   jinak je stav `after_track`
        -   Tracking (`tracking` je `2`)
            -   pokud je vozidlo 200 metrů od nejblizšího bodu na trase
                -   pokud vozidlo poslalo zprávu z `tm` + `odch` atributy větší než je příjezd do poslední zastávky, je stav `after_track`
                -   jinak je stav `off_track`
            -   pokud je vozidlo podle kolejového obvodu v zastávce, je stav `at_stop`
            -   pokud je vozidlo podle kolejového obvodu mimo zastávku, je stav `on_track`
-   propagateDelay
    -   podle kmenové linky a oběhu vyhledáme navazující spoje
    -   podle GTFS trip id a registračního čísla poslední pozici ve stavu `before_track`
    -   přepsání stavu na `before_track_delayed`

```mermaid
flowchart TB;
  A[TCP data]-->B[Transport TCP Input Gateway]--raw data 1MB buffer-->Blob[Azure Blob Storage];
  B-->X1[AMQP saveMetroRunsToDB]-->|oběhy| N[AMQP processMetroRunMessages];
  X1--upsert-->runs[(DB runs)];

  N--upsert-->trips;
  N--upsert-->positions;
  N--vygenerované spoje s GTFS daty-->E[AMQP updateDelay];
  E--upsert-->trips[(DB trips)];
  E--upsert-->positions[(DB positions)];
  E--select-->trips;
  E--select-->positions;
  E--uložené spoje-->F[AMQP propagateDelay];
  F--select-->trips;
  F--select-->positions;
  F--upsert-->positions;
```
