openapi: 3.0.3
info:
    title: 🚋 Public Transport | Golemio Input Gateway
    description: >-
        <p>Open RESTful API gateway for Golemio.cz Prague Data Plafrorm.</p>
        <p>Publisher of the data is ROPID. Schedules are valid for upcoming ~12
        days, but can change as they are updated every day.</p> <p>More about at <a
        href="https://pid.cz/o-systemu/opendata/">pid.cz</a>.</p>
    version: 1.0.0
    contact:
        name: Golemio Prague Data Plaform
        email: golemio@operatorict.cz
        url: https://golemio.cz
servers:
    - url: https://api.golemio.cz/v2
      description: Main (production) server
    - url: https://rabin.golemio.cz/v2
      description: Test (development) server
tags:
    - name: 🛤 Vehicle Positions
      description: ""
paths:
    /vehiclepositions:
        post:
            summary: POST Vehicle Positions
            description: ""
            tags:
                - 🛤 Vehicle Positions
            parameters:
              - in: header
                name: Content-Type
                schema:
                    type: string
                    example: "text/xml"
                required: true
            requestBody:
                required: true
                content:
                    text/xml:
                        schema:
                            $ref: "#/components/schemas/VehiclePositions"
                        examples:
                            xml:
                                $ref: "#/components/examples/VehiclePositionsBodyExample"
            responses:
                "204":
                    description: Successful Operation

                "422":
                    description: Unprocessable Entity
                    content:
                        application/json:
                            schema:
                                $ref: "#/components/schemas/Error"
                            example:
                                error_description: "VehiclePositionsController validation failed: m.spoj.0.$.lin: Path `$.lin` is required."
                                error_message: "Unprocessable Entity"
                                error_status: 422
                "400":
                    description: Bad Request
                    content:
                        application/json:
                            schema:
                                $ref: "#/components/schemas/Error"
                            example:
                                error_message: "Bad Request"
                                error_status: 400
                "406":
                    description: Not Acceptable
                "413":
                    description: Payload Too Large
                    content:
                        application/json:
                            schema:
                                $ref: "#/components/schemas/Error"
                            example:
                                error_message: "Request entity too large"
                                error_status: 413
                "401":
                    description: Unauthorized
                    content:
                        application/json:
                            schema:
                                $ref: "#/components/schemas/Error"
                            example:
                                error_message: "API key is missing or invalid"
                                error_status: 401
                    headers:
                        WWW_Authenticate:
                            schema:
                                type: string

    /ropidgtfs/presets:
        post:
            summary: POST Ropid GTFS Departure Presets
            description: ""
            tags:
                - 🛤 Vehicle Positions
            parameters:
              - in: header
                name: Content-Type
                schema:
                    type: string
                    example: "application/json"
                required: true
            requestBody:
                required: true
                content:
                    application/json:
                        schema:
                            $ref: "#/components/schemas/RopidGtfsPresets"
                        examples:
                            json:
                                $ref: "#/components/examples/RopidGtfsPresetsExample"

            responses:
                "204":
                    description: Successful Operation

                "422":
                    description: Unprocessable Entity
                    content:
                        application/json:
                            schema:
                                $ref: "#/components/schemas/Error"
                            example:
                                error_info: "validation failed [{\"keyword\":\"type\",\"dataPath\":\"[0].routeName\",\"schemaPath\":\"#/items/properties/routeName/type\",\"params\":{\"type\":\"string\"},\"message\":\"should be string\"}]"
                                error_message: "Unprocessable Entity"
                                error_status: 422
                "400":
                    description: Bad Request
                    content:
                        application/json:
                            schema:
                                $ref: "#/components/schemas/Error"
                            example:
                                error_message: "Bad Request"
                                error_status: 400
                "406":
                    description: Not Acceptable
                "413":
                    description: Payload Too Large
                    content:
                        application/json:
                            schema:
                                $ref: "#/components/schemas/Error"
                            example:
                                error_message: "Request entity too large"
                                error_status: 413
                "401":
                    description: Unauthorized
                    content:
                        application/json:
                            schema:
                                $ref: "#/components/schemas/Error"
                            example:
                                error_message: "API key is missing or invalid"
                                error_status: 401
                    headers:
                        WWW_Authenticate:
                            schema:
                                type: string

components:
    schemas:
        VehiclePositions:
            title: Vehicle Positions
            type: object
            xml:
                name: m
                wrapped: true
            properties:
                disp:
                    type: string
                    example: "http://77.93.194.81:8716/api"
                    xml:
                        attribute: true
                SpojArray:
                    type: array
                    items:
                        $ref: "#/components/schemas/VehiclePositionsSpoj"

        VehiclePositionsSpoj:
            title: Vehicle Positions Spoj
            type: object
            xml:
                name: spoj
            properties:
                lin:
                    type: number
                    format: int64
                    example: 999999
                    xml:
                        attribute: true
                alias:
                    type: string
                    example: "155"
                    xml:
                        attribute: true
                spoj:
                    type: string
                    example: "1"
                    xml:
                        attribute: true
                t:
                    type: string
                    example: "3"
                    xml:
                        attribute: true
                sled:
                    type: string
                    example: "2"
                    xml:
                        attribute: true
                np:
                    type: boolean
                    example: true
                    xml:
                        attribute: true
                zrus:
                    type: boolean
                    example: false
                    xml:
                        attribute: true
                lat:
                    type: number
                    format: float
                    example: 50.08323
                    xml:
                        attribute: true
                lng:
                    type: number
                    format: float
                    example: 14.51035
                    xml:
                        attribute: true
                cpoz:
                    type: string
                    example: "11:09:06"
                    xml:
                        attribute: true
                po:
                    type: string
                    example: "1"
                    xml:
                        attribute: true
                zast:
                    type: number
                    format: int64
                    example: 56699
                    xml:
                        attribute: true
                zpoz_prij:
                    type: string
                    example: "426"
                    xml:
                        attribute: true
                zpoz_odj:
                    type: string
                    example: "426"
                    xml:
                        attribute: true

                ZastArray:
                    type: array
                    items:
                        $ref: "#/components/schemas/VehiclePositionsZast"

            required:
                - lin
                - spoj
                - t
                - sled
                - np
                - zrus

        VehiclePositionsZast:
            title: Vehicle Positions Zast
            type: object
            xml:
                name: zast
            properties:
                zast:
                    type: number
                    format: int64
                    example: 57517
                    xml:
                        attribute: true
                stan:
                    type: string
                    example: "C"
                    xml:
                        attribute: true
                prij:
                    type: string
                    example: ""
                    xml:
                        attribute: true
                odj:
                    type: string
                    example: "00:50"
                    xml:
                        attribute: true
                zpoz_typ:
                    type: string
                    example: "3"
                    xml:
                        attribute: true
                zpoz_prij:
                    type: number
                    format: int64
                    example: 311
                    xml:
                        attribute: true
                zpoz_odj:
                    type: number
                    format: int64
                    example: 311
                    xml:
                        attribute: true
            required:
                - zast
                - zpoz_typ

        Error:
            title: Error
            type: object
            properties:
                error_description:
                    type: string
                error_message:
                    type: string
                error_status:
                    type: number
                    format: int64
            required:
                - error_message
                - error_status

        RopidGtfsPresets:
            title: Ropid GTFS Departure Presets
            type: array
            items:
                type: object
                properties:
                    routeName:
                        type: string
                        example: "apel-votice-1542"
                    note:
                        type: string
                        example: "LED na nádraží ve Voticích, bus"
                    apiVersion:
                        type: number
                        example: 2
                    route:
                        type: string
                        example: "/pid/departureboards"
                    query:
                        type: string
                        example: "?minutesAfter=300&limit=8&mode=departures&cisIds=40110"

    examples:
        VehiclePositionsBodyExample:
            value: |-
                <?xml version="1.0" encoding="UTF-8"?>
                <m disp="http://77.93.194.81:8716/api">
                    <spoj lin="999999" alias="155" spoj="1" t="3" sled="2" np="true" zrus="false" lat="50.08323" lng="14.51035" cpoz="11:09:06" po="1" zast="56699" zpoz_prij="426" zpoz_odj="426">
                        <zast zast="57517" stan="C" prij="" odj="11:00" zpoz_typ="3" zpoz_prij="311" zpoz_odj="311"></zast>
                        <zast zast="56699" stan="A" prij="" odj="11:01" zpoz_typ="3" zpoz_prij="351" zpoz_odj="351"></zast>
                        <zast zast="56699" stan="E" prij="" odj="11:02" zpoz_typ="3" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="57515" stan="A" prij="" odj="11:04" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="56700" stan="A" prij="" odj="11:06" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="57515" stan="B" prij="" odj="11:07" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="65151" stan="A" prij="" odj="11:09" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="65163" stan="A" prij="" odj="11:10" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="57511" stan="B" prij="" odj="11:11" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="57512" stan="A" prij="" odj="11:13" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="57513" stan="A" prij="" odj="11:14" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="57514" stan="B" prij="" odj="11:16" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="27902" stan="I" prij="" odj="11:18" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="27902" stan="G" prij="" odj="11:19" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="56703" stan="C" prij="11:20" odj="" zpoz_typ="0" zpoz_prij="-2147483648" zpoz_odj="-2147483648"></zast>
                    </spoj>
                    <spoj lin="999999" alias="155" spoj="2" t="3" sled="2" np="true" zrus="false" lat="50.08323" lng="14.51035" cpoz="00:02:16" po="1" zast="56699" zpoz_prij="426" zpoz_odj="426">
                        <zast zast="57517" stan="C" prij="" odj="23:50" zpoz_typ="3" zpoz_prij="311" zpoz_odj="311"></zast>
                        <zast zast="56699" stan="A" prij="" odj="23:51" zpoz_typ="3" zpoz_prij="351" zpoz_odj="351"></zast>
                        <zast zast="56699" stan="E" prij="" odj="23:52" zpoz_typ="3" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="57515" stan="A" prij="" odj="23:54" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="56700" stan="A" prij="" odj="23:56" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="57515" stan="B" prij="" odj="23:57" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="65151" stan="A" prij="" odj="23:59" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="65163" stan="A" prij="" odj="00:00" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="57511" stan="B" prij="" odj="00:01" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="57512" stan="A" prij="" odj="00:03" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="57513" stan="A" prij="" odj="00:04" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="57514" stan="B" prij="" odj="00:06" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="27902" stan="I" prij="" odj="00:08" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="27902" stan="G" prij="" odj="00:09" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="56703" stan="C" prij="00:10" odj="" zpoz_typ="0" zpoz_prij="-2147483648" zpoz_odj="-2147483648"></zast>
                    </spoj>
                    <spoj lin="999999" alias="155" spoj="3" t="3" sled="2" np="true" zrus="false" lat="50.08323" lng="14.51035" cpoz="00:51:16" po="1" zast="56699" zpoz_prij="426" zpoz_odj="426">
                        <zast zast="57517" stan="C" prij="" odj="00:50" zpoz_typ="3" zpoz_prij="311" zpoz_odj="311"></zast>
                        <zast zast="56699" stan="A" prij="" odj="00:51" zpoz_typ="3" zpoz_prij="351" zpoz_odj="351"></zast>
                        <zast zast="56699" stan="E" prij="" odj="00:52" zpoz_typ="3" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="57515" stan="A" prij="" odj="00:54" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="56700" stan="A" prij="" odj="00:56" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="57515" stan="B" prij="" odj="00:57" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="65151" stan="A" prij="" odj="00:59" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="65163" stan="A" prij="" odj="01:00" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="57511" stan="B" prij="" odj="01:01" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="57512" stan="A" prij="" odj="01:03" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="57513" stan="A" prij="" odj="01:04" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="57514" stan="B" prij="" odj="01:06" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="27902" stan="I" prij="" odj="01:08" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="27902" stan="G" prij="" odj="01:09" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast>
                        <zast zast="56703" stan="C" prij="01:10" odj="" zpoz_typ="0" zpoz_prij="-2147483648" zpoz_odj="-2147483648"></zast>
                    </spoj>
                </m>
        RopidGtfsPresetsExample:
            value: |-
                [
                    {
                        "routeName": "apex-zahradnimesto-6048",
                        "note": "Tablo Zahradní město z centra (APEX)",
                        "apiVersion": 2,
                        "route": "/pid/departureboards",
                        "query": "?minutesAfter=40&limit=6&mode=departures&filter=routeHeadingOnceNoGapFill&order=real&aswIds=2781_2&aswIds=2781_52&skip=atStop&airCondition=true"
                    },
                    {
                        "routeName": "apex-zahradnimesto-6048-kopie",
                        "note": "Tablo Zahradní město z centra (APEX)",
                        "apiVersion": 2,
                        "route": "/pid/departureboards",
                        "query": "?minutesAfter=40&limit=6&mode=departures&filter=routeHeadingOnceNoGapFill&order=real&aswIds=2781_2&aswIds=2781_52&skip=atStop&airCondition=true"
                    }
                ]