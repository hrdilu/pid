import { GenerateFilesTask } from "#ie/vehicle-positions/workers/gtfs-rt/tasks/GenerateFilesTask";
import { PostgresConnector, RedisConnector } from "@golemio/core/dist/integration-engine/connectors";
import moment from "@golemio/core/dist/shared/moment-timezone";
import { transit_realtime as gtfsRealtime } from "@golemio/ovapi-gtfs-realtime-bindings";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonFakeTimers, SinonSandbox, SinonStub } from "sinon";

chai.use(chaiAsPromised);

describe("GtfsRealTimeWorker - GenerateFilesTask", () => {
    let sandbox: SinonSandbox;
    let task: GenerateFilesTask;
    let clock: SinonFakeTimers;
    let stubbedRedisHset: SinonStub;
    let nowTime = moment().tz("Europe/Prague").hour(11).minute(40).second(0);
    const todayYMD = nowTime.format("YYYY-MM-DD");

    beforeEach(async () => {
        sandbox = sinon.createSandbox();
        clock = sinon.useFakeTimers({
            now: new Date(nowTime.valueOf()),
            shouldAdvanceTime: true,
        });

        await PostgresConnector.connect();
        sandbox.stub(RedisConnector, "getConnection");

        task = new GenerateFilesTask("test.test");
        stubbedRedisHset = sandbox.stub(task["gtfsRtRedisRepository"], "hset");
    });

    afterEach(() => {
        sandbox?.restore();
        clock?.restore();
    });

    it("should generate GTFS-RT trip updates feed", async () => {
        await task["execute"]();
        const tripUpdatesBinary = stubbedRedisHset.getCall(0).args[1];
        const tripUpdates = gtfsRealtime.FeedMessage.decode(Buffer.from(tripUpdatesBinary, "binary"));

        expect(tripUpdates.entity[0].id).to.equal(todayYMD + "T10:04:35+02:00_991_294_220901_9_9");
        expect(tripUpdates.entity[0].tripUpdate?.trip.tripId).to.equal("991_294_220901");
        expect(tripUpdates.entity[0].vehicle).to.be.null;
        expect(tripUpdates.entity[0].alert).to.be.null;
    });

    it("should generate GTFS-RT vehicle positions feed", async () => {
        await task["execute"]();
        const vehiclePositionsBinary = stubbedRedisHset.getCall(2).args[1];
        const vehiclePositions = gtfsRealtime.FeedMessage.decode(Buffer.from(vehiclePositionsBinary, "binary"));

        expect(vehiclePositions.entity[0].id).to.equal(todayYMD + "T10:04:35+02:00_991_294_220901_9_9");
        expect(vehiclePositions.entity[0].vehicle?.trip?.tripId).to.equal("991_294_220901");
        expect(vehiclePositions.entity[0].tripUpdate).to.be.null;
        expect(vehiclePositions.entity[0].alert).to.be.null;
    });

    it("should generate GTFS-RT pid feed", async () => {
        await task["execute"]();
        const pidFeedBinary = stubbedRedisHset.getCall(4).args[1];
        const pidFeed = gtfsRealtime.FeedMessage.decode(Buffer.from(pidFeedBinary, "binary"));

        expect(pidFeed.entity[0].id).to.equal(todayYMD + "T10:04:35+02:00_991_294_220901_9_9");
        expect(pidFeed.entity[0].tripUpdate?.trip.tripId).to.equal("991_294_220901");
        expect(pidFeed.entity[0].vehicle?.trip?.tripId).to.equal("991_294_220901");
        expect(pidFeed.entity[0].alert).to.be.null;
    });

    it("should generate GTFS-RT alerts feed", async () => {
        await task["execute"]();
        const alertsBinary = stubbedRedisHset.getCall(5).args[1];
        const alerts = gtfsRealtime.FeedMessage.decode(Buffer.from(alertsBinary, "binary"));

        expect(alerts.entity[0].id).to.equal("alert-83-3");
        expect(alerts.entity[0].alert?.headerText?.translation?.[0].text).to.equal(
            "Sídliště Pankrác: trvalé přemístění zastávky"
        );
        expect(alerts.entity[0].tripUpdate).to.be.null;
        expect(alerts.entity[0].vehicle).to.be.null;
    });

    it("should generate proper GtfsRt JSON", async () => {
        await task["execute"]();
        expect(stubbedRedisHset.getCall(0).args[0]).to.equal("trip_updates.pb");
        expect(stubbedRedisHset.getCall(1).args[0]).to.equal("trip_updates.json");
        expect(stubbedRedisHset.getCall(2).args[0]).to.equal("vehicle_positions.pb");
        expect(stubbedRedisHset.getCall(3).args[0]).to.equal("vehicle_positions.json");
        expect(stubbedRedisHset.getCall(4).args[0]).to.equal("pid_feed.pb");
        expect(stubbedRedisHset.getCall(5).args[0]).to.equal("alerts.pb");
        expect(stubbedRedisHset.getCall(6).args[0]).to.equal("alerts.json");

        let callArgs = {
            second: JSON.parse(stubbedRedisHset.getCall(1).args[1]),
            forth: JSON.parse(stubbedRedisHset.getCall(3).args[1]),
        };
        const redisTripUpdatesFile = JSON.parse(stubbedRedisHset.getCall(1).args[1]).entity;
        // trip updates
        const tripUpdateFirstIndex = redisTripUpdatesFile.findIndex(
            (trip: { id: string }) => trip.id === "2019-05-26T09:13:00Z_XX_152_60_201230"
        );
        expect(redisTripUpdatesFile[tripUpdateFirstIndex]).to.deep.equal({
            id: "2019-05-26T09:13:00Z_XX_152_60_201230",
            tripUpdate: {
                trip: {
                    tripId: "152_60_201230",
                    startTime: "11:39:00",
                    startDate: moment(nowTime).format("YYYYMMDD"),
                    scheduleRelationship: "CANCELED",
                    routeId: "L152",
                },
                stopTimeUpdate: [
                    { arrival: { delay: 60 }, departure: { delay: 60 }, stopSequence: 2, stopId: "U474Z4P" },
                    { arrival: { delay: 30 }, departure: { delay: 30 }, stopSequence: 3, stopId: "U603Z3P" },
                    { arrival: { delay: 30 }, departure: { delay: 30 }, stopSequence: 4, stopId: "U665Z2P" },
                    { arrival: { delay: 30 }, departure: { delay: 20 }, stopSequence: 5, stopId: "U332Z2P" },
                    { arrival: { delay: 20 }, departure: { delay: 20 }, stopSequence: 6, stopId: "U467Z2P" },
                    { arrival: { delay: 20 }, departure: { delay: 0 }, stopSequence: 7, stopId: "U740Z3P" },
                    { arrival: { delay: 0 }, departure: { delay: 0 }, stopSequence: 8, stopId: "U78Z3P" },
                    { arrival: { delay: 0 }, departure: { delay: 0 }, stopSequence: 9, stopId: "U675Z3P" },
                ],
                timestamp: callArgs.second.entity[tripUpdateFirstIndex].tripUpdate.timestamp,
                vehicle: {
                    ".transit_realtime.ovapiVehicleDescriptor": {
                        wheelchairAccessible: true,
                        vehicleType: `{\"airConditioned\":true}`,
                    },
                    id: "service-3-8581",
                    label: "8581",
                },
            },
        });

        const tripUpdateSecondIndex = redisTripUpdatesFile.findIndex(
            (trip: { id: string }) => trip.id === "2019-05-26T09:13:00Z_XX_152_37_201230"
        );
        expect(redisTripUpdatesFile[tripUpdateSecondIndex]).to.deep.equal({
            id: "2019-05-26T09:13:00Z_XX_152_37_201230",
            tripUpdate: {
                trip: {
                    tripId: "152_37_201230",
                    startTime: "11:34:00",
                    startDate: moment(nowTime).format("YYYYMMDD"),
                    scheduleRelationship: "SCHEDULED",
                    routeId: "L152",
                },
                stopTimeUpdate: [
                    { arrival: { delay: -20 }, departure: { delay: 0 }, stopSequence: 2, stopId: "U474Z4P" },
                    { arrival: { delay: 0 }, departure: { delay: 0 }, stopSequence: 3, stopId: "U603Z3P" },
                    { arrival: { delay: 0 }, departure: { delay: 0 }, stopSequence: 4, stopId: "U665Z2P" },
                    { arrival: { delay: 0 }, departure: { delay: 0 }, stopSequence: 5, stopId: "U332Z2P" },
                    { arrival: { delay: 0 }, departure: { delay: 0 }, stopSequence: 6, stopId: "U467Z2P" },
                    { arrival: { delay: 0 }, departure: { delay: 0 }, stopSequence: 7, stopId: "U740Z3P" },
                    { arrival: { delay: 0 }, departure: { delay: 0 }, stopSequence: 8, stopId: "U78Z3P" },
                    { arrival: { delay: 0 }, departure: { delay: 0 }, stopSequence: 9, stopId: "U675Z3P" },
                    { arrival: { delay: 0 }, departure: { delay: 0 }, stopSequence: 10, stopId: "U267Z1P" },
                    { arrival: { delay: 0 }, departure: { delay: 0 }, stopSequence: 11, stopId: "U543Z1P" },
                    { arrival: { delay: 0 }, departure: { delay: 0 }, stopSequence: 12, stopId: "U544Z1P" },
                    { arrival: { delay: 0 }, departure: { delay: 0 }, stopSequence: 13, stopId: "U581Z1P" },
                    { arrival: { delay: 0 }, departure: { delay: 0 }, stopSequence: 14, stopId: "U806Z1P" },
                    { arrival: { delay: 0 }, departure: { delay: 0 }, stopSequence: 15, stopId: "U330Z1P" },
                    { arrival: { delay: 0 }, departure: { delay: 0 }, stopSequence: 16, stopId: "U74Z1P" },
                    { arrival: { delay: 0 }, departure: { delay: 0 }, stopSequence: 17, stopId: "U949Z1P" },
                    { arrival: { delay: 0 }, departure: { delay: 0 }, stopSequence: 18, stopId: "U949Z2P" },
                ],
                timestamp: callArgs.second.entity[tripUpdateSecondIndex].tripUpdate.timestamp,
                vehicle: {
                    ".transit_realtime.ovapiVehicleDescriptor": {
                        wheelchairAccessible: true,
                        vehicleType: `{\"airConditioned\":false}`,
                    },
                    id: "service-3-8582",
                    label: "8582",
                },
            },
        });

        // before track delayed trip has all stops and delay
        const tripUpdateBeforeTrackIndex = redisTripUpdatesFile.findIndex(
            (trip: { id: string }) => trip.id === "2021-07-26T08:30:00Z_100115_115_1061_V3"
        );
        expect(redisTripUpdatesFile[tripUpdateBeforeTrackIndex]).to.deep.equal({
            id: "2021-07-26T08:30:00Z_100115_115_1061_V3",
            tripUpdate: {
                trip: {
                    tripId: "115_6_201230_V3",
                    startTime: "10:45:00",
                    startDate: moment(nowTime).format("YYYYMMDD"),
                    scheduleRelationship: "SCHEDULED",
                    routeId: "L115",
                },
                stopTimeUpdate: [
                    {
                        stopId: "U52Z4P",
                        stopSequence: 1,
                        arrival: {
                            delay: 60,
                        },
                        departure: {
                            delay: 60,
                        },
                    },
                    {
                        stopId: "U1131Z2P",
                        stopSequence: 2,
                        arrival: {
                            delay: 60,
                        },
                        departure: {
                            delay: 60,
                        },
                    },
                    {
                        stopId: "U1341Z1P",
                        stopSequence: 3,
                        arrival: {
                            delay: 60,
                        },
                        departure: {
                            delay: 60,
                        },
                    },
                    {
                        stopId: "U1132Z1P",
                        stopSequence: 4,
                        arrival: {
                            delay: 60,
                        },
                        departure: {
                            delay: 60,
                        },
                    },
                    {
                        stopId: "U1334Z2P",
                        stopSequence: 5,
                        arrival: {
                            delay: 60,
                        },
                        departure: {
                            delay: 60,
                        },
                    },
                    {
                        stopId: "U181Z1P",
                        stopSequence: 6,
                        arrival: {
                            delay: 60,
                        },
                        departure: {
                            delay: 60,
                        },
                    },
                    {
                        stopId: "U1131Z1P",
                        stopSequence: 7,
                        arrival: {
                            delay: 60,
                        },
                        departure: {
                            delay: 60,
                        },
                    },
                    {
                        stopId: "U52Z2P",
                        stopSequence: 8,
                        arrival: {
                            delay: 60,
                        },
                        departure: {
                            delay: 60,
                        },
                    },
                ],
                timestamp: callArgs.second.entity[tripUpdateBeforeTrackIndex].tripUpdate.timestamp,
                vehicle: {
                    ".transit_realtime.ovapiVehicleDescriptor": {
                        wheelchairAccessible: true,
                        vehicleType: `{\"airConditioned\":true}`,
                    },
                    id: "service-3-1861",
                    label: "1861",
                },
            },
        });

        // vehiclepositions updates
        expect(JSON.parse(stubbedRedisHset.getCall(3).args[1]).entity).to.deep.equal([
            {
                id: todayYMD + "T10:04:35+02:00_991_294_220901_9_9",
                vehicle: {
                    currentStopSequence: 9,
                    position: {
                        bearing: 139,
                        latitude: 50.079954,
                        longitude: 14.43101,
                    },
                    timestamp: (new Date().setHours(8, 20, 13, 0) / 1000).toString(),
                    trip: {
                        routeId: "L991",
                        scheduleRelationship: "SCHEDULED",
                        startDate: moment(nowTime).format("YYYYMMDD"),
                        startTime: "10:04:35",
                        tripId: "991_294_220901",
                    },
                    vehicle: {
                        ".transit_realtime.ovapiVehicleDescriptor": {
                            wheelchairAccessible: true,
                        },
                        id: "metro-A-9",
                    },
                },
            },
            {
                id: "2021-07-26T08:30:00Z_100115_115_1061",
                vehicle: {
                    position: {
                        latitude: 50.03082,
                        longitude: 14.49163,
                        speed: 1.67,
                    },
                    timestamp: "1627288183",
                    trip: {
                        routeId: "L115",
                        scheduleRelationship: "SCHEDULED",
                        startTime: "10:45:00",
                        startDate: moment(nowTime).format("YYYYMMDD"),
                        tripId: "115_6_201230",
                    },
                    vehicle: {
                        ".transit_realtime.ovapiVehicleDescriptor": {
                            wheelchairAccessible: true,
                            vehicleType: `{\"airConditioned\":true}`,
                        },
                        id: "service-3-1861",
                        label: "1861",
                    },
                },
            },
            {
                id: "2021-07-26T08:30:00Z_100115_115_1061_V1",
                vehicle: {
                    currentStopSequence: 3,
                    position: {
                        bearing: 335,
                        latitude: 50.03897,
                        longitude: 14.49684,
                        speed: 4.44,
                    },
                    timestamp: "1627288620",
                    trip: {
                        routeId: "L115",
                        scheduleRelationship: "SCHEDULED",
                        startTime: "10:45:00",
                        startDate: moment(nowTime).format("YYYYMMDD"),
                        tripId: "115_6_201230_V1",
                    },
                    vehicle: {
                        ".transit_realtime.ovapiVehicleDescriptor": {
                            wheelchairAccessible: true,
                            vehicleType: `{\"airConditioned\":true}`,
                        },
                        id: "service-3-1861",
                        label: "1861",
                    },
                },
            },
            {
                id: "2021-07-26T08:30:00Z_100115_115_1061_V2",
                vehicle: {
                    currentStopSequence: 4,
                    position: {
                        bearing: 332,
                        latitude: 50.04095,
                        longitude: 14.49552,
                        speed: 4.44,
                    },
                    timestamp: "1627288654",
                    trip: {
                        routeId: "L115",
                        scheduleRelationship: "SCHEDULED",
                        startTime: "10:45:00",
                        startDate: moment(nowTime).format("YYYYMMDD"),
                        tripId: "115_6_201230_V2",
                    },
                    vehicle: {
                        ".transit_realtime.ovapiVehicleDescriptor": {
                            wheelchairAccessible: true,
                            vehicleType: `{\"airConditioned\":true}`,
                        },
                        id: "service-3-1861",
                        label: "1861",
                    },
                },
            },
            {
                id: "2021-07-26T08:30:00Z_100115_115_1061_V3",
                vehicle: {
                    position: {
                        latitude: 50.03082,
                        longitude: 14.49163,
                        speed: 1.67,
                    },
                    timestamp: "1627288183",
                    trip: {
                        routeId: "L115",
                        scheduleRelationship: "SCHEDULED",
                        startTime: "10:45:00",
                        startDate: moment(nowTime).format("YYYYMMDD"),
                        tripId: "115_6_201230_V3",
                    },
                    vehicle: {
                        ".transit_realtime.ovapiVehicleDescriptor": {
                            wheelchairAccessible: true,
                            vehicleType: `{\"airConditioned\":true}`,
                        },
                        id: "service-3-1861",
                        label: "1861",
                    },
                },
            },
            {
                id: "2019-05-26T09:13:00Z_XX_152_60_201230",
                vehicle: {
                    currentStopSequence: 2,
                    trip: {
                        tripId: "152_60_201230",
                        startTime: "11:39:00",
                        startDate: moment(nowTime).format("YYYYMMDD"),
                        scheduleRelationship: "CANCELED",
                        routeId: "L152",
                    },
                    position: { latitude: 50.11063, longitude: 14.50247 },
                    timestamp: callArgs.forth.entity[5].vehicle.timestamp,
                    vehicle: {
                        ".transit_realtime.ovapiVehicleDescriptor": {
                            wheelchairAccessible: true,
                            vehicleType: `{\"airConditioned\":true}`,
                        },
                        id: "service-3-8581",
                        label: "8581",
                    },
                },
            },
            {
                id: "2019-05-26T09:13:00Z_XX_152_37_201230",
                vehicle: {
                    currentStopSequence: 2,
                    trip: {
                        tripId: "152_37_201230",
                        startTime: "11:34:00",
                        startDate: moment(nowTime).format("YYYYMMDD"),
                        scheduleRelationship: "SCHEDULED",
                        routeId: "L152",
                    },
                    position: { latitude: 50.11063, longitude: 14.50247 },
                    timestamp: callArgs.forth.entity[6].vehicle.timestamp,
                    vehicle: {
                        ".transit_realtime.ovapiVehicleDescriptor": {
                            wheelchairAccessible: true,
                            vehicleType: `{\"airConditioned\":false}`,
                        },
                        id: "service-3-8582",
                        label: "8582",
                    },
                },
            },
            {
                id: "2023-03-21T01:30:00+01:00_94_979_230320_8388",
                vehicle: {
                    trip: {
                        tripId: "94_979_230320",
                        startTime: "01:30:00",
                        startDate: "20230321",
                        scheduleRelationship: "SCHEDULED",
                        routeId: "L94",
                    },
                    position: {
                        latitude: 50.03141,
                        longitude: 14.36749,
                        bearing: 257,
                    },
                    currentStopSequence: 8,
                    timestamp: "1679363060",
                    vehicle: {
                        id: "service-0-8388",
                        label: "8388",
                        ".transit_realtime.ovapiVehicleDescriptor": {
                            wheelchairAccessible: false,
                        },
                    },
                },
            },
        ]);
    });
});
