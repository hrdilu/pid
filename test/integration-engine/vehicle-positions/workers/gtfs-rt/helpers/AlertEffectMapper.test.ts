import { getGtfsEffectByEventType } from "#ie/vehicle-positions/workers/gtfs-rt/helpers/AlertEffectMapper";
import { GtfsAlertEffectEnum } from "#ie/vehicle-positions/workers/gtfs-rt/helpers/enums/AlertEffectEnum";
import { expect } from "chai";

describe("AlertEffectMapper", () => {
    it("maps effects by correct event types", () => {
        const eventTypesMap = {
            1: GtfsAlertEffectEnum.REDUCED_SERVICE, // EFF_PROVOZ_ZASTAVEN
            4: GtfsAlertEffectEnum.REDUCED_SERVICE, // EFF_PROVOZ_OMEZEN
            8: GtfsAlertEffectEnum.OTHER_EFFECT, // EFF_PROVOZ_OBNOVEN
            128: GtfsAlertEffectEnum.REDUCED_SERVICE, // EFF_NEODJETI_SPOJE
            2048: GtfsAlertEffectEnum.REDUCED_SERVICE, // EFF_ZPOZDENI_SPOJE
            4096: GtfsAlertEffectEnum.REDUCED_SERVICE, // EFF_ZPOZDENI_SPOJU
            8192: GtfsAlertEffectEnum.NO_SERVICE, // EFF_STANICE_UZAVRENA
            16384: GtfsAlertEffectEnum.OTHER_EFFECT, // EFF_PRISTUP_OMEZEN
            131072: GtfsAlertEffectEnum.ADDITIONAL_SERVICE, // EFF_POSILENI_SPOJU
            262144: GtfsAlertEffectEnum.MODIFIED_SERVICE, // EFF_ROZVAZANI_PRESTUP
            524288: GtfsAlertEffectEnum.OTHER_EFFECT, // EFF_OSTATNI
            1048576: GtfsAlertEffectEnum.DETOUR, // EFF_ODKLON
            2097152: GtfsAlertEffectEnum.DETOUR, // EFF_NEOBSLOUZENI_ZASTAVKY
            3145728: GtfsAlertEffectEnum.DETOUR, // EFF_ODKLON + EFF_NEOBSLOUZENI_ZASTAVKY
            8388608: GtfsAlertEffectEnum.MODIFIED_SERVICE, // EFF_NAHRADNI_DOPRAVA
        };

        for (const [eventType, effect] of Object.entries(eventTypesMap)) {
            expect(getGtfsEffectByEventType(Number(eventType))).to.be.equal(effect);
        }
    });
});
