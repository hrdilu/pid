import { getGtfsCause } from "#ie/vehicle-positions/workers/gtfs-rt/helpers/AlertCauseMapper";
import { GtfsAlertCauseEnum } from "#ie/vehicle-positions/workers/gtfs-rt/helpers/enums/AlertCauseEnum";
import { expect } from "chai";

describe("AlertCauseMapper", () => {
    it("maps selected causes to known strings correctly", () => {
        const causesMap = {
            nehoda: GtfsAlertCauseEnum.ACCIDENT,
            porucha: GtfsAlertCauseEnum.TECHNICAL_PROBLEM,
            oprava: GtfsAlertCauseEnum.CONSTRUCTION,
            strom: GtfsAlertCauseEnum.WEATHER,
            demonstrace: GtfsAlertCauseEnum.DEMONSTRATION,
            policie: GtfsAlertCauseEnum.POLICE_ACTIVITY,
        };

        for (const [keyword, cause] of Object.entries(causesMap)) {
            expect(getGtfsCause(keyword)).to.be.equal(cause);
        }
    });

    it("returns unknown cause for not mapped types", () => {
        expect(getGtfsCause("vůbec nevíme proč")).to.be.equal(GtfsAlertCauseEnum.UNKNOWN_CAUSE);
    });
});
