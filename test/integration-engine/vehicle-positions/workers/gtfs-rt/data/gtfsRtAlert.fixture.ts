import { IGtfsRtAlert } from "#ie/vehicle-positions/workers/gtfs-rt/interfaces/AlertsInterfaces";
import { GtfsAlertCauseEnum } from "#ie/vehicle-positions/workers/gtfs-rt/helpers/enums/AlertCauseEnum";
import { GtfsAlertEffectEnum } from "#ie/vehicle-positions/workers/gtfs-rt/helpers/enums/AlertEffectEnum";

export const gtfsRtAlertFixture: IGtfsRtAlert[] = [
    {
        id: "alert-83-3",
        alert: {
            activePeriod: [{ start: 1652342400 }],
            informedEntity: [{ routeId: "L152" }, { routeId: "L152" }, { routeId: "L177" }],
            cause: GtfsAlertCauseEnum.UNKNOWN_CAUSE,
            effect: GtfsAlertEffectEnum.UNKNOWN_EFFECT,
            url: { translation: [{ text: "https://pid.cz/mimoradnost/?id=83-3", language: "cs" }] },
            headerText: { translation: [{ text: "Sídliště Pankrác: trvalé přemístění zastávky", language: "cs" }] },
            descriptionText: {
                translation: [
                    {
                        // eslint-disable-next-line
                        text: "Z důvodu výstavby domu na křižovatce ulic Milevská a Pujmanové dochází od středy 11. prosince 2019 (od zahájení denního provozu) k trvalému přemístění zastávky Sídliště Pankrác ve směru Pankrác pro linky číslo 134, 188 a 193.          Ve směru Pankrác se přemisťuje zastávka Sídliště Pankrác, a to v ulici Pujmanové o přibližně 60 metrů po směru jízdy, do čela nově vybudovaného zálivu, 40 metrů za přechod pro chodce, přibližně 65 metrů před  křižovatku s ulicí Hvězdovou.",
                        language: "cs",
                    },
                ],
            },
        },
    },
    {
        id: "alert-71-3",
        alert: {
            activePeriod: [{ start: 1652342400 }],
            informedEntity: [],
            cause: GtfsAlertCauseEnum.UNKNOWN_CAUSE,
            effect: GtfsAlertEffectEnum.UNKNOWN_EFFECT,
            url: { translation: [{ text: "https://pid.cz/mimoradnost/?id=71-3", language: "cs" }] },
            headerText: { translation: [{ text: "Trvalé změny PID od 1. prosince 2019", language: "cs" }] },
            descriptionText: {
                translation: [
                    {
                        // eslint-disable-next-line
                        text: "Od neděle 1. prosince 2019 dochází v provozu PID k několika trvalým změnám a  úpravám. K nejpodstatnějším na území hl. m. Prahy patří především následující změny v provozu autobusů:          Zavádějí se nové linky       číslo 153 v trase U Zvonu – Mrázovka – Malvazinky – Laurová – Radlická – Dívčí hrady – Nad Konvářkou     číslo 171 v trase Depo Hostivař – Teplárna Malešice – Kyje – Černý Most – Nádraží Horní Počernice – Třebešovská – Ve Žlíbku     číslo 204 v trase Obchodní centrum Černý Most – Nádraží Horní Počernice – Škola Dolní Počernice – Dolní Počernice – Obchodní centrum Štěrboholy – Dolní Měcholupy – Nádraží Horní Měcholupy – Nové Petrovice – Sídliště Petrovice     číslo 242 v trase Zbraslavské náměstí – Žabovřesky – Na Lhotkách – Škola Lipence – Lipence – Dolní Černošice   Změny tras linek       číslo 146 nově v trase Želivského – Třebešín – Habrová – Spojovací – Mezitraťová     číslo 156 nově v trase Nádraží Holešovice – Strossmayerovo náměstí – Veletržní palác – Obchodní centrum Stromovka – Veletržní palác – Řezáčovo náměstí – Nádraží Holešovice (již od 21. listopadu 2019)     číslo 182 je prodloužena do trasy Opatov – Prosek – Letňany – Kbely – Vinoř      číslo 185 je zkrácena do trasy Letňany – Vinořský hřbitov      číslo 194 je prodloužena do trasy Florenc – Malostranská – Malostranské náměstí – Nemocnice pod Petřínem     číslo 223 je zkrácena do trasy Breitcetlova – Černý Most – Ratibořická      číslo 224 nově v trase Ve Žlíbku – Ratibořická – Chvaly – Obchodní centrum Černý Most – Novozámecká – Dolní Počernice – Dolnopočernický hřbitov     číslo 243 je zkrácena do trasy Lipence – Kazín      číslo 302 nově v trase Českomoravská – Vysočanská – Prosek – Přezletice, Kocanda – Veleň    Ruší se linka číslo 192.  Dochází ke změně názvu zastávky:  Krausova – nový název: Sídliště Letňany  Dále dochází také k úpravám jízdních řádů některých linek, změnám charakteru některých zastávek a dalším dílčím změnám.  Od 15. prosince 2019 dochází k dalším trvalým změnám v souvislosti s integrací Rakovnicka do systému PID. K těm nejvýraznějším, které se dotknou linek obsluhujících území Prahy, patří zavedení nových linek:       číslo 304 v trase Kukulova – Nemocnice Motol – Zličín – Stochov – Nové Strašecí – Rakovník, Autobusová stanice     číslo 305 v trase Zličín – Velká Dobrá – Tuchlovice – Lány – Nové Strašecí – Řevničov – Lišany – Lužná – Rakovník, Autobusová stanice     číslo 365 v trase Motol – Nemocnice Motol – Hostivice – Jeneč – Unhošť, Náměstí – Velká Dobrá – Doksy – Kamenné Žehrovice – Tuchlovice – Stochov, Náměstí     číslo 404 v trase Zličín – Stochov – Lány – Rakovník, Autobusová stanice",
                        language: "cs",
                    },
                ],
            },
        },
    },
    {
        id: "alert-13581-1",
        alert: {
            activePeriod: [{ start: 1652342400 }],
            informedEntity: [],
            cause: GtfsAlertCauseEnum.UNKNOWN_CAUSE,
            effect: GtfsAlertEffectEnum.OTHER_EFFECT,
            url: { translation: [{ text: "https://pid.cz/mimoradnost/?id=13581-1", language: "cs" }] },
            headerText: { translation: [{ text: "Právnická fakulta", language: "cs" }] },
            descriptionText: { translation: [{ text: "Odklon přes zastávku Malostranská.", language: "cs" }] },
        },
    },
    {
        id: "alert-6942-1",
        alert: {
            activePeriod: [{ start: 2288346486.381 }],
            informedEntity: [],
            cause: GtfsAlertCauseEnum.UNKNOWN_CAUSE,
            effect: GtfsAlertEffectEnum.OTHER_EFFECT,
            url: { translation: [{ text: "https://pid.cz/mimoradnost/?id=6942-1", language: "cs" }] },
            headerText: { translation: [{ text: "EXP Právnická fakulta", language: "cs" }] },
            descriptionText: { translation: [{ text: "Odklon přes zastávku Malostranská.", language: "cs" }] },
        },
    },
];
