import { MetroMessageFilter } from "#ie/vehicle-positions/workers/runs/helpers/MetroMessageFilter";
import { IProcessMetroRunsMessagesInput } from "#ie/vehicle-positions/workers/runs/interfaces/IProcessMetroRunsMessagesInput";
import { ProcessMetroRunsMessagesTask } from "#ie/vehicle-positions/workers/runs/tasks/ProcessMetroRunsMessagesTask";
import { PostgresConnector, RedisConnector } from "@golemio/core/dist/integration-engine/connectors";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox, SinonStub } from "sinon";

chai.use(chaiAsPromised);

describe("RunsWorker - ProcessMetroRunsMessagesTask", () => {
    let sandbox: SinonSandbox;
    let sendMessageToExchangeStub: SinonStub;
    let task: ProcessMetroRunsMessagesTask;

    const testParamsLineAValid: IProcessMetroRunsMessagesInput = {
        routeName: "A",
        messages: [
            {
                route_name: "A",
                message_timestamp: "2022-07-20T13:45:34Z",
                train_set_number_scheduled: "3",
                train_set_number_real: "3",
                train_number: "279",
                track_id: "1809",
                delay_origin: 25,
                actual_position_timestamp_scheduled: "2022-07-20T13:45:59Z",
            },
        ],
    };

    const testParamsLineAInvalid = {
        routeName: "A",
        messages: {},
    };

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox
                    .stub()
                    .returns({ hasMany: sandbox.stub(), belongsTo: sandbox.stub(), removeAttribute: sandbox.stub() }),
            })
        );
        sandbox.stub(RedisConnector, "getConnection");
        sendMessageToExchangeStub = sandbox.stub(QueueManager, "sendMessageToExchange").resolves();

        task = new ProcessMetroRunsMessagesTask("test.test");
    });

    afterEach(() => {
        sandbox?.restore();
    });

    it("should validate and execute", async () => {
        const processMessageStub = sandbox.stub(task, "processMessage" as any).resolves();
        const findCoordinatesStub = sandbox.stub(task["railtrackGPSRepository"], "findCoordinates").resolves({
            coordinates: {
                "1809": {
                    lon: 14.36283,
                    lat: 50.09831,
                    gtfs_stop_id: "U157Z101P",
                },
            },
        });

        const promise = task.validateAndExecute({ content: Buffer.from(JSON.stringify(testParamsLineAValid)) } as any);

        await expect(promise).to.be.fulfilled;
        expect(findCoordinatesStub.callCount).to.equal(1);
        expect(processMessageStub.callCount).to.equal(1);
    });

    it("should return early (filtered out messages)", async () => {
        const processMessageStub = sandbox.stub(task, "processMessage" as any).resolves();
        const findCoordinatesStub = sandbox.stub(task["railtrackGPSRepository"], "findCoordinates").resolves();
        sandbox.stub(MetroMessageFilter, "getValidMessagesWithTrackIds").returns({ filteredMessages: [], trackIds: [] });

        const promise = task.validateAndExecute({ content: Buffer.from(JSON.stringify(testParamsLineAValid)) } as any);

        await expect(promise).to.be.fulfilled;
        expect(findCoordinatesStub.callCount).to.equal(0);
        expect(processMessageStub.callCount).to.equal(0);
    });

    it("should not validate (invalid messages)", async () => {
        const processMessageStub = sandbox.stub(task, "processMessage" as any).resolves();
        const promise = task.validateAndExecute({ content: Buffer.from(JSON.stringify(testParamsLineAInvalid)) } as any);
        await expect(promise).to.be.rejectedWith(
            GeneralError,
            "[Queue test.test.processMetroRunMessages] Message validation failed: [messages must be an array]"
        );

        expect(processMessageStub.callCount).to.equal(0);
    });

    // =============================================================================
    // processMessage
    // =============================================================================
    describe("processMessage", () => {
        it("should call updateDelay", async () => {
            sandbox.stub(task["gtfsTripRunManager"], "setAndReturnScheduledTrips").resolves(["defined"] as any);
            sandbox.stub(task["gtfsTripRunManager"], "generateDelayMsg").resolves({ updatedTrips: ["defined"] } as any);

            await task["processMessage"](testParamsLineAValid.messages[0], {
                coordinates: {
                    "1809": {
                        lon: 14.36283,
                        lat: 50.09831,
                        gtfs_stop_id: "U157Z101P",
                    },
                },
            });

            expect(sendMessageToExchangeStub.callCount).to.equal(1);
        });

        it("should call updateDelay with train_set_number_real", async () => {
            sandbox.stub(task["gtfsTripRunManager"], "setAndReturnScheduledTrips").resolves(["defined"] as any);
            sandbox.stub(task["gtfsTripRunManager"], "generateDelayMsg").resolves({ updatedTrips: ["defined"] } as any);

            const testParams = {
                ...testParamsLineAValid.messages[0],
                train_set_number_scheduled: "X",
            };

            await task["processMessage"](testParams, {
                coordinates: {
                    "1809": {
                        lon: 14.36283,
                        lat: 50.09831,
                        gtfs_stop_id: "U157Z101P",
                    },
                },
            });

            expect(sendMessageToExchangeStub.callCount).to.equal(1);
        });

        it("should return early (no schedule for metro run)", async () => {
            const logVerboseStub = sandbox.stub(log, "verbose");
            sandbox.stub(task["gtfsTripRunManager"], "setAndReturnScheduledTrips").resolves([] as any);

            await task["processMessage"](testParamsLineAValid.messages[0], {
                coordinates: {
                    "1809": {
                        lon: 14.36283,
                        lat: 50.09831,
                        gtfs_stop_id: "U157Z101P",
                    },
                },
            });

            expect(logVerboseStub.getCall(0).args[0]).to.equal(
                `processMessage: no schedule for metro run: ${JSON.stringify({
                    route_id: "991",
                    run_number: "3",
                    msg_last_timestamp: "2022-07-20T13:45:34Z",
                })}`
            );
            expect(sendMessageToExchangeStub.callCount).to.equal(0);
        });
    });
});
