import { ICommonRunsInput } from "#ie/vehicle-positions/workers/runs/interfaces/CommonRunsMessageInterfaces";
import { CommonRunsMessagesTransformation } from "#ie/vehicle-positions/workers/runs/transformations/CommonRunsMessagesTransformation";
import { CommonRunsMessagesModel } from "#sch/vehicle-positions/models/CommonRunsMessagesModel";
import { CommonRunsModel } from "#sch/vehicle-positions/models/CommonRunsModel";
import { log } from "@golemio/core/dist/integration-engine/helpers/Logger";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import moment from "@golemio/core/dist/shared/moment-timezone";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import sinon, { SinonSandbox, SinonStub } from "sinon";
import { sourceDataNestedTransformed } from "../data/vehiclepositions-runs-multiple-transformed";
import { sourceDataTransformed } from "../data/vehiclepositions-runs-transformed";

chai.use(chaiAsPromised);

describe("CommonRunsMessagesTransformation", () => {
    let transformation: CommonRunsMessagesTransformation;
    let sourceData: ICommonRunsInput;
    let sourceDataErrorOne: ICommonRunsInput;
    let sourceDataErrorMissingTJR: ICommonRunsInput;
    let sourceDataTCPBusCEST: ICommonRunsInput;
    let sourceDataNested: ICommonRunsInput;
    let validatorRun: JSONSchemaValidator;
    let validatorRunMessage: JSONSchemaValidator;
    let sandbox: SinonSandbox;

    before(() => {
        sandbox = sinon.createSandbox();
        validatorRun = new JSONSchemaValidator("RunsValidator", CommonRunsModel.jsonSchema);
        validatorRunMessage = new JSONSchemaValidator("RunsMessagesValidator", CommonRunsMessagesModel.arrayJsonSchema);
        sourceData = JSON.parse(fs.readFileSync(__dirname + "/../data/vehiclepositions-runs-input.json").toString("utf8"));
        sourceDataNested = JSON.parse(
            fs.readFileSync(__dirname + "/../data/vehiclepositions-runs-multiple-input.json").toString("utf8")
        );
        sourceDataErrorOne = JSON.parse(
            fs.readFileSync(__dirname + "/../data/vehiclepositions-runs-input-error-one.json").toString("utf8")
        );
        sourceDataErrorMissingTJR = JSON.parse(
            fs.readFileSync(__dirname + "/../data/vehiclepositions-runs-input-error-missing-tjr.json").toString("utf8")
        );
        sourceDataTCPBusCEST = JSON.parse(
            fs.readFileSync(__dirname + "/../data/vehiclepositions-runs-input-buscest.json").toString("utf8")
        );
    });

    afterEach(() => {
        sandbox.restore();
    });

    beforeEach(async () => {
        sandbox.stub(log, "warn");
        sandbox.stub(log, "verbose");
        transformation = new CommonRunsMessagesTransformation();
    });

    it("should have name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("CommonRunsMessagesTransformation");
    });

    it("should have transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform element (1 item)", async () => {
        const clock = sinon.useFakeTimers({
            now: moment("2021-06-28T15:05:00+02:00").toDate(),
            shouldAdvanceTime: true,
        });

        const data = await transformation.transform({ data: sourceData.M.V, timestamp: Date.now() });
        expect(data).to.have.lengthOf(1);
        await expect(validatorRun.Validate(data[0].run)).to.be.fulfilled;
        await expect(validatorRunMessage.Validate([data[0].run_message])).to.be.fulfilled;
        await expect(data[0]).to.eql(sourceDataTransformed);

        clock.restore();
    });

    it("should properly transform element (multiple items)", async () => {
        const clock = sinon.useFakeTimers({
            now: moment("2021-06-10T16:22:07+02:00").toDate(),
            shouldAdvanceTime: true,
        });

        const data = await transformation.transform({ data: sourceDataNested.M.V, timestamp: Date.now() });
        expect(data).to.have.lengthOf(2);
        expect(data).to.eql(sourceDataNestedTransformed);

        clock.restore();
    });

    it("should properly detect error transformation - empty string timestamp", async () => {
        const clock = sinon.useFakeTimers({
            now: moment("2021-06-28T15:05:00+02:00").toDate(),
            shouldAdvanceTime: true,
        });

        const data = await transformation.transform({ data: sourceDataErrorOne.M.V, timestamp: Date.now() });

        expect(data).to.have.lengthOf(0);
        sandbox.assert.calledOnce(log.verbose as SinonStub);

        clock.restore();
    });

    it("should properly detect error transformation - missing TJR attribute", async () => {
        const clock = sinon.useFakeTimers({
            now: moment("2021-06-28T15:05:00+02:00").toDate(),
            shouldAdvanceTime: true,
        });

        const data = await transformation.transform({ data: sourceDataErrorMissingTJR.M.V, timestamp: Date.now() });

        expect(data).to.have.lengthOf(0);
        sandbox.assert.calledOnce(log.verbose as SinonStub);

        clock.restore();
    });
});
