import { IProcessMetroRunsMessagesInput } from "#ie/vehicle-positions/workers/runs/interfaces/IProcessMetroRunsMessagesInput";
import { MetroRunsMessageProcessingTransformation } from "#ie/vehicle-positions/workers/runs/transformations/MetroRunsMessageProcessingTransformation";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";

describe("MetroRunsMessageProcessingTransformation", () => {
    let sandbox: SinonSandbox;
    const transformation = new MetroRunsMessageProcessingTransformation();

    const testParamsLineAValid: IProcessMetroRunsMessagesInput = {
        routeName: "A",
        messages: [
            {
                route_name: "A",
                message_timestamp: "2022-07-20T13:45:34Z",
                train_set_number_scheduled: "3",
                train_set_number_real: "3",
                train_number: "279",
                track_id: "1809",
                delay_origin: 25,
                actual_position_timestamp_scheduled: "2022-07-20T13:45:59Z",
            },
        ],
    };

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox?.restore();
    });

    it("should transform", async () => {
        const warnStub = sandbox.stub(log, "warn");
        const result = await transformation.transform({
            message: testParamsLineAValid.messages[0],
            gpsData: {
                coordinates: {
                    "1809": {
                        lon: 14.36283,
                        lat: 50.09831,
                        gtfs_stop_id: "U157Z101P",
                    },
                },
            },
        });

        expect(result?.runSchedule).to.deep.equal({
            route_id: "991",
            run_number: "3",
            msg_last_timestamp: "2022-07-20T13:45:34Z",
        });

        expect(result?.runInput).to.deep.equal({
            messageTimestamp: "2022-07-20T13:45:34Z",
            timestampScheduled: "2022-07-20T13:45:59Z",
            runNumber: 3,
            routeId: "991",
            trainSetNumberScheduled: "3",
            trainSetNumberReal: "3",
            trainNumber: "279",
            coordinates: {
                lon: 14.36283,
                lat: 50.09831,
                gtfs_stop_id: "U157Z101P",
            },
        });

        expect(warnStub.callCount).to.equal(0);
    });

    it("should return undefined (no GPS coordinates)", async () => {
        const logVerboseStub = sandbox.stub(log, "verbose");
        const result = await transformation.transform({
            message: testParamsLineAValid.messages[0],
            gpsData: { coordinates: {} },
        });

        expect(result).to.be.undefined;
        expect(logVerboseStub.getCall(0).args[0]).to.equal(
            `parseRunInputFromMessage: no GPS coordinates for the message: ${JSON.stringify(testParamsLineAValid.messages[0])}`
        );
    });

    it("should return undefined (invalid route name)", async () => {
        const warnStub = sandbox.stub(log, "warn");
        const testParams = {
            ...testParamsLineAValid.messages[0],
            route_name: "X",
        };

        const result = await transformation.transform({
            message: testParams,
            gpsData: {
                coordinates: {
                    "1809": {
                        lon: 14.36283,
                        lat: 50.09831,
                        gtfs_stop_id: "U157Z101P",
                    },
                },
            },
        });

        expect(result).to.be.undefined;
        expect(warnStub.getCall(0).args[0]).to.equal(
            `parseRunScheduleFromMessage: invalid route name in the message: ${JSON.stringify(testParams)}`
        );
    });

    it("should return undefined (run number cannot be determined)", async () => {
        const warnStub = sandbox.stub(log, "warn");
        const testParams = {
            ...testParamsLineAValid.messages[0],
            train_set_number_scheduled: "X",
            train_set_number_real: "X",
        };

        const result = await transformation.transform({
            message: testParams,
            gpsData: {
                coordinates: {
                    "1809": {
                        lon: 14.36283,
                        lat: 50.09831,
                        gtfs_stop_id: "U157Z101P",
                    },
                },
            },
        });

        expect(result).to.be.undefined;
        expect(warnStub.getCall(0).args[0]).to.equal(
            `parseRunScheduleFromMessage: run number cannot be determined from the message: ${JSON.stringify(testParams)}`
        );
    });
});
