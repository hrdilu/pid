import { IMetroRunsInput } from "#ie/vehicle-positions/workers/runs/interfaces/MetroRunsMessageInterfaces";
import { SaveMetroRunsToDBTask } from "#ie/vehicle-positions/workers/runs/tasks/SaveMetroRunsToDBTask";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("RunsWorker - SaveMetroRunsToDBTask", () => {
    let sandbox: SinonSandbox;
    let task: SaveMetroRunsToDBTask;

    const testParamsValid: IMetroRunsInput = {
        m: {
            $: { linka: "A", tm: "2022-07-20T13:45:34Z", gvd: "GD20a" },
            vlak: { $: { csp: " 3", csr: " 3", cv: "279", ko: "1809", odch: "25" } },
        },
    };

    const testParamsEmpty: IMetroRunsInput = {
        m: {
            $: { linka: "A", tm: "2022-07-20T13:45:34Z", gvd: "GD20a" },
        },
    };

    const testParamsInvalid = {
        m: {
            $: { tm: "2022-07-20T13:45:34Z", gvd: "GD20a" },
            vlak: { $: { csp: " 3", csr: " 3", cv: "279", ko: "1809" } },
        },
    };

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() => Object.assign({ define: sandbox.stub() }));
        sandbox.stub(QueueManager, "sendMessageToExchange").resolves();

        task = new SaveMetroRunsToDBTask("test.test");
    });

    afterEach(() => {
        sandbox?.restore();
    });

    it("should validate and execute", async () => {
        const saveDataStub = sinon.stub(task["messagesRepository"], "saveData").resolves();
        const promise = task.validateAndExecute({ content: Buffer.from(JSON.stringify(testParamsValid)) } as any);

        await expect(promise).to.be.fulfilled;
        expect(saveDataStub.callCount).to.equal(1);
    });

    it("should validate and execute (empty message without train data)", async () => {
        const saveDataStub = sinon.stub(task["messagesRepository"], "saveData").resolves();
        const promise = task.validateAndExecute({ content: Buffer.from(JSON.stringify(testParamsEmpty)) } as any);

        await expect(promise).to.be.fulfilled;
        expect(saveDataStub.callCount).to.equal(0);
    });

    it("should not validate (missing route_name and delay_origin)", async () => {
        const saveDataStub = sinon.stub(task["messagesRepository"], "saveData").resolves();
        const promise = task.validateAndExecute({ content: Buffer.from(JSON.stringify(testParamsInvalid)) } as any);

        await expect(promise).to.be.rejectedWith(
            GeneralError,
            // eslint-disable-next-line max-len
            "[Queue test.test.saveMetroRunsToDB] Message validation failed: [linka's byte length must fall into (1, 1) range, linka must be a string, odch must be a number string"
        );

        expect(saveDataStub.callCount).to.equal(0);
    });
});
