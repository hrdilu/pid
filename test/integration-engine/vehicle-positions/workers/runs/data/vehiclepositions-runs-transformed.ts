import { ICommonRunWithMessageDto } from "#sch/vehicle-positions/models/interfaces/ICommonRunWithMessageDto";
import { TCPEventEnum } from "src/const";

export const sourceDataTransformed: ICommonRunWithMessageDto = {
    run: {
        id: "15_1_9273_2021-06-28T15:05:00+02:00",
        route_id: "15",
        run_number: "1",
        line_short_name: "15",
        registration_number: "9273",
        msg_start_timestamp: "2021-06-28T15:05:00+02:00",
        msg_last_timestamp: "2021-06-28T15:05:00+02:00",
        wheelchair_accessible: true,
    },
    run_message: {
        lat: 50.08885,
        lng: 14.42999,
        actual_stop_asw_id: "04800002",
        actual_stop_timestamp_real: new Date(1624885478000),
        actual_stop_timestamp_scheduled: new Date(1624885440000),
        last_stop_asw_id: "10190002",
        packet_number: "2918063",
        msg_timestamp: "2021-06-28T15:04:38+02:00",
        events: TCPEventEnum.ARRIVAL_ANNOUNCED,
    },
};
