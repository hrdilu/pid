import { CommonMessageProcessor } from "#ie/vehicle-positions/workers/runs/helpers/CommonMessageProcessor";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import sinon, { SinonSandbox, SinonStub } from "sinon";

describe("CommonMessageProcessor", () => {
    let sandbox: SinonSandbox;
    let processor: CommonMessageProcessor;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        sandbox.stub(QueueManager, "sendMessageToExchange").resolves();
        processor = new CommonMessageProcessor({
            getRunRecordForUpdate: sinon.stub(),
            createAndAssociate: sinon.stub(),
            updateAndAssociate: sinon.stub(),
            runsMessagesRepository: { getLastMessage: sinon.stub() },
        } as any);
    });

    afterEach(() => {
        sandbox?.restore();
    });

    it("should call the correct methods (new record)", async () => {
        await processor.processTransformedRun({
            run: { msg_last_timestamp: "2021-06-10T17:00:00+02:00" },
            run_message: {},
        } as any);

        sandbox.assert.calledOnce(processor["runsRepository"].getRunRecordForUpdate as SinonStub);
        sandbox.assert.calledOnce(processor["runsRepository"].createAndAssociate as SinonStub);
        sandbox.assert.calledOnce(QueueManager.sendMessageToExchange as SinonStub);
    });

    it("should call the correct methods (record update)", async () => {
        (processor["runsRepository"].getRunRecordForUpdate as SinonStub).callsFake(() =>
            Promise.resolve({
                id: "91_71_8257_2021-06-10T16:21:00+02:00",
                route_id: "91",
                run_number: 71,
                line_short_name: "2",
                registration_number: 8257,
                msg_start_timestamp: "2021-06-10T16:21:00+02:00",
                msg_last_timestamp: "2021-06-10T16:33:12+02:00",
            })
        );

        (processor["runsRepository"]["runsMessagesRepository"].getLastMessage as SinonStub).callsFake(() =>
            Promise.resolve({
                actual_stop_timestamp_scheduled: 123456789,
            })
        );

        await processor.processTransformedRun({
            run: { msg_last_timestamp: "2021-06-10T17:00:00+02:00" },
            run_message: {},
        } as any);

        sandbox.assert.calledOnce(processor["runsRepository"].getRunRecordForUpdate as SinonStub);
        sandbox.assert.calledOnce(processor["runsRepository"].updateAndAssociate as SinonStub);
        sandbox.assert.calledWith(
            processor["runsRepository"].updateAndAssociate as SinonStub,
            {
                run: { msg_last_timestamp: "2021-06-10T17:00:00+02:00" },
                run_message: { actual_stop_timestamp_scheduled: 123456789 },
            },
            "91_71_8257_2021-06-10T16:21:00+02:00"
        );
        sandbox.assert.calledOnce(QueueManager.sendMessageToExchange as SinonStub);
    });
});
