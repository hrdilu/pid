import { BusMessageFilter } from "#ie/vehicle-positions/workers/runs/helpers/BusMessageFilter";
import { ICommonRunWithMessageDto } from "#sch/vehicle-positions/models/interfaces/ICommonRunWithMessageDto";
import { ILogger } from "@golemio/core/dist/helpers";
import { expect } from "chai";
import sinon, { SinonSandbox, SinonStub } from "sinon";

describe("BusMessageFilter", () => {
    let sandbox: SinonSandbox;
    let filter: BusMessageFilter;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        filter = new BusMessageFilter();
    });

    afterEach(() => {
        sandbox?.restore();
    });

    // =============================================================================
    // yieldFilteredMessages
    // =============================================================================
    describe("yieldFilteredMessages", () => {
        it("should yield messages that are valid for processing", () => {
            filter["logger"] = {
                info: sinon.stub(),
                debug: sinon.stub(),
            } as Partial<ILogger> as ILogger;

            const messages = [
                {
                    run: {
                        route_id: "X",
                    },
                },
                {
                    run: {
                        route_id: "1",
                    },
                },
                {
                    run: {
                        route_id: "50",
                    },
                },
                {
                    run: {
                        route_id: "69",
                    },
                },
                {
                    run: {
                        route_id: "75",
                    },
                },
                {
                    run: {
                        route_id: "150",
                    },
                },
            ];

            const result = Array.from(filter.yieldFilteredMessages(messages as ICommonRunWithMessageDto[]));
            expect(result).to.have.lengthOf(3);
            expect(result[0]).to.deep.equal(messages[2]);
            expect(result[1]).to.deep.equal(messages[3]);
            expect(result[2]).to.deep.equal(messages[5]);
            expect((filter["logger"].info as SinonStub).callCount).to.equal(3);
        });
    });
});
