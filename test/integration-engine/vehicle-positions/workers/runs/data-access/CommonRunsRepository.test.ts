import { CommonRunsMessagesRepository } from "#ie/vehicle-positions/workers/runs/data-access/CommonRunsMessagesRepository";
import { CommonRunsRepository } from "#ie/vehicle-positions/workers/runs/data-access/CommonRunsRepository";
import { ICommonRunsModel } from "#sch/vehicle-positions/models/interfaces/ICommonRunsModel";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import moment from "@golemio/core/dist/shared/moment-timezone";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";
import { TCPEventEnum } from "src/const";

chai.use(chaiAsPromised);

describe("CommonRunsRepository", () => {
    let runsRepository: CommonRunsRepository;
    let runsMessagesRepository: CommonRunsMessagesRepository;
    let sandbox: SinonSandbox;
    let currentTimestamp: string;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        PostgresConnector.connect();
        runsRepository = new CommonRunsRepository();
        runsMessagesRepository = new CommonRunsMessagesRepository();
        currentTimestamp = moment().tz("Europe/Prague").toISOString();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("getRunRecordForUpdate should return valid result", async () => {
        const data = (await runsRepository["getRunRecordForUpdate"]({
            id: "generated-id",
            route_id: "95",
            run_number: "52",
            line_short_name: "any",
            registration_number: "8509",
            msg_start_timestamp: "any",
            msg_last_timestamp: "any",
            wheelchair_accessible: true,
        })) as ICommonRunsModel;

        expect(data).not.to.be.null;
        expect(data.id).to.be.equal("95_52_8509_2021-06-10T16:00:00+02:00");
        expect(data.route_id).to.be.equal("95");
        expect(data.run_number).to.be.equal("52");
        expect(data.line_short_name).to.be.equal("95");
        expect(data.registration_number).to.be.equal("8509");
    });

    it("createAndAssociate should save run and run_message", async () => {
        const input = {
            run: {
                id: "2_8_8530_2021-06-10T16:22:07+02:00",
                route_id: "2",
                run_number: "2",
                line_short_name: "2",
                registration_number: "8530",
                msg_start_timestamp: currentTimestamp,
                msg_last_timestamp: currentTimestamp,
                wheelchair_accessible: false,
            },
            run_message: {
                lat: 50.09682,
                lng: 14.35231,
                actual_stop_asw_id: "00730002",
                actual_stop_timestamp_real: new Date(1623189596000),
                actual_stop_timestamp_scheduled: new Date(1623189600000),
                last_stop_asw_id: "05080002",
                packet_number: "237605",
                msg_timestamp: "2021-06-08T21:59:56",
                events: TCPEventEnum.DEPARTURED,
            },
        };
        const data = await runsRepository["createAndAssociate"](input);
        expect(data.run.id).to.be.equal(data.run_message.runs_id);
    });

    it("updateAndAssociate should update run and save run_message", async () => {
        const input = {
            run: {
                id: "96_51_5890_2021-06-10T17:00:00+02:00",
                route_id: "96",
                run_number: "51",
                line_short_name: "96",
                registration_number: "5890",
                msg_start_timestamp: currentTimestamp,
                msg_last_timestamp: currentTimestamp,
                wheelchair_accessible: true,
            },
            run_message: {
                lat: 50.09682,
                lng: 14.35231,
                actual_stop_asw_id: "00730002",
                actual_stop_timestamp_real: new Date(1623340800000),
                actual_stop_timestamp_scheduled: new Date(623340800000),
                last_stop_asw_id: "05080002",
                packet_number: "237607",
                msg_timestamp: currentTimestamp,
                events: TCPEventEnum.DEPARTURED,
            },
        };
        const data = await runsRepository["updateAndAssociate"](input, "95_52_8509_2021-06-10T16:00:00+02:00");
        expect(data.run.id).to.be.equal(data.run_message.runs_id);
    });

    it("deleteNHoursOldData should delete redundant data", async () => {
        const data = await runsRepository["deleteNHoursOldData"](24);
        expect(data).to.be.equal(1);
    });

    it("getLastMessage should return run message with the most actual msg timestamp", async () => {
        const result = await runsMessagesRepository.getLastMessage("8_10_9445_2022-12-06T06:20:58+01:00");

        expect(result.id).equal("1");
    });
});
