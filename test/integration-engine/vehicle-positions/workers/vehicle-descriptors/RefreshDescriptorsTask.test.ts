import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { RefreshDescriptorsTask } from "#ie/vehicle-positions/workers/vehicle-descriptors/tasks/RefreshDescriptorsTask";
import IPostgresConnector from "@golemio/core/dist/integration-engine/connectors/interfaces/IPostgresConnector";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc/ContainerToken";
import { expect } from "chai";
import sinon, { SinonSandbox, SinonStub } from "sinon";
import { InputDataFixture } from "./fixtures/InputDataFixture";

describe("DescriptorWorker - RefreshDescriptorsTask", () => {
    let sandbox: SinonSandbox;
    let task: RefreshDescriptorsTask;
    let getAllStub: SinonStub;

    before(async () => {
        sandbox = sinon.createSandbox();
        getAllStub = sandbox.stub().resolves([InputDataFixture[0]]);

        const postgresConnector = VPContainer.resolve<IPostgresConnector>(ContainerToken.PostgresConnector);
        await postgresConnector.connect();

        VPContainer.registerSingleton(
            VPContainerToken.DescriptorDataSourceFactory,
            class DummyFactory {
                getDataSource() {
                    return {
                        getAll: getAllStub,
                    };
                }
            }
        );
    });

    beforeEach(() => {
        task = VPContainer.resolve<RefreshDescriptorsTask>(VPContainerToken.RefreshDescriptorsTask);
    });

    after(() => {
        VPContainer.clearInstances();
    });

    it("should refresh data", async () => {
        await task["execute"]();
        const result = await task["descriptorRepository"].findOne({ where: { id: InputDataFixture[0].id } });
        expect(result.id).to.equal(InputDataFixture[0].id);
        expect(result.is_air_conditioned).to.be.true;

        getAllStub.resolves([{ ...InputDataFixture[0], airCondition: 0 }]);
        await task["execute"]();
        const updatedResult = await task["descriptorRepository"].findOne({ where: { id: InputDataFixture[0].id } });
        expect(updatedResult.id).to.equal(InputDataFixture[0].id);
        expect(updatedResult.is_air_conditioned).to.be.false;
    });
});
