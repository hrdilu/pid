import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { DeleteDataTask } from "#ie/vehicle-positions/workers/vehicle-descriptors/tasks/DeleteDataTask";
import IPostgresConnector from "@golemio/core/dist/integration-engine/connectors/interfaces/IPostgresConnector";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc/ContainerToken";
import Sequelize, { Op } from "@golemio/core/dist/shared/sequelize";
import { expect } from "chai";

describe("DescriptorWorker - DeleteDataTask", () => {
    let task: DeleteDataTask;

    before(async () => {
        const postgresConnector = VPContainer.resolve<IPostgresConnector>(ContainerToken.PostgresConnector);
        await postgresConnector.connect();
    });

    beforeEach(() => {
        task = VPContainer.resolve<DeleteDataTask>(VPContainerToken.DeleteDataTask);
    });

    it("should delete old descriptors", async () => {
        await task["execute"]({ targetHours: 24 });
        const result = await task["descriptorRepository"]["sequelizeModel"].findAll({
            where: {
                updated_at: {
                    [Op.lt]: Sequelize.literal(`NOW() - INTERVAL '${20} HOURS'`),
                },
            },
        });

        expect(result).to.have.length(0);
    });
});
