import { PropagateDelayTask } from "#ie/vehicle-positions/workers/vehicle-positions/tasks/PropagateDelayTask";
import { IVPTripsModel } from "#sch/vehicle-positions/models/interfaces/IVPTripsModel";
import { IVPTripsPositionAttributes } from "#sch/vehicle-positions/models/interfaces/IVPTripsPositionAttributes";
import { PostgresConnector, RedisConnector } from "@golemio/core/dist/integration-engine/connectors";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import sinon, { SinonFakeTimers, SinonSandbox, SinonStub } from "sinon";
import { StatePositionEnum } from "src/const";
import { runTripsId_9_22_getPDTWPositions } from "./data/runTripsId_9_22_getPDTWPositions";
import { runTripsId_9_22_IComputedTrip } from "./data/runTripsId_9_22_IComputedTrip";

chai.use(chaiAsPromised);

describe("VPWorker - PropagateDelayTask", () => {
    let sandbox: SinonSandbox;
    let clock: SinonFakeTimers;
    let sequelizeModelStub: Record<string, SinonStub>;
    let modelTripsBulkUpdatePropagatedDelay: SinonStub;
    let runTripsModelGetStub: SinonStub;
    let propagateDelayForGtfsTripStub: SinonStub;
    let getPropagateDelayTripsWithPositionsStub: SinonStub;
    let task: PropagateDelayTask;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        clock = sinon.useFakeTimers({
            now: new Date(2021, 12, 9, 10, 0),
            shouldAdvanceTime: true,
        });

        sequelizeModelStub = Object.assign({
            belongsTo: sandbox.stub(),
            hasMany: sandbox.stub(),
            hasOne: sandbox.stub(),
            removeAttribute: sandbox.stub(),
            upsert: sandbox.stub().resolves([{ id: 42 }, true]),
        });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().callsFake(() => sequelizeModelStub),
                query: sandbox.stub().callsFake(() => [{ exists: true }]),
                transaction: sandbox.stub().callsFake(() => Object.assign({ commit: sandbox.stub() })),
            })
        );
        sandbox.stub(RedisConnector, "getConnection");

        sandbox.stub(QueueManager, "sendMessageToExchange").resolves();

        task = new PropagateDelayTask("test.test");
        modelTripsBulkUpdatePropagatedDelay = sandbox.stub(task["tripsRepository"], "bulkUpdatePropagatedDelay");
    });

    afterEach(() => {
        sandbox?.restore();
        clock?.restore();
    });

    it("should take second gtfs_trip_id from schedule for same start_timestamp", async () => {
        propagateDelayForGtfsTripStub = sandbox
            .stub(task["tripsRepository"], "getPropagateDelayTripsWithPositions")
            .resolves(runTripsId_9_22_getPDTWPositions as Array<IVPTripsModel & { last_position: IVPTripsPositionAttributes }>);

        runTripsModelGetStub = sandbox
            .stub(task["runTripsRedisRepository"], "get")
            .callsFake(() => JSON.parse(fs.readFileSync(__dirname + "/data/runTripsId_9_22_redis.json").toString()));

        await task.validateAndExecute({
            content: JSON.stringify(runTripsId_9_22_IComputedTrip),
        } as any);

        expect(modelTripsBulkUpdatePropagatedDelay.callCount).to.be.equal(1);
        expect(modelTripsBulkUpdatePropagatedDelay.getCall(0).args).to.be.deep.equal([
            [
                {
                    position: {
                        id: "58895276",
                        delay: 2155,
                        state_position: StatePositionEnum.BEFORE_TRACK_DELAYED,
                        origin_timestamp: new Date(1639034440000),
                    },
                    trip: {
                        id: "2021-12-09T08:03:00+01:00_9_4904_211202_9381",
                    },
                },
            ],
        ]);
    });

    it("should call the correct methods by propagateDelay method", async () => {
        runTripsModelGetStub = sandbox.stub(task["runTripsRedisRepository"], "get").callsFake(() => Promise.resolve(null));

        propagateDelayForGtfsTripStub = sandbox.stub(task, "propagateDelayForGtfsTrip" as any);
        runTripsModelGetStub.callsFake(() =>
            Promise.resolve({
                schedule: [
                    {
                        trip_id: "19_2383_210830",
                        start_timestamp: "2021-09-08T14:03:00+02:00",
                        end_timestamp: "2021-09-08T14:56:00+02:00",
                    },
                    {
                        trip_id: "19_2384_210830",
                        start_timestamp: "2021-09-08T15:06:00+02:00",
                        end_timestamp: "2021-09-08T15:58:00+02:00",
                    },
                ],
            })
        );

        await task["execute"]({
            processedPositions: [
                {
                    context: {
                        lastPositionTracking: {
                            // POINT ...
                            // @ts-expect-error
                            properties: {
                                delay: 154,
                                state_position: StatePositionEnum.AT_STOP,
                                origin_timestamp: new Date(1631102586000),
                            },
                        },
                        tripId: "2021-09-08T14:03:00+02:00_19_2383_210830_9085",
                    },
                    positions: [],
                },
            ],
            trips: [
                {
                    id: "2021-09-08T14:03:00+02:00_19_2383_210830_9085",
                    gtfs_trip_id: "19_2383_210830",
                    start_timestamp: new Date(1631102580000).toISOString(),
                    origin_route_name: "19",
                    internal_route_name: "19",
                    run_number: 13,
                    internal_run_number: 13,
                    vehicle_registration_number: 9085,
                },
            ],
        });

        sandbox.assert.calledOnce(task["runTripsRedisRepository"].get as SinonStub);
        sandbox.assert.calledOnce(propagateDelayForGtfsTripStub);
        sandbox.assert.calledWith(propagateDelayForGtfsTripStub, {
            currentGtfsTripId: "19_2383_210830",
            currentOriginTimestamp: new Date("2021-09-08T12:03:06.000Z"),
            currentDelay: 154,
            currentRegistrationNumber: 9085,
            currentStartTime: "2021-09-08T14:03:00+02:00",
            currentEndTime: "2021-09-08T14:56:00+02:00",
            nextGtfsTrips: [{ trip_id: "19_2384_210830", requiredTurnaroundSeconds: undefined }],
        });
    });

    it("should calculate and propagate delay for GTFS run trip", async () => {
        getPropagateDelayTripsWithPositionsStub = sandbox
            .stub(task["tripsRepository"], "getPropagateDelayTripsWithPositions")
            .callsFake(
                async (): Promise<any> => [
                    {
                        last_position: {
                            id: "227255",
                            delay: null,
                            origin_timestamp: new Date(1629882900000),
                        },
                        id: "trip1",
                        start_timestamp: new Date(1629884100000),
                        end_timestamp: new Date(1629885180000),
                        // ...
                    },
                    {
                        last_position: {
                            id: "227398",
                            delay: null,
                            origin_timestamp: new Date(1629882900000),
                        },
                        id: "trip2",
                        start_timestamp: new Date(1629887100000),
                        end_timestamp: new Date(1629888180000),
                        // ...
                    },
                ]
            );

        await task["propagateDelayForGtfsTrip"]({
            currentGtfsTripId: "21_2255_210809",
            currentDelay: 360,
            currentRegistrationNumber: 9219,
            currentOriginTimestamp: new Date(1629882900000),
            currentStartTime: "2021-08-25T11:15:00+02:00",
            currentEndTime: "2021-08-25T11:33:00+02:00",
            nextGtfsTrips: [{ trip_id: "21_2256_210809" }, { trip_id: "21_2258_210809" }],
        });

        sandbox.assert.calledOnce(propagateDelayForGtfsTripStub);
        sandbox.assert.calledOnce(getPropagateDelayTripsWithPositionsStub);
        sandbox.assert.calledOnce(modelTripsBulkUpdatePropagatedDelay);
        sandbox.assert.calledWith(modelTripsBulkUpdatePropagatedDelay, [
            {
                position: {
                    id: "227255",
                    delay: 240,
                    state_position: StatePositionEnum.BEFORE_TRACK_DELAYED,
                    origin_timestamp: new Date(1629882900000),
                },
                trip: { id: "trip1" },
            },
            {
                position: {
                    id: "227398",
                    delay: 0,
                    state_position: StatePositionEnum.BEFORE_TRACK_DELAYED,
                    origin_timestamp: new Date(1629882900000),
                },
                trip: { id: "trip2" },
            },
        ]);
    });

    it("should calculate and propagate delay for GTFS run trip with consideration requiredTurnaroundSeconds", async () => {
        getPropagateDelayTripsWithPositionsStub = sandbox
            .stub(task["tripsRepository"], "getPropagateDelayTripsWithPositions")
            .callsFake(
                async (): Promise<any> => [
                    {
                        last_position: {
                            id: "227255",
                            delay: null,
                            origin_timestamp: 1629882900000,
                        },
                        id: "trip1",
                        start_timestamp: "1629884100000",
                        end_timestamp: "1629885180000",
                        gtfs_trip_id: "21_2256_210809",
                        // ...
                    },
                    {
                        last_position: {
                            id: "227398",
                            delay: null,
                            origin_timestamp: 1629882900000,
                        },
                        id: "trip2",
                        start_timestamp: "1629885420000",
                        end_timestamp: "1629888180000",
                        gtfs_trip_id: "21_2258_210809",
                        // ...
                    },
                ]
            );

        await task["propagateDelayForGtfsTrip"]({
            currentGtfsTripId: "21_2255_210809",
            currentDelay: 360,
            currentRegistrationNumber: 9219,
            currentOriginTimestamp: new Date(1629882900000),
            currentStartTime: "2021-08-25T11:15:00+02:00",
            currentEndTime: "2021-08-25T11:33:00+02:00",
            nextGtfsTrips: [{ trip_id: "21_2256_210809" }, { trip_id: "21_2258_210809", requiredTurnaroundSeconds: 30 }],
        });

        sandbox.assert.calledOnce(propagateDelayForGtfsTripStub);
        sandbox.assert.calledOnce(getPropagateDelayTripsWithPositionsStub);
        sandbox.assert.calledOnce(modelTripsBulkUpdatePropagatedDelay);
        sandbox.assert.calledWith(modelTripsBulkUpdatePropagatedDelay, [
            {
                position: {
                    id: "227255",
                    delay: 240,
                    state_position: StatePositionEnum.BEFORE_TRACK_DELAYED,
                    origin_timestamp: 1629882900000,
                },
                trip: { id: "trip1" },
            },
            {
                position: {
                    id: "227398",
                    delay: 30,
                    state_position: StatePositionEnum.BEFORE_TRACK_DELAYED,
                    origin_timestamp: 1629882900000,
                },
                trip: { id: "trip2" },
            },
        ]);
    });
});
