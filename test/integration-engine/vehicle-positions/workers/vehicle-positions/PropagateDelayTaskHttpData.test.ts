import { CommonRunsRepository } from "#ie/vehicle-positions/workers/runs/data-access/CommonRunsRepository";
import { TripsRepository } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/TripsRepository";
import { AbstractGTFSTripRunManager } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/gtfs-trip-run/AbstractGTFSTripRunManager";
import {
    GTFSTripRunManagerFactory,
    GTFSTripRunType,
} from "#ie/vehicle-positions/workers/vehicle-positions/helpers/gtfs-trip-run/GTFSTripRunManagerFactory";
import { PropagateDelayTask } from "#ie/vehicle-positions/workers/vehicle-positions/tasks/PropagateDelayTask";
import { UpdateDelayTask } from "#ie/vehicle-positions/workers/vehicle-positions/tasks/UpdateDelayTask";
import { PostgresConnector, RedisConnector } from "@golemio/core/dist/integration-engine/connectors";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import { Message } from "amqplib";
import { expect } from "chai";
import moment from "moment";
import sinon, { SinonFakeTimers, SinonSandbox } from "sinon";
import { StatePositionEnum } from "src/const";

describe("VPWorker - PropagateDelayTask HTTP data", () => {
    let sandbox: SinonSandbox;
    let clock: SinonFakeTimers;
    let messageData: any;
    let tripsRepository: TripsRepository;
    let propagateDelayTask: PropagateDelayTask;
    let updateDelayTask: UpdateDelayTask;

    beforeEach(async () => {
        sandbox = sinon.createSandbox();
        clock = sinon.useFakeTimers({
            now: moment().hour(12).minute(24).toDate(),
            shouldAdvanceTime: true,
        });
        await PostgresConnector.connect();
        await RedisConnector.connect();

        tripsRepository = new TripsRepository();

        propagateDelayTask = new PropagateDelayTask("test.test");
        updateDelayTask = new UpdateDelayTask("test.test");

        sandbox
            .stub(QueueManager, "sendMessageToExchange")
            .callsFake((_queuePrefix: string, _queueName: string, data: Record<string, any>) => {
                messageData = JSON.stringify(data);
                return Promise.resolve(true);
            });
    });

    afterEach(() => {
        sandbox.restore();
        clock.restore();
    });

    it("setPropagate for 212 line (HTTP data)", async () => {
        const gtfsTripRunManager: AbstractGTFSTripRunManager = GTFSTripRunManagerFactory.create(
            GTFSTripRunType.Common,
            updateDelayTask["positionsRepository"],
            updateDelayTask["tripsRepository"],
            new CommonRunsRepository(),
            propagateDelayTask["runTripsRedisRepository"]
        );

        await gtfsTripRunManager.setAndReturnScheduledTrips({
            route_id: "212",
            run_number: 2,
            msg_last_timestamp: new Date().toISOString(),
        });

        const tripsToUpdate = await tripsRepository.findAllAssocTripIds([
            "2022-12-23T11:24:00Z_100212_212_1044",
            "2022-12-23T11:31:00Z_100212_212_1043",
            "2022-12-23T12:01:00Z_100212_212_1045",
            "2022-12-23T11:54:00Z_100212_212_1046",
        ]);

        await updateDelayTask.validateAndExecute({
            content: Buffer.from(
                JSON.stringify({
                    updatedTrips: tripsToUpdate,
                    positions: [],
                })
            ),
        } as Message);

        await propagateDelayTask.validateAndExecute({
            content: Buffer.from(messageData), //stubbed rabbit queue message
        } as Message);

        const finalTrips = await tripsRepository.findAll({
            include: {
                as: "last_position",
                model: tripsRepository["positionsRepository"].sequelizeModel,
            },
            where: {
                gtfs_route_id: "L212",
            },
            order: ["start_time"],
            raw: true,
            nest: true,
        });

        const createTimestampFromHoursMinutes = (hours: number, minutes: number) => {
            return moment().startOf("minute").tz("Europe/Prague").hours(hours).minutes(minutes).toDate();
        };

        const expectedResults = [
            {
                gtfs_trip_id: "212_365_221211",
                start_timestamp: createTimestampFromHoursMinutes(12, 24),
                end_timestamp: createTimestampFromHoursMinutes(12, 30),
                last_position: {
                    state_position: StatePositionEnum.AFTER_TRACK,
                    delay: null,
                },
                last_position_id: "2563981366",
            },
            {
                gtfs_trip_id: "212_366_221211",
                start_timestamp: createTimestampFromHoursMinutes(12, 31),
                end_timestamp: createTimestampFromHoursMinutes(12, 36),
                last_position: {
                    state_position: StatePositionEnum.AT_STOP,
                    delay: 1288,
                },
                last_position_id: "2563981365",
            },
            {
                gtfs_trip_id: "212_367_221211",
                start_timestamp: createTimestampFromHoursMinutes(12, 54),
                end_timestamp: createTimestampFromHoursMinutes(13, 0),
                last_position: {
                    state_position: StatePositionEnum.BEFORE_TRACK_DELAYED,
                    //
                    // 36" -> 54" = 18" waiting (1080sec)
                    // >>> same first stop_id as previous trip last stop_id so 0s for turnaround
                    // 1288 - 1090 + 0 = 208s
                    //
                    delay: 208,
                },
                last_position_id: "2563981368",
                last_position_context: {
                    lastPositionBeforeTrackDelayed: {
                        delay: 208,
                    },
                },
            },
            {
                gtfs_trip_id: "212_368_221211",
                start_timestamp: createTimestampFromHoursMinutes(13, 1),
                end_timestamp: createTimestampFromHoursMinutes(13, 6),
                last_position: {
                    state_position: StatePositionEnum.BEFORE_TRACK_DELAYED,
                    //
                    // 36" -> 54" = 18" waiting (1080sec)
                    // >>> same first stop_id as previous trip last stop_id so 0s for turnaround
                    // 00" -> 01" = 1" waiting (60sec)
                    // >>> NOT same first stop_id as previous trip last stop_id so 30s for turnaround
                    // 1288 - 1090 + 0 - 60 + 30 = 178s
                    //
                    delay: 178,
                },
                last_position_id: "2563981367",
                last_position_context: {
                    lastPositionBeforeTrackDelayed: {
                        delay: 178,
                    },
                },
            },
        ];

        const testObjectProperties = (testedObject: any, expectedObject: any) => {
            for (const key of Object.keys(expectedObject)) {
                if (typeof expectedObject[key] === "object" && expectedObject[key] !== null) {
                    testObjectProperties(testedObject[key], expectedObject[key]);
                } else {
                    expect(testedObject[key]).to.equal(expectedObject[key]);
                }
            }
        };

        for (const [i, expectedResult] of expectedResults.entries()) {
            testObjectProperties(finalTrips[i], expectedResult);
        }
    });
});
