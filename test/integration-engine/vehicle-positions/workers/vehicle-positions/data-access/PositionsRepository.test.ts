import PositionsMapper from "#ie/vehicle-positions/workers/vehicle-positions/data-access/helpers/PositionsMapper";
import { PositionsRepository } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/PositionsRepository";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("PositionsRepository", () => {
    let repository: PositionsRepository;
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        PostgresConnector.connect();
        repository = new PositionsRepository();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("upsertCommonRunPosition should upsert and return plain record", async () => {
        const getRecordStub = sandbox.stub().returns("done");
        sandbox.stub(repository["sequelizeModel"], "upsert").resolves([{ get: getRecordStub }] as any);
        sandbox.stub(PositionsMapper, "mapCommonRunToDto");

        const result = await repository.upsertCommonRunPosition({ run: "dummy1", run_message: "dummy2" } as any, "dummy2" as any);
        expect(result).to.equal("done");
    });

    it("upsertCommonRunPosition should throw", async () => {
        sandbox.stub(repository["sequelizeModel"], "upsert").rejects();
        sandbox.stub(PositionsMapper, "mapCommonRunToDto");

        const promise = repository.upsertCommonRunPosition({ run: "dummy1", run_message: "dummy2" } as any, "dummy3" as any);
        await expect(promise).to.be.rejectedWith(GeneralError, "upsertCommonRunPosition: Error while saving to database.");
    });

    it("upsertMetroRunPosition should upsert and return plain record", async () => {
        const getRecordStub = sandbox.stub().returns("done");
        sandbox.stub(repository["sequelizeModel"], "upsert").resolves([{ get: getRecordStub }] as any);
        sandbox.stub(PositionsMapper, "mapMetroRunToDto");

        const result = await repository.upsertMetroRunPosition("dummy1" as any, "dummy2" as any, "dummy3" as any);
        expect(result).to.equal("done");
    });

    it("upsertMetroRunPosition should throw", async () => {
        sandbox.stub(repository["sequelizeModel"], "upsert").rejects();
        sandbox.stub(PositionsMapper, "mapMetroRunToDto");

        const promise = repository.upsertMetroRunPosition("dummy1" as any, "dummy2" as any, "dummy3" as any);
        await expect(promise).to.be.rejectedWith(GeneralError, "upsertMetroRunPosition: Error while saving to database.");
    });
});
