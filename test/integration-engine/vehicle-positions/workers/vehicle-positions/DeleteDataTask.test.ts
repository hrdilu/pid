import { IDataDeletionParams } from "#ie/shared/interfaces/IDataDeletionParams";
import { DeleteDataTask } from "#ie/vehicle-positions/workers/vehicle-positions/tasks/DeleteDataTask";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("VPWorker - DeleteDataTask", () => {
    let sandbox: SinonSandbox;
    let task: DeleteDataTask;

    const testParamsValid: IDataDeletionParams = {
        targetHours: 24,
    };

    const testParamsInvalid: IDataDeletionParams = {
        targetHours: 6,
    };

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox
                    .stub()
                    .returns({ removeAttribute: sandbox.stub(), belongsTo: sandbox.stub(), hasMany: sandbox.stub() }),
                removeAttribute: sandbox.stub(),
            })
        );

        task = new DeleteDataTask("test.test");
    });

    afterEach(() => {
        sandbox?.restore();
    });

    it("should validate and execute", async () => {
        const deleteTripsStub = sinon.stub(task["tripsRepository"], "deleteNHoursOldData").resolves();
        const deleteTripsHistoryStub = sinon.stub(task["tripsHistoryRepository"], "deleteNHoursOldData").resolves();
        const deletePositionsStub = sinon.stub(task["positionsRepository"], "deleteNHoursOldData").resolves();
        const deletePositionsHistoryStub = sinon.stub(task["positionsHistoryRepository"], "deleteNHoursOldData").resolves();
        const promise = task.validateAndExecute({ content: Buffer.from(JSON.stringify(testParamsValid)) } as any);

        await expect(promise).to.be.fulfilled;
        expect(deleteTripsStub.getCall(0).args).to.deep.equal([24]);
        expect(deleteTripsHistoryStub.getCall(0).args).to.deep.equal([24]);
        expect(deletePositionsStub.getCall(0).args).to.deep.equal([24]);
        expect(deletePositionsHistoryStub.getCall(0).args).to.deep.equal([24]);
    });

    it("should not validate (deletion of new data is disabled)", async () => {
        const deleteDataStub = sinon.stub(task["tripsRepository"], "deleteNHoursOldData").resolves();
        const promise = task.validateAndExecute({ content: Buffer.from(JSON.stringify(testParamsInvalid)) } as any);

        await expect(promise).to.be.rejectedWith(
            GeneralError,
            "[Queue test.test.deleteData] Message validation failed: [Data newer than 8 hours cannot be deleted]"
        );

        expect(deleteDataStub.callCount).to.equal(0);
    });
});
