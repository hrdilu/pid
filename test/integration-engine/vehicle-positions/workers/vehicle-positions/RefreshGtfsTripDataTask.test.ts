import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { RefreshGtfsTripDataTask } from "#ie/vehicle-positions/workers/vehicle-positions/tasks/RefreshGtfsTripDataTask";
import IPostgresConnector from "@golemio/core/dist/integration-engine/connectors/interfaces/IPostgresConnector";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc/ContainerToken";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("VPWorker - RefreshGtfsTripDataTask", () => {
    let sandbox: SinonSandbox;
    let task: RefreshGtfsTripDataTask;

    before(async () => {
        const postgresConnector = VPContainer.resolve<IPostgresConnector>(ContainerToken.PostgresConnector);
        await postgresConnector.connect();
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        task = new RefreshGtfsTripDataTask("test.test");
    });

    afterEach(() => {
        sandbox?.restore();
    });

    it("should update gtfs trip id 991_294_220901", async () => {
        const logStub = sandbox.stub(log, "info");
        const promise = task.validateAndExecute(null);

        await expect(promise).to.be.fulfilled;
        expect(logStub.lastCall.args[0]).to.contain("refreshGtfsTripData: updated");

        const trip = (await task["tripsRepository"].findOne({ where: { gtfs_trip_id: "991_294_220901" } }))?.toJSON();
        expect(trip.updated_at.getTime()).to.be.greaterThan(new Date().getTime() - 5 * 1000);
        expect(trip.gtfs_trip_headsign).to.equal("Depo Hostivař");
    });
});
