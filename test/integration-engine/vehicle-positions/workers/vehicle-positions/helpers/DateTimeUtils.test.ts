import { expect } from "chai";
import { DateTimeUtils } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/DateTimeUtils";

describe("DateTimeUtils", () => {
    // =============================================================================
    // formatSQLTimestamp
    // =============================================================================
    describe("formatSQLTimestamp", () => {
        it("should format timestamp", () => {
            const result = DateTimeUtils.formatSQLTimestamp("2022-10-26 08:55:10+01");
            expect(result).to.equal("2022-10-26T09:55:10+02:00");
        });
    });

    // =============================================================================
    // parseUTCTimeFromISO
    // =============================================================================
    describe("parseUTCTimeFromISO", () => {
        it("should parse time", () => {
            const result = DateTimeUtils.parseUTCTimeFromISO("2022-10-26T08:55:10+01:00");
            expect(result).to.equal("09:55:10");
        });
    });
});
