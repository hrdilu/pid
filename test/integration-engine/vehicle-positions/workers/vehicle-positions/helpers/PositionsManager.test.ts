import { IStopTime } from "#ie/ropid-gtfs";
import PositionsManager from "#ie/vehicle-positions/workers/vehicle-positions/helpers/PositionsManager";
import {
    ICurrentPositionProperties,
    IShapeAnchorPoint,
} from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/VPInterfaces";
import { IVPTripsLastPositionContext } from "#sch/vehicle-positions/models/interfaces/VPTripsLastPositionInterfaces";
import { DateTime } from "@golemio/core/dist/shared/luxon";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonFakeTimers } from "sinon";
import { StatePositionEnum, StateProcessEnum, TCPEventEnum } from "src/const";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";

chai.use(chaiAsPromised);

describe("PositionsManager", () => {
    const defaultContext: IVPTripsLastPositionContext = {
        atStopStreak: {
            stop_sequence: null,
            firstPositionTimestamp: null,
            firstPositionDelay: null,
        },
        lastPositionLastStop: {
            id: null,
            sequence: null,
            arrival_time: null,
            arrival_delay: null,
            departure_time: null,
            departure_delay: null,
        },
        lastPositionDelay: null,
        lastPositionId: null,
        lastPositionOriginTimestamp: null,
        lastPositionTracking: null,
        lastPositionCanceled: null,
        lastPositionBeforeTrackDelayed: null,
        isLastPositionAfterTrack: false,
        tripId: "2022-12-03T23:11:00Z_100375_375_4077",
    };

    const currentContext: IVPTripsLastPositionContext = {
        tripId: "2022-12-03T23:11:00Z_100375_375_4077",
        atStopStreak: {
            stop_sequence: null,
            firstPositionDelay: null,
            firstPositionTimestamp: null,
        },
        lastPositionId: "33",
        lastPositionDelay: 40,
        lastPositionCanceled: false,
        lastPositionLastStop: {
            id: null,
            sequence: null,
            arrival_time: null,
            arrival_delay: null,
            departure_time: null,
            departure_delay: null,
        },
        lastPositionTracking: {
            type: "Feature",
            geometry: { type: "Point", coordinates: [14.53414, 50.12516] },
            properties: {
                id: "33",
                lat: 50.12516,
                lng: 14.53414,
                delay: 40,
                bearing: 58,
                tracking: 2,
                valid_to: new Date(1670110340000),
                tcp_event: null,
                is_canceled: false,
                origin_time: "00:27:20",
                this_stop_id: null,
                last_stop_id: "U109Z1",
                next_stop_id: "U171Z1",
                state_process: StateProcessEnum.PROCESSED,
                last_stop_name: "Důstojnické domy",
                next_stop_name: "Letecké muzeum",
                state_position: StatePositionEnum.ON_TRACK,
                origin_timestamp: new Date(11670110040000),
                scheduled_timestamp: null,
                this_stop_sequence: null,
                last_stop_sequence: 8,
                next_stop_sequence: 9,
                shape_dist_traveled: 6.527,
                last_stop_arrival_time: new Date(11670109960000),
                next_stop_arrival_time: new Date(11670110020000),
                last_stop_departure_time: new Date(11670109960000),
                next_stop_departure_time: new Date(11670110020000),
            },
        },
        isLastPositionAfterTrack: false,
        lastPositionOriginTimestamp: 1670110040000,
        lastPositionBeforeTrackDelayed: null,
    };

    // =============================================================================
    // getCurrentContext
    // =============================================================================
    describe("getCurrentContext", () => {
        it("should return current context", () => {
            const context = PositionsManager["getCurrentContext"]({
                id: "2022-12-03T23:11:00Z_100375_375_4077",
                last_position_context: currentContext,
            } as any);

            expect(context).to.deep.equal(currentContext);
        });

        it("should return default context", () => {
            const context = PositionsManager["getCurrentContext"]({
                id: "2022-12-03T23:11:00Z_100375_375_4077",
            } as any);

            expect(context).to.deep.equal(defaultContext);
        });
    });

    // =============================================================================
    // getDelayBeforeTrack
    // =============================================================================
    describe("getDelayBeforeTrack", () => {
        const startOfDay = DateTime.now().startOf("day");
        const firstStopDepartureTime = startOfDay.plus({ seconds: 50 }).toSeconds() - startOfDay.toSeconds();

        it("should propagate last position's delay", () => {
            const lastPositionsDelay = 42;
            const positionOriginUnixTimestamp = startOfDay.plus({ seconds: 60 }).toMillis();
            const args: Parameters<(typeof PositionsManager)["getDelayBeforeTrack"]> = [
                lastPositionsDelay,
                firstStopDepartureTime,
                positionOriginUnixTimestamp,
                startOfDay.toMillis(),
            ];
            const delay = PositionsManager["getDelayBeforeTrack"](...args);
            expect(delay).to.equal(lastPositionsDelay);
        });

        it("should return corrected delay from attributes", () => {
            const lastPositionsDelay = 5;
            const positionOriginUnixTimestamp = startOfDay.plus({ seconds: 90 }).toMillis();
            const args: Parameters<(typeof PositionsManager)["getDelayBeforeTrack"]> = [
                lastPositionsDelay,
                firstStopDepartureTime,
                positionOriginUnixTimestamp,
                startOfDay.toMillis(),
            ];
            const delay = PositionsManager["getDelayBeforeTrack"](...args);
            expect(delay).to.equal(40);
        });

        it("should return null", () => {
            const lastPositionsDelay = undefined;
            const positionOriginUnixTimestamp = startOfDay.plus({ seconds: 60 }).toMillis();
            const args: Parameters<(typeof PositionsManager)["getDelayBeforeTrack"]> = [
                lastPositionsDelay,
                firstStopDepartureTime,
                positionOriginUnixTimestamp,
                startOfDay.toMillis(),
            ];
            const delay = PositionsManager["getDelayBeforeTrack"](...args);
            expect(delay).to.equal(null);
        });
    });

    // =============================================================================
    // getStateAndStopSequences
    // =============================================================================
    describe("getStateAndStopSequences", () => {
        const metroStopTimes = require("./fixtures/metro_992_1291_230213_stop_times.json");

        it("should return data (TRAM)", () => {
            const stopTimes = [
                {
                    arrival_time: "16:58:00",
                    departure_time: "16:58:00",
                    shape_dist_traveled: 0,
                    stop_headsign: null,
                    stop_id: "U945Z1P",
                    stop_sequence: 1,
                    arrival_time_seconds: 61080,
                    departure_time_seconds: 61080,
                    stop: {
                        stop_id: "U945Z1P",
                        stop_lat: 50.005123,
                        stop_lon: 14.436082,
                        stop_name: "Levského",
                    },
                },
                {
                    arrival_time: "16:58:00",
                    departure_time: "16:58:00",
                    shape_dist_traveled: 0.258011,
                    stop_headsign: null,
                    stop_id: "U946Z1P",
                    stop_sequence: 2,
                    arrival_time_seconds: 61080,
                    departure_time_seconds: 61080,
                    stop: {
                        stop_id: "U946Z1P",
                        stop_lat: 50.00523,
                        stop_lon: 14.432528,
                        stop_name: "Sídliště Modřany",
                    },
                },
                {
                    arrival_time: "16:59:00",
                    departure_time: "16:59:00",
                    shape_dist_traveled: 0.54451,
                    stop_headsign: null,
                    stop_id: "U947Z1P",
                    stop_sequence: 3,
                    arrival_time_seconds: 61140,
                    departure_time_seconds: 61140,
                    stop: {
                        stop_id: "U947Z1P",
                        stop_lat: 50.006416,
                        stop_lon: 14.429029,
                        stop_name: "Modřanská rokle",
                    },
                },
            ];

            const point: IShapeAnchorPoint = {
                index: Infinity, // Arbitrary value
                at_stop: true,
                bearing: 0,
                coordinates: [14.432528, 50.00523],
                distance_from_last_stop: 0,
                last_stop_sequence: 2,
                next_stop_sequence: 3,
                shape_dist_traveled: 0,
                this_stop_sequence: 2,
                time_scheduled_seconds: 0,
            };

            const data = PositionsManager["getStateAndStopSequences"](
                GTFSRouteTypeEnum.TRAM,
                stopTimes as any,
                point as any,
                { this_stop_id: null } as ICurrentPositionProperties,
                null
            );

            expect(data.statePosition).to.equal(StatePositionEnum.AT_STOP);
            expect(data.thisStopSequence).to.equal(2);
            expect(data.lastStopSequence).to.equal(2);
            expect(data.nextStopSequence).to.equal(3);
        });

        it("should return data (METRO AT_STOP)", () => {
            const point: IShapeAnchorPoint = {
                index: 13,
                at_stop: false,
                bearing: 256,
                coordinates: [14.55949, 50.10669],
                distance_from_last_stop: 0.023,
                last_stop_sequence: 1,
                next_stop_sequence: 2,
                shape_dist_traveled: 1.216,
                this_stop_sequence: null,
                time_scheduled_seconds: 57235,
            };

            const data = PositionsManager["getStateAndStopSequences"](
                GTFSRouteTypeEnum.METRO,
                metroStopTimes as any,
                point as any,
                { this_stop_id: "U818Z101P" } as ICurrentPositionProperties,
                null
            );

            expect(data.statePosition).to.equal(StatePositionEnum.AT_STOP);
            expect(data.thisStopSequence).to.equal(2);
            expect(data.lastStopSequence).to.equal(2);
            expect(data.nextStopSequence).to.equal(3);
        });

        it("should return data (METRO ON_TRACK)", () => {
            const point: IShapeAnchorPoint = {
                index: 13,
                at_stop: false,
                bearing: 256,
                coordinates: [14.55949, 50.10669],
                distance_from_last_stop: 0.023,
                last_stop_sequence: 1,
                next_stop_sequence: 2,
                shape_dist_traveled: 1.216,
                this_stop_sequence: null,
                time_scheduled_seconds: 57235,
            };

            const data = PositionsManager["getStateAndStopSequences"](
                GTFSRouteTypeEnum.METRO,
                metroStopTimes as any,
                point as any,
                { this_stop_id: null } as ICurrentPositionProperties,
                null
            );

            expect(data.statePosition).to.equal(StatePositionEnum.ON_TRACK);
            expect(data.thisStopSequence).to.equal(null);
            expect(data.lastStopSequence).to.equal(1);
            expect(data.nextStopSequence).to.equal(2);
        });

        it("should return data (METRO ON_TRACK, context fallback)", () => {
            const point: IShapeAnchorPoint = {
                index: 13,
                at_stop: false,
                bearing: 256,
                coordinates: [14.55949, 50.10669],
                distance_from_last_stop: 0.023,
                last_stop_sequence: 1,
                next_stop_sequence: 2,
                shape_dist_traveled: 1.216,
                this_stop_sequence: null,
                time_scheduled_seconds: 57235,
            };

            const context = {
                lastPositionTracking: {
                    properties: {
                        // Arbitrary values to test fallback
                        last_stop_sequence: 999,
                        next_stop_sequence: 1000,
                    },
                },
            };

            const data = PositionsManager["getStateAndStopSequences"](
                GTFSRouteTypeEnum.METRO,
                metroStopTimes as any,
                point as any,
                { this_stop_id: null } as ICurrentPositionProperties,
                context as any
            );

            expect(data.statePosition).to.equal(StatePositionEnum.ON_TRACK);
            expect(data.thisStopSequence).to.equal(null);
            expect(data.lastStopSequence).to.equal(999);
            expect(data.nextStopSequence).to.equal(1000);
        });

        it("should return data (mismatched TRAM event T)", () => {
            const point: IShapeAnchorPoint = {
                index: 21,
                at_stop: false,
                bearing: 264,
                coordinates: [14.44979, 50.12201],
                distance_from_last_stop: 0.103,
                last_stop_sequence: 5,
                next_stop_sequence: 6,
                shape_dist_traveled: 1.72,
                this_stop_sequence: null,
                time_scheduled_seconds: 56720,
            };

            const context = {
                lastPositionTracking: {
                    properties: {
                        tcp_event: TCPEventEnum.ARRIVAL_ANNOUNCED,
                        last_stop_sequence: 6,
                        next_stop_sequence: 7,
                    },
                },
            };

            const data = PositionsManager["getStateAndStopSequences"](
                GTFSRouteTypeEnum.TRAM,
                [],
                point,
                { tcp_event: TCPEventEnum.TIME } as ICurrentPositionProperties,
                context as IVPTripsLastPositionContext
            );

            expect(data.statePosition).to.equal(StatePositionEnum.MISMATCHED);
            expect(data.thisStopSequence).to.equal(null);
            expect(data.lastStopSequence).to.equal(5);
            expect(data.nextStopSequence).to.equal(6);
        });

        it("should return data (on_track BUS event T, close to the previous station)", () => {
            const point: IShapeAnchorPoint = {
                index: 282,
                at_stop: true,
                bearing: 351,
                coordinates: [14.48836, 50.02348],
                distance_from_last_stop: 0.024,
                last_stop_sequence: 44,
                next_stop_sequence: 45,
                shape_dist_traveled: 26.143,
                this_stop_sequence: 44,
                time_scheduled_seconds: 61200,
            };

            const context = {
                lastPositionTracking: {
                    properties: {
                        tcp_event: TCPEventEnum.DEPARTURED,
                        last_stop_sequence: 44,
                        next_stop_sequence: 45,
                    },
                },
            };

            const data = PositionsManager["getStateAndStopSequences"](
                GTFSRouteTypeEnum.BUS,
                [],
                point,
                { tcp_event: TCPEventEnum.TIME } as ICurrentPositionProperties,
                context as IVPTripsLastPositionContext
            );

            expect(data.statePosition).to.equal(StatePositionEnum.ON_TRACK);
            expect(data.thisStopSequence).to.equal(null);
            expect(data.lastStopSequence).to.equal(44);
            expect(data.nextStopSequence).to.equal(45);
        });

        it("should return data (mismatched BUS event T)", () => {
            const point: IShapeAnchorPoint = {
                index: 282,
                at_stop: true,
                bearing: 351,
                coordinates: [14.48836, 50.02348],
                distance_from_last_stop: 0.024,
                last_stop_sequence: 44,
                next_stop_sequence: 45,
                shape_dist_traveled: 26.143,
                this_stop_sequence: 44,
                time_scheduled_seconds: 61200,
            };

            const context = {
                lastPositionTracking: {
                    properties: {
                        tcp_event: TCPEventEnum.DEPARTURED,
                        last_stop_sequence: 45,
                        next_stop_sequence: 46,
                    },
                },
            };

            const data = PositionsManager["getStateAndStopSequences"](
                GTFSRouteTypeEnum.BUS,
                [],
                point,
                { tcp_event: TCPEventEnum.TIME } as ICurrentPositionProperties,
                context as IVPTripsLastPositionContext
            );

            expect(data.statePosition).to.equal(StatePositionEnum.MISMATCHED);
            expect(data.thisStopSequence).to.equal(null);
            expect(data.lastStopSequence).to.equal(44);
            expect(data.nextStopSequence).to.equal(45);
        });
    });

    // =============================================================================
    // isAfterTrack
    // =============================================================================
    describe("isAfterTrack", () => {
        const startDayTimestamp = new Date(2021, 2, 17).getTime();
        const lastStopTime: Partial<IStopTime> = { arrival_time_seconds: 58260 };
        const tripEndTimestamp = startDayTimestamp + lastStopTime.arrival_time_seconds! * 1000;
        const closestPoint: Pick<IShapeAnchorPoint, "last_stop_sequence"> = { last_stop_sequence: 9 };
        const lastTripPoint: Pick<IShapeAnchorPoint, "last_stop_sequence"> = { last_stop_sequence: 10 };
        let clock: SinonFakeTimers;

        beforeEach(() => {
            clock = sinon.useFakeTimers({
                now: new Date(2021, 2, 17, 10, 0),
                shouldAdvanceTime: true,
            });
        });

        afterEach(() => {
            clock.restore();
        });

        it("should return false (unknown TCP event)", () => {
            const currentPosition = {
                properties: {
                    tcp_event: undefined,
                },
            };

            expect(
                PositionsManager["isAfterTrack"](
                    currentPosition as any,
                    closestPoint.last_stop_sequence,
                    lastTripPoint as IShapeAnchorPoint,
                    tripEndTimestamp
                )
            ).to.be.false;
        });

        it("should return true (terminated)", () => {
            const currentPosition = {
                properties: {
                    tcp_event: TCPEventEnum.TERMINATED,
                },
            };

            expect(
                PositionsManager["isAfterTrack"](
                    currentPosition as any,
                    closestPoint.last_stop_sequence,
                    lastTripPoint as IShapeAnchorPoint,
                    tripEndTimestamp
                )
            ).to.be.true;
        });

        it("should return true (close to/in the terminus)", () => {
            const currentPosition = {
                properties: {
                    tcp_event: TCPEventEnum.TIME,
                },
            };

            const closestPoint: Pick<IShapeAnchorPoint, "last_stop_sequence"> = lastTripPoint;
            expect(
                PositionsManager["isAfterTrack"](
                    currentPosition as any,
                    closestPoint.last_stop_sequence,
                    lastTripPoint as IShapeAnchorPoint,
                    tripEndTimestamp
                )
            ).to.be.true;
        });

        it("should return true (arrival announced too late)", () => {
            const currentPosition = {
                properties: {
                    tcp_event: TCPEventEnum.ARRIVAL_ANNOUNCED,
                    scheduled_timestamp: new Date(tripEndTimestamp + 60 * 1000),
                },
            };

            expect(
                PositionsManager["isAfterTrack"](
                    currentPosition as any,
                    closestPoint.last_stop_sequence,
                    lastTripPoint as IShapeAnchorPoint,
                    tripEndTimestamp
                )
            ).to.be.true;
        });

        it("should return false (unknown scheduled timestamp)", () => {
            const currentPosition = {
                properties: {
                    tcp_event: TCPEventEnum.ARRIVAL_ANNOUNCED,
                },
            };

            expect(
                PositionsManager["isAfterTrack"](
                    currentPosition as any,
                    closestPoint.last_stop_sequence,
                    lastTripPoint as IShapeAnchorPoint,
                    tripEndTimestamp
                )
            ).to.be.false;
        });

        it("should return false (still on track)", () => {
            const currentPosition = {
                properties: {
                    tcp_event: TCPEventEnum.ARRIVAL_ANNOUNCED,
                    scheduled_timestamp: new Date(
                        PositionsManager["getLocalTimeToDateObject"](
                            startDayTimestamp,
                            lastStopTime.arrival_time_seconds!
                        ).getTime() -
                            60 * 1000
                    ),
                },
            };

            expect(
                PositionsManager["isAfterTrack"](
                    currentPosition as any,
                    closestPoint.last_stop_sequence,
                    lastTripPoint as IShapeAnchorPoint,
                    tripEndTimestamp
                )
            ).to.be.false;
        });
    });
});
