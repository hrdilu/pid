import ComputeDelayHelper from "#ie/vehicle-positions/workers/vehicle-positions/helpers/compute-positions/ComputeDelayHelper";
import { IPositionToUpdate } from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/VPInterfaces";
import { IVPTripsPositionAttributes } from "#sch/vehicle-positions/models/interfaces/IVPTripsPositionAttributes";
import { IVPTripsLastPositionContext } from "#sch/vehicle-positions/models/interfaces/VPTripsLastPositionInterfaces";
import { expect } from "chai";
import { PositionTrackingEnum, StatePositionEnum } from "src/const";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";

describe("ComputeDelayHelper - Strategy MetroDelayAtStop", () => {
    const routeType = GTFSRouteTypeEnum.METRO;

    it("should return updated context", () => {
        const positiontToUpdate: Partial<IPositionToUpdate> = {
            state_position: StatePositionEnum.ON_TRACK,
            last_stop_sequence: 3,
            delay: 40,
            delay_stop_arrival: 30,
            delay_stop_departure: 50,
        };

        const context: Partial<IVPTripsLastPositionContext> = {
            tripId: "2022-11-04T12:37:35+01:00_991_416_220901_13_13",
            lastPositionLastStop: {
                id: "U209Z102P",
                sequence: null,
                arrival_time: null,
                arrival_delay: null,
                departure_time: null,
                departure_delay: null,
            },
        };

        const resultContext = ComputeDelayHelper.updateContext(
            context as IVPTripsLastPositionContext,
            positiontToUpdate as IPositionToUpdate,
            { tracking: PositionTrackingEnum.TRACKING } as IVPTripsPositionAttributes,
            routeType
        );

        expect(resultContext).to.deep.equal({
            tripId: "2022-11-04T12:37:35+01:00_991_416_220901_13_13",
            lastPositionDelay: 40,
            lastPositionLastStop: {
                id: "U209Z102P",
                sequence: 3,
                arrival_time: null,
                arrival_delay: 30,
                departure_time: null,
                departure_delay: 50,
            },
        });
    });

    describe("updatePositionToUpdate", () => {
        it("should return updated position (AT_STOP arrival)", () => {
            const positionToUpdate: Partial<IPositionToUpdate> = {
                state_position: StatePositionEnum.AT_STOP,
                tracking: PositionTrackingEnum.TRACKING,
                last_stop_sequence: 3,
                last_stop_departure_time: new Date(1675809281000),
                delay: 95,
            };

            const position: Partial<IVPTripsPositionAttributes> = {
                state_position: StatePositionEnum.AT_STOP,
                tracking: PositionTrackingEnum.TRACKING,
                last_stop_sequence: 3,
                origin_timestamp: new Date(1675809165000 + positionToUpdate.delay! * 1000),
                scheduled_timestamp: new Date(1675809165000),
            };

            const context: Partial<IVPTripsLastPositionContext> = {
                tripId: "2022-11-04T12:37:35+01:00_991_416_220901_13_13",
                lastPositionDelay: null,
                lastPositionLastStop: {
                    id: "", // Arbitrary value
                    sequence: 2,
                    arrival_time: null,
                    arrival_delay: null,
                    departure_time: null,
                    departure_delay: null,
                },
                atStopStreak: {
                    firstPositionDelay: null,
                    firstPositionTimestamp: null,
                    stop_sequence: null,
                },
            };

            const resultPositionToUpdate = ComputeDelayHelper.updatePositionToUpdate(
                context as IVPTripsLastPositionContext,
                positionToUpdate as IPositionToUpdate,
                position as IVPTripsPositionAttributes,
                routeType
            );

            expect(resultPositionToUpdate).to.deep.equal({
                state_position: StatePositionEnum.AT_STOP,
                tracking: PositionTrackingEnum.TRACKING,
                last_stop_sequence: 3,
                last_stop_departure_time: new Date(1675809281000),
                delay_stop_arrival: 115,
                delay_stop_departure: null,
                delay: 95,
            });
        });

        it("should return updated position (AT_STOP streak)", () => {
            const positionToUpdate: Partial<IPositionToUpdate> = {
                state_position: StatePositionEnum.AT_STOP,
                tracking: PositionTrackingEnum.TRACKING,
                last_stop_sequence: 3,
                last_stop_departure_time: new Date(1675809281000),
                delay: 70,
            };

            const position: Partial<IVPTripsPositionAttributes> = {
                state_position: StatePositionEnum.AT_STOP,
                tracking: PositionTrackingEnum.TRACKING,
                last_stop_sequence: 3,
                scheduled_timestamp: null,
            };

            const context: Partial<IVPTripsLastPositionContext> = {
                tripId: "2022-11-04T12:37:35+01:00_991_416_220901_13_13",
                lastPositionDelay: null,
                lastPositionLastStop: {
                    id: "U557Z2P",
                    sequence: 3,
                    arrival_time: null,
                    arrival_delay: 115,
                    departure_time: null,
                    departure_delay: null,
                },
                atStopStreak: {
                    firstPositionDelay: null,
                    firstPositionTimestamp: null,
                    stop_sequence: 3,
                },
            };

            const resultPositionToUpdate = ComputeDelayHelper.updatePositionToUpdate(
                context as IVPTripsLastPositionContext,
                positionToUpdate as IPositionToUpdate,
                position as IVPTripsPositionAttributes,
                routeType
            );

            expect(resultPositionToUpdate).to.deep.equal({
                state_position: StatePositionEnum.AT_STOP,
                tracking: PositionTrackingEnum.TRACKING,
                last_stop_sequence: 3,
                last_stop_departure_time: new Date(1675809281000),
                delay_stop_arrival: 115,
                delay_stop_departure: null,
                delay: 70,
            });
        });

        it("should return updated position (ON_TRACK departure)", () => {
            const positionToUpdate: Partial<IPositionToUpdate> = {
                state_position: StatePositionEnum.ON_TRACK,
                tracking: PositionTrackingEnum.TRACKING,
                last_stop_sequence: 3,
                last_stop_departure_time: new Date(1675809281000),
                delay: 70,
            };

            const position: Partial<IVPTripsPositionAttributes> = {
                state_position: StatePositionEnum.ON_TRACK,
                tracking: PositionTrackingEnum.TRACKING,
                last_stop_sequence: 3,
                origin_timestamp: new Date(1675809281000 + positionToUpdate.delay! * 1000),
                scheduled_timestamp: new Date(1675809281000),
            };

            const context: Partial<IVPTripsLastPositionContext> = {
                tripId: "2022-11-04T12:37:35+01:00_991_416_220901_13_13",
                lastPositionDelay: null,
                lastPositionLastStop: {
                    id: "U557Z2P",
                    sequence: 3,
                    arrival_time: null,
                    arrival_delay: 115,
                    departure_time: null,
                    departure_delay: null,
                },
                atStopStreak: {
                    firstPositionDelay: null,
                    firstPositionTimestamp: null,
                    stop_sequence: 3,
                },
            };

            const resultPositionToUpdate = ComputeDelayHelper.updatePositionToUpdate(
                context as IVPTripsLastPositionContext,
                positionToUpdate as IPositionToUpdate,
                position as IVPTripsPositionAttributes,
                routeType
            );

            expect(resultPositionToUpdate).to.deep.equal({
                state_position: StatePositionEnum.ON_TRACK,
                tracking: PositionTrackingEnum.TRACKING,
                last_stop_sequence: 3,
                last_stop_departure_time: new Date(1675809281000),
                delay_stop_arrival: 115,
                delay_stop_departure: 65,
                delay: 70,
            });
        });

        it("should return updated position (ON_TRACK streak)", () => {
            const positionToUpdate: Partial<IPositionToUpdate> = {
                state_position: StatePositionEnum.ON_TRACK,
                tracking: PositionTrackingEnum.TRACKING,
                last_stop_sequence: 3,
                last_stop_departure_time: new Date(1675809281000),
                delay: 60,
            };

            const position: Partial<IVPTripsPositionAttributes> = {
                state_position: StatePositionEnum.ON_TRACK,
                tracking: PositionTrackingEnum.TRACKING,
                last_stop_sequence: 3,
                scheduled_timestamp: null,
            };

            const context: Partial<IVPTripsLastPositionContext> = {
                tripId: "2022-11-04T12:37:35+01:00_991_416_220901_13_13",
                lastPositionDelay: null,
                lastPositionLastStop: {
                    id: "U557Z2P",
                    sequence: 3,
                    arrival_time: null,
                    arrival_delay: 115,
                    departure_time: null,
                    departure_delay: 65,
                },
                atStopStreak: {
                    firstPositionDelay: null,
                    firstPositionTimestamp: null,
                    stop_sequence: null,
                },
            };

            const resultPositionToUpdate = ComputeDelayHelper.updatePositionToUpdate(
                context as IVPTripsLastPositionContext,
                positionToUpdate as IPositionToUpdate,
                position as IVPTripsPositionAttributes,
                routeType
            );

            expect(resultPositionToUpdate).to.deep.equal({
                state_position: StatePositionEnum.ON_TRACK,
                tracking: PositionTrackingEnum.TRACKING,
                last_stop_sequence: 3,
                last_stop_departure_time: new Date(1675809281000),
                delay_stop_arrival: 115,
                delay_stop_departure: 65,
                delay: 60,
            });
        });
    });
});
