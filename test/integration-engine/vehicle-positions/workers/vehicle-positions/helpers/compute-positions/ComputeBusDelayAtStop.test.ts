import ComputeDelayHelper from "#ie/vehicle-positions/workers/vehicle-positions/helpers/compute-positions/ComputeDelayHelper";
import { IPositionToUpdate } from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/VPInterfaces";
import { IVPTripsPositionAttributes } from "#sch/vehicle-positions/models/interfaces/IVPTripsPositionAttributes";
import { IVPTripsLastPositionContext } from "#sch/vehicle-positions/models/interfaces/VPTripsLastPositionInterfaces";
import { expect } from "chai";
import { PositionTrackingEnum, StatePositionEnum, TCPEventEnum } from "src/const";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";

describe("ComputeDelayHelper - Strategy BusDelayAtStop", () => {
    const routeType = GTFSRouteTypeEnum.BUS;

    it("should return updated context", () => {
        const position: Partial<IVPTripsPositionAttributes> = {
            tcp_event: TCPEventEnum.DEPARTURED,
            state_position: StatePositionEnum.ON_TRACK,
            tracking: PositionTrackingEnum.TRACKING,
            last_stop_sequence: 3,
            origin_timestamp: new Date(1675809271000),
            delay: 95,
        };

        const context: Partial<IVPTripsLastPositionContext> = {
            tripId: "2023-02-07T23:33:00+01:00_234_115_220928_3906",
            lastPositionLastStop: {
                id: "U557Z2P",
                sequence: null,
                arrival_time: null,
                arrival_delay: null,
                departure_time: null,
                departure_delay: null,
            },
        };

        const resultContext = ComputeDelayHelper.updateContext(
            context as IVPTripsLastPositionContext,
            null,
            position as IVPTripsPositionAttributes,
            routeType
        );

        expect(resultContext).to.deep.equal({
            tripId: "2023-02-07T23:33:00+01:00_234_115_220928_3906",
            lastPositionDelay: 95,
            lastPositionLastStop: {
                id: "U557Z2P",
                sequence: 3,
                arrival_time: null,
                arrival_delay: null,
                departure_time: null,
                departure_delay: 95,
            },
        });
    });

    it("should return updated position (event O)", () => {
        const positionToUpdate: Partial<IPositionToUpdate> = {
            state_position: StatePositionEnum.ON_TRACK,
            tracking: PositionTrackingEnum.TRACKING,
            last_stop_sequence: 3,
            last_stop_departure_time: new Date(1675809281000),
            delay: 95,
        };

        const position: Partial<IVPTripsPositionAttributes> = {
            tcp_event: TCPEventEnum.DEPARTURED,
            state_position: StatePositionEnum.ON_TRACK,
            tracking: PositionTrackingEnum.TRACKING,
            last_stop_sequence: 3,
        };

        const context: Partial<IVPTripsLastPositionContext> = {
            tripId: "2023-02-07T23:33:00+01:00_234_115_220928_3906",
            lastPositionDelay: null,
            lastPositionLastStop: {
                id: "U557Z2P",
                sequence: 3,
                arrival_time: null,
                arrival_delay: null,
                departure_time: null,
                departure_delay: null,
            },
        };

        const resultPositionToUpdate = ComputeDelayHelper.updatePositionToUpdate(
            context as IVPTripsLastPositionContext,
            positionToUpdate as IPositionToUpdate,
            position as IVPTripsPositionAttributes,
            routeType
        );

        expect(resultPositionToUpdate).to.deep.equal({
            state_position: StatePositionEnum.ON_TRACK,
            tracking: PositionTrackingEnum.TRACKING,
            last_stop_sequence: 3,
            last_stop_departure_time: new Date(1675809281000),
            delay_stop_departure: 95,
            delay: 95,
        });
    });

    describe("Event T", () => {
        const positionToUpdate: Partial<IPositionToUpdate> = {
            state_position: StatePositionEnum.ON_TRACK,
            tracking: PositionTrackingEnum.TRACKING,
            last_stop_sequence: 3,
            last_stop_departure_time: new Date(1675809281000),
        };

        const position: Partial<IVPTripsPositionAttributes> = {
            tcp_event: TCPEventEnum.TIME,
            state_position: StatePositionEnum.ON_TRACK,
            tracking: PositionTrackingEnum.TRACKING,
            last_stop_sequence: 3,
        };

        it("should return updated position", () => {
            const context: Partial<IVPTripsLastPositionContext> = {
                tripId: "2023-02-07T23:33:00+01:00_234_115_220928_3906",
                lastPositionDelay: 95,
                lastPositionLastStop: {
                    id: "U557Z2P",
                    sequence: 3,
                    arrival_time: null,
                    arrival_delay: null,
                    departure_time: null,
                    departure_delay: 95,
                },
            };

            const resultPositionToUpdate = ComputeDelayHelper.updatePositionToUpdate(
                context as IVPTripsLastPositionContext,
                Object.assign({}, positionToUpdate) as IPositionToUpdate,
                position as IVPTripsPositionAttributes,
                routeType
            );

            expect(resultPositionToUpdate).to.deep.equal({
                state_position: StatePositionEnum.ON_TRACK,
                tracking: PositionTrackingEnum.TRACKING,
                last_stop_sequence: 3,
                last_stop_departure_time: new Date(1675809281000),
                delay_stop_departure: 95,
            });
        });

        it("should return updated position (last stop delay 0)", () => {
            const context: Partial<IVPTripsLastPositionContext> = {
                tripId: "2023-02-07T23:33:00+01:00_234_115_220928_3906",
                lastPositionDelay: 95,
                lastPositionLastStop: {
                    id: "U557Z2P",
                    sequence: 3,
                    arrival_time: null,
                    arrival_delay: null,
                    departure_time: null,
                    departure_delay: 0,
                },
            };

            const resultPositionToUpdate = ComputeDelayHelper.updatePositionToUpdate(
                context as IVPTripsLastPositionContext,
                Object.assign({}, positionToUpdate) as IPositionToUpdate,
                position as IVPTripsPositionAttributes,
                routeType
            );

            expect(resultPositionToUpdate).to.deep.equal({
                state_position: StatePositionEnum.ON_TRACK,
                tracking: PositionTrackingEnum.TRACKING,
                last_stop_sequence: 3,
                last_stop_departure_time: new Date(1675809281000),
                delay_stop_departure: 0,
            });
        });
    });

    it("should just return (unknown TCP event)", () => {
        const positionToUpdate: Partial<IPositionToUpdate> = {
            state_position: StatePositionEnum.ON_TRACK,
            tracking: PositionTrackingEnum.TRACKING,
            last_stop_sequence: 3,
            last_stop_departure_time: new Date(1675809281000),
        };

        const position: Partial<IVPTripsPositionAttributes> = {
            state_position: StatePositionEnum.ON_TRACK,
            tracking: PositionTrackingEnum.TRACKING,
            last_stop_sequence: 3,
            origin_timestamp: new Date(1675809281000 + 95 * 1000),
        };

        const context: Partial<IVPTripsLastPositionContext> = {
            tripId: "2023-02-07T23:33:00+01:00_234_115_220928_3906",
            lastPositionDelay: 95,
            lastPositionLastStop: {
                id: "U557Z2P",
                sequence: 3,
                arrival_time: null,
                arrival_delay: null,
                departure_time: null,
                departure_delay: 95,
            },
        };

        const resultPositionToUpdate = ComputeDelayHelper.updatePositionToUpdate(
            context as IVPTripsLastPositionContext,
            positionToUpdate as IPositionToUpdate,
            position as IVPTripsPositionAttributes,
            routeType
        );

        expect(resultPositionToUpdate).to.deep.equal(positionToUpdate);
    });
});
