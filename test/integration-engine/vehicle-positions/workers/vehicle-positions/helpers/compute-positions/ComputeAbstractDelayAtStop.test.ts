// eslint-disable-next-line max-len
import AbstractDelayAtStop from "#ie/vehicle-positions/workers/vehicle-positions/helpers/compute-positions/strategy/AbstractDelayAtStop";
import { IPositionToUpdate } from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/VPInterfaces";
import { IVPTripsPositionAttributes } from "#sch/vehicle-positions/models/interfaces/IVPTripsPositionAttributes";
import { IVPTripsLastPositionContext } from "#sch/vehicle-positions/models/interfaces/VPTripsLastPositionInterfaces";
import { expect } from "chai";

class DummyDelayAtStop extends AbstractDelayAtStop {
    updateContext(
        context: IVPTripsLastPositionContext,
        _positionToUpdate: IPositionToUpdate | null,
        _position: IVPTripsPositionAttributes
    ): IVPTripsLastPositionContext {
        return context;
    }

    updatePosition(
        _context: IVPTripsLastPositionContext,
        positionToUpdate: IPositionToUpdate,
        _position: IVPTripsPositionAttributes | undefined
    ): IPositionToUpdate {
        return positionToUpdate;
    }
}

describe("ComputeDelayHelper - Strategy AbstractDelayAtStop", () => {
    const dummyDelayAtStop = new DummyDelayAtStop();

    describe("calcDelayAtStop", () => {
        it("should return delay in seconds", () => {
            const actualTimestamp = "2023-01-01T00:00:10.000Z";
            const scheduledTimestamp = "2023-01-01T00:00:00.000Z";

            const result = dummyDelayAtStop["calcDelayAtStop"](new Date(actualTimestamp).getTime(), new Date(scheduledTimestamp));
            expect(result).to.equal(10);
        });

        it("should return null if scheduledTimestamp is undefined", () => {
            const actualTimestamp = "2020-01-01T00:00:00.000Z";
            const scheduledTimestamp = undefined;

            const result = dummyDelayAtStop["calcDelayAtStop"](new Date(actualTimestamp).getTime(), scheduledTimestamp);
            expect(result).to.be.null;
        });
    });
});
