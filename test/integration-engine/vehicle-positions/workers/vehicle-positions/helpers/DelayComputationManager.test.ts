import { IShape, IStopTime, ITripsShapes, ITripsStopTimesStops, RopidGTFSTripsModel } from "#ie/ropid-gtfs";
import { DelayComputationManager } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/DelayComputationManager";
import { config } from "@golemio/core/dist/integration-engine/config";
import { PostgresConnector, RedisConnector } from "@golemio/core/dist/integration-engine/connectors";
import { expect } from "chai";
import fs from "fs";
import sinon, { SinonFakeTimers, SinonSandbox } from "sinon";

describe("DelayComputationManager", () => {
    let manager: DelayComputationManager;
    let sandbox: SinonSandbox;
    let clock: SinonFakeTimers;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        clock = sinon.useFakeTimers({
            now: new Date(2021, 2, 17, 10, 0),
            shouldAdvanceTime: true,
        });
        PostgresConnector.connect();
        sandbox.stub(RedisConnector, "getConnection");

        manager = new DelayComputationManager(new RopidGTFSTripsModel());

        const step = 0.05;
        sandbox.stub(config.vehiclePositions, "stepBetweenPoints").value(step);
    });

    afterEach(() => {
        sandbox.restore();
        clock.restore();
    });

    it("getShapesGeometryAnchorPoints should return correct data", async () => {
        const redisObject = JSON.parse(fs.readFileSync(__dirname + "/../data/delay-redisobject-01.json").toString());

        const points: Array<Record<string, any>> = manager["getShapesGeometryAnchorPoints"](
            "TRIP_1",
            redisObject.shapes as IShape[],
            redisObject.stopTimes as IStopTime[]
        );

        points.forEach((point, i) => {
            if (i < points.length - 1) {
                expect(points[i + 1].shape_dist_traveled - point.shape_dist_traveled <= 0.11).to.be.true;
            }
            expect(points.length).to.be.at.least(redisObject.stopTimes.length);
        });
    });

    it("getShapesGeometryAnchorPoints should return proper data (estimate point)", async () => {
        const shapes = (await manager["gtfsTripsRepository"]["findByIdWithShapes"]("TRIP_1")) as ITripsShapes;
        const stopTimes = (await manager["gtfsTripsRepository"]["findByIdWithStopTimes"]("TRIP_1")) as ITripsStopTimesStops;

        const shapesGeometryAnchorPoints = manager["getShapesGeometryAnchorPoints"](
            shapes.shape_id,
            shapes.shapes as IShape[],
            stopTimes.stop_times
        );

        // JSON.parse/JSON.stringify - to avoid undefined values from method
        expect(JSON.parse(JSON.stringify(shapesGeometryAnchorPoints))).to.deep.equal(
            JSON.parse(fs.readFileSync(__dirname + "/../data/delay-shapeanchor-01.json").toString())
        );
    });
});
