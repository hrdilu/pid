import DPPUtils from "#ie/vehicle-positions/workers/vehicle-positions/helpers/DPPUtils";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("DPPUtils - Invisibility check", () => {
    let sandbox: SinonSandbox;
    const scheduleMockup: any[] = require("../data/schedule-107-01.json").schedule;
    const DPPRAHA = "DP PRAHA";
    const dejvickaStopGps: [number, number] = [14.394204, 50.100861];
    const closeToDejvickaGps: [number, number] = [14.3931678, 50.1003456];
    const midLineGps: [number, number] = [14.3940819, 50.1127206];
    const farGPS: [number, number] = [14.3014528, 50.0936625];
    const suchdolStopGps: [number, number] = [14.378922, 50.13903];
    const closeToSuchdolGps: [number, number] = [14.3798269, 50.1394017];
    const startTime = 1657163040000;
    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("Before line start time and close to first station ~ false", () => {
        expect(
            DPPUtils.isInvisible(
                DPPRAHA,
                new Date(1657162800000),
                startTime,
                closeToDejvickaGps,
                dejvickaStopGps,
                scheduleMockup,
                "",
                null,
                0
            )
        ).equal(false);
    });

    it("Before line start time and far from first station ~ true", () => {
        expect(
            DPPUtils.isInvisible(
                DPPRAHA,
                new Date(1657162800000),
                startTime,
                farGPS,
                dejvickaStopGps,
                scheduleMockup,
                "",
                null,
                0
            )
        ).equal(true);
    });

    it("After first line start time various GPS ~ false", () => {
        expect(
            DPPUtils.isInvisible(
                DPPRAHA,
                new Date(1657188000000),
                startTime,
                closeToDejvickaGps,
                dejvickaStopGps,
                scheduleMockup,
                "",
                null,
                0
            )
        ).equal(false);
        expect(
            DPPUtils.isInvisible(
                DPPRAHA,
                new Date(1657188000000),
                startTime,
                closeToSuchdolGps,
                dejvickaStopGps,
                scheduleMockup,
                "",
                null,
                0
            )
        ).equal(false);
        expect(
            DPPUtils.isInvisible(
                DPPRAHA,
                new Date(1657188000000),
                startTime,
                midLineGps,
                dejvickaStopGps,
                scheduleMockup,
                "",
                null,
                0
            )
        ).equal(false);
    });

    it("After line end correct gtfs trip id ~ true", () => {
        expect(
            DPPUtils.isInvisible(
                DPPRAHA,
                new Date(1657218640000),
                startTime,
                closeToSuchdolGps,
                dejvickaStopGps,
                scheduleMockup,
                "107_621_220701",
                {} as any, // some last position
                0
            )
        ).equal(true);
    });

    it("After line end incorrect gtfs trip id ~ false", () => {
        expect(
            DPPUtils.isInvisible(
                DPPRAHA,
                new Date(1657218640000),
                startTime,
                closeToSuchdolGps,
                dejvickaStopGps,
                scheduleMockup,
                "107_621_220702",
                null,
                0
            )
        ).equal(false);
    });

    it("Other company than DP Praha ~ false", () => {
        expect(
            DPPUtils.isInvisible(
                "Other Company",
                new Date(1657162800000),
                startTime,
                closeToDejvickaGps,
                dejvickaStopGps,
                scheduleMockup,
                "",
                null,
                0
            )
        ).equal(false);
    });
});
