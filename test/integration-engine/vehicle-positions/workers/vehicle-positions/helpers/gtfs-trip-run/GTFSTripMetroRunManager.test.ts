import { IMetroRunInputForProcessing } from "#ie/vehicle-positions/workers/runs/interfaces/IMetroRunInputForProcessing";
import PositionsMapper from "#ie/vehicle-positions/workers/vehicle-positions/data-access/helpers/PositionsMapper";
import TripsMapper from "#ie/vehicle-positions/workers/vehicle-positions/data-access/helpers/TripsMapper";
import { PositionsRepository } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/PositionsRepository";
import { TripsRepository } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/TripsRepository";
import { AbstractGTFSTripRunManager } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/gtfs-trip-run/AbstractGTFSTripRunManager";
import {
    GTFSTripRunManagerFactory,
    GTFSTripRunType,
} from "#ie/vehicle-positions/workers/vehicle-positions/helpers/gtfs-trip-run/GTFSTripRunManagerFactory";
import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("GTFSTripMetroRunManager", () => {
    let sandbox: SinonSandbox;
    let tripsRepository: TripsRepository;
    let positionsRepository: PositionsRepository;
    let manager: AbstractGTFSTripRunManager;
    const schedule: IScheduleDto[] = [
        {
            // origin_route_id: "L991",
            trip_id: "991_77_220901",
            service_id: "1111100-1",
            date: "2022-10-24",
            route_id: "L991",
            route_type: 1,
            route_short_name: "A",
            is_regional: "0",
            is_substitute_transport: "0",
            is_night: "0",
            trip_headsign: "Nemocnice Motol",
            trip_short_name: null,
            block_id: null,
            exceptional: 0,
            min_stop_time: {
                hours: 9,
                minutes: 55,
            },
            max_stop_time: {
                hours: 10,
                minutes: 26,
            },
            start_timestamp: "2022-10-24T09:55:10+02:00",
            end_timestamp: "2022-10-24T10:26:20+02:00",
            first_stop_id: "U1071Z102P",
            last_stop_id: "U306Z102P",
        },
        {
            // origin_route_id: "L991",
            trip_id: "991_78_220321",
            service_id: "1111100-1",
            date: "2022-10-24",
            route_id: "L991",
            route_type: 1,
            route_short_name: "A",
            is_regional: "0",
            is_substitute_transport: "0",
            is_night: "0",
            trip_headsign: "Skalka",
            trip_short_name: null,
            block_id: null,
            exceptional: 0,
            min_stop_time: {
                hours: 10,
                minutes: 0,
            },
            max_stop_time: {
                hours: 11,
                minutes: 0,
            },
            start_timestamp: "2022-10-24T10:32:35+02:00",
            end_timestamp: "2022-10-24T11:00:40+02:00",
            first_stop_id: "U306Z101P",
            last_stop_id: "U953Z101P",
        },
        {
            // origin_route_id: "L991",
            trip_id: "991_124_201230",
            service_id: "1111100-1",
            date: "2022-10-24",
            route_id: "L991",
            route_type: 1,
            route_short_name: "A",
            is_regional: "0",
            is_substitute_transport: "0",
            is_night: "0",
            trip_headsign: "Nemocnice Motol",
            trip_short_name: null,
            block_id: null,
            exceptional: 0,
            min_stop_time: {
                hours: 11,
                minutes: 2,
            },
            max_stop_time: {
                hours: 11,
                minutes: 31,
            },
            start_timestamp: "2022-10-24T11:02:40+02:00",
            end_timestamp: "2022-10-24T11:31:20+02:00",
            first_stop_id: "U953Z102P",
            last_stop_id: "U306Z102P",
        },
    ];

    const runInput: IMetroRunInputForProcessing = {
        messageTimestamp: "2022-10-24T08:25:34Z",
        timestampScheduled: new Date("2022-10-24T08:25:34Z").toISOString(),
        runNumber: 5,
        routeId: "991",
        trainSetNumberScheduled: "5",
        trainSetNumberReal: "5",
        trainNumber: "182",
        coordinates: {
            lon: 14.36283,
            lat: 50.09831,
            gtfs_stop_id: "U157Z101P",
        },
    };

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        tripsRepository = Object.assign({
            upsertMetroRunTrip: sandbox.stub().callsFake((gtfsTrip: IScheduleDto, runInput: IMetroRunInputForProcessing) => {
                return TripsMapper.mapMetroRunToDto(gtfsTrip, runInput);
            }),
        });
        positionsRepository = Object.assign({
            upsertMetroRunPosition: sandbox
                .stub()
                .callsFake((runInput: IMetroRunInputForProcessing, gtfsTrip: IScheduleDto, isCurrent = true) => {
                    const result = PositionsMapper.mapMetroRunToDto(isCurrent, gtfsTrip, runInput);
                    return result;
                }),
        });
        manager = GTFSTripRunManagerFactory.create(
            GTFSTripRunType.Metro,
            positionsRepository,
            tripsRepository,
            sandbox.stub() as any,
            sandbox.stub() as any,
            true
        );
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("generateDelayMsg should return 1x tracking, 2x untracked", async () => {
        const result = await manager.generateDelayMsg(schedule, runInput);
        const tracking = result.positions.filter((element) => element.tracking === 2);
        const untracked = result.positions.filter((element) => element.tracking === 0);

        expect(tracking.length).to.equal(1);
        expect(untracked.length).to.equal(1);
    });
});
