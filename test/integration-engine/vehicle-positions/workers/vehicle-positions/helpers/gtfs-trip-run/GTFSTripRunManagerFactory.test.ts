import { GTFSTripCommonRunManager } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/gtfs-trip-run/GTFSTripCommonRunManager";
import { GTFSTripMetroRunManager } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/gtfs-trip-run/GTFSTripMetroRunManager";
import {
    GTFSTripRunManagerFactory,
    GTFSTripRunType,
} from "#ie/vehicle-positions/workers/vehicle-positions/helpers/gtfs-trip-run/GTFSTripRunManagerFactory";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon from "sinon";

chai.use(chaiAsPromised);

describe("GTFSTripRunManagerFactory", () => {
    it("should create an instance of GTFSTripCommonRunManager", async () => {
        const instance = GTFSTripRunManagerFactory.create(
            GTFSTripRunType.Common,
            sinon.stub() as any,
            sinon.stub() as any,
            sinon.stub() as any,
            sinon.stub() as any
        );

        expect(instance).to.be.instanceOf(GTFSTripCommonRunManager);
    });

    it("should create an instance of GTFSTripMetroRunManager", async () => {
        const instance = GTFSTripRunManagerFactory.create(
            GTFSTripRunType.Metro,
            sinon.stub() as any,
            sinon.stub() as any,
            sinon.stub() as any,
            sinon.stub() as any
        );

        expect(instance).to.be.instanceOf(GTFSTripMetroRunManager);
    });
});
