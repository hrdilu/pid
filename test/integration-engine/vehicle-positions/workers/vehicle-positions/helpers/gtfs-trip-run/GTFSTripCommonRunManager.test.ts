import PositionsMapper from "#ie/vehicle-positions/workers/vehicle-positions/data-access/helpers/PositionsMapper";
import TripsMapper from "#ie/vehicle-positions/workers/vehicle-positions/data-access/helpers/TripsMapper";
import { PositionsRepository } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/PositionsRepository";
import { TripsRepository } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/TripsRepository";
import { AbstractGTFSTripRunManager } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/gtfs-trip-run/AbstractGTFSTripRunManager";
import {
    GTFSTripRunManagerFactory,
    GTFSTripRunType,
} from "#ie/vehicle-positions/workers/vehicle-positions/helpers/gtfs-trip-run/GTFSTripRunManagerFactory";
import { IUpdateRunsGtfsTripInput } from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/IUpdateRunsGtfsTripInput";
import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";
import { TCPEventEnum } from "src/const";

chai.use(chaiAsPromised);

describe("GTFSTripCommonRunManager", () => {
    let sandbox: SinonSandbox;
    let tripsRepository: TripsRepository;
    let positionsRepository: PositionsRepository;
    let manager: AbstractGTFSTripRunManager;
    const schedule: IScheduleDto[] = [
        {
            //origin_route_id: "L352",
            trip_id: "301_41_210901",
            service_id: "1111100-1",
            date: "2022-02-28",
            route_id: "L301",
            route_type: 3,
            route_short_name: "301",
            is_regional: "1",
            is_substitute_transport: "0",
            is_night: "0",
            trip_headsign: "Praha,Luka",
            trip_short_name: null,
            block_id: "301_41_210901",
            exceptional: 0,
            min_stop_time: {
                hours: 7,
                minutes: 13,
            },
            max_stop_time: {
                hours: 7,
                minutes: 35,
            },
            start_timestamp: "2022-02-28T07:13:00+01:00",
            end_timestamp: "2022-02-28T07:35:00+01:00",
            first_stop_id: "U1200Z2",
            last_stop_id: "U1007Z71",
        },
        {
            //origin_route_id: "L352",
            trip_id: "174_452_210901",
            service_id: "1111100-1",
            date: "2022-02-28",
            route_id: "L174",
            route_type: 3,
            route_short_name: "174",
            is_regional: "0",
            is_substitute_transport: "0",
            is_night: "0",
            trip_headsign: "Vypich",
            trip_short_name: null,
            block_id: "301_41_210901",
            exceptional: 0,
            min_stop_time: {
                hours: 7,
                minutes: 35,
            },
            max_stop_time: {
                hours: 7,
                minutes: 53,
            },
            start_timestamp: "2022-02-28T07:35:00+01:00",
            end_timestamp: "2022-02-28T07:53:00+01:00",
            first_stop_id: "U1007Z71P",
            last_stop_id: "U872Z8P",
        },
        {
            //origin_route_id: "L352",
            trip_id: "174_453_210901",
            service_id: "1111100-1",
            date: "2022-02-28",
            route_id: "L174",
            route_type: 3,
            route_short_name: "174",
            is_regional: "0",
            is_substitute_transport: "0",
            is_night: "0",
            trip_headsign: "Třebonice",
            trip_short_name: null,
            block_id: null,
            exceptional: 0,
            min_stop_time: {
                hours: 8,
                minutes: 8,
            },
            max_stop_time: {
                hours: 8,
                minutes: 37,
            },
            start_timestamp: "2022-02-28T08:08:00+01:00",
            end_timestamp: "2022-02-28T08:37:00+01:00",
            first_stop_id: "U872Z7P",
            last_stop_id: "U768Z1P",
        },
    ];
    const input: IUpdateRunsGtfsTripInput = {
        run: {
            id: "352_5_3818_2022-02-28T05:51:42+01:00",
            route_id: "352",
            run_number: "5",
            line_short_name: "174",
            registration_number: "3818",
            msg_start_timestamp: "2022-02-28 05:51:42+01",
            msg_last_timestamp: "2022-02-28 11:46:16+01",
            wheelchair_accessible: true,
        },
        run_message: {
            id: 105596595,
            runs_id: "352_5_3818_2022-02-28T05:51:42+01:00",
            lat: 50.04156,
            lng: 14.32793,
            actual_stop_asw_id: "10290001",
            actual_stop_timestamp_real: new Date(1646030221000).toISOString(),
            actual_stop_timestamp_scheduled: new Date(1646029980000).toISOString(),
            last_stop_asw_id: "08720008",
            packet_number: "",
            msg_timestamp: "2022-02-28 07:37:01+01",
            events: TCPEventEnum.TERMINATED,
        },
    };

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        tripsRepository = Object.assign({
            upsertCommonRunTrip: sandbox.stub().callsFake(({ run }: IUpdateRunsGtfsTripInput, gtfsTrip: IScheduleDto) => {
                return TripsMapper.mapCommonRunToDto(gtfsTrip, run);
            }),
        });
        positionsRepository = Object.assign({
            upsertCommonRunPosition: sandbox
                .stub()
                .callsFake(({ run, run_message }: IUpdateRunsGtfsTripInput, gtfsTrip: IScheduleDto, isCurrent = true) => {
                    const result = PositionsMapper.mapCommonRunToDto(run_message, isCurrent, gtfsTrip, run);
                    return result;
                }),
        });
        manager = GTFSTripRunManagerFactory.create(
            GTFSTripRunType.Common,
            positionsRepository,
            tripsRepository,
            sandbox.stub() as any,
            sandbox.stub() as any,
            true
        );
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("lines 301 time 07:34 - 1x on track", async () => {
        //Mon Feb 28 2022 07:34:00 GMT+0100
        input.run_message.actual_stop_timestamp_scheduled = new Date(1646030040000).toISOString();

        const result = await manager.generateDelayMsg(schedule, input);
        expect(result.positions).to.have.lengthOf(3);
        const onTrack = result.positions.filter((element) => element.tracking == 2);
        expect(onTrack).to.have.lengthOf(1);
        expect(onTrack[0].trips_id).contains("_301_");
    });

    it("lines 174 time 07:35 - 1x on track", async () => {
        //Mon Feb 28 2022 07:35:00 GMT+0100
        input.run_message.actual_stop_timestamp_scheduled = new Date(1646030100000).toISOString();

        const result = await manager.generateDelayMsg(schedule, input);
        expect(result.positions).to.have.lengthOf(3);
        const onTrack = result.positions.filter((element) => element.tracking == 2);
        expect(onTrack).to.have.lengthOf(1);
        expect(onTrack[0].trips_id).contains("_174_");
    });

    it("lines 174 time 07:38 - 1x on track", async () => {
        //Mon Feb 28 2022 07:38:00 GMT+0100
        input.run_message.actual_stop_timestamp_scheduled = new Date(1646030280000).toISOString();

        const result = await manager.generateDelayMsg(schedule, input);
        expect(result.positions).to.have.lengthOf(3);
        const onTrack = result.positions.filter((element) => element.tracking == 2);
        expect(onTrack).to.have.lengthOf(1);
        expect(onTrack[0].trips_id).contains("_174_");
    });
});
