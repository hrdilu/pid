import { IUpdateDelayTripsIdsData } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/TripsRepository";
import { VPUtils } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/VPUtils";
import { IPositionTransformationResult } from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/TransformationInterfaces";
import { TripStopsSchedule } from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/VPInterfaces";
import { config } from "@golemio/core/dist/integration-engine/config";
import { DateTime } from "@golemio/core/dist/shared/luxon";
import { expect } from "chai";
import sinon from "sinon";
import { PositionTrackingEnum } from "src/const";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";

describe("VPUtils", () => {
    it("determineNewPositionTracking should determine tracking (train)", () => {
        const tripStopSchedule: TripStopsSchedule = {
            "1302_6057_211212": {
                trip_stops: [
                    {
                        stop_id: "U2432Z301",
                        stop_sequence: 1,
                        cis: 5453114,
                    },
                    {
                        stop_id: "U2973Z301",
                        stop_sequence: 2,
                        cis: 5453214,
                    },
                ],
            },
        };
        const trip: Partial<IUpdateDelayTripsIdsData> = {
            gtfs_block_id: "1336_6057_211212",
            gtfs_trip_id: "1302_6057_211212",
            gtfs_route_type: GTFSRouteTypeEnum.TRAIN,
            start_timestamp: new Date().toISOString(),
            end_timestamp: new Date().toISOString(),
        };
        const position: Partial<IPositionTransformationResult> = {
            cis_last_stop_id: 5453114,
        };

        const res = VPUtils.determineNewPositionTracking(
            tripStopSchedule,
            trip as IUpdateDelayTripsIdsData,
            position as IPositionTransformationResult
        );

        expect(res).to.equal(PositionTrackingEnum.TRACKING);
    });

    it("determineNewPositionTracking should determine not tracking (train)", () => {
        const tripStopSchedule: TripStopsSchedule = {
            "1302_6057_211212": {
                trip_stops: [
                    {
                        stop_id: "U2973Z301",
                        stop_sequence: 1,
                        cis: 5453214,
                    },
                ],
            },
        };
        const trip: Partial<IUpdateDelayTripsIdsData> = {
            gtfs_block_id: "1336_6057_211212",
            gtfs_trip_id: "1302_6057_211212",
            gtfs_route_type: GTFSRouteTypeEnum.TRAIN,
            start_timestamp: new Date().toISOString(),
            end_timestamp: new Date().toISOString(),
        };
        const position: Partial<IPositionTransformationResult> = {
            cis_last_stop_id: 4242,
        };

        const res = VPUtils.determineNewPositionTracking(
            tripStopSchedule,
            trip as IUpdateDelayTripsIdsData,
            position as IPositionTransformationResult
        );

        expect(res).to.equal(PositionTrackingEnum.NOT_TRACKING);
    });

    it("determineTrackingStatus should return assigned tracking status (already not tracking)", () => {
        const tripStopSchedule = {};
        const position: Partial<IPositionTransformationResult> = {
            tracking: PositionTrackingEnum.NOT_TRACKING,
        };

        const trackingStatus = VPUtils.determineTrackingStatus(
            "1302_6057_211212",
            tripStopSchedule,
            position as IPositionTransformationResult
        );

        expect(trackingStatus).to.equal(PositionTrackingEnum.NOT_TRACKING);
    });

    it("determineTrackingStatus should return assigned tracking status (undefined trip stops)", () => {
        const tripStopSchedule = {};
        const position: Partial<IPositionTransformationResult> = {
            tracking: PositionTrackingEnum.TRACKING,
        };

        const trackingStatus = VPUtils.determineTrackingStatus(
            "1302_6057_211212",
            tripStopSchedule,
            position as IPositionTransformationResult
        );

        expect(trackingStatus).to.equal(PositionTrackingEnum.TRACKING);
    });

    it("determineTrackingStatus should return determined tracking status", () => {
        const tripStopSchedule = {
            "1302_6057_211212": {
                trip_stops: [
                    {
                        stop_id: "U2973Z301",
                        stop_sequence: 1,
                        cis: 5453214,
                    },
                ],
            },
        };
        const position: Partial<IPositionTransformationResult> = {
            tracking: PositionTrackingEnum.TRACKING,
            cis_last_stop_id: 5453215,
        };

        const trackingStatus = VPUtils.determineTrackingStatus(
            "1302_6057_211212",
            tripStopSchedule,
            position as IPositionTransformationResult
        );

        expect(trackingStatus).to.equal(PositionTrackingEnum.NOT_TRACKING);
    });

    it("determineTrackingStatus should return determined tracking status (last stop)", () => {
        const tripStopSchedule = {
            "1302_6057_211212": {
                trip_stops: [
                    {
                        stop_id: "U2973Z301",
                        stop_sequence: 1,
                        cis: 5453214,
                    },
                ],
            },
        };
        const position: Partial<IPositionTransformationResult> = {
            tracking: PositionTrackingEnum.TRACKING,
            cis_last_stop_id: 5453214,
        };

        const trackingStatus = VPUtils.determineTrackingStatus(
            "1302_6057_211212",
            tripStopSchedule,
            position as IPositionTransformationResult
        );

        expect(trackingStatus).to.equal(PositionTrackingEnum.NOT_TRACKING);
    });

    it("getNextExpireTimestamp should return PXAT timestamp (current day)", () => {
        const clock = sinon.useFakeTimers({
            now: DateTime.fromObject({ year: 2022, month: 5, day: 26, hour: 2 }, { zone: "UTC" }).valueOf(),
        });
        config.vehiclePositions.redisExpireTime = "03:00";

        const timestamp = VPUtils.getNextExpireTimestamp();
        expect(DateTime.fromMillis(timestamp!, { zone: "UTC" }).toISO()).to.eq("2022-05-26T03:00:00.000Z");

        clock.restore();
    });

    it("getNextExpireTimestamp should return PXAT timestamp (next day - just before expiration time)", () => {
        const clock = sinon.useFakeTimers({
            now: DateTime.fromObject({ year: 2022, month: 5, day: 26, hour: 2, minute: 59 }, { zone: "UTC" }).valueOf(),
        });
        config.vehiclePositions.redisExpireTime = "03:00";

        const timestamp = VPUtils.getNextExpireTimestamp();
        expect(DateTime.fromMillis(timestamp!, { zone: "UTC" }).toISO()).to.eq("2022-05-27T03:00:00.000Z");

        clock.restore();
    });

    it("getNextExpireTimestamp should return PXAT timestamp (next day - after previous expiration time)", () => {
        const clock = sinon.useFakeTimers({
            now: DateTime.fromObject({ year: 2022, month: 5, day: 26, hour: 5 }, { zone: "UTC" }).valueOf(),
        });
        config.vehiclePositions.redisExpireTime = "03:00";

        const timestamp = VPUtils.getNextExpireTimestamp();
        expect(DateTime.fromMillis(timestamp!, { zone: "UTC" }).toISO()).to.eq("2022-05-27T03:00:00.000Z");

        clock.restore();
    });

    it("getNextExpireTimestamp should return undefined", () => {
        config.vehiclePositions.redisExpireTime = "homer:simpson";
        const timestamp = VPUtils.getNextExpireTimestamp();
        expect(timestamp).to.eq(undefined);
    });
});
