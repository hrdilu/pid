import { RopidGTFSTripsModel } from "#ie/ropid-gtfs/RopidGTFSTripsModel";
import { DelayComputationManager } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/DelayComputationManager";
import PositionsManager from "#ie/vehicle-positions/workers/vehicle-positions/helpers/PositionsManager";
import { ICurrentPositionProperties } from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/VPInterfaces";
import { config } from "@golemio/core/dist/integration-engine/config";
import { PostgresConnector, RedisConnector } from "@golemio/core/dist/integration-engine/connectors";
import moment from "@golemio/core/dist/shared/moment-timezone";
import * as turf from "@turf/turf";
import { expect } from "chai";
import fs from "fs";
import sinon, { SinonFakeTimers, SinonSandbox } from "sinon";
import { StatePositionEnum } from "src/const";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";

describe("PositionsManager - EstimatePoint", () => {
    let sandbox: SinonSandbox;
    let clock: SinonFakeTimers;
    let manager: DelayComputationManager;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        clock = sinon.useFakeTimers({
            now: new Date(2021, 2, 17, 10, 0),
            shouldAdvanceTime: true,
        });
        PostgresConnector.connect();
        sandbox.stub(RedisConnector, "getConnection");

        const step = 0.05;
        sandbox.stub(config.vehiclePositions, "stepBetweenPoints").value(step);

        manager = new DelayComputationManager(new RopidGTFSTripsModel());
    });

    afterEach(() => {
        sandbox.restore();
        clock.restore();
    });

    it("getStartDayTimestamp should return same day", async () => {
        expect(
            PositionsManager["getStartDayTimestamp"](new Date("2021-03-17T00:00:00+01:00").getTime(), 14 * 3600 + 10 * 60 + 0 * 1)
        ).to.equal(new Date("2021-03-17T00:00:00+01:00").getTime());
    });

    it("getStartDayTimestamp should return previous day", async () => {
        expect(
            PositionsManager["getStartDayTimestamp"](new Date("2021-03-17T00:00:00+01:00").getTime(), 31 * 3600 + 10 * 60 + 0 * 1)
        ).to.equal(new Date("2021-03-16T00:00:00+01:00").getTime());
    });

    it("getStartDayTimestamp should return previous day CET from CEST", async () => {
        expect(
            PositionsManager["getStartDayTimestamp"](new Date("2021-03-29T04:00:00+02:00").getTime(), 28 * 3600 + 10 * 60 + 0 * 1)
        ).to.equal(new Date("2021-03-28T00:00:00+01:00").getTime());
    });

    it("getResultObjectForDelayCalculation should return proper data", async () => {
        const data = await manager.getComputationObject("TRIP_1");
        const testData = JSON.parse(fs.readFileSync(__dirname + "/../data/delay-redisobject-01.json").toString());
        expect(data.trip_id).to.deep.equal(testData.trip_id);
        expect(data.shapes_anchor_points).to.deep.equal(testData.shapes_anchor_points);
        expect(data.stop_times).to.deep.equal(testData.stop_times);
    });

    it("getLocalTimeToDateObject test - normal day in 12:15", async () => {
        expect(
            await PositionsManager["getLocalTimeToDateObject"](
                new Date("2021-03-27T00:00:00+01:00").getTime(),
                12 * 3600 + 15 * 60 + 0
            )
        ).to.deep.equal(new Date("2021-03-27T12:15:00+01:00"));
    });

    it("getLocalTimeToDateObject test - saturday trip in sunday in 1:15", async () => {
        expect(
            await PositionsManager["getLocalTimeToDateObject"](
                new Date("2021-03-27T00:00:00+01:00").getTime(),
                25 * 3600 + 15 * 60 + 0
            )
        ).to.deep.equal(new Date("2021-03-28T01:15:00+01:00"));
    });

    it("getLocalTimeToDateObject test - saturday trip in sunday in 3:15 (CET to CEST)", async () => {
        expect(
            await PositionsManager["getLocalTimeToDateObject"](
                new Date("2021-03-27T00:00:00+01:00").getTime(),
                27 * 3600 + 15 * 60 + 0
            )
        ).to.deep.equal(new Date("2021-03-28T03:15:00+02:00"));
    });

    it("getLocalTimeToDateObject test - sunday trip in monday in 3:15 (CET to CEST)", async () => {
        expect(
            await PositionsManager["getLocalTimeToDateObject"](
                new Date("2021-03-28T00:00:00+01:00").getTime(),
                27 * 3600 + 15 * 60 + 0
            )
        ).to.deep.equal(new Date("2021-03-29T03:15:00+02:00"));
    });

    it("getLocalTimeToDateObject test - sunday trip in sunday in 5:15 (CEST)", async () => {
        expect(
            await PositionsManager["getLocalTimeToDateObject"](
                new Date("2021-03-28T00:00:00+01:00").getTime(),
                5 * 3600 + 15 * 60 + 0
            )
        ).to.deep.equal(new Date("2021-03-28T05:15:00+02:00"));
    });

    it("getLocalTimeToDateObject test - sunday trip in monday in 1:15 (CET)", async () => {
        expect(
            await PositionsManager["getLocalTimeToDateObject"](
                new Date("2021-10-31T00:00:00+02:00").getTime(),
                25 * 3600 + 15 * 60 + 0
            )
        ).to.deep.equal(new Date("2021-11-01T01:15:00+01:00"));
    });

    it("getEstimatedPoint should return for current position only on track", async () => {
        // position on track
        const currentPosition = turf.point([14.4747, 50.1173], {
            id: "1231241",
            origin_time: "10:00:10",
            origin_timestamp: moment("2021-03-17T10:00:10+01:00").toDate(),
        } as ICurrentPositionProperties);
        const lastPosition = null;
        const startTimestamp = new Date("2021-03-17T10:00:00+01:00").getTime();
        const redisObject = JSON.parse(fs.readFileSync(__dirname + "/../data/delay-redisobject-01.json").toString());

        const startDayTimestamp = PositionsManager["getStartDayTimestamp"](
            startTimestamp,
            redisObject.shapes_anchor_points[0].time_scheduled_seconds
        );
        const estimatedPosition = await PositionsManager["getEstimatedPoint"](
            redisObject,
            currentPosition,
            lastPosition,
            startDayTimestamp,
            undefined as any,
            3 as GTFSRouteTypeEnum
        );

        expect(estimatedPosition.state_position).to.be.equal(StatePositionEnum.ON_TRACK);
        expect(estimatedPosition.delay).to.be.equal(-21);
        expect(estimatedPosition.shape_dist_traveled).to.be.equal(0.163);
    });

    it("getEstimatedPoint should return not found for gps too far (300m, off_track)", async () => {
        // position 350m far south
        const currentPosition = turf.point([14.4759, 50.1139], {
            id: "1231241",
            origin_time: "10:00:10",
            origin_timestamp: moment("2021-03-17T10:00:10+01:00").toDate(), //1615971610000
        } as ICurrentPositionProperties);
        const lastPosition = null;
        const startTimestamp = new Date("2021-03-17T10:00:00+01:00").getTime();
        const redisObject = JSON.parse(fs.readFileSync(__dirname + "/../data/delay-redisobject-01.json").toString());

        const startDayTimestamp = PositionsManager["getStartDayTimestamp"](
            startTimestamp,
            redisObject.shapes_anchor_points[0].time_scheduled_seconds
        );
        const estimatedPosition = await PositionsManager["getEstimatedPoint"](
            redisObject,
            currentPosition,
            lastPosition,
            startDayTimestamp,
            undefined as any,
            3 as GTFSRouteTypeEnum
        );

        expect(estimatedPosition.state_position).to.be.equal(StatePositionEnum.OFF_TRACK);
    });

    it("getEstimatedPoint should return not found for gps too far (300m, after_track)", async () => {
        // position 350m far south
        const currentPosition = turf.point([14.4759, 50.1139], {
            id: "1231241",
            origin_time: "10:00:10",
            origin_timestamp: moment("2021-03-17T10:00:10+01:00").toDate(), //1615971610000
            scheduled_timestamp: moment("2021-03-17T10:00:10+01:00").toDate(),
        } as ICurrentPositionProperties);
        const lastPosition = null;
        const startTimestamp = new Date("2021-03-17T10:00:00+01:00").getTime();
        const endTimestamp = moment("2021-03-17T10:00:00+01:00").add(5, "seconds").valueOf();
        const redisObject = JSON.parse(fs.readFileSync(__dirname + "/../data/delay-redisobject-01.json").toString());

        const startDayTimestamp = PositionsManager["getStartDayTimestamp"](
            startTimestamp,
            redisObject.shapes_anchor_points[0].time_scheduled_seconds
        );
        const estimatedPosition = await PositionsManager["getEstimatedPoint"](
            redisObject,
            currentPosition,
            lastPosition,
            startDayTimestamp,
            endTimestamp,
            3 as GTFSRouteTypeEnum
        );

        expect(estimatedPosition.state_position).to.be.equal(StatePositionEnum.AFTER_TRACK);
    });

    it("getEstimatedPoint should return first from two possible", async () => {
        // position between two shape anchor points south, time is closer to first anchor
        const currentPosition = turf.point([14.4676, 50.117], {
            id: "1231241",
            origin_time: "10:00:10",
            origin_timestamp: moment("2021-03-17T10:00:10+01:00").toDate(), //1615971610000
        } as ICurrentPositionProperties);
        const lastPosition = null;
        const startTimestamp = new Date("2021-03-17T10:00:00+01:00").getTime();
        const redisObject = JSON.parse(fs.readFileSync(__dirname + "/../data/delay-redisobject-01.json").toString());

        const startDayTimestamp = PositionsManager["getStartDayTimestamp"](
            startTimestamp,
            redisObject.shapes_anchor_points[0].time_scheduled_seconds
        );
        const estimatedPosition = await PositionsManager["getEstimatedPoint"](
            redisObject,
            currentPosition,
            lastPosition,
            startDayTimestamp,
            undefined as any,
            3 as GTFSRouteTypeEnum
        );

        expect(estimatedPosition.state_position).to.be.equal(StatePositionEnum.ON_TRACK);
        expect(estimatedPosition.bearing).to.be.equal(16);
        expect(estimatedPosition.delay).to.be.equal(-156);
        expect(estimatedPosition.shape_dist_traveled).to.be.equal(0.692);
    });

    it("getEstimatedPoint should return second from two possible", async () => {
        // position between two shape anchor points south, time is closer to second anchor
        const currentPosition = turf.point([14.4676, 50.117], {
            id: "1231241",
            origin_time: "10:20:10",
            origin_timestamp: moment("2021-03-17T10:20:10+01:00").toDate(), //1615971610000
        } as ICurrentPositionProperties);
        const lastPosition = null;
        const startTimestamp = new Date("2021-03-17T10:00:00+01:00").getTime();
        const redisObject = JSON.parse(fs.readFileSync(__dirname + "/../data/delay-redisobject-01.json").toString());

        const startDayTimestamp = PositionsManager["getStartDayTimestamp"](
            startTimestamp,
            redisObject.shapes_anchor_points[0].time_scheduled_seconds
        );
        const estimatedPosition = await PositionsManager["getEstimatedPoint"](
            redisObject,
            currentPosition,
            lastPosition,
            startDayTimestamp,
            undefined as any,
            3 as GTFSRouteTypeEnum
        );

        expect(estimatedPosition.state_position).to.be.equal(StatePositionEnum.AT_STOP);
        expect(estimatedPosition.bearing).to.be.equal(266);
        expect(estimatedPosition.delay).to.be.equal(880);
        expect(estimatedPosition.shape_dist_traveled).to.be.equal(1.192);
    });

    it("getEstimatedPoint should return proper delay (70s delayed)", async () => {
        // position on track
        const currentPosition = turf.point([14.4747, 50.1173], {
            id: "1231241",
            origin_time: "10:01:41",
            origin_timestamp: moment("2021-03-17T10:01:41+01:00").toDate(), //1615971610000
        } as ICurrentPositionProperties);
        const lastPosition = null;
        const startTimestamp = new Date("2021-03-17T10:00:00+01:00").getTime();
        const redisObject = JSON.parse(fs.readFileSync(__dirname + "/../data/delay-redisobject-01.json").toString());

        const startDayTimestamp = PositionsManager["getStartDayTimestamp"](
            startTimestamp,
            redisObject.shapes_anchor_points[0].time_scheduled_seconds
        );
        const estimatedPosition = await PositionsManager["getEstimatedPoint"](
            redisObject,
            currentPosition,
            lastPosition,
            startDayTimestamp,
            undefined as any,
            3 as GTFSRouteTypeEnum
        );

        expect(estimatedPosition.state_position).to.be.equal(StatePositionEnum.ON_TRACK);
        expect(estimatedPosition.bearing).to.be.equal(265);
        expect(estimatedPosition.delay).to.be.equal(70);
        expect(estimatedPosition.shape_dist_traveled).to.be.equal(0.163);
    });

    it("getEstimatedPoint should return position with proper at_stop & this_stop attributes", async () => {
        // position on track
        const currentPosition = turf.point([14.4690267, 50.1162556], {
            id: "1231241",
            origin_time: "10:02:41",
            origin_timestamp: moment("2021-03-17T10:02:41+01:00").toDate(), //1615971610000
        } as ICurrentPositionProperties);
        const lastPosition = null;
        const startTimestamp = new Date("2021-03-17T10:00:00+01:00").getTime();
        const redisObject = JSON.parse(fs.readFileSync(__dirname + "/../data/delay-redisobject-01.json").toString());

        const startDayTimestamp = PositionsManager["getStartDayTimestamp"](
            startTimestamp,
            redisObject.shapes_anchor_points[0].time_scheduled_seconds
        );
        const estimatedPosition = await PositionsManager["getEstimatedPoint"](
            redisObject,
            currentPosition,
            lastPosition,
            startDayTimestamp,
            undefined as any,
            3 as GTFSRouteTypeEnum
        );

        expect(estimatedPosition.state_position).to.be.equal(StatePositionEnum.AT_STOP);
        expect(estimatedPosition.bearing).to.be.equal(287);
        expect(estimatedPosition.delay).to.be.equal(11);
        expect(estimatedPosition.shape_dist_traveled).to.be.equal(0.563);
        expect(estimatedPosition.this_stop_id).to.be.equal("U2Z1");
        expect(estimatedPosition.this_stop_sequence).to.be.equal(2);
    });
});
