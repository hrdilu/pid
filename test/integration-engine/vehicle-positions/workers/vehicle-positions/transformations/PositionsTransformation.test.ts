import { PositionsTransformation } from "#ie/vehicle-positions/workers/vehicle-positions/transformations/PositionsTransformation";
import moment from "@golemio/core/dist/shared/moment-timezone";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import sinon from "sinon";

chai.use(chaiAsPromised);

describe("PositionsTransformation", () => {
    let transformation: PositionsTransformation;
    let testSourceData: Record<string, any>;
    let testSourceDataTimestamps: any;
    let sandbox: any;
    let clock: any;

    before(() => {
        sandbox = sinon.createSandbox();
        clock = sinon.useFakeTimers({
            now: new Date(2021, 3, 17, 14, 0),
            shouldAdvanceTime: true,
        });
        transformation = new PositionsTransformation();
        testSourceDataTimestamps = JSON.parse(
            fs.readFileSync(__dirname + "/../data/vehiclepositions-input-timestamps.json").toString("utf8")
        );
        testSourceData = JSON.parse(fs.readFileSync(__dirname + "/../data/vehiclepositions-input.json").toString("utf8"));
    });

    after(() => {
        sandbox.restore();
        clock.restore();
    });

    it("should has name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("VehiclePositions");
    });

    it("should has transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform element (negative bearing)", async () => {
        const data = await transformation.transform(testSourceData.m.spoj[0]);
        expect(data).to.have.property("positions");
        expect(data.positions[0]).to.have.property("lat", 50.16252);
        expect(data.positions[0]).to.have.property("lng", 14.52483);
        expect(data.positions[0]).to.have.property("bearing", -10 + 256);
        expect(data).to.have.property("stops");
        expect(data.stops.length).to.equal(28);
        expect(data).to.have.property("trips");
        expect(data.trips[0]).to.have.property("cis_line_id", "100110");
    });

    it("should properly transform element (positive bearing)", async () => {
        const data = await transformation.transform(testSourceData.m.spoj[1]);
        expect(data).to.have.property("positions");
        expect(data.positions[0]).to.have.property("lat", 50.1053);
        expect(data.positions[0]).to.have.property("lng", 14.54619);
        expect(data.positions[0]).to.have.property("bearing", 114);
        expect(data).to.have.property("stops");
        expect(data.stops.length).to.equal(24);
        expect(data).to.have.property("trips");
        expect(data.trips[0]).to.have.property("cis_line_id", "100110");
    });

    it("should properly deduce timestamps (normal situation))", async () => {
        const clock = sinon.useFakeTimers({
            now: moment("2021-03-16T11:58:59+01:00").toDate(),
            shouldAdvanceTime: true,
        });
        const data = await transformation["deduceTimestamps"](
            testSourceDataTimestamps.m.spoj[0].zast[0],
            testSourceDataTimestamps.m.spoj[0].$.cpoz
        );
        expect(moment("2021-03-16T11:15:00+01:00").isSame(data.startTimestamp)).to.equal(true);
        expect(moment("2021-03-16T11:58:57+01:00").isSame(data.positionTimestamp)).to.equal(true);

        clock.restore();
    });

    it("should properly deduce timestamps (23.45 starts, 23.59 sends position, 0.00 is now))", async () => {
        const clock = sinon.useFakeTimers({
            now: moment("2021-03-17T00:00:10+01:00").toDate(),
            shouldAdvanceTime: true,
        });
        const data = await transformation["deduceTimestamps"](
            testSourceDataTimestamps.m.spoj[1].zast[0],
            testSourceDataTimestamps.m.spoj[1].$.cpoz
        );
        expect(moment("2021-03-16T23:45:00+01:00").isSame(data.startTimestamp)).to.equal(true);
        expect(moment("2021-03-16T23:59:50+01:00").isSame(data.positionTimestamp)).to.equal(true);

        clock.restore();
    });

    it("should properly deduce timestamps (23.45 starts, 0.00 sends position, 0.00 is now))", async () => {
        const clock = sinon.useFakeTimers({
            now: moment("2021-03-17T00:00:15+01:00").toDate(),
            shouldAdvanceTime: true,
        });
        const data = await transformation["deduceTimestamps"](
            testSourceDataTimestamps.m.spoj[2].zast[0],
            testSourceDataTimestamps.m.spoj[2].$.cpoz
        );
        expect(moment("2021-03-16T23:45:00+01:00").isSame(data.startTimestamp)).to.equal(true);
        expect(moment("2021-03-17T00:00:10+01:00").isSame(data.positionTimestamp)).to.equal(true);

        clock.restore();
    });

    it("should properly deduce timestamps (23.45+1 starts, 3.10+2 sends position, 1.10Z is now)", async () => {
        const clock = sinon.useFakeTimers({
            now: moment("2021-03-28T01:10:15+00:00").toDate(),
            shouldAdvanceTime: true,
        });
        const data = await transformation["deduceTimestamps"](
            testSourceDataTimestamps.m.spoj[3].zast[0],
            testSourceDataTimestamps.m.spoj[3].$.cpoz
        );
        expect(moment("2021-03-27T23:45:00+01:00").isSame(data.startTimestamp)).to.equal(true);
        expect(moment("2021-03-28T03:10:10+02:00").isSame(data.positionTimestamp)).to.equal(true);

        clock.restore();
    });

    it("should properly deduce timestamps (1.00+1 starts, 3.10+2 sends position, 1.10Z is now)", async () => {
        const clock = sinon.useFakeTimers({
            now: moment("2021-03-28T01:10:15+00:00").toDate(),
            shouldAdvanceTime: true,
        });
        const data = await transformation["deduceTimestamps"](
            testSourceDataTimestamps.m.spoj[4].zast[0],
            testSourceDataTimestamps.m.spoj[4].$.cpoz
        );
        expect(moment("2021-03-28T01:00:00+01:00").isSame(data.startTimestamp)).to.equal(true);
        expect(moment("2021-03-28T03:10:10+02:00").isSame(data.positionTimestamp)).to.equal(true);

        clock.restore();
    });

    it("should properly deduce timestamps (3.10+2 starts, 3.30+2 sends position, 1.10Z is now)", async () => {
        const clock = sinon.useFakeTimers({
            now: moment("2021-03-28T01:30:15+00:00").toDate(),
            shouldAdvanceTime: true,
        });
        const data = await transformation["deduceTimestamps"](
            testSourceDataTimestamps.m.spoj[5].zast[0],
            testSourceDataTimestamps.m.spoj[5].$.cpoz
        );
        expect(moment("2021-03-28T03:10:00+02:00").isSame(data.startTimestamp)).to.equal(true);
        expect(moment("2021-03-28T03:30:10+02:00").isSame(data.positionTimestamp)).to.equal(true);

        clock.restore();
    });

    it("should properly transform element (with old zpoz_typ attribute)", async () => {
        const data = await transformation.transform(testSourceData.m.spoj[0]);
        expect(data).to.have.property("stops");
        expect(data.stops[0]).to.have.property("delay_type", 3);
        expect(data.stops[16]).to.have.property("delay_type", 2);
    });

    it("should properly transform element (with new zpoz_typ_odj attribute)", async () => {
        const testSourceDataNew = JSON.parse(
            fs.readFileSync(__dirname + "/../data/vehiclepositions-input-zpoz_typ_odj.json").toString("utf8")
        );
        const data = await transformation.transform(testSourceDataNew.m.spoj[0]);
        expect(data).to.have.property("stops");
        expect(data.stops[0]).to.have.property("delay_type", 3);
        expect(data.stops[16]).to.have.property("delay_type", 2);
    });

    it("should exclude DP PRAHA tram while transformation", async () => {
        const testSourceDataRN = JSON.parse(
            fs.readFileSync(__dirname + "/../data/vehiclepositions-input-registrationnumber.json").toString("utf8")
        );

        const data = await transformation.transform(testSourceDataRN.m.spoj[1]);
        expect(data).to.eql({ positions: [], stops: [], trips: [] });
    });

    it("should set full atributes for registration number for Jaroslav Štěpánek vehicles)", async () => {
        const testSourceDataRN = JSON.parse(
            fs.readFileSync(__dirname + "/../data/vehiclepositions-input-registrationnumber.json").toString("utf8")
        );

        const data = await transformation.transform(testSourceDataRN.m.spoj[2]);
        expect(data.trips[0]).to.have.property("agency_name_real", "Jaroslav Štěpánek");
        expect(data.trips[0]).to.have.property("vehicle_registration_number", 1030);
        expect(data.positions[0]).to.have.property("bearing", 246);
        expect(data.positions[0]).to.have.property("speed", 0);
    });

    it('should return empty arrays for cpoz (origin time) and falsy or "false" zrus (cancellation) attributes', async () => {
        const testSourceDataRN = JSON.parse(
            fs.readFileSync(__dirname + "/../data/vehiclepositions-input-invalidattributes.json").toString("utf8")
        );

        const data = await transformation.transform(testSourceDataRN.m.spoj);

        expect(data.positions).to.have.length(0);
        expect(data.stops).to.have.length(0);
        expect(data.trips).to.have.length(0);
    });

    it("should (not) calculate origin time", async () => {
        const testSourceDataRN = JSON.parse(
            fs.readFileSync(__dirname + "/../data/vehiclepositions-input-origintime.json").toString("utf8")
        );

        const data = await transformation.transform(testSourceDataRN.m.spoj);

        // Should calculate origin time (zrus "true", cpoz null)
        expect(data.positions[0]).to.have.property(
            "origin_time",
            moment.tz("Europe/Prague").format(PositionsTransformation["ORIGIN_TIME_FORMAT"])
        );

        // Should NOT calculate origin time (zrus "true", cpoz is defined)
        expect(data.positions[1]).to.have.property("origin_time", testSourceDataRN.m.spoj[1].$.cpoz);
    });

    it("should filter out one future cpoz from two trips", async () => {
        const testSourceDataRN = JSON.parse(
            fs.readFileSync(__dirname + "/../data/vehiclepositions-input-futuremessage.json").toString("utf8")
        );

        const data = await transformation.transform(testSourceDataRN.m.spoj);

        expect(data.positions).to.have.length(1);
    });
});
