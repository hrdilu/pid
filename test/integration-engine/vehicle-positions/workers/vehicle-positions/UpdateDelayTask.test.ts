import PositionsManager from "#ie/vehicle-positions/workers/vehicle-positions/helpers/PositionsManager";
import { UpdateDelayTask } from "#ie/vehicle-positions/workers/vehicle-positions/tasks/UpdateDelayTask";
import { PostgresConnector, RedisConnector } from "@golemio/core/dist/integration-engine/connectors";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonFakeTimers, SinonSandbox, SinonStub } from "sinon";

chai.use(chaiAsPromised);

describe("VPWorker - UpdateDelayTask", () => {
    let sandbox: SinonSandbox;
    let clock: SinonFakeTimers;
    let task: UpdateDelayTask;

    beforeEach(async () => {
        sandbox = sinon.createSandbox();
        clock = sinon.useFakeTimers({
            now: new Date(2021, 12, 9, 10, 0),
            shouldAdvanceTime: true,
        });

        await PostgresConnector.connect();
        await RedisConnector.connect();

        sandbox.stub(QueueManager, "sendMessageToExchange").resolves();

        task = new UpdateDelayTask("test.test");
    });

    afterEach(() => {
        sandbox?.restore();
        clock?.restore();
    });

    it("should call the correct methods by updateDelay method", async () => {
        sandbox
            .stub(task["positionsRepository"], "getPositionsForUpdateDelay")
            .callsFake(() => [{ gtfs_trip_id: "0000", positions: [{ delay: null }] }] as any);
        sandbox.stub(task["delayComputationRedisRepository"], "get").callsFake(() => Object.assign({ shape_points: [] }));
        sandbox
            .stub(PositionsManager, "computePositions")
            .callsFake(() => ({ context: { tripId: "TRIP1" }, positions: [{ delay: 10, id: "12321" }] } as any));
        sandbox.stub(task["positionsRepository"], "bulkUpdate");
        sandbox.stub(task["tripsRepository"], "bulkUpdate");

        await task["execute"]({
            positions: [],
            updatedTrips: ["TRIP_ID"],
        } as any);

        sandbox.assert.calledOnce(task["positionsRepository"].getPositionsForUpdateDelay as SinonStub);
        sandbox.assert.calledOnce(task["delayComputationRedisRepository"].get as SinonStub);
        sandbox.assert.calledOnce(PositionsManager["computePositions"] as SinonStub);
        sandbox.assert.calledOnce(task["positionsRepository"].bulkUpdate as SinonStub);
        sandbox.assert.calledOnce(task["tripsRepository"].bulkUpdate as SinonStub);
        sandbox.assert.calledOnce(QueueManager.sendMessageToExchange as SinonStub);
    });

    it("should assign the correct values to the 'valid_to' property", async () => {
        const input = {
            updatedTrips: [
                {
                    cis_line_id: "",
                    cis_trip_number: "",
                    run_number: 51,
                    cis_line_short_name: "",
                    created_at: "2022-03-17 01:50:30",
                    gtfs_route_id: "L91",
                    gtfs_route_short_name: 91,
                    gtfs_trip_id: "91_158_211029",
                    id: "2022-03-17T02:51:00+01:00_91_158_211029_8338",
                    updated_at: "2022-03-17 03:54:33",
                    start_cis_stop_id: "",
                    start_cis_stop_platform_code: "",
                    start_time: "02:51:00",
                    start_timestamp: new Date(1647481860000),
                    vehicle_type_id: 6,
                    wheelchair_accessible: false,
                    agency_name_scheduled: "DP PRAHA",
                    origin_route_name: 91,
                    agency_name_real: "DP PRAHA",
                    vehicle_registration_number: 8338,
                    gtfs_trip_headsign: "Staré Strašnice",
                    start_asw_stop_id: "",
                    gtfs_route_type: 0,
                    gtfs_block_id: "",
                    last_position_id: 317041616,
                    is_canceled: "",
                    end_timestamp: new Date(1647485640000),
                    gtfs_trip_short_name: "",
                },
            ],
            positions: [],
        };

        const updateComputedPositionsAndTripsStub = sandbox.stub(task, "updateComputedPositionsAndTrips" as any);

        await task.validateAndExecute({ content: Buffer.from(JSON.stringify(input)) } as any);

        expect(updateComputedPositionsAndTripsStub.callCount).to.be.equal(1);

        const processedPositions = updateComputedPositionsAndTripsStub.getCall(0).args[0] as any[];
        for (let position of processedPositions[0].positions) {
            switch (position.id) {
                case "317031566":
                    expect(position.valid_to).to.deep.equal(new Date(1647484111000));
                    break;
                case "317031710":
                    expect(position.valid_to).to.deep.equal(new Date(1647484450000));
                    break;
                case "317041616":
                    expect(position.valid_to).to.deep.equal(new Date(1647485932000));
                    break;
            }
        }
    });
});
