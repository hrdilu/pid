import { SaveDataToDBTask } from "#ie/vehicle-positions/workers/vehicle-positions/tasks/SaveDataToDBTask";
import { PostgresConnector, RedisConnector } from "@golemio/core/dist/integration-engine/connectors";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import chai from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import sinon, { SinonFakeTimers, SinonSandbox, SinonStub } from "sinon";

chai.use(chaiAsPromised);

describe("VPWorker - SaveDataToDBTask", () => {
    let sandbox: SinonSandbox;
    let sequelizeModelStub: Record<string, SinonStub>;
    let clock: SinonFakeTimers;
    let task: SaveDataToDBTask;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        clock = sinon.useFakeTimers({
            now: new Date(2021, 2, 17, 10, 0),
            shouldAdvanceTime: true,
        });

        sequelizeModelStub = Object.assign({
            belongsTo: sandbox.stub(),
            hasMany: sandbox.stub(),
            hasOne: sandbox.stub(),
            removeAttribute: sandbox.stub(),
            upsert: sandbox.stub().resolves([{ id: 42 }, true]),
            findAll: sandbox.stub().resolves([]),
        });

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().callsFake(() => sequelizeModelStub),
                query: sandbox.stub().callsFake(() => [{ exists: true }]),
                transaction: sandbox.stub().callsFake(() => Object.assign({ commit: sandbox.stub() })),
            })
        );
        sandbox.stub(RedisConnector, "getConnection");

        sandbox.stub(QueueManager, "sendMessageToExchange").resolves();

        task = new SaveDataToDBTask("test.test");

        sandbox
            .stub(task["positionsTransformation"], "transform")
            .callsFake(() => Object.assign({ positions: [], stops: [], trips: [] }));

        sandbox.stub(task["tripsRepository"], "bulkUpsert").resolves({
            inserted: [
                {
                    cis_short_name: "999",
                    id: "999",
                    start_cis_stop_id: "999",
                    start_cis_stop_platform_code: "a",
                    start_timestamp: null,
                },
            ],
            updated: [],
        } as any);
    });

    afterEach(() => {
        sandbox?.restore();
        clock?.restore();
    });

    it("should call the correct methods", async () => {
        await task.validateAndExecute({
            content: fs.readFileSync(__dirname + "/./data/vehiclepositions-input.json"),
        } as any);

        sandbox.assert.calledOnce(task["positionsTransformation"].transform as SinonStub);
        sandbox.assert.calledOnce(task["tripsRepository"].bulkUpsert as SinonStub);
        sandbox.assert.calledWith(task["tripsRepository"].bulkUpsert as SinonStub, []);
        sandbox.assert.calledOnce(QueueManager.sendMessageToExchange as SinonStub);
        sandbox.assert.callOrder(
            task["positionsTransformation"].transform as SinonStub,
            task["tripsRepository"].bulkUpsert as SinonStub,
            QueueManager.sendMessageToExchange as SinonStub
        );
        sandbox.assert.callCount(PostgresConnector.getConnection as SinonStub, 5);
    });
});
