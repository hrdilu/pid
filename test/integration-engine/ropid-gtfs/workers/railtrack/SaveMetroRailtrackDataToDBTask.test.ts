import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { IMetroRailtrackDataInput } from "#ie/ropid-gtfs/workers/railtrack/tasks/interfaces/MetroRailtrackDataInterfaces";
import { SaveMetroRailtrackDataToDBTask } from "#ie/ropid-gtfs/workers/railtrack/tasks/SaveMetroRailtrackDataToDBTask";

chai.use(chaiAsPromised);

describe("RailtrackWorker - SaveMetroRailtrackDataToDBTask", () => {
    let sandbox: SinonSandbox;
    let task: SaveMetroRailtrackDataToDBTask;

    const testDataValid: IMetroRailtrackDataInput = {
        data: [
            {
                track_section: "2107",
                line: "A",
                gps_lat: "50.075360",
                gps_lon: "14.341560",
                stop_id: "U306Z101P",
            },
        ],
    };

    const testDataInvalid: IMetroRailtrackDataInput<"stop_id"> = {
        data: [
            {
                track_section: "2107",
                line: "AA",
                gps_lat: "50.075360",
                gps_lon: "14.341560x",
            },
        ],
    };

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() => Object.assign({ define: sandbox.stub() }));

        task = new SaveMetroRailtrackDataToDBTask("test.test");
    });

    afterEach(() => {
        sandbox?.restore();
    });

    it("should validate and execute", async () => {
        const saveDataStub = sinon.stub(task["railtrackGPSRepository"], "saveData").resolves();
        const promise = task.validateAndExecute({ content: Buffer.from(JSON.stringify(testDataValid)) } as any);

        await expect(promise).to.be.fulfilled;
        expect(saveDataStub.callCount).to.equal(1);
    });

    it("should not validate (invalid line and gps_lon, missing stop_id)", async () => {
        const saveDataStub = sinon.stub(task["railtrackGPSRepository"], "saveData").resolves();
        const promise = task.validateAndExecute({ content: Buffer.from(JSON.stringify(testDataInvalid)) } as any);

        await expect(promise).to.be.rejectedWith(
            GeneralError,
            // eslint-disable-next-line max-len
            "[Queue test.test.saveMetroRailtrackDataToDB] Message validation failed: [line's byte length must fall into (1, 1) range, gps_lon must be a longitude string or number, stop_id must be a string]"
        );

        expect(saveDataStub.callCount).to.equal(0);
    });
});
