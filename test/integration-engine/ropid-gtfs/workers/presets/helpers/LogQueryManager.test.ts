import { LogQueryManager } from "#ie/ropid-gtfs/workers/presets/helpers/LogQueryManager";
import { expect } from "chai";

describe("LogQueryManager", () => {
    describe("getCompletedPresetRequestLogsQuery", () => {
        it("should return query", () => {
            const query = LogQueryManager.getCompletedPresetRequestLogsQuery();
            expect(query).to.equal(
                // eslint-disable-next-line max-len
                '{container_name="url-proxy"} | json | req_url =~ `\\/pid\\/[^\\/]*\\/?` | req_method = `GET` | message = `request completed`'
            );
        });
    });
});
