import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { PresetLogTransformation } from "#ie/ropid-gtfs/workers/presets/transformations/PresetLogTransformation";
import { container } from "@golemio/core/dist/shared/tsyringe";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";
import { InputLogsFixture } from "../fixtures/InputLogsFixture";

describe("PresetLogTransformation", () => {
    let sandbox: SinonSandbox;

    const createContainer = () =>
        container
            .createChildContainer()
            .register(ModuleContainerToken.PresetLogTransformation, PresetLogTransformation)
            .resolve<PresetLogTransformation>(ModuleContainerToken.PresetLogTransformation);

    before(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        container.clearInstances();
        sinon.reset();
        sandbox.restore();
    });

    after(() => {
        sinon.restore();
    });

    it("should transform input data", async () => {
        const transformation = createContainer();
        const result = await transformation.transform(InputLogsFixture as any);
        expect(result).to.deep.equal([
            {
                received_at: new Date("2023-06-27T14:39:58.696Z"),
                device_alias: "test-povstani-fhana",
                is_processed: false,
                request_url: "/pid/test-povstani-fhana?fhana",
                request_method: "GET",
                request_user_agent: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/114.0",
                response_status: 200,
                response_time_ms: 41,
            },
            {
                received_at: new Date("2023-06-27T14:39:55.160Z"),
                device_alias: "test-povstani",
                is_processed: false,
                request_url: "/pid/test-povstani",
                request_method: "GET",
                request_user_agent: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/114.0",
                response_status: 200,
                response_time_ms: 530,
            },
            {
                received_at: new Date("2023-06-27T14:40:06.731Z"),
                device_alias: "bustec-testovaci",
                is_processed: false,
                request_url: "/pid/bustec-testovaci/",
                request_method: "GET",
                request_user_agent: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/114.0",
                response_status: 200,
                response_time_ms: 44,
            },
            {
                received_at: new Date("2023-06-27T14:40:04.396Z"),
                device_alias: "bustec-testovaci",
                is_processed: false,
                request_url: "/pid/bustec-testovaci",
                request_method: "GET",
                request_user_agent: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/114.0",
                response_status: 200,
                response_time_ms: 1614,
            },
        ]);
    });
});
