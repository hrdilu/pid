import { PidContainer } from "#ie/ioc/Di";
import { ProcessAndSendLogsTask } from "#ie/ropid-gtfs/workers/presets/tasks/ProcessAndSendLogsTask";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { QueueManager } from "@golemio/core/dist/integration-engine";
import IPostgresConnector from "@golemio/core/dist/integration-engine/connectors/interfaces/IPostgresConnector";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { expect } from "chai";
import sinon, { SinonStub } from "sinon";

describe("RopidPresetWorker - ProcessAndSendLogsTask", () => {
    let task: ProcessAndSendLogsTask;
    let queueStub: SinonStub;

    before(async () => {
        const postgresConnector = PidContainer.resolve<IPostgresConnector>(CoreToken.PostgresConnector);
        await postgresConnector.connect();
    });

    beforeEach(() => {
        queueStub = sinon.stub(QueueManager, "sendMessageToExchange");
        task = new ProcessAndSendLogsTask("test.test");
    });

    afterEach(async () => {
        sinon.restore();
    });

    it("should process and send logs", async () => {
        const logRepositoryStub = sinon.stub(task["logRepository"], "findUnprocessed").resolves([
            {
                toJSON: () => ({
                    device_alias: "test-povstani-fhana",
                    received_at: new Date("2023-06-27T14:39:58.555Z"),
                }),
            },
        ] as any);
        const processStub = sinon.stub(task, "processBatch" as any).resolves();

        await task["execute"]();
        expect(logRepositoryStub.calledOnce).to.be.true;
        expect(processStub.calledOnce).to.be.true;
    });

    it("should process and send logs in batches", async () => {
        const logRepositoryStub = sinon.stub(task["logRepository"], "findUnprocessed").resolves([
            {
                toJSON: () => ({
                    device_alias: "test-povstani-fhana",
                    received_at: new Date("2023-06-27T14:39:58.555Z"),
                }),
            },
            {
                toJSON: () => ({
                    device_alias: "test-povstani-fhana",
                    received_at: new Date("2023-06-27T14:39:58.555Z"),
                }),
            },
            {
                toJSON: () => ({
                    device_alias: "test-povstani-fhana",
                    received_at: new Date("2023-06-27T14:39:58.555Z"),
                }),
            },
        ] as any);
        const processStub = sinon.stub(task, "processBatch" as any).resolves();

        task["batchSize"] = 2;
        await task["execute"]();
        expect(logRepositoryStub.calledOnce).to.be.true;
        expect(processStub.calledTwice).to.be.true;
    });

    it("should not process and send logs if there are no logs", async () => {
        const logRepositoryStub = sinon.stub(task["logRepository"], "findUnprocessed").resolves([]);
        const processStub = sinon.stub(task, "processBatch" as any).resolves();

        await task["execute"]();
        expect(logRepositoryStub.calledOnce).to.be.true;
        expect(processStub.called).to.be.false;
    });

    describe("processBatch", () => {
        it("should process and send logs", async () => {
            const logRepositoryStub = sinon.stub(task["logRepository"], "markAsProcessed").resolves();
            const monitoringServiceStub = sinon.stub(task["monitoringService"], "sendLogsToMonitoringSystem").resolves();

            await task["processBatch"]([
                {
                    toJSON: () => ({
                        device_alias: "test-povstani-fhana",
                        received_at: new Date("2023-06-27T14:39:58.555Z"),
                    }),
                    get: () => "111",
                },
            ] as any);
            expect(logRepositoryStub.calledOnce).to.be.true;
            expect(logRepositoryStub.calledWith(["111"])).to.be.true;

            expect(monitoringServiceStub.calledOnce).to.be.true;
            expect(
                monitoringServiceStub.calledWith([{ device_alias: "test-povstani-fhana", received_at: "2023-06-27T14:39:58Z" }])
            ).to.be.true;
        });

        it("should mark logs as processed even if sending to the monitoring system fails", async () => {
            const logRepositoryStub = sinon.stub(task["logRepository"], "markAsProcessed").resolves();
            const monitoringServiceStub = sinon
                .stub(task["monitoringService"], "sendLogsToMonitoringSystem")
                .rejects(new GeneralError("test"));

            await expect(
                task["processBatch"]([
                    {
                        toJSON: () => ({
                            device_alias: "test-povstani-fhana",
                            received_at: new Date("2023-06-27T14:39:58.555Z"),
                        }),
                        get: () => "111",
                    },
                ] as any)
            ).to.be.rejectedWith("test");
            expect(logRepositoryStub.calledOnce).to.be.true;
            expect(logRepositoryStub.calledWith(["111"])).to.be.true;

            expect(monitoringServiceStub.calledOnce).to.be.true;
            expect(
                monitoringServiceStub.calledWith([{ device_alias: "test-povstani-fhana", received_at: "2023-06-27T14:39:58Z" }])
            ).to.be.true;
        });
    });
});
