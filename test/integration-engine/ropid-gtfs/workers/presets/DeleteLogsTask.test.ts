import { PidContainer } from "#ie/ioc/Di";
import { DeleteLogsTask } from "#ie/ropid-gtfs/workers/presets/tasks/DeleteLogsTask";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import IPostgresConnector from "@golemio/core/dist/integration-engine/connectors/interfaces/IPostgresConnector";
import { expect } from "chai";

describe("RopidPresetWorker - DeleteLogsTask", () => {
    let task: DeleteLogsTask;

    before(async () => {
        const postgresConnector = PidContainer.resolve<IPostgresConnector>(CoreToken.PostgresConnector);
        await postgresConnector.connect();
    });

    beforeEach(() => {
        task = new DeleteLogsTask("test.test");
    });

    it("should delete old logs", async () => {
        await task["execute"]({ targetHours: 23 });
        const result = await task["logRepository"]["sequelizeModel"].findAll({
            where: {
                device_alias: "test-povstani-del",
            },
        });

        expect(result).to.have.length(0);
    });
});
