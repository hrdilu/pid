import { DeparturesRepository } from "#ie/ropid-gtfs/data-access/precomputed/DeparturesRepository";
import { SourceTableSuffixEnum } from "#ie/ropid-gtfs/helpers/SourceTableSuffixEnum";
import { DeparturesModel } from "#sch/ropid-gtfs/models/precomputed";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { config } from "@golemio/core/dist/integration-engine/config";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("DeparturesRepository", () => {
    let sandbox: SinonSandbox;
    let repository: DeparturesRepository;

    before(async () => {
        sandbox = sinon.createSandbox();
        await PostgresConnector.connect();
        repository = new DeparturesRepository();
    });

    afterEach(() => {
        sandbox?.restore();
    });

    it("should return data (trip_id 91_158_211029)", async () => {
        const result = await repository["sequelizeModel"].findAll<DeparturesModel>({
            where: { trip_id: "91_158_211029" },
            raw: true,
        });

        for (const departure of result) {
            expect(departure.trip_headsign).to.equal("Staré Strašnice");
        }
    });

    it("createAndPopulate should generate and execute query", async () => {
        const queryStub = sandbox.stub(repository["sequelizeModel"].sequelize, "query" as never);
        sandbox.stub(config, "NODE_ENV").value("development");

        await repository.createAndPopulate(SourceTableSuffixEnum.Tmp);
        expect(queryStub.getCall(0).args[0]).to.equal(
            /* eslint-disable max-len */
            `
                SET LOCAL search_path TO pid;
                DROP TABLE IF EXISTS ropidgtfs_precomputed_departures_tmp;
                CREATE TABLE ropidgtfs_precomputed_departures_tmp (LIKE ropidgtfs_precomputed_departures including defaults);
                INSERT INTO ropidgtfs_precomputed_departures_tmp \n            SELECT
                t.stop_sequence,
                t.stop_headsign,
                t.pickup_type,
                t.drop_off_type,
                t.arrival_time,
                gtfs_timestamp(t.arrival_time, t4.date) AS arrival_datetime,
                t.departure_time,
                gtfs_timestamp(t.departure_time, t4.date) AS departure_datetime,
                t0.stop_id,
                t0.stop_name,
                t0.platform_code,
                t0.wheelchair_boarding,
                t1.min_stop_sequence,
                t1.max_stop_sequence,
                t2.trip_id,
                t2.trip_headsign,
                t2.trip_short_name,
                t2.wheelchair_accessible,
                t3.service_id,
                t4.date,
                t5.route_short_name,
                coalesce(t5.route_type, :defaultRouteType) AS route_type,
                t5.route_id,
                t5.is_night,
                t5.is_regional,
                t5.is_substitute_transport,
                t6.stop_sequence AS next_stop_sequence,
                t6.stop_id AS next_stop_id,
                t7.stop_sequence AS last_stop_sequence,
                t7.stop_id AS last_stop_id
            FROM ropidgtfs_stop_times_tmp t
                LEFT JOIN ropidgtfs_stops_tmp t0 ON t.stop_id = t0.stop_id
                LEFT JOIN ropidgtfs_trips_tmp t2 ON t.trip_id = t2.trip_id
                INNER JOIN ropidgtfs_precomputed_services_calendar_tmp t4 ON t2.service_id = t4.service_id
                INNER JOIN ropidgtfs_precomputed_minmax_stop_sequences_tmp t1 ON t.trip_id = t1.trip_id
                LEFT JOIN ropidgtfs_calendar_tmp t3 ON t2.service_id = t3.service_id
                LEFT JOIN ropidgtfs_routes_tmp t5 ON t2.route_id = t5.route_id
                LEFT JOIN ropidgtfs_stop_times_tmp t6 ON t.trip_id = t6.trip_id AND t6.stop_sequence = t.stop_sequence + 1
                LEFT JOIN ropidgtfs_stop_times_tmp t7 ON t.trip_id = t7.trip_id AND t7.stop_sequence = t.stop_sequence - 1
            WHERE gtfs_timestamp(t.arrival_time, t4.date) > now() - interval '6 hours';`
            /* eslint-enable max-len */
        );
    });

    it("replaceByTmp should generate and execute query", async () => {
        const queryStub = sandbox.stub(repository["sequelizeModel"].sequelize, "query" as never);

        await repository.replaceByTmp("test" as any);
        expect(queryStub.getCall(0).args[0]).to.equal(
            /* eslint-disable max-len */
            `
                SET LOCAL search_path TO pid;
                LOCK ropidgtfs_precomputed_departures IN EXCLUSIVE MODE;
                ALTER TABLE ropidgtfs_precomputed_departures RENAME TO ropidgtfs_precomputed_departures_drop;
                ALTER TABLE ropidgtfs_precomputed_departures_tmp RENAME TO ropidgtfs_precomputed_departures;
                DROP TABLE ropidgtfs_precomputed_departures_drop CASCADE;
                ALTER INDEX IF EXISTS "pid".ropidgtfs_precomputed_departures_stop_id_idx_tmp RENAME TO ropidgtfs_precomputed_departures_stop_id_idx; ALTER INDEX IF EXISTS "pid".ropidgtfs_precomputed_departures_unique_idx_tmp RENAME TO ropidgtfs_precomputed_departures_unique_idx;`
            /* eslint-enable max-len */
        );

        expect(queryStub.getCall(0).args[1]).to.deep.equal({ transaction: "test" });
    });

    it("createIndexes should generate and execute query", async () => {
        const queryStub = sandbox.stub(repository["sequelizeModel"].sequelize, "query" as never);

        await repository.createIndexes();
        expect(queryStub.getCall(0).args[0]).to.equal(
            // eslint-disable-next-line max-len
            'CREATE  INDEX IF NOT EXISTS ropidgtfs_precomputed_departures_stop_id_idx_tmp\n                ON "pid".ropidgtfs_precomputed_departures_tmp USING BTREE (stop_id); CREATE UNIQUE INDEX IF NOT EXISTS ropidgtfs_precomputed_departures_unique_idx_tmp\n                ON "pid".ropidgtfs_precomputed_departures_tmp USING BTREE (date,trip_id,stop_sequence);'
        );
    });
});
