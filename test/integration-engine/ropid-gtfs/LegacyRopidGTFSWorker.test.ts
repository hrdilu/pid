import { PidContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import {
    DatasetEnum,
    IDeparturesPresetsData,
    IRopidGtfsTransformationData,
    LegacyRopidGTFSWorker,
    RopidGTFSMetadataModel,
} from "#ie/ropid-gtfs";
import { StaticFileRedisRepository } from "#ie/ropid-gtfs/data-access/cache/StaticFileRedisRepository";
import { DeparturePresetsFacade } from "#ie/ropid-gtfs/facade/DeparturePresetsFacade";
import { DataCacheManager } from "#ie/ropid-gtfs/helpers/DataCacheManager";
import { RopidGtfsContainer } from "#ie/ropid-gtfs/ioc/Di";
import { RopidGtfsContainerToken } from "#ie/ropid-gtfs/ioc/RopidGtfsContainerToken";
import { CheckForNewDataTask } from "#ie/ropid-gtfs/workers/timetables/tasks/CheckForNewDataTask";
import { CheckSavedRowsAndReplaceTablesTask } from "#ie/ropid-gtfs/workers/timetables/tasks/CheckSavedRowsAndReplaceTablesTask";
import { DownloadDatasetsTask } from "#ie/ropid-gtfs/workers/timetables/tasks/DownloadDatasetsTask";
import { TransformAndSaveDataTask } from "#ie/ropid-gtfs/workers/timetables/tasks/TransformAndSaveDataTask";
import { PrecomputedTablesFacade } from "#ie/ropid-gtfs/workers/timetables/tasks/helpers/PrecomputedTablesFacade";
import { RopidGtfsFactory } from "#ie/ropid-gtfs/workers/timetables/tasks/helpers/RopidGtfsFactory";
import { ICheckForNewDataInput } from "#ie/ropid-gtfs/workers/timetables/tasks/interfaces/ICheckForNewDataInput";
import { IDatasetsInput } from "#ie/ropid-gtfs/workers/timetables/tasks/interfaces/IDatasetsInput";
import { DatasetsInputSchema } from "#ie/ropid-gtfs/workers/timetables/tasks/schema/DownloadDataInputSchema";
import { IRopidDeparturesPresetsOutput } from "#sch/ropid-departures-presets";
import { QueueManager } from "@golemio/core/dist/integration-engine";
import { config } from "@golemio/core/dist/integration-engine/config";
import { PostgresConnector, RedisConnector } from "@golemio/core/dist/integration-engine/connectors";
import { log } from "@golemio/core/dist/integration-engine/helpers/Logger";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import sinon, { SinonSandbox, SinonSpy, SinonStub } from "sinon";
import { Readable } from "stream";

chai.use(chaiAsPromised);

describe("LegacyRopidGTFSlegacyWorker", () => {
    let legacyWorker: LegacyRopidGTFSWorker;
    let sandbox: SinonSandbox;
    let testGtfsData: any[];
    let agencyData: Record<string, any>;
    let testTransformedData: IRopidGtfsTransformationData;
    let testDataCis: Record<string, any>;
    let testDataRunNumbers: Record<string, any>;
    let testDataOis: Record<string, any>;
    let testDataDeparturesPresets: IDeparturesPresetsData;
    let testTransformedDeparturesPresets: IRopidDeparturesPresetsOutput[];
    let modelTruncateStub: SinonStub;
    let modelSaveStub: SinonStub;
    let modelSyncStub: SinonStub;
    let staticFileRedisRepository: StaticFileRedisRepository;
    let dataCacheManager: DataCacheManager;
    let ropidGtfsFactory: RopidGtfsFactory;
    let transformAndSaveTask: TransformAndSaveDataTask;
    let precomputeFacade: PrecomputedTablesFacade;
    let metaModel: RopidGTFSMetadataModel;

    beforeEach(async () => {
        sandbox = sinon.createSandbox({ useFakeTimers: true });
        await RedisConnector.connect();
        await PostgresConnector.connect();
        metaModel = RopidGtfsContainer.resolve<RopidGTFSMetadataModel>(ModuleContainerToken.RopidGTFSMetadataModel);

        testGtfsData = [
            {
                filepath: "PID_GTFS/agency.txt",
                mtime: "2021-03-18T03:54:20.000Z",
                name: "agency",
                path: "agency.txt",
            },
            {
                filepath: "PID_GTFS/calendar.txt",
                mtime: "2021-03-18T03:54:32.000Z",
                name: "calendar",
                path: "calendar.txt",
            },
        ];

        testDataCis = { filepath: "Stops.Min.json", name: "Stops.Min" };

        testDataOis = { filepath: "oisMapping.json", name: "oisMapping" };

        testDataRunNumbers = { filepath: "obehy.csv", name: "obehy" };

        agencyData = {
            agency_id: "99",
            agency_name: "Pražská integrovaná doprava",
            agency_url: "https://pid.cz",
            agency_timezone: "Europe/Prague",
            agency_lang: "cs",
            agency_phone: "+420234704560",
        };

        testTransformedData = { sourceStream: Readable.from(JSON.stringify(agencyData)), name: "agency" };

        testDataDeparturesPresets = JSON.parse(
            fs.readFileSync(__dirname + "/data/ropiddeparturespresets-data.json").toString("utf8")
        );

        testTransformedDeparturesPresets = JSON.parse(
            fs.readFileSync(__dirname + "/data/ropiddeparturespresets-transformed.json").toString("utf8")
        );

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().callsFake(() =>
                    Object.assign({
                        removeAttribute: sandbox.stub(),
                    })
                ),
            })
        );
        sandbox.stub(RedisConnector, "getConnection");

        legacyWorker = new LegacyRopidGTFSWorker();

        sandbox.stub(config, "datasources").value({
            RopidGTFSRunNumbersFilename: "obehy.csv",
            RopidGTFSOisFilename: "oisMapping.json",
            RopidGTFSCisStopsFilename: "Stops.Min.json",
        });

        sandbox
            .stub(metaModel, "getLastModified")
            .callsFake(() => Object.assign({ lastModified: "2019-01-18T03:24:09.000Z", version: 0 }));
        sandbox.stub(metaModel, "save");
        sandbox.stub(metaModel, "checkSavedRows");
        sandbox.stub(metaModel, "replaceTables");
        sandbox.stub(metaModel, "replaceTmpTables");
        sandbox.stub(metaModel, "rollbackFailedSaving");
        sandbox.stub(metaModel, "updateState");
        sandbox.stub(metaModel, "checkIfNewVersionIsAlreadyDeployed");
        sandbox.stub(metaModel, "checkSavedTmpTables");
        sandbox.stub(metaModel, "checkAllTablesHasSavedState").callsFake(() => Promise.resolve(true));
        RopidGtfsContainer.register(ModuleContainerToken.RopidGTFSMetadataModel, { useValue: metaModel });

        sandbox.stub(legacyWorker, "sendMessageToExchange" as any);
        sandbox.stub(QueueManager, "sendMessageToExchange" as any);

        staticFileRedisRepository = RopidGtfsContainer.resolve<StaticFileRedisRepository>(
            RopidGtfsContainerToken.StaticFileRedisRepository
        );
        sandbox.stub(staticFileRedisRepository, "set");
        sandbox.stub(staticFileRedisRepository, "get").callsFake(async () => Buffer.from(JSON.stringify(agencyData)));
        sandbox.stub(staticFileRedisRepository, "pipeStream");
        sandbox.stub(staticFileRedisRepository, "getReadableStream").callsFake(() => Readable.from(JSON.stringify(agencyData)));
        dataCacheManager = DataCacheManager.getInstance();
        sandbox.stub(dataCacheManager, "cleanCache");

        precomputeFacade = RopidGtfsContainer.resolve<PrecomputedTablesFacade>(RopidGtfsContainerToken.PrecomputedTablesFacade);
        sandbox.stub(precomputeFacade, "createAndPopulatePrecomputedTmpTables" as any);

        ropidGtfsFactory = RopidGtfsContainer.resolve<RopidGtfsFactory>(RopidGtfsContainerToken.RopidGtfsFactory);
        sandbox.stub(ropidGtfsFactory["dataSourceGtfs"], "getAll").resolves(testGtfsData);
        sandbox.stub(ropidGtfsFactory["transformationGtfs"], "transform");

        sandbox.stub(ropidGtfsFactory["dataSourceCisStops"], "getAll").resolves(testDataCis);
        sandbox.stub(ropidGtfsFactory["dataSourceCisStops"], "getLastModified").resolves("2019-01-18T03:24:09.000Z");
        sandbox.stub(ropidGtfsFactory["transformationCisStops"], "transform");

        sandbox.stub(ropidGtfsFactory["dataSourceOisMapping"], "getAll").resolves(testDataOis);
        sandbox.stub(ropidGtfsFactory["dataSourceOisMapping"], "getLastModified").resolves("2019-01-18T03:24:09.000Z");
        sandbox.stub(ropidGtfsFactory["transformationOisMapping"], "transform");

        sandbox.stub(ropidGtfsFactory["dataSourceRunNumbers"], "getAll").resolves(testDataRunNumbers);
        sandbox.stub(ropidGtfsFactory["dataSourceRunNumbers"], "getLastModified").resolves("2019-01-18T03:24:09.000Z");
        RopidGtfsContainer.registerInstance(RopidGtfsContainerToken.RopidGtfsFactory, ropidGtfsFactory);

        // Presets stubs
        const facade = PidContainer.resolve<DeparturePresetsFacade>(ModuleContainerToken.DeparturePresetsFacade);
        sandbox.stub(facade["transformationDeparturesPresets"], "transform").resolves(testTransformedDeparturesPresets);
        sandbox.stub(facade["departurePresetsRepository"], "truncate");
        sandbox.stub(facade["departurePresetsRepository"], "save");
        sandbox
            .stub(facade["metaModel"], "getLastModified")
            .callsFake(() => Object.assign({ lastModified: "2019-01-18T03:24:09.000Z", version: 0 }));
        sandbox.stub(facade["metaModel"], "save");
        sandbox.stub(facade["metaModel"], "checkSavedRows");
        sandbox.stub(facade["metaModel"], "replaceTables");
        sandbox.stub(facade["metaModel"], "replaceTmpTables");
        sandbox.stub(facade["metaModel"], "rollbackFailedSaving");
        sandbox.stub(facade["metaModel"], "updateState");
        sandbox.stub(facade["metaModel"], "checkIfNewVersionIsAlreadyDeployed");
        sandbox.stub(facade["metaModel"], "checkSavedTmpTables");
        sandbox.stub(facade["metaModel"], "checkAllTablesHasSavedState").callsFake(() => Promise.resolve(true));

        sandbox.stub(legacyWorker["dataSourceDeparturesPresets"], "getAll").resolves(testDataDeparturesPresets);
        sandbox.stub(legacyWorker["dataSourceDeparturesPresets"], "getLastModified");

        transformAndSaveTask = new TransformAndSaveDataTask("test");
        modelTruncateStub = sandbox.stub();
        modelSaveStub = sandbox.stub();
        modelSyncStub = sandbox.stub();
        sandbox.stub(transformAndSaveTask, "getTmpModelByName" as any).callsFake(() =>
            Object.assign({
                save: modelSaveStub,
                saveBySqlFunction: modelSaveStub,
                truncate: modelTruncateStub,
                sync: modelSyncStub,
            })
        );
        sandbox.stub(transformAndSaveTask, "streamDataToTmp" as any);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should calls the correct methods by checkForNewData method", async () => {
        sandbox.stub(ropidGtfsFactory["dataSourceGtfs"], "getLastModified").resolves("2019-01-18T03:22:09.000Z");
        sandbox.stub(metaModel, "isDeployed").resolves(false);

        await new CheckForNewDataTask("test").validateAndExecute({
            content: Buffer.from(JSON.stringify({ forceRefresh: false } as ICheckForNewDataInput)),
        } as any);

        sandbox.assert.calledOnce(ropidGtfsFactory["dataSourceGtfs"].getLastModified as SinonSpy);
        sandbox.assert.calledOnce(ropidGtfsFactory["dataSourceCisStops"].getLastModified as SinonSpy);
        sandbox.assert.calledOnce(ropidGtfsFactory["dataSourceOisMapping"].getLastModified as SinonSpy);
        sandbox.assert.calledOnce(ropidGtfsFactory["dataSourceRunNumbers"].getLastModified as SinonSpy);
        sandbox.assert.callCount(metaModel.getLastModified as SinonSpy, 4);
        sandbox.assert.calledOnce(QueueManager["sendMessageToExchange"] as SinonSpy);
        sandbox.assert.callOrder(
            ropidGtfsFactory["dataSourceGtfs"].getLastModified as SinonSpy,
            metaModel.getLastModified as SinonSpy,
            ropidGtfsFactory["dataSourceCisStops"].getLastModified as SinonSpy,
            metaModel.getLastModified as SinonSpy,
            ropidGtfsFactory["dataSourceOisMapping"].getLastModified as SinonSpy,
            metaModel.getLastModified as SinonSpy,
            ropidGtfsFactory["dataSourceRunNumbers"].getLastModified as SinonSpy,
            metaModel.getLastModified as SinonSpy,
            QueueManager["sendMessageToExchange"] as SinonSpy
        );
    });

    it("checkForNewData should send a message when forceRefresh is true", async () => {
        sandbox.stub(ropidGtfsFactory["dataSourceGtfs"], "getLastModified").resolves("2019-01-18T03:24:09.000Z");
        sandbox.stub(metaModel, "isDeployed").resolves(true);
        const logInfoStub = sandbox.stub(log, "info").resolves(true);
        Object.defineProperty(legacyWorker, "queuePrefix", { value: "oict-test" });

        await new CheckForNewDataTask("test").validateAndExecute({
            content: Buffer.from(JSON.stringify({ forceRefresh: true } as ICheckForNewDataInput)),
        } as any);

        expect(logInfoStub.lastCall.args).to.deep.equal(["RopidGTFS datasets are up to date"]);
        expect((QueueManager["sendMessageToExchange"] as SinonStub).lastCall.args).to.deep.equal([
            "test",
            "refreshPrecomputedTables",
            {},
        ]);
    });

    it("should calls the correct methods by downloadDatasets method", async () => {
        const subscribeStub = sandbox.stub().resolves();
        const listenStub = sandbox.stub().resolves();
        const unsubscribeStub = sandbox.stub();
        const task = new DownloadDatasetsTask("test");
        sandbox.stub(ropidGtfsFactory["dataSourceGtfs"], "getLastModified").resolves("2019-01-18T03:22:09.000Z");
        sandbox.stub(task["gtfsRedisChannel"], "createSubscriber").returns({
            subscribe: subscribeStub,
            listen: listenStub,
            unsubscribe: unsubscribeStub,
        } as any);

        const promise = task.validateAndExecute({
            content: Buffer.from(
                JSON.stringify({ datasets: ["PID_GTFS", "RUN_NUMBERS", "CIS_STOPS", "OIS_MAPPING"] } as IDatasetsInput)
            ),
        } as any);
        await sandbox.clock.tickAsync(5 * 60 * 1000);
        await promise;

        sandbox.assert.calledOnce(ropidGtfsFactory["dataSourceGtfs"].getAll as SinonSpy);
        sandbox.assert.calledOnce(ropidGtfsFactory["dataSourceCisStops"].getAll as SinonSpy);
        sandbox.assert.calledOnce(ropidGtfsFactory["dataSourceOisMapping"].getAll as SinonSpy);
        sandbox.assert.calledOnce(ropidGtfsFactory["dataSourceRunNumbers"].getAll as SinonSpy);

        sandbox.assert.callCount(staticFileRedisRepository.set as SinonSpy, 2);
        sandbox.assert.callCount(staticFileRedisRepository.pipeStream as SinonSpy, 3);

        testGtfsData.forEach((f) => {
            sandbox.assert.calledWith(staticFileRedisRepository.pipeStream as SinonSpy, f.filepath, f.data);
        });

        sandbox.assert.calledOnce(ropidGtfsFactory["dataSourceGtfs"].getLastModified as SinonSpy);
        sandbox.assert.calledOnce(ropidGtfsFactory["dataSourceCisStops"].getLastModified as SinonSpy);
        sandbox.assert.calledOnce(ropidGtfsFactory["dataSourceOisMapping"].getLastModified as SinonSpy);
        sandbox.assert.calledOnce(ropidGtfsFactory["dataSourceRunNumbers"].getLastModified as SinonSpy);

        sandbox.assert.callCount(metaModel.getLastModified as SinonSpy, 4);
        sandbox.assert.callCount(metaModel.save as SinonSpy, 10);

        sandbox.assert.callCount(QueueManager["sendMessageToExchange"] as SinonSpy, 6);

        testGtfsData.forEach((f) => {
            sandbox.assert.calledWith(QueueManager["sendMessageToExchange"] as SinonSpy, "test", "transformAndSaveData", {
                ...f,
                dataset: DatasetEnum.PID_GTFS,
            });
        });

        [testDataRunNumbers].forEach((f) => {
            sandbox.assert.calledWith(QueueManager["sendMessageToExchange"] as SinonSpy, "test", "transformAndSaveData", {
                ...f,
                dataset: DatasetEnum.RUN_NUMBERS,
            });
        });

        [testDataCis].forEach((f) => {
            sandbox.assert.calledWith(QueueManager["sendMessageToExchange"] as SinonSpy, "test", "transformAndSaveData", {
                ...f,
                dataset: DatasetEnum.CIS_STOPS,
            });
        });

        [testDataOis].forEach((f) => {
            sandbox.assert.calledWith(QueueManager["sendMessageToExchange"] as SinonSpy, "test", "transformAndSaveData", {
                ...f,
                dataset: DatasetEnum.OIS_MAPPING,
            });
        });

        sandbox.assert.callCount(subscribeStub, 1);
        sandbox.assert.callCount(listenStub, 1);
        sandbox.assert.callCount(unsubscribeStub, 1);
        const expectedResult = new DatasetsInputSchema();
        expectedResult.datasets = [DatasetEnum.PID_GTFS, DatasetEnum.RUN_NUMBERS, DatasetEnum.CIS_STOPS, DatasetEnum.OIS_MAPPING];
        sandbox.assert.calledWith(
            QueueManager["sendMessageToExchange"] as SinonSpy,
            "test",
            "checkSavedRowsAndReplaceTables",
            expectedResult
        );

        sandbox.assert.callOrder(
            ropidGtfsFactory["dataSourceGtfs"].getAll as SinonSpy,
            metaModel.save as SinonSpy,
            subscribeStub,
            QueueManager["sendMessageToExchange"] as SinonSpy,
            listenStub,
            unsubscribeStub,
            QueueManager["sendMessageToExchange"] as SinonSpy
        );
    });

    it("should calls the correct methods by transformAndSaveData method (agency)", async () => {
        const publishStub = sandbox.stub(transformAndSaveTask["gtfsRedisChannel"], "publishMessage").resolves();
        sandbox.stub(transformAndSaveTask["transformationGtfs"], "transform");
        await transformAndSaveTask.validateAndExecute({
            content: Buffer.from(JSON.stringify({ ...testGtfsData[0], dataset: DatasetEnum.PID_GTFS })),
            properties: { headers: { "x-origin-hostname": "test" } },
        } as any);

        sandbox.assert.calledOnce(transformAndSaveTask["transformationGtfs"].transform as SinonSpy);
        expect((transformAndSaveTask["transformationGtfs"].transform as SinonSpy).getCall(0).args[0].name).to.equal(
            testGtfsData[0].name
        );
        expect(
            (transformAndSaveTask["transformationGtfs"].transform as SinonSpy).getCall(0).args[0].sourceStream
        ).to.be.instanceOf(Readable);
        sandbox.assert.calledOnce(transformAndSaveTask["getTmpModelByName"] as SinonSpy);
        sandbox.assert.calledWith(transformAndSaveTask["getTmpModelByName"] as SinonSpy, testTransformedData.name);
        sandbox.assert.calledOnce(modelSyncStub);
        sandbox.assert.calledOnce(transformAndSaveTask["streamDataToTmp"] as SinonSpy);
        sandbox.assert.calledOnce(metaModel.getLastModified as SinonSpy);
        sandbox.assert.calledOnce(metaModel.updateState as SinonSpy);
        expect(publishStub.getCall(0).args[0]).to.equal(`OK (${testGtfsData[0].name})`);
        expect(publishStub.getCall(0).args[1]).to.deep.equal({ channelSuffix: "test" });
    });

    it("should calls the correct methods by checkSavedRowsAndReplaceTables method", async () => {
        const task = new CheckSavedRowsAndReplaceTablesTask("test");
        await task.validateAndExecute({
            content: Buffer.from(JSON.stringify({ datasets: [DatasetEnum.PID_GTFS] })),
        } as any);

        sandbox.assert.calledOnce(metaModel.getLastModified as SinonSpy);
        sandbox.assert.calledOnce(metaModel.checkIfNewVersionIsAlreadyDeployed as SinonSpy);
        sandbox.assert.calledOnce(metaModel.checkAllTablesHasSavedState as SinonSpy);
        sandbox.assert.calledOnce(metaModel.checkSavedTmpTables as SinonSpy);
        sandbox.assert.calledOnce(metaModel.replaceTables as SinonSpy);
        sandbox.assert.calledOnce(dataCacheManager.cleanCache as SinonSpy);
        sandbox.assert.calledOnce(precomputeFacade["createAndPopulatePrecomputedTmpTables"] as SinonSpy);
    });

    it("should call the correct methods by checkForNewDeparturesPresets method", async () => {
        await legacyWorker.checkForNewDeparturesPresets();
        sandbox.assert.calledOnce(legacyWorker["dataSourceDeparturesPresets"].getLastModified as SinonSpy);
        sandbox.assert.calledOnce(metaModel.getLastModified as SinonSpy);
        sandbox.assert.calledOnce(legacyWorker["sendMessageToExchange"] as SinonSpy);
    });

    it("should call the correct methods by downloadDeparturesPresets method", async () => {
        await legacyWorker.downloadDeparturesPresets();
        const facade = PidContainer.resolve<DeparturePresetsFacade>(ModuleContainerToken.DeparturePresetsFacade);
        sandbox.assert.calledOnce(legacyWorker["dataSourceDeparturesPresets"].getAll as SinonSpy);
        sandbox.assert.calledOnce(legacyWorker["dataSourceDeparturesPresets"].getLastModified as SinonSpy);
        sandbox.assert.calledOnce(facade["metaModel"].getLastModified as SinonSpy);
        sandbox.assert.callCount(facade["metaModel"].save as SinonSpy, 3);
        sandbox.assert.calledOnce(facade["transformationDeparturesPresets"].transform as SinonSpy);
        sandbox.assert.calledOnce(facade["departurePresetsRepository"].truncate as SinonSpy);
        sandbox.assert.calledOnce(facade["departurePresetsRepository"].save as SinonSpy);
        sandbox.assert.calledOnce(facade["metaModel"].checkSavedRows as SinonSpy);
        sandbox.assert.calledOnce(facade["metaModel"].replaceTmpTables as SinonSpy);
    });
});
