import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { RopidVYMIEventsStopsModel } from "#ie/ropid-vymi/models/RopidVYMIEventsStopsModel";

chai.use(chaiAsPromised);

describe("RopidVYMIEventsStopsModel", () => {
    let model: RopidVYMIEventsStopsModel;

    before(async () => {
        await PostgresConnector.connect();
        model = new RopidVYMIEventsStopsModel();
    });

    it("gtfs_stop_id U663Z2P -> text sample 1", async () => {
        const result = await model.findOne({
            where: {
                gtfs_stop_id: "U663Z2P",
            },
        });

        expect(result.dataValues.text).to.eq("text sample 1");
    });

    it("gtfs_stop_id U21Z1P -> text sample 2", async () => {
        const result = await model.findOne({
            where: {
                gtfs_stop_id: "U21Z1P",
            },
        });

        expect(result.dataValues.text).to.eq("text sample 2");
    });

    it("gtfs_stop_id U21Z2P -> text sample 3", async () => {
        const result = await model.findOne({
            where: {
                gtfs_stop_id: "U21Z2P",
            },
        });

        expect(result.dataValues.text).to.eq("text sample 3");
    });
});
