import { Portman } from "@apideck/portman/dist/Portman";
import { PortmanOptions } from "@apideck/portman/dist/types/PortmanOptions";

export const ApiTestPortman = async (portmanOptions: PortmanOptions): Promise<void> => {
    const portmanProcess = new Portman(portmanOptions);
    await portmanProcess.run();
};
