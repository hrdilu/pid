// Load reflection lib
import "@golemio/core/dist/shared/_global";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import { ApiTestPortman } from "./ApiTestPortman";
import { InputApiTestServer } from "./input-gateway/InputApiTestServer";
import { portmanInputConfig } from "./input-gateway/portman-config/portman-input-config";
import { OutputApiTestServer } from "./output-gateway/OutputApiTestServer";
import { portmanOutputConfig } from "./output-gateway/portman-config/portman-output-config";

(async () => {
    const targetServer = process.argv[2].split(",");
    if (targetServer.includes("input")) {
        const inputServer = new InputApiTestServer();
        await inputServer.start();
        log.info("Starting Portman process for Input Gateway");
        await ApiTestPortman(portmanInputConfig);
        await inputServer.stop();
    }

    if (targetServer.includes("output")) {
        const outputServer = new OutputApiTestServer();
        await outputServer.start();
        log.info("Starting Portman process for Output Gateway");
        await ApiTestPortman(portmanOutputConfig);
        await outputServer.stop();
    }

    process.exit(0);
})();
