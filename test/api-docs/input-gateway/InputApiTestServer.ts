import { GeneralError, ErrorHandler, HTTPErrorHandler, FatalError } from "@golemio/core/dist/shared/golemio-errors";
import express, { Request, Response, NextFunction } from "@golemio/core/dist/shared/express";
import http from "http";

// Import core components
import { AMQPConnector } from "@golemio/core/dist/input-gateway/connectors";
import { requestLogger, log } from "@golemio/core/dist/input-gateway/helpers";
import { BaseApp } from "@golemio/core/dist/helpers";

// Import resources
import { vehiclePositionsRouter } from "#ig/vehicle-positions/VehiclePositionsRouter";
import { RopidGtfsRouter } from "#ig/ropid-gtfs/RopidGtfsRouter";

const bodyParser = require("body-parser");
require("body-parser-xml")(bodyParser);

export class InputApiTestServer extends BaseApp {
    public express = express();
    public server?: http.Server;
    public port = 3012;

    public start = async (): Promise<void> => {
        try {
            this.express = express();
            await this.connections();
            this.middleware();
            this.routes();
            this.errorHandlers();

            return new Promise((resolve) => {
                this.server = http.createServer(this.express);
                this.server.on("error", (err: any) => {
                    ErrorHandler.handle(new FatalError("Could not start a server", "App", err), log);
                });

                this.server.listen(this.port, () => {
                    log.info(`Listening at http://localhost:${this.port}/`);
                    resolve();
                });
            });
        } catch (err) {
            ErrorHandler.handle(err, log);
        }
    };

    public stop = async (): Promise<void> => {
        await AMQPConnector.disconnect();
        this.server?.close();
    };

    private connections = async (): Promise<void> => {
        await AMQPConnector.connect();
    };

    private customHeaders = (_req: Request, res: Response, next: NextFunction) => {
        res.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS, HEAD");
        next();
    };

    private middleware = (): void => {
        this.express.use(
            bodyParser.xml({
                xmlParseOptions: {
                    strict: false,
                    explicitArray: false,
                    normalize: true,
                    normalizeTags: false,
                },
            })
        );

        this.express.use(requestLogger);
        this.express.use(this.commonHeaders);
        this.express.use(this.customHeaders);
    };

    private routes = (): void => {
        this.express.use("/vehiclepositions", vehiclePositionsRouter);
        this.express.use("/ropidgtfs", new RopidGtfsRouter().router);
    };

    private errorHandlers = (): void => {
        this.express.use((req: Request, _res: Response, next: NextFunction) => {
            next(new GeneralError("Route not found", "App", new Error(`Called ${req.method} ${req.url}`), 404));
        });

        this.express.use((err: any, _req: Request, res: Response, _next: NextFunction) => {
            const warnCodes = [404];
            const error = HTTPErrorHandler.handle(err, log, warnCodes.includes(err.status) ? "warn" : "error");
            if (error) {
                log.silly("Error caught by the router error handler.");
                res.setHeader("Content-Type", "application/json; charset=utf-8");
                res.status(error.error_status || 500).send(error);
            }
        });
    };
}
