import { PortmanOptions } from "@apideck/portman/dist/types/PortmanOptions";

export const portmanInputConfig: PortmanOptions = {
    baseUrl: "http://localhost:3012",
    oaLocal: "docs/openapi-input.yaml",
    oaOutput: "docs/postman-input-collection-generated.json",
    portmanConfigPath: "test/api-docs/input-gateway/portman-config/portman-config.json",

    // swagger transformation config (docs: https://github.com/postmanlabs/openapi-to-postman/blob/develop/OPTIONS.md)
    postmanConfigPath: "test/api-docs/input-gateway/portman-config/postman-config.json",
    filterFile: "test/api-docs/input-gateway/portman-config/filter-config.json",
    includeTests: true,

    // run tests
    runNewman: true,

    // not in use for current tests task
    syncPostman: false,
    oaUrl: "",
    bundleContractTests: false,
    newmanIterationData: "",
    portmanConfigFile: "",
    postmanConfigFile: "",
    envFile: ".env",
};
