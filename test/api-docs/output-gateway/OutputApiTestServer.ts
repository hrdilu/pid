import {
    GeneralError,
    ErrorHandler,
    HTTPErrorHandler,
    IGolemioError,
    FatalError,
} from "@golemio/core/dist/shared/golemio-errors";
import http from "http";
import fs from "fs";
import { promisify } from "util";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { RedisConnector } from "@golemio/core/dist/output-gateway/redis/";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import { gtfsRouter } from "#og/ropid-gtfs";
import { vehiclepositionsRouter } from "#og/vehicle-positions";
import { pidRouter } from "#og/pid";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { ContainerToken, OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc/";

type SetCommand = (key: string, arg1: string, arg2: string, cb?: Function) => boolean;

export class OutputApiTestServer {
    public express = express();
    public port = 3011;
    private server?: http.Server;
    private postgresConnector: IDatabaseConnector;

    constructor() {
        this.postgresConnector = OutputGatewayContainer.resolve<IDatabaseConnector>(ContainerToken.PostgresDatabase);
    }

    public start = async (): Promise<void> => {
        try {
            this.express = express();
            await this.database();
            await this.prepareGtfsRtData();
            this.middleware();
            this.routes();
            this.errorHandlers();

            return new Promise((resolve) => {
                this.server = http.createServer(this.express);
                this.server.on("error", (err: Error) => {
                    ErrorHandler.handle(new FatalError("Could not start a server", "App", err), log);
                });

                this.server.listen(this.port, () => {
                    log.info(`Listening at http://localhost:${this.port}/`);
                    resolve();
                });
            });
        } catch (err) {
            ErrorHandler.handle(err, log);
        }
    };

    public stop = async (): Promise<void> => {
        OutputGatewayContainer.dispose();
        this.server?.close();
    };

    private setHeaders = (req: Request, res: Response, next: NextFunction): void => {
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Methods", "GET, OPTIONS, HEAD");
        next();
    };

    private database = async (): Promise<void> => {
        await this.postgresConnector.connect();
        await RedisConnector.connect();
    };

    private middleware = (): void => {
        this.express.use(this.setHeaders);
        this.express.use(express.static("public"));
    };

    private routes = (): void => {
        this.express.use("/gtfs", gtfsRouter);
        this.express.use("/vehiclepositions", vehiclepositionsRouter);
        this.express.use("/pid", pidRouter);
    };

    private errorHandlers = (): void => {
        this.express.use((req, res, next) => {
            next(new GeneralError("Route not found", "ApiTestServer", new Error(`Called ${req.method} ${req.url}`), 404));
        });

        this.express.use((err: any, req: Request, res: Response, next: NextFunction) => {
            const warnCodes = [400, 404];
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log, warnCodes.includes(err.status) ? "warn" : "error");
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    };

    private async prepareGtfsRtData() {
        const redisClient = RedisConnector.getConnection();
        const redisHSetAsync = promisify(redisClient.hset as SetCommand).bind(redisClient);

        const bufferTrips = fs.readFileSync(__dirname + "/../../../db/example/pb/trip_updates.pb");
        const bufferPositions = fs.readFileSync(__dirname + "/../../../db/example/pb/vehicle_positions.pb");
        const bufferPidFeed = fs.readFileSync(__dirname + "/../../../db/example/pb/pid_feed.pb");
        const bufferAlerts = fs.readFileSync(__dirname + "/../../../db/example/pb/alerts.pb");

        await redisHSetAsync("files:gtfsRt", "trip_updates.pb", bufferTrips.toString("binary"));
        await redisHSetAsync("files:gtfsRt", "vehicle_positions.pb", bufferPositions.toString("binary"));
        await redisHSetAsync("files:gtfsRt", "pid_feed.pb", bufferPidFeed.toString("binary"));
        await redisHSetAsync("files:gtfsRt", "alerts.pb", bufferAlerts.toString("binary"));
    }
}
