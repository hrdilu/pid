import sinon from "sinon";
import request from "supertest";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import moment from "@golemio/core/dist/shared/moment-timezone";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import { vehiclepositionsRouter } from "#og/vehicle-positions/VehiclePositionsRouter";
import { RedisConnector } from "@golemio/core/dist/output-gateway/redis";

chai.use(chaiAsPromised);

describe("VehiclePositions Router", () => {
    // Create clean express instance
    const app = express();

    // Basic configuration: create a sinon sandbox for testing
    let sandbox: any = null;

    before(async () => {
        await RedisConnector.connect();
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox && sandbox.restore();
    });

    before(async () => {
        // Mount the tested router to the express instance
        app.use("/vehiclepositions", vehiclepositionsRouter);
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
        const bufferTrips = fs.readFileSync("./db/example/pb/trip_updates.pb");
        const bufferVehicles = fs.readFileSync("./db/example/pb/vehicle_positions.pb");
        const bufferPidFeed = fs.readFileSync("./db/example/pb/pid_feed.pb");
        const bufferAlerts = fs.readFileSync("./db/example/pb/alerts.pb");

        const redisClient = RedisConnector.getConnection();
        await Promise.all([
            new Promise<void>((done) =>
                redisClient.hset("files:gtfsRt", "trip_updates.pb", bufferTrips.toString("binary"), () => done())
            ),
            new Promise<void>((done) =>
                redisClient.hset("files:gtfsRt", "vehicle_positions.pb", bufferVehicles.toString("binary"), () => done())
            ),
            new Promise<void>((done) =>
                redisClient.hset("files:gtfsRt", "pid_feed.pb", bufferPidFeed.toString("binary"), () => done())
            ),
            new Promise<void>((done) =>
                redisClient.hset("files:gtfsRt", "alerts.pb", bufferAlerts.toString("binary"), () => done())
            ),
        ]);
    });

    it("should respond with json to GET /vehiclepositions", (done) => {
        request(app).get("/vehiclepositions").set("Accept", "application/json").expect("Content-Type", /json/).expect(200, done);
    });

    it("should respond with json to GET /vehiclepositions/115_6_201230_V1", (done) => {
        request(app)
            .get("/vehiclepositions/115_6_201230_V1")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200, (_err, res) => {
                expect(res.body.properties.trip.air_conditioned).to.be.true;
                done();
            });
    });

    it("should respond with json to GET /vehiclepositions/115_6_201230_V1?includePositions=true", (done) => {
        request(app)
            .get("/vehiclepositions/115_6_201230_V1?includePositions=true")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200, (_err, res) => {
                const lastPosition = res.body.properties.last_position;
                const includedPosition = res.body.properties.all_positions?.features[0]?.properties;

                delete lastPosition.state_position;
                delete lastPosition.tracking;

                expect(includedPosition).to.deep.equal(lastPosition);
                done();
            });
    });

    it("should respond with json to GET /vehiclepositions/152_37_201230", (done) => {
        request(app)
            .get("/vehiclepositions/152_37_201230")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200, (_err, res) => {
                expect(res.body.properties.trip.air_conditioned).to.be.false;
                done();
            });
    });

    it("should respond with json to GET /vehiclepositions/991_294_220901", (done) => {
        const now = new Date();
        const todayYMD = moment(now).format("YYYY-MM-DD");
        const clock = sinon.useFakeTimers({
            now: moment.tz(`${moment(now).format("YYYY-MM-DD")}T10:20:00`, "Europe/Prague").valueOf(),
            shouldAdvanceTime: true,
        });

        request(app)
            .get("/vehiclepositions/991_294_220901")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200, (_err, res) => {
                expect(res.body.properties.trip).to.deep.equal({
                    agency_name: {
                        real: "DP PRAHA",
                        scheduled: "DP PRAHA",
                    },
                    cis: {
                        line_id: null,
                        trip_number: null,
                    },
                    gtfs: {
                        route_id: "L991",
                        route_short_name: "A",
                        route_type: 1,
                        trip_headsign: "Depo Hostivař",
                        trip_id: "991_294_220901",
                        trip_short_name: null,
                    },
                    origin_route_name: "991",
                    sequence_id: 9,
                    start_timestamp: moment(`${todayYMD}T10:04:35+02:00`).tz("Europe/Prague").format(),
                    vehicle_registration_number: null,
                    vehicle_type: {
                        description_cs: "metro",
                        description_en: "metro",
                        id: 1,
                    },
                    wheelchair_accessible: true,
                    air_conditioned: null,
                });

                expect(res.body.properties.last_position).to.deep.equal({
                    bearing: 139,
                    delay: {
                        actual: -13,
                        last_stop_arrival: null,
                        last_stop_departure: null,
                    },
                    is_canceled: null,
                    last_stop: {
                        arrival_time: moment(`${todayYMD}T10:19:00+02:00`).tz("Europe/Prague").format(),
                        departure_time: moment(`${todayYMD}T10:19:30+02:00`).tz("Europe/Prague").format(),
                        id: "U1072Z101P",
                        sequence: 9,
                    },
                    next_stop: {
                        arrival_time: moment(`${todayYMD}T10:20:40+02:00`).tz("Europe/Prague").format(),
                        departure_time: moment(`${todayYMD}T10:21:10+02:00`).tz("Europe/Prague").format(),
                        id: "U400Z101P",
                        sequence: 10,
                    },
                    origin_timestamp: moment(`${todayYMD}T10:20:13+02:00`).tz("Europe/Prague").format(),
                    shape_dist_traveled: "9.656",
                    speed: null,
                    state_position: "on_track",
                    tracking: true,
                });

                clock.restore();
                done();
            });
    });

    it("should respond with protobuffer to GET /vehiclepositions/gtfsrt/trip_updates.pb", (done) => {
        request(app)
            .get("/vehiclepositions/gtfsrt/trip_updates.pb")
            .set("Accept", "application/octet-stream")
            .expect("Content-Type", /octet-stream/)
            .expect(200)
            .end((err, data) => {
                if (err) {
                    return done(err);
                }
                fs.readFile("./db/example/pb/trip_updates.pb", (readerr: any, rawdata: Buffer) => {
                    if (readerr) {
                        return done(readerr);
                    }
                    chai.expect(data.body).to.deep.equal(rawdata);
                    done();
                });
            });
    });

    it("should respond with protobuffer to GET /vehiclepositions/gtfsrt/vehicle_positions.pb", (done) => {
        request(app)
            .get("/vehiclepositions/gtfsrt/vehicle_positions.pb")
            .set("Accept", "application/octet-stream")
            .expect("Content-Type", /octet-stream/)
            .expect(200)
            .end((err, data) => {
                if (err) {
                    return done(err);
                }
                fs.readFile("./db/example/pb/vehicle_positions.pb", (readerr: any, rawdata: any) => {
                    if (readerr) {
                        return done(readerr);
                    }
                    chai.expect(data.body).to.deep.equal(rawdata);
                    done();
                });
            });
    });

    it("should respond with protobuffer to GET /vehiclepositions/gtfsrt/pid_feed.pb", (done) => {
        request(app)
            .get("/vehiclepositions/gtfsrt/pid_feed.pb")
            .set("Accept", "application/octet-stream")
            .expect("Content-Type", /octet-stream/)
            .expect(200)
            .end((err, data) => {
                if (err) {
                    return done(err);
                }
                chai.expect(data.body).to.be.instanceOf(Buffer);
                chai.expect(data.body.length).to.be.greaterThan(0);
                done();
            });
    });

    it("should respond with some data to GET /vehiclepositions/gtfsrt/alerts.pb", (done) => {
        request(app)
            .get("/vehiclepositions/gtfsrt/alerts.pb")
            .set("Accept", "application/octet-stream")
            .expect("Content-Type", /octet-stream/)
            .expect(200)
            .end((err, data) => {
                if (err) {
                    return done(err);
                }
                chai.expect(data).to.be.not.null;
                done();
            });
    });

    it("should respond with validation error for bad timezone query", (done) => {
        request(app)
            .get("/vehiclepositions?limit=10&preferredTimezone=Europe%252FPrague")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(400)
            .end((err, data) => {
                if (err) {
                    return done(err);
                }
                chai.expect(data).to.be.not.null;
                done();
            });
    });

    it("should respond with no validation error for valid timezone query", (done) => {
        request(app)
            .get("/vehiclepositions?limit=10&preferredTimezone=Europe_Prague")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .end((err, data) => {
                if (err) {
                    return done(err);
                }
                chai.expect(data).to.be.not.null;
                done();
            });
    });
});
