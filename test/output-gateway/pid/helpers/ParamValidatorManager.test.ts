import { ParamValidatorManager } from "#og/pid/helpers/ParamValidatorManager";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";

chai.use(chaiAsPromised);

describe("ParamValidatorManager", () => {
    // =============================================================================
    // getAswIdsValidator
    // =============================================================================
    describe("getAswIdsValidator", () => {
        const validate = ParamValidatorManager.getAswIdsValidator();

        it("should validate", async () => {
            await expect(validate("69_420", {} as any)).to.be.fulfilled;
        });

        it("should validate (array)", async () => {
            await expect(validate(["69_420"], {} as any)).to.be.fulfilled;
        });

        it("should reject (aswIds must be defined)", async () => {
            await expect(validate(undefined, {} as any)).to.be.rejectedWith("aswIds must be defined");
        });

        it("should reject (invalid aswIds format)", async () => {
            await expect(validate(["69_420", "21__45"], {} as any)).to.be.rejectedWith("Invalid aswIds format");
        });

        it("should reject (invalid aswIds format - number)", async () => {
            await expect(validate([30], {} as any)).to.be.rejectedWith("Invalid aswIds format");
        });
    });
});
