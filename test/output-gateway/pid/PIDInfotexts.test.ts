import sinon, { SinonSandbox, SinonFakeTimers } from "sinon";
import request from "supertest";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import moment from "@golemio/core/dist/shared/moment-timezone";
import express, { Express, Request, Response } from "@golemio/core/dist/shared/express";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import { RopidRouterUtils } from "#og/shared";
import { pidRouter } from "#og/pid/PIDRouter";

chai.use(chaiAsPromised);

describe("PIDInfotexts Router", () => {
    let app: Express;
    let sandbox: SinonSandbox;
    const todayYMD = moment().format("YYYY-MM-DD");
    let clock: SinonFakeTimers;

    beforeEach(() => {
        clock = sinon.useFakeTimers({
            now: moment.tz(`${todayYMD}T11:35:00`, "Europe/Prague").valueOf(),
            shouldAdvanceTime: true,
        });

        sandbox = sinon.createSandbox();
        sandbox.stub(RopidRouterUtils, "formatTimestamp").returns("2021-01-07T12:48:06+01:00");
    });

    afterEach(() => {
        sandbox.restore();
    });

    before(() => {
        app = express();
        // Mount the tested router to the express instance
        app.use("/pid", pidRouter);
        app.use((err: any, _req: Request, res: Response) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    it("should return all infotexts with stops and routes", (done) => {
        request(app)
            .get("/pid/infotexts")
            .end((_err, res) => {
                const expectedResult = {
                    vymi_id: 83,
                    vymi_id_dtb: 3,
                    display_type: "general",
                    text_en: null,
                    text: "text sample 1",
                    related_stops: [
                        {
                            id: "U663Z2P",
                            name: "Sídliště Pankrác",
                            platform_code: "B",
                        },
                    ],
                    related_routes: ["L152", "L177"],
                    expiration_date: "2021-01-07T12:48:06+01:00",
                    last_updated: "2021-01-07T12:48:06+01:00",
                    last_updated_user: "KubatO",
                    valid_from: "2021-01-07T12:48:06+01:00",
                    valid_to: "2021-01-07T12:48:06+01:00",
                };

                expect(res.statusCode).to.be.equal(200);
                expect(res.body).to.be.an("array");
                expect(res.body[0]).to.deep.equal(expectedResult);
                done();
            });
    });
});
