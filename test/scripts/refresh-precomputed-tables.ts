// Load reflection lib
import "@golemio/core/dist/shared/_global";

import { RefreshPrecomputedTablesTask } from "#ie/ropid-gtfs/workers/timetables/tasks/RefreshPrecomputedTablesTask";
import { RefreshGtfsTripDataTask } from "#ie/vehicle-positions/workers/vehicle-positions/tasks/RefreshGtfsTripDataTask";
import { IRedisConnector } from "@golemio/core/dist/helpers/data-access/redis/IRedisConnector";
import IPostgresConnector from "@golemio/core/dist/integration-engine/connectors/interfaces/IPostgresConnector";
import { ContainerToken, IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc";

const PostgresConnector = IntegrationEngineContainer.resolve<IPostgresConnector>(ContainerToken.PostgresConnector);
const RedisConnector = IntegrationEngineContainer.resolve<IRedisConnector>(ContainerToken.RedisConnector);

(async () => {
    await PostgresConnector.connect();
    await RedisConnector.connect();

    const task = new RefreshPrecomputedTablesTask("test");
    await task.validateAndExecute(null);

    const refreshGtfsTripDataTask = new RefreshGtfsTripDataTask("test");
    await refreshGtfsTripDataTask.validateAndExecute(null);

    await PostgresConnector.disconnect();
    await RedisConnector.disconnect();
})();
