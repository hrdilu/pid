import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { VehiclePositionsController } from "#ig/vehicle-positions/VehiclePositionsController";

chai.use(chaiAsPromised);

describe("VehiclePositionsController", () => {
    let sandbox: SinonSandbox;
    let controller: VehiclePositionsController;

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        controller = new VehiclePositionsController();

        sandbox.stub(controller["validator"], "Validate").callsFake(() => Promise.resolve(true));
        // @ts-ignore
        sandbox.stub(controller, "sendMessageToExchange");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should has name", () => {
        expect(controller.name).not.to.be.undefined;
    });

    it("should has processData method", () => {
        expect(controller.processData).not.to.be.undefined;
    });

    it("should properly process data", async () => {
        await controller.processData({});
        sandbox.assert.calledOnce(controller["validator"].Validate as SinonSpy);
        // @ts-ignore
        sandbox.assert.calledOnce(controller.sendMessageToExchange);
    });
});
