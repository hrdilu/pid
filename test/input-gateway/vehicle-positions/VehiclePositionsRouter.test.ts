import chai from "chai";
import chaiAsPromised from "chai-as-promised";
import request from "supertest";
import express, { Request, Response, NextFunction } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler } from "@golemio/core/dist/shared/golemio-errors";
import { log } from "@golemio/core/dist/input-gateway/helpers/Logger";
import { AMQPConnector } from "@golemio/core/dist/input-gateway/connectors";
import { vehiclePositionsRouter } from "#ig/vehicle-positions/VehiclePositionsRouter";

const bodyParser = require("body-parser");
require("body-parser-xml")(bodyParser);

chai.use(chaiAsPromised);

describe("VehiclePositionsRouter", () => {
    // Create clean express instance
    const app = express();
    app.use(
        bodyParser.xml({
            limit: "2MB", // Reject payload bigger than limit
            xmlParseOptions: {
                explicitArray: false, // Only put nodes in array if >1
                normalize: true, // Trim whitespace inside text nodes
                normalizeTags: true, // Transform tags to lowercase
            },
        })
    );
    let testDataOneItem: string;
    let testDataArray: string;
    let badTestData: string;

    before(async () => {
        // Connect to MQ
        await AMQPConnector.connect();
        // Mount the tested router to the express instance
        app.use("/vehiclepositions", vehiclePositionsRouter);
        app.use((err: any, _req: Request, res: Response, _next: NextFunction) => {
            const error = HTTPErrorHandler.handle(err, log);
            if (error) {
                res.setHeader("Content-Type", "application/json; charset=utf-8");
                res.status(error.error_status || 500).send(error);
            }
        });
        testDataOneItem =
            '\
<?xml version="1.0" encoding="UTF-8"?> \
<m disp="http://77.93.194.81:8716/api"> \
    <spoj lin="999999" alias="155" spoj="1" t="3" sled="2" np="true" zrus="false" lat="50.08323" lng="14.51035" \
        cpoz="11:09:06" po="1" zast="56699" zpoz_prij="426" zpoz_odj="426"> \
    <zast zast="57517" stan="C" prij="" odj="11:00" zpoz_typ="3" zpoz_prij="311" zpoz_odj="311"></zast> \
    <zast zast="56699" stan="A" prij="" odj="11:01" zpoz_typ="3" zpoz_prij="351" zpoz_odj="351"></zast> \
    <zast zast="56699" stan="E" prij="" odj="11:02" zpoz_typ="3" zpoz_prij="426" zpoz_odj="426"></zast> \
    <zast zast="57515" stan="A" prij="" odj="11:04" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast> \
    <zast zast="56700" stan="A" prij="" odj="11:06" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast> \
    <zast zast="57515" stan="B" prij="" odj="11:07" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast> \
    <zast zast="65151" stan="A" prij="" odj="11:09" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast> \
    <zast zast="65163" stan="A" prij="" odj="11:10" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast> \
    <zast zast="57511" stan="B" prij="" odj="11:11" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast> \
    <zast zast="57512" stan="A" prij="" odj="11:13" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast> \
    <zast zast="57513" stan="A" prij="" odj="11:14" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast> \
    <zast zast="57514" stan="B" prij="" odj="11:16" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast> \
    <zast zast="27902" stan="I" prij="" odj="11:18" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast> \
    <zast zast="27902" stan="G" prij="" odj="11:19" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast> \
    <zast zast="56703" stan="C" prij="11:20" odj="" zpoz_typ="0" zpoz_prij="-2147483648" \
        zpoz_odj="-2147483648"></zast> \
    </spoj> \
</m>';
        testDataArray =
            '\
<?xml version="1.0" encoding="UTF-8"?> \
<m disp="http://77.93.194.81:8716/api"> \
    <spoj lin="999999" alias="155" spoj="1" t="3" sled="2" np="true" zrus="false" lat="50.08323" lng="14.51035" \
        cpoz="11:09:06" po="1" zast="56699" zpoz_prij="426" zpoz_odj="426"> \
    <zast zast="57517" stan="C" prij="" odj="11:00" zpoz_typ="3" zpoz_prij="311" zpoz_odj="311"></zast> \
    <zast zast="56699" stan="A" prij="" odj="11:01" zpoz_typ="3" zpoz_prij="351" zpoz_odj="351"></zast> \
    <zast zast="56699" stan="E" prij="" odj="11:02" zpoz_typ="3" zpoz_prij="426" zpoz_odj="426"></zast> \
    <zast zast="57515" stan="A" prij="" odj="11:04" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast> \
    <zast zast="56700" stan="A" prij="" odj="11:06" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast> \
    <zast zast="57515" stan="B" prij="" odj="11:07" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast> \
    <zast zast="65151" stan="A" prij="" odj="11:09" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast> \
    <zast zast="65163" stan="A" prij="" odj="11:10" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast> \
    <zast zast="57511" stan="B" prij="" odj="11:11" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast> \
    <zast zast="57512" stan="A" prij="" odj="11:13" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast> \
    <zast zast="57513" stan="A" prij="" odj="11:14" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast> \
    <zast zast="57514" stan="B" prij="" odj="11:16" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast> \
    <zast zast="27902" stan="I" prij="" odj="11:18" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast> \
    <zast zast="27902" stan="G" prij="" odj="11:19" zpoz_typ="2" zpoz_prij="426" zpoz_odj="426"></zast> \
    <zast zast="56703" stan="C" prij="11:20" odj="" zpoz_typ="0" zpoz_prij="-2147483648" \
        zpoz_odj="-2147483648"></zast> \
    </spoj> \
</m>';
        badTestData =
            '\
<?xml version="1.0" encoding="UTF-8"?> \
<m> \
    <spoj lin="999999" alias="155" spoj="1" t="3" sled="2" np="true" zrus="false" lat="50.08323" lng="14.51035" \
        cpoz="11:09:06" po="1" zast="56699" zpoz_prij="426" zpoz_odj="426"> \
    <zast zast="57517" stan="C" prij="" odj="11:00" zpoz_typ="3" zpoz_prij="311" zpoz_odj="311"></zast> \
    <zast zast="56699" stan="A" prij="" odj="11:01" zpoz_typ="3" zpoz_prij="351" zpoz_odj="351"></zast> \
    </spoj> \
    <spoj lin="999999" alias="155" spoj="1" t="3" sled="2" np="true" zrus="false" lat="50.08323" lng="14.51035" \
        cpoz="11:09:06" po="1" zast="56699" zpoz_prij="426" zpoz_odj="426"> \
    <zast zast="57517" stan="C" prij="" odj="11:00" zpoz_typ="3" zpoz_prij="311" zpoz_odj="311"></zast> \
    <zast stan="A" prij="" odj="11:01" zpoz_typ="3" zpoz_prij="351" zpoz_odj="351"></zast> \
    </spoj> \
</m>';
    });

    it("should respond with 204 to POST /vehiclepositions (one item)", (done) => {
        request(app)
            .post("/vehiclepositions")
            .send(testDataOneItem)
            .set("Content-Type", "text/xml; charset=utf-8")
            .expect(204)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                done();
            });
    });

    it("should respond with 204 to POST /vehiclepositions (array)", (done) => {
        request(app)
            .post("/vehiclepositions")
            .send(testDataArray)
            .set("Content-Type", "text/xml; charset=utf-8")
            .expect(204)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                done();
            });
    });

    it("should respond with 406 to POST /vehiclepositions with bad content type", (done) => {
        request(app)
            .post("/vehiclepositions")
            .send("value=0") // x-www-form-urlencoded upload
            .expect(406)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                done();
            });
    });

    it("should respond with 406 to POST /vehiclepositions with no content type", (done) => {
        request(app)
            .post("/vehiclepositions")
            .send(testDataArray)
            .unset("Content-Type")
            .expect(406)
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                done();
            });
    });
});
