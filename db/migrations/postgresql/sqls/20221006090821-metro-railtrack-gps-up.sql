CREATE TABLE IF NOT EXISTS ropidgtfs_metro_railtrack_gps (
    "track_id" VARCHAR(15) NOT NULL,
    "route_name" CHAR(1) NOT NULL,
    "coordinates" GEOMETRY NOT NULL,
    "gtfs_stop_id" VARCHAR(25),

    -- Audit fields
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),

    CONSTRAINT ropidgtfs_metro_railtrack_gps_pkey PRIMARY KEY (track_id, route_name)
);

CREATE INDEX IF NOT EXISTS ropidgtfs_metro_railtrack_gps_stop_idx ON ropidgtfs_metro_railtrack_gps (gtfs_stop_id);
