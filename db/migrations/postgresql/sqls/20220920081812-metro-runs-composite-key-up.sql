ALTER TABLE vehiclepositions_metro_runs_messages DROP CONSTRAINT "metro_runs_messages_pkey";
ALTER TABLE vehiclepositions_metro_runs_messages DROP COLUMN "id";
ALTER TABLE vehiclepositions_metro_runs_messages
    ADD CONSTRAINT "metro_runs_messages_pkey" PRIMARY KEY (route_name, message_timestamp, train_number, track_id)
