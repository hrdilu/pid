CREATE SCHEMA IF NOT EXISTS tmp;

-- ropid_departures_directions
CREATE SEQUENCE IF NOT EXISTS ropid_departures_directions_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE TABLE IF NOT EXISTS ropid_departures_directions (
    id integer DEFAULT nextval('ropid_departures_directions_id_seq'::regclass) NOT NULL,
    departure_stop_id text NOT NULL,
    next_stop_id_regexp text NOT NULL,
    direction text NOT NULL,
    rule_order integer DEFAULT 0 NOT NULL,
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),

    CONSTRAINT ropid_departures_directions_rules_pkey PRIMARY KEY (id)
);

-- ropid_departures_presets
CREATE SEQUENCE IF NOT EXISTS ropid_departures_presets_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE IF NOT EXISTS ropid_departures_presets (
    id integer DEFAULT nextval('ropid_departures_presets_id_seq'::regclass) NOT NULL,
    route_name character varying(100) NOT NULL,
    api_version smallint NOT NULL,
    route character varying(100) NOT NULL,
    url_query_params character varying(250) NOT NULL,
    note character varying(1000) NOT NULL,
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),

    CONSTRAINT ropid_departures_presets_pkey PRIMARY KEY (id)
);
