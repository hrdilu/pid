alter table ropidgtfs_routes alter column route_type type smallint using route_type::smallint;
alter table ropidgtfs_routes alter column route_type set not null;
alter table if exists ropidgtfs_routes_tmp alter column route_type type smallint using route_type::smallint;
alter table if exists ropidgtfs_routes_tmp alter column route_type set not null;

alter table ropidgtfs_precomputed_trip_schedule alter column route_type type smallint using route_type::smallint;
alter table ropidgtfs_precomputed_trip_schedule alter column route_type set not null;
alter table if exists ropidgtfs_precomputed_trip_schedule_tmp alter column route_type type smallint using route_type::smallint;
alter table if exists ropidgtfs_precomputed_trip_schedule_tmp alter column route_type set not null;

alter table ropidgtfs_precomputed_departures alter column route_type type smallint using route_type::smallint;
alter table ropidgtfs_precomputed_departures alter column route_type set not null;
alter table ropidgtfs_precomputed_departures alter column route_type set default 1700; -- 1700 is a miscelaneous service defined in GTFS spec
alter table if exists ropidgtfs_precomputed_departures_tmp alter column route_type type smallint using route_type::smallint;
alter table if exists ropidgtfs_precomputed_departures_tmp alter column route_type set not null;
alter table if exists ropidgtfs_precomputed_departures_tmp alter column route_type set default 1700;
