DROP INDEX IF EXISTS vehiclepositions_positions_created_at;
DROP INDEX IF EXISTS vehiclepositions_positions_origin_time;
DROP INDEX IF EXISTS vehiclepositions_positions_trips_id;
DROP INDEX IF EXISTS vehiclepositions_positions_updated_at;
DROP TABLE IF EXISTS vehiclepositions_positions;
DROP SEQUENCE IF EXISTS vehiclepositions_positions_id_seq;

DROP INDEX IF EXISTS vehiclepositions_runs_messages_fkey;
DROP TABLE IF EXISTS vehiclepositions_runs_messages;
DROP SEQUENCE IF EXISTS vehiclepositions_runs_messages_id_seq;

DROP INDEX IF EXISTS vehiclepositions_runs_idx;
DROP TABLE IF EXISTS vehiclepositions_runs;

DROP TABLE IF EXISTS vehiclepositions_stops;

DROP TABLE IF EXISTS vehiclepositions_trips;

DROP TABLE IF EXISTS vehiclepositions_vehicle_types;
