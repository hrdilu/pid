ALTER TABLE "vehiclepositions_trips"
  ADD COLUMN "gtfs_trip_short_name" character varying(255);

ALTER TABLE "vehiclepositions_positions" 
    ADD COLUMN "last_stop_name" character varying(255),
    ADD COLUMN "next_stop_name" character varying(255),
    ADD COLUMN "this_stop_name" character varying(255);
