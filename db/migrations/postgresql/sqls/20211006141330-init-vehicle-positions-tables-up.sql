-- vehiclepositions_positions
CREATE SEQUENCE IF NOT EXISTS vehiclepositions_positions_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

CREATE TABLE IF NOT EXISTS vehiclepositions_positions (
    created_at timestamp with time zone,
    delay integer,
    delay_stop_arrival integer,
    delay_stop_departure integer,
    next_stop_id character varying(255),
    shape_dist_traveled numeric,
    is_canceled boolean,
    lat numeric,
    lng numeric,
    origin_time time without time zone,
    origin_timestamp bigint,
    tracking integer,
    trips_id character varying(255),
    create_batch_id bigint,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),
    id bigint DEFAULT nextval('vehiclepositions_positions_id_seq'::regclass) NOT NULL,
    bearing integer,
    cis_last_stop_id integer,
    cis_last_stop_sequence integer,
    last_stop_id character varying(255),
    last_stop_sequence integer,
    next_stop_sequence integer,
    speed integer,
    last_stop_arrival_time bigint,
    last_stop_departure_time bigint,
    next_stop_arrival_time bigint,
    next_stop_departure_time bigint,
    asw_last_stop_id character varying(50),
    state_process character varying(50),
    state_position character varying(50),
    this_stop_id character varying(50),
    this_stop_sequence integer,
    tcp_event character varying(50),
    last_stop_headsign character varying(255),

    CONSTRAINT vehiclepositions_positions_pkey PRIMARY KEY (id)
);

CREATE INDEX IF NOT EXISTS vehiclepositions_positions_created_at ON vehiclepositions_positions USING btree (created_at);
CREATE INDEX IF NOT EXISTS vehiclepositions_positions_origin_time ON vehiclepositions_positions USING btree (origin_time);
CREATE INDEX IF NOT EXISTS vehiclepositions_positions_trips_id ON vehiclepositions_positions USING btree (trips_id);
CREATE INDEX IF NOT EXISTS vehiclepositions_positions_updated_at ON vehiclepositions_positions USING btree (updated_at DESC);

-- vehiclepositions_runs
CREATE TABLE IF NOT EXISTS vehiclepositions_runs (
    id character varying(50) NOT NULL,
    route_id character varying(50) NOT NULL,
    run_number character varying(50) NOT NULL,
    line_short_name character varying(50),
    registration_number character varying(50) NOT NULL,
    msg_start_timestamp timestamp with time zone NOT NULL,
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),
    msg_last_timestamp timestamp with time zone,
    wheelchair_accessible boolean,

    CONSTRAINT vehiclepositions_runs_pkey PRIMARY KEY (id)
);

CREATE INDEX IF NOT EXISTS vehiclepositions_runs_idx ON vehiclepositions_runs USING btree (route_id, run_number, registration_number, msg_start_timestamp);

-- vehiclepositions_runs_messages
CREATE SEQUENCE IF NOT EXISTS vehiclepositions_runs_messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE IF NOT EXISTS vehiclepositions_runs_messages (
    id bigint DEFAULT nextval('vehiclepositions_runs_messages_id_seq'::regclass) NOT NULL,
    runs_id character varying(50) NOT NULL,
    lat double precision,
    lng double precision,
    actual_stop_asw_id character varying(50),
    actual_stop_timestamp_real bigint,
    actual_stop_timestamp_scheduled bigint,
    last_stop_asw_id character varying(50),
    packet_number character varying(50),
    msg_timestamp timestamp with time zone,
    events character varying(50),
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),

    CONSTRAINT vehiclepositions_runs_messages_pkey PRIMARY KEY (id),
    CONSTRAINT vehiclepositions_runs_messages_fkey FOREIGN KEY (runs_id) REFERENCES vehiclepositions_runs(id)
        ON DELETE CASCADE
);

CREATE INDEX IF NOT EXISTS vehiclepositions_runs_messages_fkey ON vehiclepositions_runs_messages USING btree (runs_id);

-- vehiclepositions_stops
CREATE TABLE IF NOT EXISTS vehiclepositions_stops (
    arrival_time time without time zone,
    arrival_timestamp bigint,
    cis_stop_id integer,
    cis_stop_platform_code character varying(255),
    cis_stop_sequence integer NOT NULL,
    created_at timestamp with time zone,
    delay_arrival integer,
    delay_departure integer,
    delay_type integer,
    departure_time time without time zone,
    departure_timestamp bigint,
    updated_at timestamp with time zone,
    trips_id character varying(255) NOT NULL,
    create_batch_id bigint,
    created_by character varying(150),
    update_batch_id bigint,
    updated_by character varying(150),
    arrival_delay_type integer,
    asw_stop_id character varying(50),
    stop_id character varying(50),
    stop_sequence integer,
    arrival_timestamp_real bigint,
    departure_timestamp_real bigint,

    CONSTRAINT vehiclepositions_stops_pkey PRIMARY KEY (cis_stop_sequence, trips_id)
);

-- vehiclepositions_trips
CREATE TABLE IF NOT EXISTS vehiclepositions_trips (
    cis_line_id character varying(50),
    cis_trip_number integer,
    sequence_id integer,
    cis_line_short_name character varying(255),
    created_at timestamp with time zone,
    gtfs_route_id character varying(255),
    gtfs_route_short_name character varying(255),
    gtfs_trip_id character varying(255),
    id character varying(255) NOT NULL,
    updated_at timestamp with time zone,
    start_cis_stop_id integer,
    start_cis_stop_platform_code character varying(255),
    start_time time without time zone,
    start_timestamp bigint,
    vehicle_type_id integer,
    wheelchair_accessible boolean,
    create_batch_id bigint,
    created_by character varying(150),
    update_batch_id bigint,
    updated_by character varying(150),
    agency_name_scheduled character varying(255),
    origin_route_name character varying(255),
    agency_name_real character varying(255),
    vehicle_registration_number integer,
    gtfs_trip_headsign character varying(255),
    start_asw_stop_id character varying(50),
    gtfs_route_type integer,
    gtfs_block_id character varying(255),
    last_position_id bigint,
    is_canceled boolean,
    end_timestamp bigint,

    CONSTRAINT vehiclepositions_trips_pkey PRIMARY KEY (id)
);

-- vehiclepositions_vehicle_types
CREATE TABLE IF NOT EXISTS vehiclepositions_vehicle_types (
    id integer NOT NULL,
    abbreviation text NOT NULL,
    description_cs text NOT NULL,
    description_en text NOT NULL,

    CONSTRAINT vehiclepositions_vehicle_types_pkey PRIMARY KEY (id)
);

INSERT INTO vehiclepositions_vehicle_types ("id", "abbreviation", "description_cs", "description_en") VALUES
    (1, 'Metro', 'metro', 'metro'),
    (2, 'Tram', 'tramvaj', 'tram'),
    (3, 'Bus', 'autobus', 'bus'),
    (4, 'BusReg', 'regionální autobus', 'regional bus'),
    (5, 'BusNoc', 'noční autobus', 'night bus'),
    (6, 'TramNoc', 'noční tramvaj', 'night tram'),
    (7, 'Nahradni', 'náhradní doprava', 'replacement'),
    (8, 'Lanovka', 'lanová dráha', 'cableway'),
    (9, 'Skolni', 'školní spoj', 'school trip'),
    (10, 'Invalid', 'spoj pro lidi s hendikepem', 'trip for people with disabilities'),
    (11, 'Smluvni', 'smluvní spoj', 'contracted trip'),
    (12, 'Lod', 'loď', 'boat'),
    (13, 'Vlak', 'vlak', 'train'),
    (14, 'VlakNAD', 'náhradní autobus za vlak', 'bus replacement for a train'),
    (15, 'NahrTram', 'náhradní tramvaj', 'replacement tram'),
    (16, 'BusRegNoc', 'noční regionální autobus', 'night regional bus'),
    (17, 'Ostatni', 'ostatní', 'other')
ON CONFLICT (id) DO NOTHING;
