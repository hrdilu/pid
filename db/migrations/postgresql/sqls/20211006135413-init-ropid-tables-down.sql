DROP TABLE IF EXISTS ropid_departures_directions;
DROP SEQUENCE IF EXISTS ropid_departures_directions_id_seq;

DROP TABLE IF EXISTS ropid_departures_presets;
DROP SEQUENCE IF EXISTS ropid_departures_presets_id_seq;
