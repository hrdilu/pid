CREATE INDEX IF NOT EXISTS ropidgtfs_stops_stop_name_idx
    ON ropidgtfs_stops_actual USING btree (stop_name);

CREATE INDEX IF NOT EXISTS ropidgtfs_stops_location_type_idx
    ON ropidgtfs_stops_actual USING btree (location_type);
