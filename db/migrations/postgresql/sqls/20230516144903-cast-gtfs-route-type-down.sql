alter table ropidgtfs_precomputed_departures alter column route_type type bpchar(1) using left(route_type::text, 1)::bpchar;
alter table ropidgtfs_precomputed_departures alter column route_type drop default;
alter table ropidgtfs_precomputed_departures alter column route_type drop not null;
alter table if exists ropidgtfs_precomputed_departures_tmp alter column route_type type bpchar(1) using left(route_type::text, 1)::bpchar;
alter table if exists ropidgtfs_precomputed_departures_tmp alter column route_type drop default;
alter table if exists ropidgtfs_precomputed_departures_tmp alter column route_type drop not null;

alter table ropidgtfs_precomputed_trip_schedule alter column route_type type varchar(255) using route_type::varchar;
alter table ropidgtfs_precomputed_trip_schedule alter column route_type drop not null;
alter table if exists ropidgtfs_precomputed_trip_schedule_tmp alter column route_type type varchar(255) using route_type::varchar;
alter table if exists ropidgtfs_precomputed_trip_schedule_tmp alter column route_type drop not null;

alter table ropidgtfs_routes alter column route_type type varchar(255) using route_type::varchar;
alter table ropidgtfs_routes alter column route_type drop not null;
alter table if exists ropidgtfs_routes_tmp alter column route_type type varchar(255) using route_type::varchar;
alter table if exists ropidgtfs_routes_tmp alter column route_type drop not null;
