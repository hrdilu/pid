CREATE TABLE IF NOT EXISTS vehiclepositions_metro_runs_messages (
    "id" SERIAL,
    "route_name" CHAR(1) NOT NULL,
    "message_timestamp" TIMESTAMP WITH TIME ZONE NOT NULL,
    "train_set_number_scheduled" VARCHAR(15) NOT NULL,
    "train_set_number_real" VARCHAR(15) NOT NULL,
    "train_number" VARCHAR(15) NOT NULL,
    "track_id" VARCHAR(15) NOT NULL,
    "delay_origin" SMALLINT NOT NULL,
    "actual_position_timestamp_scheduled" TIMESTAMP WITH TIME ZONE NOT NULL,

    -- Audit fields
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),

    CONSTRAINT metro_runs_messages_pkey PRIMARY KEY (id)
);

COMMENT ON COLUMN vehiclepositions_metro_runs_messages.delay_origin
    IS 'Delay in seconds, positive number means the train is ahead of time';

COMMENT ON COLUMN vehiclepositions_metro_runs_messages.actual_position_timestamp_scheduled
    IS 'Timestamp (message_timestamp) with added delay (delay_origin)';
