INSERT INTO vehiclepositions_vehicle_descriptors (id,state,registration_number,registration_number_index,license_plate,"operator",manufacturer,"type",traction,gtfs_route_type,is_air_conditioned,has_usb_chargers,paint,thumbnail_url,photo_url,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by) VALUES
	 (10,'zar',1712,1,'7AX XXXX','STENBUS','SOR','BN 8.5','autobus',3,true,true,'schéma PID, červeno-bílo-modrá',NULL,NULL,NULL,now(),NULL,NULL,now(),NULL),
	 (11,'zar',1861,2,'4SU XXXX','ČSAD POLKOST','Scania','Citywide LF 12M','autobus',3,true,true,'schéma PID, červeno-bílo-modrá',NULL,NULL,NULL,now(),NULL,NULL,now(),NULL),
	 (12,'zar',8338,1,NULL,'Dopravní podnik hl. m. Prahy','ČKD Tatra','T3R.P','tramvaj',0,false,false,'červeno-krémová',NULL,NULL,NULL,now(),NULL,NULL,now(),NULL),
	 (13,'zar',8338,1,'3SB XXXX','Transdev Střední Čechy','Irisbus','Crossway LE 12M','autobus',3,true,false,'bílá',NULL,NULL,NULL,now(),NULL,NULL,now(),NULL),
	 (14,'zar',9366,1,NULL,'Dopravní podnik hl. m. Prahy','Škoda','15T5','tramvaj',0,true,false,'CVR Tommy Hilfiger – Klenoty Aurum, bílá s modrými a červenými pruhy',NULL,NULL,NULL,now(),NULL,NULL,now(),NULL),
	 (15,'zar',9366,1,'8T0 XXXX','ARRIVA STŘEDNÍ ČECHY','SOR','C 10.5','autobus',3,false,false,'Veolia, červená s šedým pruhem',NULL,NULL,NULL,now(),NULL,NULL,now(),NULL),
	 (16,'zar',8582,2,'5SX XXXX','OAD Kolín','Iveco','Crossway LE LINE 12M','autobus',3,false,false,'schéma PID, šedá s červenými svislými pruhy',NULL,NULL,NULL,now(),NULL,NULL,now(),NULL),
	 (17,'zar',8581,2,'5SU XXXX','OAD Kolín','Iveco','Crossway LE LINE 12M','autobus',3,true,false,'schéma PID, šedá s červenými svislými pruhy',NULL,NULL,NULL,now(),NULL,NULL,now(),NULL);

-- To be deleted by the test
INSERT INTO vehiclepositions_vehicle_descriptors (id,state,registration_number,registration_number_index,license_plate,"operator",manufacturer,"type",traction,gtfs_route_type,is_air_conditioned,has_usb_chargers,paint,thumbnail_url,photo_url,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by) VALUES
	 (18,'zar',1094,1,'5AC XXXX','ČSAD Česká Lípa','Iveco','Crossway LINE 12M','autobus',3,true,false,'bílá',NULL,NULL,NULL,now() - interval '1 day',NULL,NULL,now() - interval '1 day',NULL);
