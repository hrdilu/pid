-- trip 1302_9472_211213
INSERT INTO ropidgtfs_stop_times_actual (arrival_time,arrival_time_seconds,departure_time,departure_time_seconds,drop_off_type,pickup_type,shape_dist_traveled,stop_headsign,stop_id,stop_sequence,trip_id,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by,timepoint) VALUES
	 ('8:02:00',NULL,'8:02:00',NULL,'0','0',0.0,NULL,'U2816Z301',1,'1302_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('8:02:30',NULL,'8:02:30',NULL,'1','1',0.529417,NULL,'T57152',2,'1302_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('8:03:30',NULL,'8:03:30',NULL,'1','1',1.215105,NULL,'T53435',3,'1302_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('8:04:00',NULL,'8:04:00',NULL,'1','1',1.453727,NULL,'U2880Z301',4,'1302_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('8:05:30',NULL,'8:05:30',NULL,'1','1',3.827775,NULL,'T58434',5,'1302_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('8:06:30',NULL,'8:06:30',NULL,'1','1',5.567491,NULL,'U2879Z301',6,'1302_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('8:09:00',NULL,'8:09:30',NULL,'0','0',8.751582,NULL,'U2878Z301',7,'1302_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('8:12:00',NULL,'8:12:30',NULL,'0','0',11.741363,NULL,'U2877Z301',8,'1302_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('8:16:30',NULL,'8:17:30',NULL,'0','0',16.809405,NULL,'U2579Z301',9,'1302_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('8:20:00',NULL,'8:20:00',NULL,'0','0',19.157768,NULL,'U2875Z301',10,'1302_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('8:21:30',NULL,'8:21:30',NULL,'1','1',20.80335,NULL,'T58043',11,'1302_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('8:23:30',NULL,'8:23:30',NULL,'1','1',22.875531,NULL,'T58083',12,'1302_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('8:25:00',NULL,'8:31:00',NULL,'0','0',24.333933,NULL,'U2973Z301',13,'1302_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('8:32:00',NULL,'8:32:00',NULL,'1','1',24.774666,NULL,'T58084',14,'1302_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('8:34:00',NULL,'8:34:00',NULL,'1','1',27.507128,NULL,'U2874Z301',15,'1302_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('8:36:00',NULL,'8:36:30',NULL,'0','0',30.526424,NULL,'U1333Z301',16,'1302_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('8:39:00',NULL,'8:39:00',NULL,'1','1',34.432723,NULL,'U2802Z301',17,'1302_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('8:40:00',NULL,'8:40:00',NULL,'1','1',35.61771,NULL,'U2801Z301',18,'1302_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('8:43:00',NULL,'8:43:00',NULL,'0','0',39.211109,NULL,'U2432Z301',19,'1302_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL);

INSERT INTO ropidgtfs_cis_stop_groups_actual (avg_jtsk_x,avg_jtsk_y,avg_lat,avg_lon,cis,district_code,full_name,idos_category,idos_name,municipality,"name",node,created_at,unique_name,create_batch_id,created_by,update_batch_id,updated_at,updated_by) VALUES
	 (-700084.3,-1037332.5,50.1889763,15.0031519,5453224,'NB','Kamenné Zboží','600003','Kamenné Zboží','Kamenné Zboží','Kamenné Zboží',2874,'2022-03-12 04:55:07.282+00','Kamenné Zboží (vlak)',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-687399.75,-1057319.25,50.0252457,15.21459,5453414,'KO','Kolín','600003','Kolín','Kolín','Kolín',2816,'2022-03-12 04:55:07.282+00','Kolín',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-687478.8,-1057360.5,50.02479,15.2135687,66313,'KO','Kolín,nádraží','301003','Kolín,Nádraží','Kolín','Kolín,nádraží',2816,'2022-03-12 04:55:07.282+00','Kolín,Nádraží',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-687475.438,-1055853.0,50.03824,15.2109661,15050,'KO','Kolín,Soja','301003','Kolín,Soja','Kolín','Kolín,Soja',2880,'2022-03-12 04:55:07.282+00','Kolín,Soja',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-687547.7,-1055956.0,50.03724,15.2101469,5453434,'KO','Kolín-Zálabí','600003','Kolín-Zálabí','Kolín','Kolín-Zálabí',2880,'2022-03-12 04:55:07.282+00','Kolín-Zálabí',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-703088.938,-1037477.5,50.1842041,14.96169,15603,'NB','Kostomlaty nad Labem,Železniční stanice','301003','Kostomlaty n.L.,Žel.st.','Kostomlaty n.L.','Kostomlaty n.L.,Žel.st.',1333,'2022-03-12 04:55:07.282+00','Kostomlaty n.L.,Žel.st.',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-703034.563,-1037291.69,50.18592,14.96211,5453134,'NB','Kostomlaty nad Labem','600003','Kostomlaty n.Labem','Kostomlaty n.L.','Kostomlaty nad Labem',1333,'2022-03-12 04:55:07.282+00','Kostomlaty n.Labem',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-688061.0,-1045656.81,50.1285172,15.1848907,18268,'NB','Libice n.Cidl.,železniční stanice','301003','Libice n.Cidl.,žel.st.','Libice n.Cidl.','Libice n.Cidl.,žel.st.',2877,'2022-03-12 04:55:07.282+00','Libice n.Cidl.,žel.st.',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-688203.9,-1045734.31,50.1276665,15.1830444,5453254,'NB','Libice nad Cidlinou','600003','Libice n.Cidlinou','Libice n.Cidl.','Libice nad Cidlinou',2877,'2022-03-12 04:55:07.282+00','Libice n.Cidlinou',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-711385.9,-1035081.63,50.19587,14.842123,19882,'NB','Lysá nad Labem,železniční stanice','301003','Lysá n.L.,žel.st.','Lysá n.L.','Lysá n.L.,žel.st.',2432,'2022-03-12 04:55:07.282+00','Lysá n.L.,žel.st.',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-711348.4,-1035122.69,50.1955452,14.8427191,5453114,'NB','Lysá nad Labem','600003','Lysá n.Labem','Lysá n.L.','Lysá nad Labem',2432,'2022-03-12 04:55:07.282+00','Lysá n.Labem',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-697007.7,-1037242.94,50.193325,15.04572,5453214,'NB','Nymburk hlavní nádraží','600003','Nymburk hl.n.','Nymburk','Nymburk hl.n.',2973,'2022-03-12 04:55:07.282+00','Nymburk hl.n.',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-697049.7,-1037271.44,50.1930237,15.045188,24309,'NB','Nymburk,hlavní nádraží','301003','Nymburk,hl.nádr.','Nymburk','Nymburk,hl.nádr.',2973,'2022-03-12 04:55:07.282+00','Nymburk,hl.nádr.',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-708021.438,-1036423.31,50.18785,14.89129,5453154,'NB','Ostrá','600003','Ostrá','Ostrá','Ostrá',2801,'2022-03-12 04:55:07.282+00','Ostrá (vlak)',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-692099.4,-1042782.69,50.1495552,15.12376,5453234,'NB','Poděbrady','600003','Poděbrady','Poděbrady','Poděbrady',2579,'2022-03-12 04:55:07.282+00','Poděbrady',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-693513.9,-1040961.75,50.1641769,15.1008844,27148,'NB','Poděbrady,Velké Zboží,železniční zastávka','301003','Poděbrady,Velké Zboží,žel.zast.','Poděbrady','Poděbrady,Velké Zboží,žel.zast.',2875,'2022-03-12 04:55:07.282+00','Poděbrady,Velké Zboží,žel.zast.',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-692060.7,-1042901.75,50.1485367,15.1245079,27137,'NB','Poděbrady,železniční stanice','301003','Poděbrady,žel.st.','Poděbrady','Poděbrady,žel.st.',2579,'2022-03-12 04:55:07.282+00','Poděbrady,žel.st.',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-706859.063,-1036618.94,50.1874657,14.9077845,5453144,'NB','Stratov','600003','Stratov','Stratov','Stratov',2802,'2022-03-12 04:55:07.282+00','Stratov (vlak)',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-693507.75,-1040906.81,50.1646729,15.100872,5453244,'NB','Velké Zboží','600003','Velké Zboží','Poděbrady','Velké Zboží',2875,'2022-03-12 04:55:07.282+00','Velké Zboží',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-687827.3,-1048666.13,50.101944,15.19344,5453314,'KO','Velký Osek','600003','Velký Osek','Velký Osek','Velký Osek',2878,'2022-03-12 04:55:07.282+00','Velký Osek',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-687861.25,-1048649.25,50.10206,15.19294,39059,'KO','Velký Osek,nádraží','301003','Velký Osek,nádraží','Velký Osek','Velký Osek,nádraží',2878,'2022-03-12 04:55:07.282+00','Velký Osek,nádraží',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-687753.75,-1051850.0,50.0736275,15.2000694,5453324,'KO','Veltruby','600003','Veltruby','Veltruby','Veltruby',2879,'2022-03-12 04:55:07.282+00','Veltruby',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL);


-- trip 1309_9472_211213
INSERT INTO ropidgtfs_stop_times_actual (arrival_time,arrival_time_seconds,departure_time,departure_time_seconds,drop_off_type,pickup_type,shape_dist_traveled,stop_headsign,stop_id,stop_sequence,trip_id,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by,timepoint) VALUES
	 ('8:44:00',NULL,'8:44:00',NULL,'0','0',0.0,NULL,'U2432Z301',1,'1309_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('8:47:30',NULL,'8:47:30',NULL,'1','1',4.840683,NULL,'T53117',2,'1309_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('8:49:30',NULL,'8:49:30',NULL,'0','0',7.035025,NULL,'U2214Z301',3,'1309_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('8:51:30',NULL,'8:52:30',NULL,'0','0',8.38719,NULL,'U2349Z301',4,'1309_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('8:54:00',NULL,'8:54:00',NULL,'1','1',10.024104,NULL,'T54807',5,'1309_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('8:55:00',NULL,'8:55:00',NULL,'1','1',11.122879,NULL,'T54805',6,'1309_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('8:57:00',NULL,'8:57:00',NULL,'1','1',14.234702,NULL,'U2281Z301',7,'1309_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('8:58:30',NULL,'8:58:30',NULL,'0','0',16.385635,NULL,'U1093Z301',8,'1309_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('9:00:00',NULL,'9:00:00',NULL,'1','1',17.774068,NULL,'T54817',9,'1309_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('9:01:00',NULL,'9:01:00',NULL,'1','1',17.954951,NULL,'T54827',10,'1309_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('9:04:30',NULL,'9:05:30',NULL,'0','0',20.46122,NULL,'U452Z301',11,'1309_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('9:07:30',NULL,'9:07:30',NULL,'1','1',22.749062,NULL,'T58841',12,'1309_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('9:08:30',NULL,'9:08:30',NULL,'1','1',23.299319,NULL,'T58326',13,'1309_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('9:16:00',NULL,'9:17:00',NULL,'0','0',29.174223,NULL,'U474Z301',14,'1309_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('9:19:00',NULL,'9:19:00',NULL,'1','1',31.306043,NULL,'T58322',15,'1309_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('9:20:00',NULL,'9:20:00',NULL,'1','1',32.546601,NULL,'T58323',16,'1309_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL),
	 ('9:23:00',NULL,'9:23:00',NULL,'0','0',35.292918,NULL,'U142Z301',17,'1309_9472_211213',-1,'2022-03-12 04:55:05.171+00',NULL,-1,'2022-03-12 04:55:05.171+00',NULL,NULL);

INSERT INTO ropidgtfs_cis_stop_groups_actual (avg_jtsk_x,avg_jtsk_y,avg_lat,avg_lon,cis,district_code,full_name,idos_category,idos_name,municipality,"name",node,created_at,unique_name,create_batch_id,created_by,update_batch_id,updated_at,updated_by) VALUES
	 (-718160.6,-1038435.13,50.1579781,14.754261,5454776,'PH','Čelákovice','600003','Čelákovice','Čelákovice','Čelákovice',2349,'2022-03-12 04:55:07.282+00','Čelákovice',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-718260.3,-1038520.0,50.1571045,14.7530346,4354,'PH','Čelákovice,Železniční stanice','301003','Čelákovice,Žel.st.','Čelákovice','Čelákovice,Žel.st.',2349,'2022-03-12 04:55:07.282+00','Čelákovice,Žel.st.',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-717233.3,-1037483.69,50.1675568,14.7653713,5454775,'PH','Čelákovice-Jiřina','600003','Čelákovice-Jiřina','Čelákovice','Čelákovice-Jiřina',2214,'2022-03-12 04:55:07.282+00','Čelákovice-Jiřina',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-741901.75,-1043525.69,50.0840073,14.4347982,55775,'AB','Hlavní nádraží','301003','Hlavní nádraží','Praha','Hlavní nádraží',142,'2022-03-12 04:55:07.282+00','Hlavní nádraží',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	--  (-711385.9,-1035081.63,50.19587,14.842123,19882,'NB','Lysá nad Labem,železniční stanice','301003','Lysá n.L.,žel.st.','Lysá n.L.','Lysá n.L.,žel.st.',2432,'2022-03-12 04:55:07.282+00','Lysá n.L.,žel.st.',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	--  (-711348.4,-1035122.69,50.1955452,14.8427191,5453114,'NB','Lysá nad Labem','600003','Lysá n.Labem','Lysá n.L.','Lysá nad Labem',2432,'2022-03-12 04:55:07.282+00','Lysá n.Labem',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-722871.1,-1040168.19,50.13693,14.6921415,5454806,'PH','Mstětice','600003','Mstětice','Zeleneč','Mstětice',2281,'2022-03-12 04:55:07.282+00','Mstětice',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-728801.4,-1041791.13,50.11536,14.6129665,27925,'AB','Nádraží Horní Počernice','301003','Nádraží Horní Počernice','Praha','Nádraží Horní Počernice',452,'2022-03-12 04:55:07.282+00','Nádraží Horní Počernice',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-736952.0,-1041260.63,50.11023,14.4990454,62886,'AB','Nádraží Vysočany','301003','Nádraží Vysočany','Praha','Nádraží Vysočany',474,'2022-03-12 04:55:07.282+00','Nádraží Vysočany',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-741816.7,-1043640.06,50.0830956,14.4361935,5457076,'AB','Praha hlavní nádraží','600003','Praha hl.n.','Praha','Praha hlavní nádraží',142,'2022-03-12 04:55:07.282+00','Praha hl.n.',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-728801.063,-1041642.0,50.1166878,14.6126928,5454826,'AB','Praha-Horní Počernice','600003','Praha-Hor.Počernice','Praha','Praha-Horní Počernice',452,'2022-03-12 04:55:07.282+00','Praha-Hor.Počernice',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-737039.7,-1041010.31,50.11235,14.4973574,5457316,'AB','Praha-Vysočany','600003','Praha-Vysočany','Praha','Praha-Vysočany',474,'2022-03-12 04:55:07.282+00','Praha-Vysočany',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-736719.7,-1041236.81,50.11072,14.5022182,47183,'AB','Vysočanská','301003','Vysočanská','Praha','Vysočanská',474,'2022-03-12 04:55:07.282+00','Vysočanská',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-724898.7,-1040646.81,50.1302376,14.6649218,5454816,'PH','Zeleneč','600003','Zeleneč','Zeleneč','Zeleneč',1093,'2022-03-12 04:55:07.282+00','Zeleneč (vlak)',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL),
	 (-724949.9,-1040601.5,50.13058,14.6641283,41808,'PH','Zeleneč,Železniční zastávka','301003','Zeleneč,Žel.zast.','Zeleneč','Zeleneč,Žel.zast.',1093,'2022-03-12 04:55:07.282+00','Zeleneč,Žel.zast.',NULL,NULL,NULL,'2022-03-12 04:55:07.282+00',NULL);
