
-- TEST created_batch_id
--
-- 1000000115
--
-- REALTIME with changed headsign
--

INSERT INTO "ropidgtfs_trips_actual"("bikes_allowed","block_id","direction_id","exceptional","route_id","service_id","shape_id","trip_headsign","trip_id","wheelchair_accessible","create_batch_id","created_at","created_by","update_batch_id","updated_at","updated_by","trip_operation_type","trip_short_name")
VALUES
(2,NULL,0,0,E'L115',E'1111111-1',E'L115V1',E'Chodov',E'115_6_201230',1,1000000115,E'2021-07-26 05:35:28.331+02',NULL,-1,E'2021-07-26 05:35:28.331+02',NULL,NULL,NULL),
(2,NULL,0,0,E'L115',E'1111111-1',E'L115V1',E'Chodov',E'115_6_201230_V1',1,1000000115,E'2021-07-26 05:35:28.331+02',NULL,-1,E'2021-07-26 05:35:28.331+02',NULL,NULL,NULL),
(2,NULL,0,0,E'L115',E'1111111-1',E'L115V1',E'Chodov',E'115_6_201230_V2',1,1000000115,E'2021-07-26 05:35:28.331+02',NULL,-1,E'2021-07-26 05:35:28.331+02',NULL,NULL,'115_short'),
(2,NULL,0,0,E'L115',E'1111111-1',E'L115V1',E'Chodov',E'115_6_201230_V3',1,1000000115,E'2021-07-26 05:35:28.331+02',NULL,-1,E'2021-07-26 05:35:28.331+02',NULL,NULL,'115_short');

INSERT INTO "ropidgtfs_stops_actual"("location_type","parent_station","platform_code","stop_id","stop_lat","stop_lon","stop_name","stop_url","wheelchair_boarding","zone_id","create_batch_id","created_at","created_by","update_batch_id","updated_at","updated_by","level_id","stop_code","stop_desc","stop_timezone")
VALUES
(0,NULL,E'A',E'U1131Z1P',50.03554,14.49369,E'Pod Chodovem',NULL,0,E'P',1000000115,E'2021-07-26 05:35:24.266+02',NULL,-1,E'2021-07-26 05:35:24.266+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'A',E'U181Z1P',50.03953,14.49044,E'Chodovec',NULL,0,E'P',1000000115,E'2021-07-26 05:35:24.134+02',NULL,-1,E'2021-07-26 05:35:24.134+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'B',E'U1131Z2P',50.03568,14.49417,E'Pod Chodovem',NULL,0,E'P',1000000115,E'2021-07-26 05:35:24.266+02',NULL,-1,E'2021-07-26 05:35:24.266+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'B',E'U52Z2P',50.03077,14.49146,E'Chodov',NULL,0,E'P',1000000115,E'2021-07-26 05:35:24.113+02',NULL,-1,E'2021-07-26 05:35:24.113+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'A',E'U1132Z1P',50.04108,14.49525,E'Městský archiv',NULL,0,E'P',1000000115,E'2021-07-26 05:35:24.266+02',NULL,-1,E'2021-07-26 05:35:24.266+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'B',E'U1334Z2P',50.04258,14.49285,E'Blažimská',NULL,0,E'P',1000000115,E'2021-07-26 05:35:24.28+02',NULL,-1,E'2021-07-26 05:35:24.28+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'D',E'U52Z4P',50.03088,14.49175,E'Chodov',NULL,0,E'P',1000000115,E'2021-07-26 05:35:24.113+02',NULL,-1,E'2021-07-26 05:35:24.113+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'A',E'U1341Z1P',50.04137,14.50013,E'Knovízská',NULL,0,E'P',1000000115,E'2021-07-26 05:35:24.281+02',NULL,-1,E'2021-07-26 05:35:24.281+02',NULL,NULL,NULL,NULL,NULL)
ON CONFLICT DO NOTHING;

INSERT INTO "ropidgtfs_stop_times_actual"("arrival_time","arrival_time_seconds","departure_time","departure_time_seconds","drop_off_type","pickup_type","shape_dist_traveled","stop_headsign","stop_id","stop_sequence","trip_id","create_batch_id","created_at","created_by","update_batch_id","updated_at","updated_by","timepoint")
VALUES
(E'10:45:00',NULL,E'10:45:00',NULL,E'0',E'0',0,E'Městský archiv',E'U52Z4P',1,E'115_6_201230',1000000115,E'2021-07-26 05:36:31.28+02',NULL,-1,E'2021-07-26 05:36:31.28+02',NULL,NULL),
(E'10:47:00',NULL,E'10:47:00',NULL,E'3',E'3',0.78376,E'Městský archiv',E'U1131Z2P',2,E'115_6_201230',1000000115,E'2021-07-26 05:36:31.28+02',NULL,-1,E'2021-07-26 05:36:31.28+02',NULL,NULL),
(E'10:49:00',NULL,E'10:49:00',NULL,E'3',E'3',1.70808,E'Městský archiv',E'U1341Z1P',3,E'115_6_201230',1000000115,E'2021-07-26 05:36:31.28+02',NULL,-1,E'2021-07-26 05:36:31.28+02',NULL,NULL),
(E'10:51:00',NULL,E'10:51:00',NULL,E'3',E'3',2.53184,NULL,E'U1132Z1P',4,E'115_6_201230',1000000115,E'2021-07-26 05:36:31.28+02',NULL,-1,E'2021-07-26 05:36:31.28+02',NULL,NULL),
(E'10:52:00',NULL,E'10:52:00',NULL,E'3',E'3',2.81847,NULL,E'U1334Z2P',5,E'115_6_201230',1000000115,E'2021-07-26 05:36:31.28+02',NULL,-1,E'2021-07-26 05:36:31.28+02',NULL,NULL),
(E'10:55:00',NULL,E'10:55:00',NULL,E'0',E'0',3.34135,NULL,E'U181Z1P',6,E'115_6_201230',1000000115,E'2021-07-26 05:36:31.281+02',NULL,-1,E'2021-07-26 05:36:31.281+02',NULL,NULL),
(E'10:56:00',NULL,E'10:56:00',NULL,E'3',E'3',3.91433,NULL,E'U1131Z1P',7,E'115_6_201230',1000000115,E'2021-07-26 05:36:31.281+02',NULL,-1,E'2021-07-26 05:36:31.281+02',NULL,NULL),
(E'10:58:00',NULL,E'10:58:00',NULL,E'0',E'0',4.87451,NULL,E'U52Z2P',8,E'115_6_201230',1000000115,E'2021-07-26 05:36:31.281+02',NULL,-1,E'2021-07-26 05:36:31.281+02',NULL,NULL);

INSERT INTO "ropidgtfs_stop_times_actual"("arrival_time","arrival_time_seconds","departure_time","departure_time_seconds","drop_off_type","pickup_type","shape_dist_traveled","stop_headsign","stop_id","stop_sequence","trip_id","create_batch_id","created_at","created_by","update_batch_id","updated_at","updated_by","timepoint")
VALUES
(E'10:45:00',NULL,E'10:45:00',NULL,E'0',E'0',0,E'Městský archiv',E'U52Z4P',1,E'115_6_201230_V1',1000000115,E'2021-07-26 05:36:31.28+02',NULL,-1,E'2021-07-26 05:36:31.28+02',NULL,NULL),
(E'10:47:00',NULL,E'10:47:00',NULL,E'3',E'3',0.78376,E'Městský archiv',E'U1131Z2P',2,E'115_6_201230_V1',1000000115,E'2021-07-26 05:36:31.28+02',NULL,-1,E'2021-07-26 05:36:31.28+02',NULL,NULL),
(E'10:49:00',NULL,E'10:49:00',NULL,E'3',E'3',1.70808,E'Městský archiv',E'U1341Z1P',3,E'115_6_201230_V1',1000000115,E'2021-07-26 05:36:31.28+02',NULL,-1,E'2021-07-26 05:36:31.28+02',NULL,NULL),
(E'10:51:00',NULL,E'10:51:00',NULL,E'3',E'3',2.53184,NULL,E'U1132Z1P',4,E'115_6_201230_V1',1000000115,E'2021-07-26 05:36:31.28+02',NULL,-1,E'2021-07-26 05:36:31.28+02',NULL,NULL),
(E'10:52:00',NULL,E'10:52:00',NULL,E'3',E'3',2.81847,NULL,E'U1334Z2P',5,E'115_6_201230_V1',1000000115,E'2021-07-26 05:36:31.28+02',NULL,-1,E'2021-07-26 05:36:31.28+02',NULL,NULL),
(E'10:55:00',NULL,E'10:55:00',NULL,E'0',E'0',3.34135,NULL,E'U181Z1P',6,E'115_6_201230_V1',1000000115,E'2021-07-26 05:36:31.281+02',NULL,-1,E'2021-07-26 05:36:31.281+02',NULL,NULL),
(E'10:56:00',NULL,E'10:56:00',NULL,E'3',E'3',3.91433,NULL,E'U1131Z1P',7,E'115_6_201230_V1',1000000115,E'2021-07-26 05:36:31.281+02',NULL,-1,E'2021-07-26 05:36:31.281+02',NULL,NULL),
(E'10:58:00',NULL,E'10:58:00',NULL,E'0',E'0',4.87451,NULL,E'U52Z2P',8,E'115_6_201230_V1',1000000115,E'2021-07-26 05:36:31.281+02',NULL,-1,E'2021-07-26 05:36:31.281+02',NULL,NULL);

INSERT INTO "ropidgtfs_stop_times_actual"("arrival_time","arrival_time_seconds","departure_time","departure_time_seconds","drop_off_type","pickup_type","shape_dist_traveled","stop_headsign","stop_id","stop_sequence","trip_id","create_batch_id","created_at","created_by","update_batch_id","updated_at","updated_by","timepoint")
VALUES
(E'10:45:00',NULL,E'10:45:00',NULL,E'0',E'0',0,E'Městský archiv',E'U52Z4P',1,E'115_6_201230_V2',1000000115,E'2021-07-26 05:36:31.28+02',NULL,-1,E'2021-07-26 05:36:31.28+02',NULL,NULL),
(E'10:47:00',NULL,E'10:47:00',NULL,E'3',E'3',0.78376,E'Městský archiv',E'U1131Z2P',2,E'115_6_201230_V2',1000000115,E'2021-07-26 05:36:31.28+02',NULL,-1,E'2021-07-26 05:36:31.28+02',NULL,NULL),
(E'10:49:00',NULL,E'10:49:00',NULL,E'3',E'3',1.70808,E'Městský archiv',E'U1341Z1P',3,E'115_6_201230_V2',1000000115,E'2021-07-26 05:36:31.28+02',NULL,-1,E'2021-07-26 05:36:31.28+02',NULL,NULL),
(E'10:51:00',NULL,E'10:51:00',NULL,E'3',E'3',2.53184,NULL,E'U1132Z1P',4,E'115_6_201230_V2',1000000115,E'2021-07-26 05:36:31.28+02',NULL,-1,E'2021-07-26 05:36:31.28+02',NULL,NULL),
(E'10:52:00',NULL,E'10:52:00',NULL,E'3',E'3',2.81847,NULL,E'U1334Z2P',5,E'115_6_201230_V2',1000000115,E'2021-07-26 05:36:31.28+02',NULL,-1,E'2021-07-26 05:36:31.28+02',NULL,NULL),
(E'10:55:00',NULL,E'10:55:00',NULL,E'0',E'0',3.34135,NULL,E'U181Z1P',6,E'115_6_201230_V2',1000000115,E'2021-07-26 05:36:31.281+02',NULL,-1,E'2021-07-26 05:36:31.281+02',NULL,NULL),
(E'10:56:00',NULL,E'10:56:00',NULL,E'3',E'3',3.91433,NULL,E'U1131Z1P',7,E'115_6_201230_V2',1000000115,E'2021-07-26 05:36:31.281+02',NULL,-1,E'2021-07-26 05:36:31.281+02',NULL,NULL),
(E'10:58:00',NULL,E'10:58:00',NULL,E'0',E'0',4.87451,NULL,E'U52Z2P',8,E'115_6_201230_V2',1000000115,E'2021-07-26 05:36:31.281+02',NULL,-1,E'2021-07-26 05:36:31.281+02',NULL,NULL);


INSERT INTO "ropidgtfs_stop_times_actual"("arrival_time","arrival_time_seconds","departure_time","departure_time_seconds","drop_off_type","pickup_type","shape_dist_traveled","stop_headsign","stop_id","stop_sequence","trip_id","create_batch_id","created_at","created_by","update_batch_id","updated_at","updated_by","timepoint")
VALUES
(E'10:45:00',NULL,E'10:45:00',NULL,E'0',E'0',0,E'Městský archiv',E'U52Z4P',1,E'115_6_201230_V3',1000000115,E'2021-07-26 05:36:31.28+02',NULL,-1,E'2021-07-26 05:36:31.28+02',NULL,NULL),
(E'10:47:00',NULL,E'10:47:00',NULL,E'3',E'3',0.78376,E'Městský archiv',E'U1131Z2P',2,E'115_6_201230_V3',1000000115,E'2021-07-26 05:36:31.28+02',NULL,-1,E'2021-07-26 05:36:31.28+02',NULL,NULL),
(E'10:49:00',NULL,E'10:49:00',NULL,E'3',E'3',1.70808,E'Městský archiv',E'U1341Z1P',3,E'115_6_201230_V3',1000000115,E'2021-07-26 05:36:31.28+02',NULL,-1,E'2021-07-26 05:36:31.28+02',NULL,NULL),
(E'10:51:00',NULL,E'10:51:00',NULL,E'3',E'3',2.53184,NULL,E'U1132Z1P',4,E'115_6_201230_V3',1000000115,E'2021-07-26 05:36:31.28+02',NULL,-1,E'2021-07-26 05:36:31.28+02',NULL,NULL),
(E'10:52:00',NULL,E'10:52:00',NULL,E'3',E'3',2.81847,NULL,E'U1334Z2P',5,E'115_6_201230_V3',1000000115,E'2021-07-26 05:36:31.28+02',NULL,-1,E'2021-07-26 05:36:31.28+02',NULL,NULL),
(E'10:55:00',NULL,E'10:55:00',NULL,E'0',E'0',3.34135,NULL,E'U181Z1P',6,E'115_6_201230_V3',1000000115,E'2021-07-26 05:36:31.281+02',NULL,-1,E'2021-07-26 05:36:31.281+02',NULL,NULL),
(E'10:56:00',NULL,E'10:56:00',NULL,E'3',E'3',3.91433,NULL,E'U1131Z1P',7,E'115_6_201230_V3',1000000115,E'2021-07-26 05:36:31.281+02',NULL,-1,E'2021-07-26 05:36:31.281+02',NULL,NULL),
(E'10:58:00',NULL,E'10:58:00',NULL,E'0',E'0',4.87451,NULL,E'U52Z2P',8,E'115_6_201230_V3',1000000115,E'2021-07-26 05:36:31.281+02',NULL,-1,E'2021-07-26 05:36:31.281+02',NULL,NULL);


WITH init_values AS (
    SELECT NOW()::DATE AS now_date,
    make_timestamptz(
        DATE_PART('year', NOW())::INT,
        DATE_PART('month', NOW())::INT,
        DATE_PART('day', NOW())::INT,
        10,41,'0','Europe/Prague'
    ) as start_datetime,
    make_timestamptz(
        DATE_PART('year', NOW())::INT,
        DATE_PART('month', NOW())::INT,
        DATE_PART('day', NOW())::INT,
        10,41,'0','Europe/Prague'
    ) as end_datetime,
    make_timestamptz(
        DATE_PART('year', NOW())::INT,
        DATE_PART('month', NOW())::INT,
        DATE_PART('day', NOW())::INT,
        08,51,'10','Europe/Prague'
    ) as updated_at
)
INSERT INTO "vehiclepositions_trips"("cis_line_id","cis_trip_number","run_number","cis_line_short_name","created_at","gtfs_route_id","gtfs_route_short_name","gtfs_trip_id","id","updated_at","start_cis_stop_id","start_cis_stop_platform_code","start_time","start_timestamp","vehicle_type_id","wheelchair_accessible","create_batch_id","created_by","update_batch_id","updated_by","agency_name_scheduled","origin_route_name","agency_name_real","vehicle_registration_number","gtfs_trip_headsign","start_asw_stop_id","gtfs_route_type","gtfs_block_id","last_position_id","is_canceled","end_timestamp","gtfs_trip_short_name")
VALUES
(E'100115',1061,39,E'115',E'2021-07-26 10:00:12.568+02',E'L115',E'115',E'115_6_201230',E'2021-07-26T08:30:00Z_100115_115_1061',(SELECT updated_at FROM init_values),56714,E'D',E'10:45:00',(SELECT start_datetime FROM init_values),3,TRUE,1000000115,NULL,NULL,NULL,E'ČSAD POLKOST',E'115',E'ČSAD POLKOST',1861,E'Chodov',NULL,3,NULL,1740029522,FALSE,(SELECT end_datetime FROM init_values),NULL),
(E'100115',1061,39,E'115',E'2021-07-26 10:00:12.568+02',E'L115',E'115',E'115_6_201230_V1',E'2021-07-26T08:30:00Z_100115_115_1061_V1',(SELECT updated_at FROM init_values),56714,E'D',E'10:45:00',(SELECT start_datetime FROM init_values),3,TRUE,1000000115,NULL,NULL,NULL,E'ČSAD POLKOST',E'115',E'ČSAD POLKOST',1861,E'Chodov',NULL,3,NULL,1740057206,FALSE,(SELECT end_datetime FROM init_values),NULL),
(E'100115',1061,39,E'115',E'2021-07-26 10:00:12.568+02',E'L115',E'115',E'115_6_201230_V2',E'2021-07-26T08:30:00Z_100115_115_1061_V2',(SELECT updated_at FROM init_values),56714,E'D',E'10:45:00',(SELECT start_datetime FROM init_values),3,TRUE,1000000115,NULL,NULL,NULL,E'ČSAD POLKOST',E'115',E'ČSAD POLKOST',1861,E'Chodov',NULL,3,NULL,1740058453,FALSE,(SELECT end_datetime FROM init_values),'115_short'),
(E'100115',1061,39,E'115',E'2021-07-26 10:00:12.568+02',E'L115',E'115',E'115_6_201230_V3',E'2021-07-26T08:30:00Z_100115_115_1061_V3',(SELECT updated_at FROM init_values),56714,E'D',E'10:45:00',(SELECT start_datetime FROM init_values),3,TRUE,1000000115,NULL,NULL,NULL,E'ČSAD POLKOST',E'115',E'ČSAD POLKOST',1861,E'Chodov',NULL,3,NULL,1740029525,FALSE,(SELECT end_datetime FROM init_values),'115_short');

WITH init_values AS (
    select make_timestamptz(
        DATE_PART('year', NOW())::INT,
        DATE_PART('month', NOW())::INT,
        DATE_PART('day', NOW())::INT,
        10,36,'10','Europe/Prague'
    ) as updated_at
)
INSERT INTO "vehiclepositions_positions"("created_at","delay","delay_stop_arrival","delay_stop_departure","next_stop_id","shape_dist_traveled","is_canceled","lat","lng","origin_time","origin_timestamp","tracking","trips_id","create_batch_id","created_by","update_batch_id","updated_at","updated_by","id","bearing","cis_last_stop_id","cis_last_stop_sequence","last_stop_id","last_stop_sequence","last_stop_name","next_stop_sequence","speed","last_stop_arrival_time","last_stop_departure_time","next_stop_arrival_time","next_stop_departure_time","asw_last_stop_id","state_process","state_position","this_stop_id","this_stop_sequence","tcp_event","last_stop_headsign")
VALUES
(E'2021-07-26 10:30:15.582+02',NULL,NULL,NULL,E'U52Z4P',0,FALSE,50.03082,14.49163,E'10:29:43','2021-07-26T08:29:43.000Z',0,E'2021-07-26T08:30:00Z_100115_115_1061',1000000115,NULL,NULL,(SELECT updated_at FROM init_values),NULL,1740029522,NULL,NULL,NULL,NULL,NULL,NULL,1,6,NULL,NULL,'2021-07-26T08:30:00.000Z','2021-07-26T08:30:00.000Z',NULL,E'processed',E'before_track',NULL,NULL,NULL,'Městský archiv'),
(E'2021-07-26 10:37:31.285+02',94,NULL,88,E'U1132Z1P',2.3,FALSE,50.03897,14.49684,E'10:37:00','2021-07-26T08:37:00.000Z',2,E'2021-07-26T08:30:00Z_100115_115_1061_V1',1000000115,NULL,NULL,(SELECT updated_at FROM init_values),NULL,1740057206,335,59066,3,E'U1341Z1P',3,'Knovízská',4,16,'2021-07-26T08:34:00.000Z','2021-07-26T08:34:00.000Z','2021-07-26T08:36:00.000Z','2021-07-26T08:36:00.000Z',NULL,E'processed',E'on_track',NULL,NULL,NULL,E'Městský archiv'),
(E'2021-07-26 10:37:51.809+02',101,NULL,94,E'U1334Z2P',2.5,FALSE,50.04095,14.49552,E'10:37:34','2021-07-26T08:37:34.000Z',2,E'2021-07-26T08:30:00Z_100115_115_1061_V2',1000000115,NULL,NULL,(SELECT updated_at FROM init_values),NULL,1740058453,332,59113,4,E'U1341Z1P',4,'Knovízská',5,16,'2021-07-26T08:36:00.000Z','2021-07-26T08:36:00.000Z','2021-07-26T08:37:00.000Z','2021-07-26T08:37:00.000Z',NULL,E'processed',E'at_stop',E'U1132Z1P',4,NULL,NULL),
(E'2021-07-26 10:30:15.582+02',60,NULL,NULL,E'U52Z4P',0,FALSE,50.03082,14.49163,E'10:29:43','2021-07-26T08:29:43.000Z',0,E'2021-07-26T08:30:00Z_100115_115_1061_V3',1000000115,NULL,NULL,(SELECT updated_at FROM init_values),NULL,1740029525,NULL,NULL,NULL,NULL,NULL,NULL,1,6,NULL,NULL,'2021-07-26T08:30:00.000Z','2021-07-26T08:30:00.000Z',NULL,E'processed',E'before_track_delayed',NULL,NULL,NULL,'Městský archiv');

-- <<<
