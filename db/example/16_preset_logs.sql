INSERT INTO ropid_departures_presets (route_name,api_version,route,url_query_params,note,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by,is_testing) VALUES
	 ('test-povstani',2,'/pid/departureboards','minutesAfter=60&limit=8&filter=routeHeadingOnceNoGapFill&aswIds=100','Obecná tabule',NULL,now(),NULL,NULL,now(),NULL,false);

INSERT INTO ropid_departures_presets (route_name,api_version,route,url_query_params,note,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by,is_testing) VALUES
	 ('bustec-testovaci',2,'/pid/departureboards','minutesAfter=180&limit=8&mode=departures&filter=routeHeadingOnceNoGapFill&order=real&aswIds=680_3','Testovací tabule pro Bustec (testovací)',NULL,now(),NULL,NULL,now(),NULL,true);

-- To be deleted by the test
INSERT INTO ropid_departures_preset_logs (device_alias,received_at,is_processed,request_url,request_method,request_user_agent,response_status,response_time_ms,create_batch_id,created_at,created_by,update_batch_id,updated_at,updated_by) VALUES
	 ('test-povstani-del',now() - interval '1 day',true,'/pid/test-povstani-del','GET','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/114.0',200,496,NULL,now() - interval '1 day',NULL,NULL,now() - interval '1 day',NULL);
