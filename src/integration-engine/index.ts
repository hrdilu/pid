import { AbstractWorker } from "@golemio/core/dist/integration-engine/workers";
import { PresetWorker } from "./ropid-gtfs/workers/presets/PresetWorker";
import { RailtrackWorker } from "./ropid-gtfs/workers/railtrack/RailtrackWorker";
import { RopidVYMIWorker } from "./ropid-vymi/workers/RopidVYMIWorker";
import { DescriptorWorker, GtfsRealTimeWorker, RunsWorker, VPWorker } from "./vehicle-positions";
import { TimetableWorker } from "./ropid-gtfs/workers/timetables/TimetableWorker";

export * from "./queueDefinitions";
export * as RopidGTFS from "./ropid-gtfs";
export * as RopidVYMI from "./ropid-vymi";
export * as VehiclePositions from "./vehicle-positions";

export const workers: Array<new () => AbstractWorker> = [
    //#region Vehicle Positions
    VPWorker,
    DescriptorWorker,
    RunsWorker,
    GtfsRealTimeWorker,
    //#endregion

    //#region ROPID GTFS
    RailtrackWorker,
    PresetWorker,
    TimetableWorker,
    //#endregion

    //#region ROPID VYMI
    RopidVYMIWorker,
    //#endregion
];
