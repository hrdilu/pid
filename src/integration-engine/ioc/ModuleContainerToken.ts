const ModuleContainerToken = {
    RopidGTFSMetadataModel: Symbol(),
    DeparturePresetsRepository: Symbol(),
    RopidDeparturesPresetsTransformation: Symbol(),
    DeparturePresetsFacade: Symbol(),
    GrafanaLokiDataSourceProvider: Symbol(),
    PresetLogDataSourceFactory: Symbol(),
    PresetLogTransformation: Symbol(),
    PresetLogRepository: Symbol(),
    PresetLogFilter: Symbol(),
    RopidMonitoringService: Symbol(),
};

export { ModuleContainerToken };
