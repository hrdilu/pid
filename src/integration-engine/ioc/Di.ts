import { RopidGTFSMetadataModel } from "#ie/ropid-gtfs/RopidGTFSMetadataModel";
import { DeparturePresetsRepository } from "#ie/ropid-gtfs/data-access/DeparturePresetsRepository";
import { DeparturePresetsFacade } from "#ie/ropid-gtfs/facade/DeparturePresetsFacade";
import { RopidDeparturesPresetsTransformation } from "#ie/ropid-gtfs/transformations/RopidDeparturesPresetsTransformation";
import { PresetLogRepository } from "#ie/ropid-gtfs/workers/presets/data-access/PresetLogRepository";
import { PresetLogDataSourceFactory } from "#ie/ropid-gtfs/workers/presets/datasources/PresetLogDataSourceFactory";
import { GrafanaLokiDataSourceProvider } from "#ie/ropid-gtfs/workers/presets/datasources/grafana-loki/GrafanaLokiDataSourceProvider";
import { LogFilter } from "#ie/ropid-gtfs/workers/presets/helpers/LogFilter";
import { RopidMonitoringService } from "#ie/ropid-gtfs/workers/presets/helpers/RopidMonitoringService";
import { PresetLogTransformation } from "#ie/ropid-gtfs/workers/presets/transformations/PresetLogTransformation";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import { ModuleContainerToken } from "./ModuleContainerToken";

//#region Initialization
const PidContainer: DependencyContainer = IntegrationEngineContainer.createChildContainer();
//#endregion

//#region Datasources
PidContainer.register(ModuleContainerToken.GrafanaLokiDataSourceProvider, GrafanaLokiDataSourceProvider);
PidContainer.registerSingleton(ModuleContainerToken.PresetLogDataSourceFactory, PresetLogDataSourceFactory);
//#endregion

//#region Data Access
PidContainer.register(ModuleContainerToken.RopidGTFSMetadataModel, RopidGTFSMetadataModel);
PidContainer.register(ModuleContainerToken.DeparturePresetsRepository, DeparturePresetsRepository);
PidContainer.register(ModuleContainerToken.PresetLogRepository, PresetLogRepository);
//#endregion

//#region Transformations
PidContainer.register(ModuleContainerToken.RopidDeparturesPresetsTransformation, RopidDeparturesPresetsTransformation);
PidContainer.register(ModuleContainerToken.PresetLogTransformation, PresetLogTransformation);
//#endregion

//#region Helpers
PidContainer.registerSingleton(ModuleContainerToken.DeparturePresetsFacade, DeparturePresetsFacade);
PidContainer.registerSingleton(ModuleContainerToken.PresetLogFilter, LogFilter);
PidContainer.registerSingleton(ModuleContainerToken.RopidMonitoringService, RopidMonitoringService);
//#endregion

export { PidContainer };
