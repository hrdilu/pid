import { IsInt, Min } from "@golemio/core/dist/shared/class-validator";
import { IDataDeletionParams } from "../interfaces/IDataDeletionParams";

export class DataDeletionValidationSchema implements IDataDeletionParams {
    @IsInt()
    @Min(8, { message: "Data newer than 8 hours cannot be deleted" })
    targetHours!: number;
}
