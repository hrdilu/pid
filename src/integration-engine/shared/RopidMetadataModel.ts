import { PG_SCHEMA } from "#sch/const";
import { IRopidSchemaDefinitions } from "#sch/shared";
import { MetadataDto } from "#sch/shared/models/MetadataDto";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import Sequelize from "@golemio/core/dist/shared/sequelize";

export enum MetaTypeEnum {
    DATASET_INFO = "DATASET_INFO",
    STATE = "STATE",
    TABLE_TOTAL_COUNT = "TABLE_TOTAL_COUNT",
    SAVED_ROWS = "SAVED_ROWS",
}

export enum MetaStateEnum {
    DOWNLOADED = "DOWNLOADED",
    SAVED = "SAVED",
}

export enum MetaDatasetInfoKeyEnum {
    LAST_MODIFIED = "last_modified",
    FAILED = "failed",
    DEPLOYED = "deployed",
    NUMBER_OF_RETRIES = "number_of_retries",
    DIGEST = "digest",
}

export class RopidMetadataModel extends PostgresModel implements IModel {
    constructor(protected readonly schemaDefinitions: IRopidSchemaDefinitions) {
        super(
            schemaDefinitions.metadata.name + "Model",
            {
                outputSequelizeAttributes: MetadataDto.attributeModel,
                pgTableName: schemaDefinitions.metadata.pgTableName,
                pgSchema: PG_SCHEMA,
                savingType: "insertOnly",
            },
            new JSONSchemaValidator(schemaDefinitions.metadata.name + "ModelValidator", MetadataDto.jsonSchema)
        );
    }

    public getLastModified = async (dataset: string): Promise<any | null> => {
        try {
            const lastMod = await this.sequelizeModel.findOne({
                order: [["version", "DESC"]],
                where: {
                    dataset,
                    key: MetaDatasetInfoKeyEnum.LAST_MODIFIED,
                    type: MetaTypeEnum.DATASET_INFO,
                },
            });
            return lastMod
                ? {
                      lastModified: lastMod.dataValues ? lastMod.dataValues.value : null,
                      version: lastMod.dataValues ? parseInt(lastMod.dataValues.version, 10) : 1,
                  }
                : {
                      lastModified: null,
                      version: 0,
                  };
        } catch (err) {
            log.warn(err);
            return {
                lastModified: null,
                version: 0,
            };
        }
    };

    public isDeployed = async (dataset: string): Promise<boolean> => {
        try {
            const metaRecord = await this.sequelizeModel.findOne({
                attributes: ["key", "value"],
                where: {
                    dataset,
                    type: MetaTypeEnum.DATASET_INFO,
                },
                order: [
                    ["version", "DESC"],
                    ["id", "DESC"],
                ],
                raw: true,
            });

            return !!(metaRecord && metaRecord.key === MetaDatasetInfoKeyEnum.DEPLOYED && metaRecord.value === "true");
        } catch (err) {
            log.warn(err);
            return false;
        }
    };

    public checkSavedRows = async (dataset: string, version: number): Promise<void> => {
        const meta = await this.getTotalFromMeta(dataset, version);
        const tables = await this.getTotalFromTables(dataset, version);
        if (meta.totalRows !== tables.totalRows || meta.numOfTables !== tables.numOfTables) {
            throw new GeneralError(this.name + ": checkSavedRows() failed.");
        }
    };

    public replaceTmpTables = async (dataset: string, version: number): Promise<boolean> => {
        const connection = PostgresConnector.getConnection();
        const t = await connection.transaction();
        try {
            const tables = await this.sequelizeModel.findAll({
                attributes: [["key", "tn"]],
                transaction: t,
                where: {
                    dataset,
                    type: MetaTypeEnum.STATE,
                    value: MetaStateEnum.SAVED,
                    version,
                },
            });

            const promises = tables.map(async (table) => {
                const pgTableName = this.schemaDefinitions[table.dataValues.tn].pgTableName;
                log.debug(`Replacing tables: processing table ${pgTableName}`);

                const tableName = `"${PG_SCHEMA}"."${pgTableName}"`;
                const tmpTableName = `"tmp"."${pgTableName}"`;

                // use DELETE (ROW EXCLUSIVE lock mode) instead of TRUNCATE (ACCESS EXCLUSIVE lock mode)
                // as we don't want to have conflicts with SELECT queries (ACCESS SHARE lock mode) in output gateway
                const rawReplaceTableQuery = `
                    DELETE FROM ${tableName};
                    INSERT INTO ${tableName} (SELECT * FROM ${tmpTableName});
                    DROP TABLE ${tmpTableName};
                `;

                return connection.query(rawReplaceTableQuery, {
                    type: Sequelize.QueryTypes.RAW,
                    transaction: t,
                });
            });
            await Promise.all(promises);
            log.debug(`Replacing tables: processing finished.`);

            // save meta
            await this.sequelizeModel.create(
                {
                    dataset,
                    key: MetaDatasetInfoKeyEnum.DEPLOYED,
                    type: MetaTypeEnum.DATASET_INFO,
                    value: "true",
                    version,
                },
                { transaction: t }
            );

            await this.sequelizeModel.destroy({
                transaction: t,
                where: {
                    dataset,
                    version: {
                        [Sequelize.Op.and]: [
                            // delete all versions older than ten versions back
                            { [Sequelize.Op.lt]: version - 10 },
                            { [Sequelize.Op.ne]: -1 },
                        ],
                    },
                },
            });
            await t.commit();
            return true;
        } catch (err) {
            log.error(err);
            await t.rollback();
            throw new GeneralError(this.name + ": replaceTmpTables() failed.", undefined, err);
        }
    };

    public rollbackFailedSaving = async (dataset: string, version: number): Promise<any> => {
        await this.save({
            dataset,
            key: MetaDatasetInfoKeyEnum.FAILED,
            type: MetaTypeEnum.DATASET_INFO,
            value: new Date().toISOString(),
            version,
        });

        const numberOfRetries = await this.sequelizeModel.findOne({
            attributes: [["value", "retries"]],
            where: {
                dataset,
                key: MetaDatasetInfoKeyEnum.NUMBER_OF_RETRIES,
                type: MetaTypeEnum.DATASET_INFO,
                version: version - 1,
            },
        });
        if (!numberOfRetries) {
            await this.save({
                dataset,
                key: MetaDatasetInfoKeyEnum.NUMBER_OF_RETRIES,
                type: MetaTypeEnum.DATASET_INFO,
                value: 1,
                version,
            });
        } else {
            await this.save({
                dataset,
                key: MetaDatasetInfoKeyEnum.NUMBER_OF_RETRIES,
                type: MetaTypeEnum.DATASET_INFO,
                value: +numberOfRetries.dataValues.retries + 1,
                version,
            });
        }
    };

    private getTotalFromMeta = async (dataset: string, version: number): Promise<any> => {
        const tables = await this.sequelizeModel.findAll({
            attributes: [["key", "tn"]],
            where: {
                dataset,
                type: MetaTypeEnum.TABLE_TOTAL_COUNT,
                version,
            },
        });
        const result = await this.sequelizeModel.findAll({
            attributes: [[Sequelize.fn("SUM", Sequelize.cast(Sequelize.col("value"), "INTEGER")), "total"]],
            where: {
                dataset,
                type: MetaTypeEnum.TABLE_TOTAL_COUNT,
                version,
            },
        });
        const res = {
            numOfTables: tables.length,
            totalRows: result[0].dataValues.total,
        };
        log.debug(this.name + " Total from metadata: " + JSON.stringify(res));
        return res;
    };

    private getTotalFromTables = async (dataset: string, version: number): Promise<any> => {
        const connection = PostgresConnector.getConnection();
        const tables = await this.sequelizeModel.findAll({
            attributes: [["key", "tn"]],
            where: {
                dataset,
                type: MetaTypeEnum.TABLE_TOTAL_COUNT,
                version,
            },
        });
        const tablesArray: any[] = [];
        tables.map((table) => {
            tablesArray.push("'" + this.schemaDefinitions[table.dataValues.tn].pgTableName + "'");
        });

        // TODO zbavit se raw query
        const result = await connection.query(
            "SELECT SUM(count_rows(table_schema, table_name)) as total " +
                "FROM information_schema.tables " +
                "WHERE " +
                "table_schema NOT IN ('pg_catalog', 'information_schema') " +
                "AND table_type = 'BASE TABLE' " +
                "AND table_name in (" +
                tablesArray.join(",") +
                ") " +
                "AND table_schema = 'tmp'; ",
            { type: Sequelize.QueryTypes.SELECT }
        );

        const res = {
            numOfTables: tables.length,
            totalRows: (result[0] as Record<string, any>).total,
        };
        log.debug(this.name + " Total from tables: " + JSON.stringify(res));
        return res;
    };
}
