import { decode as decodeHtmlEntities } from "html-entities";

export class HTMLUtils {
    public static outputPlainText(html: string | null | undefined): string | null {
        if (!html) return null;
        return decodeHtmlEntities(html).replace(/<[^>]+>/g, "");
    }
}
