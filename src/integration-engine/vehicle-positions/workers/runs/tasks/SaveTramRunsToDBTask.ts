import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { MessageProperties } from "amqplib";
import { CommonRunsRepository } from "../data-access/CommonRunsRepository";
import { CommonMessageProcessor } from "../helpers/CommonMessageProcessor";
import { ICommonRunsInput } from "../interfaces/CommonRunsMessageInterfaces";
import { CommonRunsValidationSchema } from "../schema/CommonRunsSchema";
import { CommonRunsMessagesTransformation } from "../transformations/CommonRunsMessagesTransformation";

export class SaveTramRunsToDBTask extends AbstractTask<ICommonRunsInput> {
    public readonly queueName = "saveTramRunsToDB";
    public readonly queueTtl = 3 * 60 * 1000; // 3 minutes
    public readonly schema = CommonRunsValidationSchema;

    private readonly messagesTransformation: CommonRunsMessagesTransformation;
    private readonly runsRepository: CommonRunsRepository;
    private readonly messageProcessor: CommonMessageProcessor;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.messagesTransformation = new CommonRunsMessagesTransformation();
        this.runsRepository = new CommonRunsRepository();
        this.messageProcessor = new CommonMessageProcessor(this.runsRepository);
    }

    protected async execute(data: ICommonRunsInput, msgProperties?: MessageProperties) {
        const timestamp = msgProperties?.timestamp;
        if (!timestamp) {
            throw new GeneralError(`Missing tram run message timestamp: ${JSON.stringify(data)}`, this.constructor.name);
        }

        const transformedData = await this.messagesTransformation.transform({ data: data.M.V, timestamp }, true);
        for (const element of transformedData) {
            await this.messageProcessor.processTransformedRun(element);
        }
    }
}
