import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { MessageProperties } from "amqplib";
import { CommonRunsRepository } from "../data-access/CommonRunsRepository";
import { BusMessageFilter } from "../helpers/BusMessageFilter";
import { CommonMessageProcessor } from "../helpers/CommonMessageProcessor";
import { IBusMessageFilter } from "../helpers/interfaces/IBusMessageFilter";
import { ICommonRunsInput } from "../interfaces/CommonRunsMessageInterfaces";
import { CommonRunsValidationSchema } from "../schema/CommonRunsSchema";
import { CommonRunsMessagesTransformation } from "../transformations/CommonRunsMessagesTransformation";

export class SaveBusRunsToDBTask extends AbstractTask<ICommonRunsInput> {
    public readonly queueName = "saveBusRunsToDB";
    public readonly queueTtl = 2 * 60 * 1000; // 2 minutes
    public readonly schema = CommonRunsValidationSchema;

    private readonly messagesTransformation: CommonRunsMessagesTransformation;
    private readonly runsRepository: CommonRunsRepository;
    private readonly messageProcessor: CommonMessageProcessor;
    private readonly messageFilter: IBusMessageFilter;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.messagesTransformation = new CommonRunsMessagesTransformation();
        this.runsRepository = new CommonRunsRepository();
        this.messageProcessor = new CommonMessageProcessor(this.runsRepository);
        this.messageFilter = new BusMessageFilter();
    }

    protected async execute(data: ICommonRunsInput, msgProperties?: MessageProperties) {
        const timestamp = msgProperties?.timestamp;
        if (!timestamp) {
            throw new GeneralError(`Missing bus run message timestamp: ${JSON.stringify(data)}`, this.constructor.name);
        }

        const messages = await this.messagesTransformation.transform({ data: data.M.V, timestamp });
        const filteredMessages = this.messageFilter.yieldFilteredMessages(messages);

        for (const runMessage of filteredMessages) {
            await this.messageProcessor.processTransformedRun(runMessage);
        }
    }
}
