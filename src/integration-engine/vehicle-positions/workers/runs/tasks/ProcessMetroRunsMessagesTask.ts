import { RunTripsRedisRepository } from "#ie/ropid-gtfs/data-access/cache";
import { MetroRailtrackGPSRepository } from "#ie/ropid-gtfs/workers/railtrack/tasks/data-access/MetroRailtrackGPSRepository";
import { IAggregatedRailtrackGPSData } from "#ie/ropid-gtfs/workers/railtrack/tasks/interfaces/IAggregatedRailtrackGPSData";
import { PositionsRepository } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/PositionsRepository";
import { TripsRepository } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/TripsRepository";
import { AbstractGTFSTripRunManager } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/gtfs-trip-run/AbstractGTFSTripRunManager";
import {
    GTFSTripRunManagerFactory,
    GTFSTripRunType,
} from "#ie/vehicle-positions/workers/vehicle-positions/helpers/gtfs-trip-run/GTFSTripRunManagerFactory";
import { VehiclePositions } from "#sch/vehicle-positions";
import { AbstractTask, config, QueueManager } from "@golemio/core/dist/integration-engine";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { CommonRunsRepository } from "../data-access/CommonRunsRepository";
import { MetroMessageFilter } from "../helpers/MetroMessageFilter";
import { IProcessMetroRunsMessage, IProcessMetroRunsMessagesInput } from "../interfaces/IProcessMetroRunsMessagesInput";
import { MetroTransformedRunsValidationSchema } from "../schema/MetroTransformedRunsSchema";
import { MetroRunsMessageProcessingTransformation } from "../transformations/MetroRunsMessageProcessingTransformation";

export class ProcessMetroRunsMessagesTask extends AbstractTask<IProcessMetroRunsMessagesInput> {
    public readonly queueName = "processMetroRunMessages";
    public readonly queueTtl = 10 * 60 * 1000; // 10 minutes
    public readonly schema = MetroTransformedRunsValidationSchema;

    private readonly processingTransformation: MetroRunsMessageProcessingTransformation;
    private readonly railtrackGPSRepository: MetroRailtrackGPSRepository;
    private readonly positionsRepository: PositionsRepository;
    private readonly tripsRepository: TripsRepository;
    private readonly runsRepository: CommonRunsRepository;
    private readonly runTripsRedisRepository: RunTripsRedisRepository;
    private readonly gtfsTripRunManager: AbstractGTFSTripRunManager;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.processingTransformation = new MetroRunsMessageProcessingTransformation();
        this.railtrackGPSRepository = new MetroRailtrackGPSRepository();
        this.positionsRepository = new PositionsRepository();
        this.tripsRepository = new TripsRepository();
        this.runsRepository = new CommonRunsRepository();
        this.runTripsRedisRepository = new RunTripsRedisRepository();

        this.gtfsTripRunManager = GTFSTripRunManagerFactory.create(
            GTFSTripRunType.Metro,
            this.positionsRepository,
            this.tripsRepository,
            this.runsRepository,
            this.runTripsRedisRepository
        );
    }

    protected async execute({ routeName, messages }: IProcessMetroRunsMessagesInput) {
        const { filteredMessages, trackIds } = MetroMessageFilter.getValidMessagesWithTrackIds(messages);
        if (filteredMessages.length === 0) {
            return;
        }

        const gpsData = await this.railtrackGPSRepository.findCoordinates(routeName, trackIds);

        for (const message of filteredMessages) {
            await this.processMessage(message, gpsData);
        }
    }

    private async processMessage(message: IProcessMetroRunsMessage, gpsData: IAggregatedRailtrackGPSData | null) {
        const data = await this.processingTransformation.transform({ message, gpsData });
        if (!data) {
            return;
        }

        const scheduledTrips = await this.gtfsTripRunManager.setAndReturnScheduledTrips(data.runSchedule);
        if (scheduledTrips.length === 0) {
            log.verbose(`processMessage: no schedule for metro run: ${JSON.stringify(data.runSchedule)}`);
            return;
        }

        const delayMsg = await this.gtfsTripRunManager.generateDelayMsg(scheduledTrips, data.runInput);
        if (delayMsg.updatedTrips.length > 0) {
            await QueueManager.sendMessageToExchange(
                `${config.RABBIT_EXCHANGE_NAME}.${VehiclePositions.name.toLowerCase()}`,
                "updateDelay",
                delayMsg
            );
        }
    }
}
