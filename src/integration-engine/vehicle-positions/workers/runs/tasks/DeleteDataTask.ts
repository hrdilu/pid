import { IDataDeletionParams } from "#ie/shared/interfaces/IDataDeletionParams";
import { DataDeletionValidationSchema } from "#ie/shared/schema/DataDeletionSchema";
import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import { CommonRunsRepository } from "../data-access/CommonRunsRepository";
import { MetroRunsMessagesRepository } from "../data-access/MetroRunsMessagesRepository";

export class DeleteDataTask extends AbstractTask<IDataDeletionParams> {
    public readonly queueName = "deleteData";
    public readonly queueTtl = 1 * 60 * 60 * 1000; // 1 hour
    public readonly schema = DataDeletionValidationSchema;

    private readonly commonRunsRepository: CommonRunsRepository;
    private readonly metroMessagesRepository: MetroRunsMessagesRepository;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.commonRunsRepository = new CommonRunsRepository();
        this.metroMessagesRepository = new MetroRunsMessagesRepository();
    }

    protected async execute(data: IDataDeletionParams) {
        await this.commonRunsRepository.deleteNHoursOldData(data.targetHours);
        await this.metroMessagesRepository.deleteNHoursOldData(data.targetHours);
    }
}
