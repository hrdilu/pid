import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import { MetroRunsMessagesRepository } from "../data-access/MetroRunsMessagesRepository";
import { IMetroRunsInput } from "../interfaces/MetroRunsMessageInterfaces";
import { MetroRunsValidationSchema } from "../schema/MetroRunsSchema";
import { MetroRunsMessagesTransformation } from "../transformations/MetroRunsMessagesTransformation";

export class SaveMetroRunsToDBTask extends AbstractTask<IMetroRunsInput> {
    public readonly queueName = "saveMetroRunsToDB";
    public readonly queueTtl = 3 * 60 * 1000; // 3 minutes
    public readonly schema = MetroRunsValidationSchema;

    private readonly messagesTransformation: MetroRunsMessagesTransformation;
    private readonly messagesRepository: MetroRunsMessagesRepository;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.messagesTransformation = new MetroRunsMessagesTransformation();
        this.messagesRepository = new MetroRunsMessagesRepository();
    }

    protected async execute(data: IMetroRunsInput) {
        const messages = await this.messagesTransformation.transform(data);
        if (messages.length === 0) {
            return;
        }

        await this.messagesRepository.saveData(messages);
        await QueueManager.sendMessageToExchange(this.queuePrefix, "processMetroRunMessages", {
            routeName: data.m.$.linka,
            messages,
        });
    }
}
