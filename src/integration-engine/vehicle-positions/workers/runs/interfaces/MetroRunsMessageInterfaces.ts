export interface IMetroRunsMessageProperties {
    linka: string;
    tm: string;
    gvd?: string;
}

export interface IMetroRunsTrainProperties {
    csp: string;
    csr: string;
    cv: string;
    ko: string;
    odch: string;
}

export interface IMetroRunsTrainContent {
    $: IMetroRunsTrainProperties;
}

export interface IMetroRunsMessageContent {
    $: IMetroRunsMessageProperties;
    vlak?: IMetroRunsTrainContent | IMetroRunsTrainContent[];
}

export interface IMetroRunsInput {
    m: IMetroRunsMessageContent;
}
