import { IRailtrackCoordinates } from "#ie/ropid-gtfs/workers/railtrack/tasks/interfaces/IAggregatedRailtrackGPSData";

export interface IMetroRunInputForProcessing {
    messageTimestamp: string;
    timestampScheduled: string;
    runNumber: number;
    routeId: string;
    trainSetNumberScheduled: string;
    trainSetNumberReal: string;
    trainNumber: string;
    coordinates: IRailtrackCoordinates;
}
