import { VehiclePositions } from "#sch/vehicle-positions";
import { AbstractWorker } from "@golemio/core/dist/integration-engine/workers";
import { DeleteDataTask } from "./tasks/DeleteDataTask";
import { ProcessMetroRunsMessagesTask } from "./tasks/ProcessMetroRunsMessagesTask";
import { SaveBusRunsToDBTask } from "./tasks/SaveBusRunsToDBTask";
import { SaveMetroRunsToDBTask } from "./tasks/SaveMetroRunsToDBTask";
import { SaveTramRunsToDBTask } from "./tasks/SaveTramRunsToDBTask";

export class RunsWorker extends AbstractWorker {
    protected name = VehiclePositions.name + "Runs";

    constructor() {
        super();

        // Register tasks
        this.registerTask(new SaveBusRunsToDBTask(this.getQueuePrefix()));
        this.registerTask(new SaveTramRunsToDBTask(this.getQueuePrefix()));
        this.registerTask(new SaveMetroRunsToDBTask(this.getQueuePrefix()));
        this.registerTask(new ProcessMetroRunsMessagesTask(this.getQueuePrefix()));
        this.registerTask(new DeleteDataTask(this.getQueuePrefix()));
    }
}
