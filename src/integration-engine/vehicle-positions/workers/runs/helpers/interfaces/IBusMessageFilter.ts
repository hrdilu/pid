import { ICommonRunWithMessageDto } from "#sch/vehicle-positions/models/interfaces/ICommonRunWithMessageDto";

export interface IBusMessageFilter {
    yieldFilteredMessages(messages: ICommonRunWithMessageDto[]): Generator<ICommonRunWithMessageDto>;
}
