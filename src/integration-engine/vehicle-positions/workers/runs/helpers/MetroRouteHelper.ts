export enum MetroRouteName {
    LINE_A = "A",
    LINE_B = "B",
    LINE_C = "C",
}

const metroRouteId: Record<MetroRouteName | string, string | undefined> = {
    [MetroRouteName.LINE_A]: "991",
    [MetroRouteName.LINE_B]: "992",
    [MetroRouteName.LINE_C]: "993",
};

export class MetroRouteHelper {
    public static getRouteId(routeName: MetroRouteName | string): string | undefined {
        return metroRouteId[routeName];
    }
}
