import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { ICommonRunWithMessageDto } from "#sch/vehicle-positions/models/interfaces/ICommonRunWithMessageDto";
import { ILogger } from "@golemio/core/dist/helpers/logger";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc";
import { IBusMessageFilter } from "./interfaces/IBusMessageFilter";

const TROLLEYBUS_ROUTE_ID_MIN = 50;
const TROLLEYBUS_ROUTE_ID_MAX = 69;

export class BusMessageFilter implements IBusMessageFilter {
    private logger: ILogger;

    constructor() {
        this.logger = VPContainer.resolve<ILogger>(ContainerToken.Logger);
    }

    /**
     * Yield messages that are valid for processing (filter out internal bus lines)
     *   - route id is a string of length 3 or more
     *   - route id is a number in range of bus and trolleybus lines
     *     - route id >= 100 (bus) ∪ [50, 69] (trolleybus)
     */
    public *yieldFilteredMessages(messages: ICommonRunWithMessageDto[]): Generator<ICommonRunWithMessageDto> {
        for (const message of messages) {
            const { run } = message;
            if (run.route_id.length > 2) {
                yield message;
                continue;
            }

            const routeId = Number.parseInt(run.route_id);
            if (Number.isNaN(routeId) || routeId < TROLLEYBUS_ROUTE_ID_MIN || routeId > TROLLEYBUS_ROUTE_ID_MAX) {
                this.logger.info(`${this.constructor.name}: route id ${routeId} is invalid or out of range`);
                this.logger.debug(message);
                continue;
            }

            yield message;
        }
    }
}
