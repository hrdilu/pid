import { VehiclePositions } from "#sch/vehicle-positions";
import { ICommonRunWithMessageDto } from "#sch/vehicle-positions/models/interfaces/ICommonRunWithMessageDto";
import { config } from "@golemio/core/dist/integration-engine/config";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import { ArrayNotPublicRegistrationNumbers } from "../../../../../const";
import { CommonRunsRepository } from "../data-access/CommonRunsRepository";

export class CommonMessageProcessor {
    constructor(private readonly runsRepository: CommonRunsRepository) {}

    /**
     * Create/update and process transformed run
     */
    public processTransformedRun = async (element: ICommonRunWithMessageDto): Promise<void> => {
        const record = await this.runsRepository.getRunRecordForUpdate(element.run);

        let outputMsg: ICommonRunWithMessageDto;
        if (record) {
            if (
                !element.run_message.actual_stop_timestamp_scheduled &&
                !ArrayNotPublicRegistrationNumbers.includes(element.run.registration_number)
            ) {
                const lastRecordMessage = await this.runsRepository["runsMessagesRepository"].getLastMessage(record.id);
                if (!lastRecordMessage || !lastRecordMessage.actual_stop_timestamp_scheduled) return;
                element.run_message.actual_stop_timestamp_scheduled = lastRecordMessage.actual_stop_timestamp_scheduled;
            }

            outputMsg = await this.runsRepository.updateAndAssociate(element, record.id);
        } else {
            outputMsg = await this.runsRepository.createAndAssociate(element);
        }

        await QueueManager.sendMessageToExchange(
            `${config.RABBIT_EXCHANGE_NAME}.${VehiclePositions.name.toLowerCase()}`,
            "updateRunsGTFSTripId",
            outputMsg
        );
    };
}
