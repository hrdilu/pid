import { PG_SCHEMA } from "#sch/const";
import { MetroRunsMessagesModel } from "#sch/vehicle-positions/models";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import Sequelize, { Op } from "@golemio/core/dist/shared/sequelize";

export class MetroRunsMessagesRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "MetroRunsMessagesRepository",
            {
                pgTableName: MetroRunsMessagesModel.TABLE_NAME,
                pgSchema: PG_SCHEMA,
                outputSequelizeAttributes: MetroRunsMessagesModel.attributeModel,
                savingType: "insertOnly",
            },
            new JSONSchemaValidator("MetroRunsMessagesRepository", MetroRunsMessagesModel.arrayJsonSchema)
        );
    }

    public saveData = async (data: MetroRunsMessagesModel[], isReturning = false): Promise<MetroRunsMessagesModel[]> => {
        try {
            await this.validate(data);
            return await this.sequelizeModel.bulkCreate<MetroRunsMessagesModel>(data, {
                ignoreDuplicates: true,
                returning: isReturning,
            });
        } catch (err) {
            const exception = new GeneralError(
                `[${this.constructor.name}] Could not save data: ${err.message}`,
                this.constructor.name,
                err
            );

            log.error(exception);
            throw exception;
        }
    };

    public deleteNHoursOldData = async (hours: number): Promise<number> => {
        try {
            return await this.sequelizeModel.destroy({
                where: {
                    created_at: {
                        [Op.lt]: Sequelize.literal(`NOW() - INTERVAL '${hours} HOURS'`),
                    },
                },
            });
        } catch (err) {
            throw new GeneralError("Error while purging old data", this.constructor.name, err);
        }
    };
}
