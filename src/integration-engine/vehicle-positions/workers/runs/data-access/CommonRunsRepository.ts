import { TripScheduleRepository } from "#ie/ropid-gtfs/data-access/precomputed";
import { PG_SCHEMA } from "#sch/const";
import { CommonRunsModel } from "#sch/vehicle-positions/models";
import { ICommonRunWithMessageDto } from "#sch/vehicle-positions/models/interfaces/ICommonRunWithMessageDto";
import { ICommonRunsModel } from "#sch/vehicle-positions/models/interfaces/ICommonRunsModel";
import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import Sequelize, { Op } from "@golemio/core/dist/shared/sequelize";
import { CommonRunsMessagesRepository } from "./CommonRunsMessagesRepository";

export class CommonRunsRepository extends PostgresModel implements IModel {
    /** Model name */
    public name!: string;
    /** The Sequelize Model */
    public sequelizeModel!: Sequelize.ModelCtor<any>;
    /** The Sequelize Model for temporary table */
    protected tmpSequelizeModel!: Sequelize.ModelCtor<any> | null;
    /** Validation helper */
    protected validator!: JSONSchemaValidator;
    /** Type/Strategy of saving the data */
    protected savingType!: "insertOnly" | "insertOrUpdate";
    /** Associated vehiclepositions_runs_messages repository */
    protected runsMessagesRepository: CommonRunsMessagesRepository;
    /** Precomputed scheduled trips model */
    private tripScheduleRepository: TripScheduleRepository;

    constructor() {
        super(
            "CommonRunsRepository",
            {
                pgTableName: CommonRunsModel.TABLE_NAME,
                pgSchema: PG_SCHEMA,
                outputSequelizeAttributes: CommonRunsModel.attributeModel,
                savingType: "insertOnly",
                sequelizeAdditionalSettings: {
                    indexes: [
                        {
                            fields: ["id"],
                            name: "vehiclepositions_runs_pkey",
                        },
                        {
                            fields: ["route_id", "run_number", "registration_number", "msg_start_timestamp"],
                            name: "vehiclepositions_runs_idx",
                        },
                    ],
                },
            },
            new JSONSchemaValidator("CommonRunsRepositoryValidator", CommonRunsModel.jsonSchema)
        );

        this.runsMessagesRepository = new CommonRunsMessagesRepository();
        this.runsMessagesRepository["sequelizeModel"].belongsTo(this.sequelizeModel, {
            foreignKey: "runs_id",
            onDelete: "CASCADE",
        });

        this.sequelizeModel.hasMany(this.runsMessagesRepository["sequelizeModel"], {
            sourceKey: "id",
            foreignKey: "runs_id",
            as: "run_messages",
        });

        this.tripScheduleRepository = new TripScheduleRepository();
    }

    public getRunRecordForUpdate = (run: ICommonRunsModel): Promise<ICommonRunsModel | null> => {
        return this.sequelizeModel.findOne({
            where: {
                route_id: run.route_id,
                run_number: run.run_number,
                registration_number: run.registration_number,
                msg_last_timestamp: {
                    [Op.gt]: Sequelize.literal("NOW() - INTERVAL '3 HOURS'"),
                },
            },
            order: [["msg_last_timestamp", "DESC"]],
            raw: true,
        });
    };

    public createAndAssociate = async ({ run, run_message }: ICommonRunWithMessageDto): Promise<ICommonRunWithMessageDto> => {
        const connection = PostgresConnector.getConnection();
        const t = await connection.transaction();
        try {
            const record = await this.sequelizeModel.create(
                {
                    ...run,
                    run_messages: [run_message],
                },
                {
                    include: {
                        as: "run_messages",
                        model: this.runsMessagesRepository["sequelizeModel"],
                    },
                    transaction: t,
                }
            );
            await t.commit();
            const { run_messages: runMessageResult, ...runResult } = record.get({ plain: true });
            return {
                run: runResult,
                run_message: { ...runMessageResult[0] },
            };
        } catch (err) {
            await t.rollback();
            throw new GeneralError("createAndAssociate: error while saving to database", this.name, err);
        }
    };

    public updateAndAssociate = async ({ run, run_message }: ICommonRunWithMessageDto, runId: string) => {
        const connection = PostgresConnector.getConnection();
        const t = await connection.transaction();
        try {
            const [[, runRecord], runMessageRecord] = await Promise.all([
                this.update(
                    { msg_last_timestamp: run.msg_last_timestamp },
                    { where: { id: runId }, returning: true, plain: true }
                ),
                this.runsMessagesRepository["sequelizeModel"].create({ ...run_message, runs_id: runId }),
            ]);
            await t.commit();
            return {
                run: runRecord.get({ plain: true }),
                run_message: runMessageRecord.get({ plain: true }),
            };
        } catch (err) {
            await t.rollback();
            throw new GeneralError("updateAndAssociate: error while saving to database", this.name, err);
        }
    };

    public deleteNHoursOldData = async (hours: number): Promise<number> => {
        try {
            // ON DELETE CASCADE for the dependent "runs_messages" table
            return await this.sequelizeModel.destroy({
                where: {
                    msg_last_timestamp: {
                        [Op.lt]: Sequelize.literal(`NOW() - INTERVAL '${hours} HOURS'`),
                    },
                },
            });
        } catch (err) {
            throw new GeneralError("Error while purging old data", this.constructor.name, err);
        }
    };

    public getScheduledTrips = (
        origin_route_name: string,
        run_number: string | number,
        msg_last_timestamp: string
    ): Promise<IScheduleDto[]> => {
        return this.tripScheduleRepository.findAll({
            attributes: {
                include: [
                    [Sequelize.cast(Sequelize.col("start_timestamp"), "varchar"), "start_timestamp"],
                    [Sequelize.cast(Sequelize.col("end_timestamp"), "varchar"), "end_timestamp"],
                ],
                exclude: ["run_number", "start_timestamp", "end_timestamp"],
            },
            where: {
                origin_route_name,
                run_number,
                [Sequelize.Op.or]: [
                    Sequelize.literal(`date = '${msg_last_timestamp}'::DATE`),
                    Sequelize.literal(`date = ('${msg_last_timestamp}'::TIMESTAMP - INTERVAL '1 day')::DATE`),
                    Sequelize.literal(`date = ('${msg_last_timestamp}'::TIMESTAMP + INTERVAL '1 day')::DATE`),
                ],
            },
            order: [
                ["date", "ASC"],
                ["min_stop_time", "ASC"],
            ],
        });
    };
}
