import { MetroRunsMessagesModel } from "#sch/vehicle-positions/models";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { IMetroRunsInput, IMetroRunsMessageProperties, IMetroRunsTrainContent } from "../interfaces/MetroRunsMessageInterfaces";

interface IMetroRunsElement {
    messageData: IMetroRunsMessageProperties;
    trainData: IMetroRunsTrainContent;
}

export class MetroRunsMessagesTransformation extends BaseTransformation implements ITransformation {
    public name = "MetroRunMessages";

    public transform = (data: IMetroRunsInput): Promise<MetroRunsMessagesModel[]> => {
        let { $: messageData, vlak: trainData } = data.m;
        if (!trainData) {
            return Promise.resolve([]);
        }

        if (!Array.isArray(trainData)) {
            trainData = [trainData];
        }

        const dto: MetroRunsMessagesModel[] = [];
        for (const trainDataItem of trainData) {
            const dtoItem = this.transformElement({ messageData, trainData: trainDataItem });
            dto.push(dtoItem);
        }

        return Promise.resolve(dto);
    };

    protected transformElement = ({ messageData, trainData }: IMetroRunsElement): MetroRunsMessagesModel => {
        const delayOriginSeconds = parseInt(trainData.$.odch);
        const messageTimestamp = new Date(messageData.tm);
        const scheduledTimestamp = new Date(messageTimestamp);
        // delay is in seconds, positive number means the train is ahead of time
        scheduledTimestamp.setSeconds(scheduledTimestamp.getSeconds() + delayOriginSeconds);

        return {
            route_name: messageData.linka,
            message_timestamp: messageTimestamp,
            train_set_number_scheduled: trainData.$.csp.trimStart(),
            train_set_number_real: trainData.$.csr.trimStart(),
            train_number: trainData.$.cv,
            track_id: trainData.$.ko,
            delay_origin: delayOriginSeconds,
            actual_position_timestamp_scheduled: scheduledTimestamp,
        } as MetroRunsMessagesModel;
    };
}
