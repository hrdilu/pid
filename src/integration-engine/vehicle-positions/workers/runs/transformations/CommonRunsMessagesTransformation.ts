import { ICommonRunWithMessageDto } from "#sch/vehicle-positions/models/interfaces/ICommonRunWithMessageDto";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import moment from "@golemio/core/dist/shared/moment-timezone";
import { ICommonRunsInputData, ICommonRunsInputElement } from "../interfaces/CommonRunsMessageInterfaces";

export class CommonRunsMessagesTransformation extends BaseTransformation implements ITransformation {
    public name = "CommonRunsMessagesTransformation";

    /**
     * Overrides BaseTransformation::transform
     */
    public transform = async (
        { data, timestamp }: ICommonRunsInputData,
        isTram: boolean = false
    ): Promise<ICommonRunWithMessageDto[]> => {
        const now = moment(timestamp).tz("Europe/Prague").format();
        const res: ICommonRunWithMessageDto[] = [];

        if (!Array.isArray(data)) {
            data = [data];
        }
        for (const element of data) {
            try {
                const elementTransformed = await this.transformElement({ data: element, timestamp: now }, isTram);
                res.push(elementTransformed);
            } catch (err) {
                log.verbose(`${this.name}: run has empty string timestamps: ${JSON.stringify(element)}`);
            }
        }
        return res;
    };

    protected transformElement = async (
        element: ICommonRunsInputElement,
        isTram: boolean = false
    ): Promise<ICommonRunWithMessageDto> => {
        const attributes = element.data.$;

        const [route_id, run_number] = attributes.turnus.split("/");

        const uniqueKey = `${route_id}` + `_${run_number}` + `_${attributes.evc}` + `_${element.timestamp}`;

        if (attributes.takt === "" || attributes.tm === "") return Promise.reject();
        if (!isTram && (!attributes.tjr || attributes.tjr === "")) return Promise.reject();

        const actualStopTimestampScheduled = attributes.tjr ? new Date(attributes.tjr + "Z") : undefined;

        return {
            run: {
                id: uniqueKey,
                route_id,
                run_number: run_number,
                line_short_name: attributes.line,
                registration_number: attributes.evc,
                msg_start_timestamp: element.timestamp,
                msg_last_timestamp: element.timestamp,
                wheelchair_accessible: attributes.np === "ano",
            },
            run_message: {
                // id - autoincrement
                // runs_id - associated fk
                lat: +attributes.lat,
                lng: +attributes.lng,
                actual_stop_asw_id: attributes.akt,
                actual_stop_timestamp_real: new Date(attributes.takt + "Z"),
                actual_stop_timestamp_scheduled: actualStopTimestampScheduled,
                last_stop_asw_id: attributes.konc,
                packet_number: attributes.pkt,
                msg_timestamp: moment.utc(attributes.tm).tz("Europe/Prague").format(),
                events: attributes.events,
            },
        };
    };
}
