import { IAggregatedRailtrackGPSData } from "#ie/ropid-gtfs/workers/railtrack/tasks/interfaces/IAggregatedRailtrackGPSData";
import { IVehiclePositionsSchedule } from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/VPInterfaces";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { MetroRouteHelper } from "../helpers/MetroRouteHelper";
import { IMetroRunInputForProcessing } from "../interfaces/IMetroRunInputForProcessing";
import { IProcessMetroRunsMessage } from "../interfaces/IProcessMetroRunsMessagesInput";

interface ITransformationInput {
    message: IProcessMetroRunsMessage;
    gpsData: IAggregatedRailtrackGPSData | null;
}

interface ITransformationOutput {
    runSchedule: IVehiclePositionsSchedule;
    runInput: IMetroRunInputForProcessing;
}

export class MetroRunsMessageProcessingTransformation extends BaseTransformation implements ITransformation {
    public name = "MetroRunMessageProcessing";

    public transform = async ({ message, gpsData }: ITransformationInput): Promise<ITransformationOutput | undefined> => {
        const runSchedule = this.parseRunScheduleFromMessage(message);
        if (!runSchedule) {
            return;
        }

        const runInput = this.parseRunInputFromMessage(message, runSchedule, gpsData);
        if (runInput) {
            return {
                runSchedule,
                runInput,
            };
        }
    };

    protected transformElement = async (): Promise<any> => {};

    private parseRunScheduleFromMessage(message: IProcessMetroRunsMessage): IVehiclePositionsSchedule | undefined {
        const routeId = MetroRouteHelper.getRouteId(message.route_name);
        if (!routeId) {
            log.warn(`parseRunScheduleFromMessage: invalid route name in the message: ${JSON.stringify(message)}`);
            return;
        }

        let runNumber: string;
        if (Number.parseInt(message.train_set_number_scheduled) > 0) {
            runNumber = message.train_set_number_scheduled;
        } else if (Number.parseInt(message.train_set_number_real) > 0) {
            runNumber = message.train_set_number_real;
        } else {
            log.warn(`parseRunScheduleFromMessage: run number cannot be determined from the message: ${JSON.stringify(message)}`);
            return;
        }

        return {
            route_id: routeId,
            run_number: runNumber,
            msg_last_timestamp: message.message_timestamp,
        };
    }

    private parseRunInputFromMessage(
        message: IProcessMetroRunsMessage,
        runSchedule: IVehiclePositionsSchedule,
        gpsData: IAggregatedRailtrackGPSData | null
    ): IMetroRunInputForProcessing | undefined {
        const coordinates = gpsData?.coordinates?.[message.track_id];
        if (!coordinates) {
            log.verbose(`parseRunInputFromMessage: no GPS coordinates for the message: ${JSON.stringify(message)}`);
            return;
        }

        return {
            messageTimestamp: message.message_timestamp,
            timestampScheduled: message.actual_position_timestamp_scheduled,
            routeId: runSchedule.route_id,
            runNumber: Number.parseInt(runSchedule.run_number as string),
            trainSetNumberScheduled: message.train_set_number_scheduled,
            trainSetNumberReal: message.train_set_number_real,
            trainNumber: message.train_number,
            coordinates,
        };
    }
}
