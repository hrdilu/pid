import { Type } from "@golemio/core/dist/shared/class-transformer";
import { IsArray, IsISO8601, IsNumber, IsObject, IsString, ValidateNested } from "@golemio/core/dist/shared/class-validator";
import { IProcessMetroRunsMessage, IProcessMetroRunsMessagesInput } from "../interfaces/IProcessMetroRunsMessagesInput";

class MetroTransformedRunMessagesValidationSchema implements IProcessMetroRunsMessage {
    @IsString()
    route_name!: string;

    @IsISO8601()
    message_timestamp!: string;

    @IsString()
    train_set_number_scheduled!: string;

    @IsString()
    train_set_number_real!: string;

    @IsString()
    train_number!: string;

    @IsString()
    track_id!: string;

    @IsNumber()
    delay_origin!: number;

    @IsISO8601()
    actual_position_timestamp_scheduled!: string;
}

export class MetroTransformedRunsValidationSchema implements IProcessMetroRunsMessagesInput {
    @IsString()
    routeName!: string;

    @IsArray()
    @IsObject({ each: true })
    @ValidateNested()
    @Type(() => MetroTransformedRunMessagesValidationSchema)
    messages!: IProcessMetroRunsMessage[];
}
