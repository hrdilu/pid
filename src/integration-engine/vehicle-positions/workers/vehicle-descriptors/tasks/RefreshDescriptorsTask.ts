import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { ILogger } from "@golemio/core/dist/helpers";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc/ContainerToken";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { WORKER_NAME } from "../constants";
import { DescriptorDataSourceFactory, DescriptorProvider } from "../datasources/DescriptorDataSourceFactory";
import { DescriptorTransformation } from "../transformations/DescriptorTransformation";
import { DescriptorRepository } from "../data-access/DescriptorRepository";

@injectable()
export class RefreshDescriptorsTask extends AbstractEmptyTask {
    public readonly queueName = "refreshDescriptors";
    public readonly queueTtl = 10 * 60 * 1000; // 10 minutes

    constructor(
        @inject(VPContainerToken.DescriptorDataSourceFactory)
        private dataSourceFactory: DescriptorDataSourceFactory,
        @inject(VPContainerToken.DescriptorRepository)
        private descriptorRepository: DescriptorRepository,
        @inject(VPContainerToken.DescriptorTransformation)
        private descriptorTransformation: DescriptorTransformation,
        @inject(ContainerToken.Logger) private logger: ILogger
    ) {
        super(WORKER_NAME);
    }

    protected async execute(): Promise<void> {
        const dataSource = this.dataSourceFactory.getDataSource(DescriptorProvider.SeznamAutobusu);
        const descriptorsResult = await dataSource.getAll();
        const descriptorsTransformed = await this.descriptorTransformation.transform(descriptorsResult);

        if (descriptorsTransformed.length === 0) {
            this.logger.warn(`${this.queueName}: no vehicle descriptors out of ${descriptorsResult.length} were transformed`);
            return;
        }

        await this.descriptorRepository.saveData(descriptorsTransformed);
        this.logger.info(
            `${this.queueName}: ${descriptorsTransformed.length}/${descriptorsResult.length} vehicle descriptors were saved`
        );
    }
}
