import { IDataDeletionParams } from "#ie/shared/interfaces/IDataDeletionParams";
import { DataDeletionValidationSchema } from "#ie/shared/schema/DataDeletionSchema";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { WORKER_NAME } from "../constants";
import { DescriptorRepository } from "../data-access/DescriptorRepository";

@injectable()
export class DeleteDataTask extends AbstractTask<IDataDeletionParams> {
    public readonly queueName = "deleteData";
    public readonly queueTtl = 10 * 60 * 1000; // 10 minutes
    public readonly schema = DataDeletionValidationSchema;

    constructor(
        @inject(VPContainerToken.DescriptorRepository)
        private descriptorRepository: DescriptorRepository
    ) {
        super(WORKER_NAME);
    }

    protected async execute(data: IDataDeletionParams) {
        await this.descriptorRepository.deleteNHoursOldData(data.targetHours);
    }
}
