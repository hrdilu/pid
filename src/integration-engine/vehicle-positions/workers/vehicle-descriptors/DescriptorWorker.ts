import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { AbstractTask, AbstractWorker } from "@golemio/core/dist/integration-engine";
import { WORKER_NAME } from "./constants";
import { DeleteDataTask } from "./tasks/DeleteDataTask";
import { RefreshDescriptorsTask } from "./tasks/RefreshDescriptorsTask";

export class DescriptorWorker extends AbstractWorker {
    protected name = WORKER_NAME;

    constructor() {
        super();

        // Register tasks
        this.registerTask(VPContainer.resolve<RefreshDescriptorsTask>(VPContainerToken.RefreshDescriptorsTask));
        this.registerTask(VPContainer.resolve<DeleteDataTask>(VPContainerToken.DeleteDataTask));
    }

    public registerTask = (task: AbstractTask<any>): void => {
        super.registerTask(task);
        task.queuePrefix = this.getQueuePrefix();
    };
}
