import { PG_SCHEMA } from "#sch/const";
import { DescriptorModel } from "#sch/vehicle-descriptors/models";
import { IDescriptorOutputDto } from "#sch/vehicle-descriptors/models/interfaces";
import { ILogger } from "@golemio/core/dist/helpers";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc/ContainerToken";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import Sequelize, { ModelStatic, Op } from "@golemio/core/dist/shared/sequelize";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class DescriptorRepository extends PostgresModel implements IModel {
    private static REPOSITORY_NAME = "VPDescriptorRepository";
    public sequelizeModel!: ModelStatic<DescriptorModel>;

    constructor(@inject(ContainerToken.Logger) private logger: ILogger) {
        super(
            DescriptorRepository.REPOSITORY_NAME,
            {
                pgTableName: DescriptorModel.tableName,
                pgSchema: PG_SCHEMA,
                outputSequelizeAttributes: DescriptorModel.attributeModel,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(DescriptorRepository.REPOSITORY_NAME + "Validator", DescriptorModel.arrayJsonSchema)
        );
    }

    public saveData = async (data: IDescriptorOutputDto[], isReturning = false): Promise<DescriptorModel[]> => {
        try {
            await this.validate(data);
            return await this.sequelizeModel.bulkCreate(data, {
                updateOnDuplicate: DescriptorModel.updateAttributes,
                returning: isReturning,
            });
        } catch (err) {
            const exception = new GeneralError(
                `[${DescriptorRepository.REPOSITORY_NAME}] Could not save data: ${err.message}`,
                this.constructor.name,
                err
            );

            this.logger.error(exception);
            throw exception;
        }
    };

    public deleteNHoursOldData = async (hours: number): Promise<number> => {
        try {
            return await this.sequelizeModel.destroy({
                where: {
                    updated_at: {
                        [Op.lt]: Sequelize.literal(`NOW() - INTERVAL '${hours} HOURS'`),
                    },
                },
            });
        } catch (err) {
            throw new GeneralError("Error while purging old data", this.constructor.name, err);
        }
    };
}
