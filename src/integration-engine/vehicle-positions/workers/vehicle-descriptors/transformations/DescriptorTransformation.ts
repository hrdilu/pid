import { IVehicleListInfo } from "#sch/vehicle-descriptors/datasources/interfaces";
import { IDescriptorOutputDto } from "#sch/vehicle-descriptors/models/interfaces";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";
import { VehicleDescriptorStateEnum, VehicleDescriptorTractionEnum } from "src/helpers/VehicleDescriptorEnums";

@injectable()
export class DescriptorTransformation extends BaseTransformation implements ITransformation {
    public name = "VPDescriptorTransformation";

    public transform = (data: IVehicleListInfo[]): Promise<IDescriptorOutputDto[]> => {
        const dto: IDescriptorOutputDto[] = [];
        for (const dataItem of data) {
            const dtoItem = this.transformElement(dataItem);
            if (dtoItem) {
                dto.push(dtoItem);
            }
        }

        return Promise.resolve(dto);
    };

    protected transformElement = (dataItem: IVehicleListInfo): IDescriptorOutputDto | void => {
        if (!this.isValidState(dataItem.state) || !this.isValidTraction(dataItem.traction)) {
            return;
        }

        return {
            id: dataItem.id,
            state: dataItem.state,
            registration_number: parseInt(dataItem.number),
            registration_number_index: dataItem.numberIndex,
            license_plate: dataItem.plate,
            operator: dataItem.operator,
            manufacturer: dataItem.manufacturer,
            type: dataItem.type,
            traction: dataItem.traction,
            gtfs_route_type: this.mapTractionToRouteType(dataItem.traction),
            is_air_conditioned: !!dataItem.airCondition,
            has_usb_chargers: !!dataItem.usbChargers,
            paint: dataItem.paint,
            thumbnail_url: dataItem.thumbnailUrl?.length === 0 ? null : dataItem.thumbnailUrl,
            photo_url: dataItem.photoUrl?.length === 0 ? null : dataItem.photoUrl,
        };
    };

    private isValidState = (state: string): state is VehicleDescriptorStateEnum => {
        return Object.values(VehicleDescriptorStateEnum).includes(state as VehicleDescriptorStateEnum);
    };

    private isValidTraction = (traction: string): traction is VehicleDescriptorTractionEnum => {
        return Object.values(VehicleDescriptorTractionEnum).includes(traction as VehicleDescriptorTractionEnum);
    };

    private mapTractionToRouteType = (traction: VehicleDescriptorTractionEnum): GTFSRouteTypeEnum => {
        switch (traction) {
            case VehicleDescriptorTractionEnum.Bus:
                return GTFSRouteTypeEnum.BUS;
            case VehicleDescriptorTractionEnum.ElectricBus:
                return GTFSRouteTypeEnum.BUS;
            case VehicleDescriptorTractionEnum.TrolleyBus:
                return GTFSRouteTypeEnum.TROLLEYBUS;
            case VehicleDescriptorTractionEnum.Tram:
                return GTFSRouteTypeEnum.TRAM;
            default:
                throw new GeneralError(`${this.name}: unknown traction: ${traction}`, this.constructor.name);
        }
    };
}
