import { IDataSourceProvider } from "#ie/shared/datasources/IDataSourceProvider";
import { seznamAutobusuJsonSchema } from "#sch/vehicle-descriptors/datasources";
import { IVehicleListInfo } from "#sch/vehicle-descriptors/datasources/interfaces";
import { DataSource, HTTPProtocolStrategy, IDataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { IConfiguration } from "@golemio/core/dist/integration-engine/config/abstract";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc/ContainerToken";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class SeznamAutobusuDataSourceProvider implements IDataSourceProvider<IVehicleListInfo[]> {
    private static DATASOURCE_NAME = "SeznamAutobusuDataSource";

    constructor(@inject(ContainerToken.Config) private config: IConfiguration) {}

    public getDataSource(): IDataSource<IVehicleListInfo[]> {
        return new DataSource<IVehicleListInfo[]>(
            SeznamAutobusuDataSourceProvider.DATASOURCE_NAME,
            this.getProtocolStrategy(),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(SeznamAutobusuDataSourceProvider.DATASOURCE_NAME + "Validator", seznamAutobusuJsonSchema)
        );
    }

    private getProtocolStrategy(): HTTPProtocolStrategy {
        return new HTTPProtocolStrategy({
            method: "GET",
            url: this.config.datasources.SeznamAutobusuApiUrl,
            headers: this.config.datasources.SeznamAutobusuApiHeaders,
            timeout: 20000, // 20 seconds
        });
    }
}
