import { IDataSourceProvider } from "#ie/shared/datasources/IDataSourceProvider";
import { VPContainerToken } from "#ie/vehicle-positions/ioc/VPContainerToken";
import { IVehicleListInfo } from "#sch/vehicle-descriptors/datasources/interfaces";
import { IDataSource } from "@golemio/core/dist/integration-engine";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

export enum DescriptorProvider {
    SeznamAutobusu = "SeznamAutobusu",
}

type DataSourceDict = {
    [DescriptorProvider.SeznamAutobusu]: IDataSource<IVehicleListInfo[]>;
};

@injectable()
export class DescriptorDataSourceFactory {
    private readonly dataSourceDict: DataSourceDict;

    constructor(
        @inject(VPContainerToken.SeznamAutobusuDataSourceProvider)
        seznamAutobusuDataSourceProvider: IDataSourceProvider<IVehicleListInfo[]>
    ) {
        this.dataSourceDict = {
            [DescriptorProvider.SeznamAutobusu]: seznamAutobusuDataSourceProvider.getDataSource(),
        };
    }

    public getDataSource<T extends keyof DataSourceDict>(descriptorProvider: T): DataSourceDict[T] {
        return this.dataSourceDict[descriptorProvider];
    }
}
