import { RopidVYMIEventsModel, RopidVYMIEventsRoutesModel } from "#ie/ropid-vymi";
import { VPContainer } from "#ie/vehicle-positions/ioc/Di";
import { TripsRepository } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/TripsRepository";
import { ILogger } from "@golemio/core/dist/helpers/logger";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine/workers";
import { transit_realtime as gtfsRealtime } from "@golemio/ovapi-gtfs-realtime-bindings";
import { GtfsRtRedisRepository } from "../data-access/GtfsRtRedisRepository";
import { AlertsGenerator } from "../helpers/AlertsGenerator";
import { MPS_TO_KMH } from "../helpers/const";
import { VehicleDescriptor } from "../helpers/VehicleDescriptor";
import { IFeedEntity, IFeedHeader, IStopTimeUpdate, ITripDescriptor } from "../interfaces/GtfsRealtimeInterfaces";

export class GenerateFilesTask extends AbstractEmptyTask {
    public readonly queueName = "generateFiles";
    public readonly queueTtl = 20 * 1000; // 20 seconds

    private readonly logger: ILogger;
    private readonly tripsRepository: TripsRepository;
    private readonly gtfsRtRedisRepository: GtfsRtRedisRepository;
    private readonly alertsGenerator: AlertsGenerator;
    private readonly vehicleDescriptor: VehicleDescriptor;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.logger = VPContainer.resolve<ILogger>(ContainerToken.Logger);

        this.tripsRepository = new TripsRepository();
        this.gtfsRtRedisRepository = new GtfsRtRedisRepository();

        const vymiEventsRepository = new RopidVYMIEventsModel();
        const vymiRoutesRepository = new RopidVYMIEventsRoutesModel();
        this.alertsGenerator = new AlertsGenerator(this.gtfsRtRedisRepository, vymiEventsRepository, vymiRoutesRepository);
        this.vehicleDescriptor = new VehicleDescriptor();
    }

    protected async execute() {
        const tripData = await this.tripsRepository.findAllForGTFSRt();
        const feedHeader = this.createFeedHeader();
        const updatesMessage = gtfsRealtime.FeedMessage.create({ header: feedHeader });
        const positionsMessage = gtfsRealtime.FeedMessage.create({ header: feedHeader });
        const pidFeedMessage = gtfsRealtime.FeedMessage.create({ header: feedHeader });

        for (const tripRecord of tripData) {
            const tripDescriptor: ITripDescriptor = {
                scheduleRelationship: tripRecord.last_position.is_canceled
                    ? gtfsRealtime.TripDescriptor.ScheduleRelationship.CANCELED
                    : gtfsRealtime.TripDescriptor.ScheduleRelationship.SCHEDULED,
                startDate: this.parseUTCDateFromISO(tripRecord.start_timestamp),
                startTime: tripRecord.start_time,
                tripId: tripRecord.gtfs_trip_id,
                routeId: tripRecord.gtfs_route_id,
            };

            const vehicleDescriptor = this.vehicleDescriptor.getVehicleDescriptor(tripRecord);
            const entityTimestamp = Math.round(new Date(tripRecord.last_position.origin_timestamp).getTime() / 1000);
            const stopTimeUpdates: IStopTimeUpdate[] = [];

            for (const stopTimeEvent of tripRecord.stop_times) {
                stopTimeUpdates.push({
                    arrival: {
                        delay: stopTimeEvent.arrival_delay_seconds,
                    },
                    departure: {
                        delay: stopTimeEvent.departure_delay_seconds,
                    },
                    stopSequence: stopTimeEvent.stop_sequence,
                    stopId: stopTimeEvent.stop_id,
                });
            }

            const updateEntity: IFeedEntity = {
                id: tripRecord.id,
                tripUpdate: {
                    stopTimeUpdate: stopTimeUpdates,
                    timestamp: entityTimestamp,
                    trip: tripDescriptor,
                    vehicle: vehicleDescriptor,
                },
            };

            const positionEntity: IFeedEntity = {
                id: tripRecord.id,
                vehicle: {
                    currentStopSequence: tripRecord.last_position.last_stop_sequence,
                    position: {
                        bearing: tripRecord.last_position.bearing,
                        latitude: tripRecord.last_position.lat,
                        longitude: tripRecord.last_position.lng,
                        speed:
                            tripRecord.last_position.speed &&
                            parseFloat((tripRecord.last_position.speed / MPS_TO_KMH).toFixed(2)),
                    },
                    timestamp: entityTimestamp,
                    trip: tripDescriptor,
                    vehicle: vehicleDescriptor,
                },
            };
            const updatesMessageEntity = gtfsRealtime.FeedEntity.fromObject(updateEntity);
            updatesMessage.entity.push(updatesMessageEntity);

            const positionsMessageEntity = gtfsRealtime.FeedEntity.fromObject(positionEntity);
            positionsMessage.entity.push(positionsMessageEntity);

            // single feed for trip updates and vehicle positions
            const feedMessageEntity = structuredClone(updatesMessageEntity);
            feedMessageEntity.vehicle = positionsMessageEntity.vehicle;
            pidFeedMessage.entity.push(feedMessageEntity);
        }

        this.validateAndSaveBuffer("trip_updates", updatesMessage, true);
        this.validateAndSaveBuffer("vehicle_positions", positionsMessage, true);
        this.validateAndSaveBuffer("pid_feed", pidFeedMessage);

        await this.alertsGenerator.generateAlerts(feedHeader);
    }

    private createFeedHeader(): IFeedHeader {
        const header: IFeedHeader = {
            gtfsRealtimeVersion: "2.0",
            incrementality: gtfsRealtime.FeedHeader.Incrementality.FULL_DATASET,
            timestamp: Math.round(new Date().getTime() / 1000),
        };

        return gtfsRealtime.FeedHeader.fromObject(header);
    }

    /**
     * Parse UTC date from ISO timestamp
     * @example 20230101
     */
    private parseUTCDateFromISO(timestamp: string): string {
        const startTimestamp = new Date(timestamp);
        const formattedDate =
            startTimestamp.getUTCFullYear() +
            ("0" + (startTimestamp.getUTCMonth() + 1)).slice(-2) +
            ("0" + startTimestamp.getUTCDate()).slice(-2);

        return formattedDate;
    }

    private validateAndSaveBuffer(feedName: string, message: gtfsRealtime.FeedMessage, shouldSaveAsJson = false): void {
        if (gtfsRealtime.FeedMessage.verify(message) !== null) {
            this.logger.error(`${this.constructor.name}: invalid GTFS-RT message for ${feedName}`);
            return;
        }

        const buffer = gtfsRealtime.FeedMessage.encode(message).finish() as Buffer;
        this.gtfsRtRedisRepository.hset(feedName + ".pb", buffer.toString("binary"));
        if (shouldSaveAsJson) {
            this.gtfsRtRedisRepository.hset(feedName + ".json", JSON.stringify(message.toJSON()));
        }
    }
}
