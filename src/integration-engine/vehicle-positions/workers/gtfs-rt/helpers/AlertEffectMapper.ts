import { GtfsAlertEffectEnum } from "./enums/AlertEffectEnum";

const RopidVyMiEventType = {
    EFF_PROVOZ_ZASTAVEN: 1 << 0,
    EFF_PROVOZ_OMEZEN: 1 << 2,
    EFF_PROVOZ_OBNOVEN: 1 << 3,
    EFF_NEODJETI_SPOJE: 1 << 7,
    EFF_ZPOZDENI_SPOJE: 1 << 11,
    EFF_ZPOZDENI_SPOJU: 1 << 12,
    EFF_STANICE_UZAVRENA: 1 << 13,
    EFF_PRISTUP_OMEZEN: 1 << 14,
    EFF_POSILENI_SPOJU: 1 << 17,
    EFF_ROZVAZANI_PRESTUP: 1 << 18,
    EFF_OSTATNI: 1 << 19,
    EFF_ODKLON: 1 << 20,
    EFF_NEOBSLOUZENI_ZASTAVKY: 1 << 21,
    EFF_NAHRADNI_DOPRAVA: 1 << 23,
};

/**
 * Mapuje eventType na dopravní efekt. EventType může být i součet několika čísel RopidVyMiEventType.
 * @param eventType number
 */
export function getGtfsEffectByEventType(eventType: number): GtfsAlertEffectEnum {
    if (eventType & RopidVyMiEventType.EFF_ODKLON || eventType & RopidVyMiEventType.EFF_NEOBSLOUZENI_ZASTAVKY) {
        return GtfsAlertEffectEnum.DETOUR;
    }

    if (
        eventType & RopidVyMiEventType.EFF_PROVOZ_ZASTAVEN ||
        eventType & RopidVyMiEventType.EFF_PROVOZ_OMEZEN ||
        eventType & RopidVyMiEventType.EFF_ZPOZDENI_SPOJE ||
        eventType & RopidVyMiEventType.EFF_NEODJETI_SPOJE ||
        eventType & RopidVyMiEventType.EFF_ZPOZDENI_SPOJU
    ) {
        return GtfsAlertEffectEnum.REDUCED_SERVICE;
    }
    if (eventType & RopidVyMiEventType.EFF_ROZVAZANI_PRESTUP || eventType & RopidVyMiEventType.EFF_NAHRADNI_DOPRAVA) {
        return GtfsAlertEffectEnum.MODIFIED_SERVICE;
    }

    if (eventType & RopidVyMiEventType.EFF_POSILENI_SPOJU) {
        return GtfsAlertEffectEnum.ADDITIONAL_SERVICE;
    }

    if (
        eventType & RopidVyMiEventType.EFF_OSTATNI ||
        eventType & RopidVyMiEventType.EFF_PROVOZ_OBNOVEN ||
        eventType & RopidVyMiEventType.EFF_PRISTUP_OMEZEN
    ) {
        return GtfsAlertEffectEnum.OTHER_EFFECT;
    }

    if (eventType & RopidVyMiEventType.EFF_STANICE_UZAVRENA) {
        return GtfsAlertEffectEnum.NO_SERVICE;
    }

    return GtfsAlertEffectEnum.UNKNOWN_EFFECT;
}
