import { IGtfsRtTripDto } from "#sch/vehicle-positions/models/interfaces/IGtfsRtTripDto";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";
import { IVehicleDescriptor } from "../interfaces/GtfsRealtimeInterfaces";

export class VehicleDescriptor {
    public getVehicleDescriptor(tripRecord: IGtfsRtTripDto): IVehicleDescriptor {
        return {
            id: this.getVehicleId(tripRecord),
            ...(tripRecord.vehicle_registration_number && {
                label: tripRecord.vehicle_registration_number,
            }),
            ".transit_realtime.ovapiVehicleDescriptor": {
                wheelchairAccessible: tripRecord.wheelchair_accessible ?? undefined,
                vehicleType: tripRecord.vehicle_descriptor
                    ? JSON.stringify({ airConditioned: tripRecord.vehicle_descriptor.is_air_conditioned })
                    : undefined,
            },
        };
    }

    private getVehicleId(feedEntity: IGtfsRtTripDto): string {
        if (feedEntity.gtfs_route_type === GTFSRouteTypeEnum.TRAIN) {
            return "train-" + feedEntity.cis_trip_number;
        }

        if (feedEntity.gtfs_route_type === GTFSRouteTypeEnum.METRO) {
            return "metro-" + feedEntity.gtfs_route_short_name + "-" + feedEntity.run_number;
        }

        return "service-" + feedEntity.gtfs_route_type + "-" + feedEntity.vehicle_registration_number;
    }
}
