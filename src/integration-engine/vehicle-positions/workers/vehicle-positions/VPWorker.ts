import { AbstractWorker } from "@golemio/core/dist/integration-engine/workers";
import { WORKER_NAME } from "./constants";
import { DataRetentionTask } from "./tasks/DataRetentionTask";
import { DeleteDataTask } from "./tasks/DeleteDataTask";
import { PropagateDelayTask } from "./tasks/PropagateDelayTask";
import { RefreshGtfsTripDataTask } from "./tasks/RefreshGtfsTripDataTask";
import { SaveDataToDBTask } from "./tasks/SaveDataToDBTask";
import { UpdateDelayTask } from "./tasks/UpdateDelayTask";
import { UpdateGtfsTripIdTask } from "./tasks/UpdateGtfsTripIdTask";
import { UpdateRunsGtfsTripIdTask } from "./tasks/UpdateRunsGtfsTripIdTask";

export class VPWorker extends AbstractWorker {
    protected name = WORKER_NAME;

    constructor() {
        super();

        // Register tasks
        this.registerTask(new SaveDataToDBTask(this.getQueuePrefix()));
        this.registerTask(new UpdateGtfsTripIdTask(this.getQueuePrefix()));
        this.registerTask(new UpdateRunsGtfsTripIdTask(this.getQueuePrefix()));
        this.registerTask(new UpdateDelayTask(this.getQueuePrefix()));
        this.registerTask(new PropagateDelayTask(this.getQueuePrefix()));
        this.registerTask(new RefreshGtfsTripDataTask(this.getQueuePrefix()));
        this.registerTask(new DataRetentionTask(this.getQueuePrefix()));
        this.registerTask(new DeleteDataTask(this.getQueuePrefix()));
    }
}
