import { VehiclePositions } from "#sch/vehicle-positions";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import moment, { Moment } from "@golemio/core/dist/shared/moment-timezone";
import { StatePositionEnum, StateProcessEnum } from "src/const";
import DPPUtils from "../helpers/DPPUtils";
import { IMpvPositionContent } from "../interfaces/MpvMessageInterfaces";
import {
    IPositionTransformationResult,
    IStopTransformationResult,
    ITripTransformationResult,
} from "../interfaces/TransformationInterfaces";
import { ProviderSourceTypeEnum } from "../helpers/ProviderSourceTypeEnum";

export interface ITransformationResult {
    positions: IPositionTransformationResult[];
    stops: IStopTransformationResult[];
    trips: ITripTransformationResult[];
}

interface ITransformationElement {
    position: IPositionTransformationResult;
    stops: IStopTransformationResult[];
    trip: ITripTransformationResult;
}

export class PositionsTransformation extends BaseTransformation implements ITransformation {
    public name: string;
    private static ORIGIN_TIME_FORMAT = "HH:mm:ss";

    constructor() {
        super();
        this.name = VehiclePositions.name;
    }

    /**
     * Overrides BaseTransformation::transform
     */
    public transform = async (data: IMpvPositionContent | IMpvPositionContent[]): Promise<ITransformationResult> => {
        let res: ITransformationResult = {
            positions: [],
            stops: [],
            trips: [],
        };

        if (!Array.isArray(data)) data = [data];
        for (const element of data) {
            const elemRes = await this.transformElement(element);
            if (elemRes) {
                res.positions.push(elemRes.position);
                res.stops = res.stops.concat(elemRes.stops);
                res.trips.push(elemRes.trip);
            }
        }

        return res;
    };

    protected transformElement = async (element: IMpvPositionContent): Promise<ITransformationElement | null> => {
        const attributes = element.$;

        // Trips of DPP to be excluded in HTTP ingress
        const isDPPTrip = attributes.dopr === DPPUtils.DPP_AGENCY_NAME;

        // Trips with null cpoz (origin time) and falsy or "false" zrus (cancellation) attributes are excluded
        const hasInvalidAttributes = !attributes.cpoz && (!attributes.zrus || attributes.zrus === "false");

        if (isDPPTrip || hasInvalidAttributes) {
            return null;
        }

        attributes.lin = attributes.lin || "none";
        attributes.alias = attributes.alias || "none";
        attributes.cpoz = attributes.cpoz || moment.tz("Europe/Prague").format(PositionsTransformation.ORIGIN_TIME_FORMAT);

        const stops = element.zast;
        const { startTimestamp, positionTimestamp } = await this.deduceTimestamps(stops[0], attributes.cpoz);

        // filter out position with future time (set to be far than 5 minutes from now)
        if (moment(positionTimestamp).isAfter(moment().add(5, "minutes"))) {
            return null;
        }

        // primary key for t = 0 (vehicle type is train) -> start_timestamp, cis_number
        // primary key for t != 0 -> start_timestamp, cis_id, cis_short_name, cis_number
        const primaryKeySuffix =
            attributes.t === "0" ? `_${attributes.spoj}` : `_${attributes.lin}` + `_${attributes.alias}` + `_${attributes.spoj}`;
        const primaryKey = startTimestamp.utc().format() + primaryKeySuffix;

        /*
            MPV route types:

            Metro:                           1
            Tramvaj:                         2
            Bus městský:                     3
            Bus regionální:                  4
            Bus noční:                       5
            Noční tramvaj:                   6
            Náhradní autobusová doprava:     7
            Lanovka:                         8
            Školní:                          9
            Invalidní:                      10
            Smluvní:                        11
            Loď:                            12
            Vlak:                           13
            Náhradní doprava za vlak:       14
            Náhradní tramvajová doprava:    15
            Bus noční regionální:           16
            Ostatní:                        17
            Trolejbus:                      18
        */

        const bearing = attributes.azimut;
        const speed = attributes.rychl;
        const aswLastStopId = null;
        const cisLastStopId = attributes.zast;

        let res: ITransformationElement = {
            position: {
                asw_last_stop_id: aswLastStopId ? this.formatASWStopId(aswLastStopId) : null,
                bearing: bearing ? this.fixSourceNegativeBearing(parseInt(bearing, 10)) : null,
                cis_last_stop_id: cisLastStopId ? parseInt(cisLastStopId, 10) : null,
                cis_last_stop_sequence: null,
                delay_stop_arrival: attributes.zpoz_prij ? parseInt(attributes.zpoz_prij, 10) : null,
                delay_stop_departure: attributes.zpoz_odj ? parseInt(attributes.zpoz_odj, 10) : null,
                is_canceled: attributes.zrus === "true",
                lat: attributes.lat ? parseFloat(attributes.lat) : null,
                lng: attributes.lng ? parseFloat(attributes.lng) : null,
                origin_time: attributes.cpoz,
                origin_timestamp: positionTimestamp.toDate(),
                speed: speed ? parseInt(speed, 10) : null,
                state_position: StatePositionEnum.UNKNOWN,
                state_process: StateProcessEnum.INPUT,
                tracking: attributes.sled ? parseInt(attributes.sled, 10) : null,
                trips_id: primaryKey,
            },
            stops: [],
            trip: {
                agency_name_real: attributes.doprSkut ? attributes.doprSkut : null,
                agency_name_scheduled: attributes.dopr ? attributes.dopr : null,
                cis_line_id: attributes.lin,
                cis_line_short_name: attributes.alias,
                cis_trip_number: parseInt(attributes.spoj, 10),
                id: primaryKey,
                is_canceled: attributes.zrus === "true",
                origin_route_name: attributes.kmenl ?? null,
                run_number: attributes.po !== undefined ? parseInt(attributes.po, 10) : null,
                start_asw_stop_id: null,
                start_cis_stop_id: stops[0].$.zast ? parseInt(stops[0].$.zast, 10) : null,
                start_cis_stop_platform_code: stops[0].$.stan ?? null,
                start_time: startTimestamp.tz("Europe/Prague").format("HH:mm"),
                start_timestamp: startTimestamp.toDate(),
                vehicle_registration_number: attributes.vuzevc ? parseInt(attributes.vuzevc, 10) : null,
                vehicle_type_id: attributes.t ? parseInt(attributes.t, 10) : null,
                wheelchair_accessible: attributes.np === "true",
                provider_source_type: ProviderSourceTypeEnum.Http,
            },
        };

        for (let i = 0; i < stops.length; i++) {
            const stop = stops[i];
            const now = moment.tz("Europe/Prague");
            let arrival: Moment | undefined;
            let departure: Moment | undefined;
            let isOverMidnight = 0;

            // creating arival from stop.$.prij
            if (stop.$.prij && stop.$.prij !== "") {
                arrival = moment.tz("Europe/Prague");
                const arrivalPlain = stop.$.prij.split(":");
                arrival.hour(parseInt(arrivalPlain[0], 10));
                arrival.minute(parseInt(arrivalPlain[1], 10));
                arrival.second(0);
                arrival.millisecond(0);

                // midnight checking
                isOverMidnight = this.checkMidnight(now, arrival); // returns -1, 1 or 0
                arrival.add(isOverMidnight, "d");
            }
            // creating departure from stop.$.odj
            if (stop.$.odj && stop.$.odj !== "") {
                departure = moment.tz("Europe/Prague");
                const departurePlain = stop.$.odj.split(":");
                departure.hour(parseInt(departurePlain[0], 10));
                departure.minute(parseInt(departurePlain[1], 10));
                departure.second(0);
                departure.millisecond(0);

                // midnight checking
                isOverMidnight = this.checkMidnight(now, departure); // returns -1, 1 or 0
                departure.add(isOverMidnight, "d");
            }

            // TEMP input data attribute rename - OLD: stop.$.zpoz_typ, NEW: stop.$.zpoz_typ_odj
            const delayDepartureType = stop.$.zpoz_typ || stop.$.zpoz_typ_odj;

            const cisStopSequence = i + 1;
            // finding cis_last_stop_sequence (positions table)
            if (
                stop.$.zast &&
                res.position.cis_last_stop_id === parseInt(stop.$.zast, 10) &&
                ((delayDepartureType && parseInt(delayDepartureType, 10) === 3) ||
                    (stop.$.zpoz_typ_prij && parseInt(stop.$.zpoz_typ_prij, 10) === 3))
            ) {
                res.position.cis_last_stop_sequence = cisStopSequence;
            }

            // assign formatted stop id according to agency's stop ids
            const aswStopId = null;
            const cisStopId = stop.$.zast ? parseInt(stop.$.zast, 10) : null;

            res.stops.push({
                arrival_delay_type: stop.$.zpoz_typ_prij ? parseInt(stop.$.zpoz_typ_prij, 10) : null,
                arrival_time: arrival ? stop.$.prij! : null,
                arrival_timestamp: arrival ? arrival.utc().valueOf() : null,
                asw_stop_id: aswStopId,
                cis_stop_id: cisStopId,
                cis_stop_platform_code: stop.$.stan ?? null,
                cis_stop_sequence: cisStopSequence,
                delay_arrival: stop.$.zpoz_prij ? parseInt(stop.$.zpoz_prij, 10) : null,
                delay_departure: stop.$.zpoz_odj ? parseInt(stop.$.zpoz_odj, 10) : null,
                delay_type: delayDepartureType ? parseInt(delayDepartureType, 10) : null,
                departure_time: departure ? stop.$.odj! : null,
                departure_timestamp: departure ? departure.utc().valueOf() : null,
                trips_id: primaryKey,
            });
        }

        return res;
    };

    /**
     * Deduce UTC timestamp of start of trip and UTC timestamp of current position
     *
     * @param {any} firstStopXML - parsed object of first stop
     * @param {string} positionTimestampXML - XML attribute of local timestamp of position (HH:mm:ss)
     * @returns {{ startTimestamp: Moment; positionTimestamp: Moment }}
     */
    protected deduceTimestamps = async (
        firstStopXML: any,
        positionTimestampXML: string
    ): Promise<{ startTimestamp: Moment; positionTimestamp: Moment }> => {
        const now = moment.tz("Europe/Prague");
        let isOverMidnight = 0;

        // creating startDate and timestamp from zast[0].prij and cpoz
        const startTimestamp = now.clone();
        let startDatePlain = firstStopXML.$.prij || firstStopXML.$.odj;
        startDatePlain = startDatePlain.split(":");
        // eslint-disable-next-line prettier/prettier
        startTimestamp.hour(parseInt(startDatePlain[0], 10)).minute(parseInt(startDatePlain[1], 10)).second(0).millisecond(0);

        // midnight checking
        isOverMidnight = this.checkMidnight(now, startTimestamp); // returns -1, 1 or 0
        startTimestamp.add(isOverMidnight, "d");

        const positionTimestamp = now.clone();
        const timestampPlain = positionTimestampXML.split(":");
        positionTimestamp
            .hour(parseInt(timestampPlain[0], 10))
            .minute(parseInt(timestampPlain[1], 10))
            .second(parseInt(timestampPlain[2], 10))
            .millisecond(0);

        // midnight checking
        isOverMidnight = this.checkMidnight(now, positionTimestamp); // returns -1, 1 or 0
        positionTimestamp.add(isOverMidnight, "d");

        return {
            startTimestamp,
            positionTimestamp,
        };
    };

    /**
     * Returns -1 if start hour is 12-23 and now is 0-12, 1 if start hour is 18-23 and now 0-6, else 0
     *
     * @param {Moment} now - Moment of position
     * @param {Moment} start - Moment of start of trip
     * @returns {number}
     */
    private checkMidnight = (now: Moment, start: Moment): number => {
        // i.e. 0 - 22 = -22
        // -22 <= -12
        // trip starting in previous day, but never starts before 12:00
        if (now.hour() - start.hour() <= -(24 - 12)) {
            // "backwards" 12 hours
            return -1;
        }
        // i.e. 23 - 1 = +22
        // +22 >= +18
        // trip starting next day, sending positions early, not eariler than 6 hours before start
        else if (now.hour() - start.hour() >= 24 - 6) {
            // "forwards" 6 hours
            return 1;
        }
        return 0; // same day
    };

    /**
     * Fix source negative bearing value due to overflow by adding 256
     *
     * @param {number} bearing
     * @returns {number}
     */
    private fixSourceNegativeBearing(bearing: number): number {
        return bearing < 0 ? bearing + 256 : bearing;
    }

    /**
     * Format input stop id from DPP agency (XXX000Y to XXX/Y) to ASW id
     *
     * @param {string} stopId
     * @returns {stringList}
     */
    private formatASWStopId(stopId: string): string {
        const fixedRightPadFactor = 10000;
        const aswParsedStopNodeId = Math.floor(parseInt(stopId, 10) / fixedRightPadFactor);
        const aswParsedStopPostId = parseInt(stopId, 10) - aswParsedStopNodeId * fixedRightPadFactor;
        return aswParsedStopNodeId + "/" + aswParsedStopPostId;
    }
}
