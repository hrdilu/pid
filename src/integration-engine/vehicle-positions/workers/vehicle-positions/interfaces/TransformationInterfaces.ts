import { StatePositionEnum, StateProcessEnum } from "src/const";
import { ProviderSourceTypeEnum } from "../helpers/ProviderSourceTypeEnum";

export interface IPositionTransformationResult {
    asw_last_stop_id: string | null;
    bearing: number | null;
    cis_last_stop_id: number | null;
    cis_last_stop_sequence: number | null;
    delay_stop_arrival: number | null;
    delay_stop_departure: number | null;
    is_canceled: boolean;
    lat: number | null;
    lng: number | null;
    origin_time: string;
    origin_timestamp: string | Date;
    speed: number | null;
    state_position: StatePositionEnum;
    state_process: StateProcessEnum;
    tracking: number | null;
    trips_id: string;
}

export interface IStopTransformationResult {
    arrival_delay_type: number | null;
    arrival_time: string | null;
    arrival_timestamp: number | null;
    asw_stop_id: string | null;
    cis_stop_id: number | null;
    cis_stop_platform_code: string | null;
    cis_stop_sequence: number;
    delay_arrival: number | null;
    delay_departure: number | null;
    delay_type: number | null;
    departure_time: string | null;
    departure_timestamp: number | null;
    trips_id: string;
}

export interface ITripTransformationResult {
    agency_name_real: string | null;
    agency_name_scheduled: string | null;
    cis_line_id: string;
    cis_line_short_name: string;
    cis_trip_number: number;
    id: string;
    is_canceled: boolean;
    origin_route_name: string | null;
    run_number: number | null;
    start_asw_stop_id: string | null;
    start_cis_stop_id: number | null;
    start_cis_stop_platform_code: string | null;
    start_time: string;
    start_timestamp: Date;
    vehicle_registration_number: number | null;
    vehicle_type_id: number | null;
    wheelchair_accessible: boolean;
    provider_source_type: ProviderSourceTypeEnum;
}
