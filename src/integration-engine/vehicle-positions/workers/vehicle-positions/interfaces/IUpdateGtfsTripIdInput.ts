import { IUpdateGTFSTripIdData } from "../data-access/TripsRepository";
import { IPositionTransformationResult } from "../interfaces/TransformationInterfaces";

export interface IUpdateGtfsTripIdInput {
    trips: IUpdateGTFSTripIdData[];
    positions: IPositionTransformationResult[];
}
