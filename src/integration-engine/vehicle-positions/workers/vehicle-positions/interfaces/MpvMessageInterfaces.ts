export interface IMpvPositionProperties {
    alias?: string;
    asw?: string;
    azimut?: string;
    cpoz?: string;
    dopr?: string;
    doprSkut?: string;
    info?: string;
    kmenl?: string;
    lat?: string;
    lin?: string;
    lng?: string;
    np?: string;
    po?: string;
    rychl?: string;
    sled?: string;
    spoj: string;
    t?: string;
    vuzevc?: string;
    zast?: string;
    zpoz_prij?: string;
    zpoz_odj?: string;
    zrus?: string;
}

export interface IMpvStopProperties {
    odj?: string;
    prij?: string;
    stan?: string;
    zast?: string;
    asw?: string;
    zpoz_odj?: string;
    zpoz_prij?: string;
    zpoz_typ?: string;
    zpoz_typ_odj?: string;
    zpoz_typ_prij?: string;
}

export interface IMpvStopContent {
    $: IMpvStopProperties;
}

export interface IMpvPositionContent {
    $: IMpvPositionProperties;
    zast: IMpvStopContent[];
}

export interface IMpvMessageContent {
    spoj: IMpvPositionContent | IMpvPositionContent[];
}

export interface IMpvMessageInput {
    m: IMpvMessageContent;
}
