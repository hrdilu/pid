import { ITripStopsResult } from "#ie/ropid-gtfs/RopidGTFSStopTimesModel";
import { IShape, IStopTime } from "#ie/ropid-gtfs/RopidGTFSTripsModel";
import { IVPTripsPositionAttributes } from "#sch/vehicle-positions/models/interfaces/IVPTripsPositionAttributes";
import {
    IVPTripsComputedPositionAtStopStreak,
    IVPTripsLastPositionContext,
} from "#sch/vehicle-positions/models/interfaces/VPTripsLastPositionInterfaces";
import { StatePositionEnum } from "src/const";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";

export interface IPositionToUpdate {
    bearing?: number;
    state_process: string;
    state_position: StatePositionEnum;
    origin_timestamp?: Date;
    id: string;
    is_canceled?: boolean;
    lat?: string;
    lng?: string;
    tracking?: number;
    delay?: number | null;
    origin_time?: string;
    gtfsData?: IComputationTrip;
    shape_dist_traveled?: number;
    last_stop_arrival_time?: Date;
    last_stop_departure_time?: Date;
    last_stop_id?: string;
    last_stop_sequence?: number;
    last_stop_headsign?: string | null;
    last_stop_name?: string;
    next_stop_arrival_time?: Date;
    next_stop_departure_time?: Date;
    next_stop_id?: string;
    next_stop_sequence?: number;
    next_stop_name?: string;
    this_stop_id?: string;
    this_stop_sequence?: number;
    this_stop_name?: string;
    tcp_event?: string | null;
    valid_to?: Date;
    delay_stop_arrival?: number | null;
    delay_stop_departure?: number | null;
}

export interface IProcessedPositions {
    context: IVPTripsLastPositionContext;
    positions: IPositionToUpdate[];
}

export interface IUpdatePositionsIteratorOptions {
    tripPositions: ITripPositionsWithGTFS;
    startTimestamp: number;
    endTimestamp: number;
    startDayTimestamp: number;
    context: IVPTripsLastPositionContext;
    computedPositions: IPositionToUpdate[];
    gtfsRouteType: GTFSRouteTypeEnum;
}

export interface IShapeGeometryAnchorPoint {
    index: number;
    coordinates: [number, number];
    last_stop_sequence: number;
    next_stop_sequence: number;
    shape_dist_traveled: number;
    this_stop_sequence: number | null;
}
export interface IShapeAnchorPoint {
    index: number;
    at_stop: boolean;
    bearing: number;
    coordinates: [number, number];
    distance_from_last_stop: number;
    last_stop_sequence: number;
    next_stop_sequence: number;
    shape_dist_traveled: number;
    this_stop_sequence: number | null;
    time_scheduled_seconds: number;
}

export interface ISegmentShape extends IShape {
    shape_at_stop: boolean;
}

export interface IComputationTrip {
    trip_id: string;
    stop_times: IStopTime[];
    shapes_anchor_points: IShapeAnchorPoint[];
}

export interface ITripPositionsWithOrWithoutGTFS {
    gtfsData: IComputationTrip | null;
    id: string;
    gtfs_trip_id: string;
    gtfs_route_type: GTFSRouteTypeEnum;
    start_timestamp: Date;
    end_timestamp: Date | null;
    agency_name_scheduled: string;
    start_cis_stop_id: number | null;
    positions: IVPTripsPositionAttributes[];
    last_position_context: IVPTripsLastPositionContext;
}
export interface ITripPositionsWithGTFS extends ITripPositionsWithOrWithoutGTFS {
    gtfsData: IComputationTrip;
}

export interface ICurrentPositionProperties {
    id: string;
    origin_time: string;
    origin_timestamp: Date;
    scheduled_timestamp: Date | null;
    this_stop_id: string | null;
    tcp_event: string | null;
}

export interface ILastPositionProperties {
    time_delay: number;
    atStopStreak: IVPTripsComputedPositionAtStopStreak;
}

export interface IPropagateDelay {
    nextGtfsTrips: Array<{ trip_id: string; requiredTurnaroundSeconds?: number | undefined }>;
    currentOriginTimestamp: Date;
    currentGtfsTripId: string;
    currentRegistrationNumber: number;
    currentDelay: number;
    currentStartTime: string;
    currentEndTime: string;
}

export interface IVehiclePositionsSchedule {
    route_id: string;
    run_number: string | number;
    msg_last_timestamp: string;
}

export type TripStopsSchedule = Record<string, Omit<ITripStopsResult, "trip_id">>;
