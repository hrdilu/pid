import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import { IUpdateDelayRunTripsData, IUpdateDelayTripsIdsData } from "../data-access/TripsRepository";
import { IPositionTransformationResult } from "../interfaces/TransformationInterfaces";

export interface IUpdateDelayInput {
    updatedTrips: Array<IUpdateDelayTripsIdsData | IUpdateDelayRunTripsData>;
    positions: IPositionTransformationResult[];
    schedule?: IScheduleDto[];
}
