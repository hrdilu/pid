import { IUpdateDelayRunTripsData, IUpdateDelayTripsIdsData } from "../data-access/TripsRepository";
import { IProcessedPositions } from "./VPInterfaces";

export interface IPropagateDelayInput {
    processedPositions: IProcessedPositions[];
    trips: Array<IUpdateDelayTripsIdsData | IUpdateDelayRunTripsData>;
}
