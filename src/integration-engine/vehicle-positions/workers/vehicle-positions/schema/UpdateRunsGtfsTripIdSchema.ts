import { ICommonRunsModel } from "#sch/vehicle-positions/models/interfaces/ICommonRunsModel";
import { IsObject } from "@golemio/core/dist/shared/class-validator";
import { IRunMessageWithStringTimestamps, IUpdateRunsGtfsTripInput } from "../interfaces/IUpdateRunsGtfsTripInput";

export class UpdateRunsGtfsTripIdValidationSchema implements IUpdateRunsGtfsTripInput {
    @IsObject()
    run!: ICommonRunsModel;

    @IsObject()
    run_message!: IRunMessageWithStringTimestamps;
}
