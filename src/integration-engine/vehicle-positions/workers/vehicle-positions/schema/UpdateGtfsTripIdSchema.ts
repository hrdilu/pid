import { IsObject, IsArray } from "@golemio/core/dist/shared/class-validator";
import { IUpdateGTFSTripIdData } from "../data-access/TripsRepository";
import { IUpdateGtfsTripIdInput } from "../interfaces/IUpdateGtfsTripIdInput";
import { IPositionTransformationResult } from "../interfaces/TransformationInterfaces";

export class UpdateGtfsTripIdValidationSchema implements IUpdateGtfsTripIdInput {
    @IsArray()
    @IsObject({ each: true })
    trips!: IUpdateGTFSTripIdData[];

    @IsArray()
    @IsObject({ each: true })
    positions!: IPositionTransformationResult[];
}
