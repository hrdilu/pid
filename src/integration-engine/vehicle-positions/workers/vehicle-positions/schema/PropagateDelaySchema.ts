import { IsArray, IsObject } from "@golemio/core/dist/shared/class-validator";
import { IUpdateDelayRunTripsData, IUpdateDelayTripsIdsData } from "../data-access/TripsRepository";
import { IPropagateDelayInput } from "../interfaces/IPropagateDelayInput";
import { IProcessedPositions } from "../interfaces/VPInterfaces";

export class PropagateDelayValidationSchema implements IPropagateDelayInput {
    @IsArray()
    @IsObject({ each: true })
    processedPositions!: IProcessedPositions[];

    @IsArray()
    @IsObject({ each: true })
    trips!: Array<IUpdateDelayTripsIdsData | IUpdateDelayRunTripsData>;
}
