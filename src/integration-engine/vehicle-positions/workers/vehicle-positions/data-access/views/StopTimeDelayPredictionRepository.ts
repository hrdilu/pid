import { PG_SCHEMA } from "#sch/const";
import { StopTimeDelayPredictionModel } from "#sch/vehicle-positions/models/views";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { ModelStatic } from "@golemio/core/dist/shared/sequelize";

export class StopTimeDelayPredictionRepository extends PostgresModel implements IModel {
    public sequelizeModel!: ModelStatic<StopTimeDelayPredictionModel>;

    constructor() {
        super(
            "StopTimeDelayPredictionRepository",
            {
                pgTableName: StopTimeDelayPredictionModel.tableName,
                pgSchema: PG_SCHEMA,
                outputSequelizeAttributes: StopTimeDelayPredictionModel.attributeModel,
                savingType: "insertOnly",
            },
            new JSONSchemaValidator("StopTimeDelayPredictionReadOnly", {})
        );
    }
}
