import { IMetroRunInputForProcessing } from "#ie/vehicle-positions/workers/runs/interfaces/IMetroRunInputForProcessing";
import { PG_SCHEMA } from "#sch/const";
import { VehiclePositions } from "#sch/vehicle-positions";
import { VPTripsModel } from "#sch/vehicle-positions/models";
import { PositionDto } from "#sch/vehicle-positions/models/PositionDto";
import { IPositionDto } from "#sch/vehicle-positions/models/interfaces/IPositionDto";
import { VPTripsNonNullableModel } from "#sch/vehicle-positions/models/interfaces/IVPTripsModel";
import { IVPTripsPositionAttributes } from "#sch/vehicle-positions/models/interfaces/IVPTripsPositionAttributes";
import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import Sequelize, { ModelStatic, Op, QueryTypes } from "@golemio/core/dist/shared/sequelize";
import { StatePositionEnum, StateProcessEnum } from "src/const";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";
import { IUpdateRunsGtfsTripInput } from "../interfaces/IUpdateRunsGtfsTripInput";
import PositionsMapper from "./helpers/PositionsMapper";

export interface ITripAttributes
    extends Pick<
        VPTripsNonNullableModel,
        | "id"
        | "gtfs_trip_id"
        | "gtfs_route_type"
        | "start_timestamp"
        | "end_timestamp"
        | "agency_name_scheduled"
        | "start_cis_stop_id"
        | "last_position_context"
    > {}

export interface ITripPositions extends ITripAttributes {
    positions: IVPTripsPositionAttributes[];
}

export class PositionsRepository extends PostgresModel implements IModel {
    public sequelizeModel!: ModelStatic<PositionDto>;
    protected tripsModel: Sequelize.ModelCtor<any>;

    constructor() {
        super(
            "VPPositionsRepository",
            {
                outputSequelizeAttributes: PositionDto.attributeModel,
                pgTableName: VehiclePositions.positions.pgTableName,
                pgSchema: PG_SCHEMA,
                savingType: "insertOnly",
                sequelizeAdditionalSettings: {
                    indexes: [
                        {
                            fields: ["trips_id"],
                            name: "vehiclepositions_positions_trips_id",
                        },
                        {
                            fields: ["origin_time"],
                            name: "vehiclepositions_positions_origin_time",
                        },
                    ],
                },
            },
            new JSONSchemaValidator(VehiclePositions.positions.name + "ModelValidator", PositionDto.jsonSchema)
        );
        this.tripsModel = PostgresConnector.getConnection().define(VPTripsModel.TABLE_NAME, VPTripsModel.attributeModel, {
            schema: PG_SCHEMA,
        });
        this.tripsModel.hasMany(this.sequelizeModel, { foreignKey: "trips_id", sourceKey: "id" });
    }

    /**
     * Get all trips with positions for given trip ids. Positions are ordered by created_at instead of origin_timestamp
     *   to ensure they are processed by the same order they are created
     *   (sometimes TCP common positions are received out of order)
     *
     * Example of processing of mismatched TCP common positions:
     *   Batch 1 - TCP position (event T) with origin_timestamp 10:00:01, created_at 10:00:04, not last position
     *     (position is at_stop, last stop sequence is 2, delay at last stop is null, is not served to the output API)
     *   Batch 1 - TCP position (event O) with origin_timestamp 10:00:00, created_at 10:00:05, last position
     *     (position is on_track, last stop sequence is 2, delay at last stop is 10 and is served to the output API)
     *   Batch 2 - TCP position (event T) with origin_timestamp 10:01:00, created_at 10:01:04, last position
     *     (position is on_track, last stop sequence is 2, delay at last stop is 10 and is served to the output API)
     *   Batch 3 - TCP position (event 0) with origin_timestamp 10:01:40, created_at 10:01:44, last position
     *     (position is on_track, last stop sequence is 3, delay at last stop is 20 and is served to the output API)
     */
    public getPositionsForUpdateDelay = async (tripIds: string[]): Promise<ITripPositions[]> => {
        // TODO - check that origin_time is not duplicate for tracking == 2.
        // const originTimeColumn = `"vehiclepositions_positions"."origin_time"`;

        // make sure to not run with empty array
        if (tripIds.length === 0) {
            return [] as ITripPositions[];
        }
        const results = await this.tripsModel.findAll({
            attributes: [
                // Sequelize.literal(`DISTINCT ON (${originTimeColumn}) ${originTimeColumn}`),
                "id",
                "gtfs_trip_id",
                "gtfs_route_type",
                "start_timestamp",
                "end_timestamp",
                "agency_name_scheduled",
                "start_cis_stop_id",
                "last_position_context",
            ],
            include: [
                {
                    attributes: [
                        "bearing",
                        "lat",
                        "lng",
                        "origin_time",
                        "origin_timestamp",
                        "scheduled_timestamp",
                        "delay",
                        "tracking",
                        "id",
                        "shape_dist_traveled",
                        "is_canceled",
                        "state_position",
                        "state_process",
                        "tcp_event",
                        "this_stop_sequence",
                        "this_stop_id",
                        "last_stop_sequence",
                        "last_stop_id",
                        "last_stop_arrival_time",
                        "last_stop_departure_time",
                    ],
                    model: this.sequelizeModel,
                    where: {
                        state_process: { [Sequelize.Op.ne]: StateProcessEnum.PROCESSED },
                    },
                },
            ],
            order: [
                [{ model: this.sequelizeModel, as: "position" }, "created_at", "ASC"],
                [{ model: this.sequelizeModel, as: "position" }, "tracking", "DESC"],
            ],
            raw: true,
            where: {
                gtfs_trip_id: { [Sequelize.Op.ne]: null },
                id: { [Sequelize.Op.any]: tripIds },
            },
        });

        // Sequlize return with raw==true flatten array of results, nest==true is available for Sequelize ver >5 only
        // We return objects of positions grouped by trips_id
        try {
            return results.reduce((p, c) => {
                let pIndex = p.findIndex((e: ITripPositions) => e.id === c.id);
                if (pIndex === -1) {
                    p.push({
                        agency_name_scheduled: c.agency_name_scheduled,
                        gtfs_trip_id: c.gtfs_trip_id,
                        gtfs_route_type: c.gtfs_route_type as GTFSRouteTypeEnum,
                        positions: [] as IVPTripsPositionAttributes[],
                        last_position_context: c.last_position_context,
                        start_cis_stop_id: c.start_cis_stop_id,
                        start_timestamp: c.start_timestamp,
                        end_timestamp: c.end_timestamp,
                        id: c.id,
                    } as ITripAttributes);
                    pIndex = p.findIndex((e: ITripPositions) => e.id === c.id);
                }
                p[pIndex].positions.push({
                    bearing: c["vehiclepositions_positions.bearing"],
                    delay: c["vehiclepositions_positions.delay"],
                    id: c["vehiclepositions_positions.id"],
                    lat: parseFloat(c["vehiclepositions_positions.lat"]),
                    lng: parseFloat(c["vehiclepositions_positions.lng"]),
                    origin_time: c["vehiclepositions_positions.origin_time"],
                    origin_timestamp: c["vehiclepositions_positions.origin_timestamp"],
                    scheduled_timestamp: c["vehiclepositions_positions.scheduled_timestamp"] ?? null,
                    shape_dist_traveled: parseFloat(c["vehiclepositions_positions.shape_dist_traveled"]) || null,
                    tracking: c["vehiclepositions_positions.tracking"],
                    is_canceled: c["vehiclepositions_positions.is_canceled"],
                    state_position: c["vehiclepositions_positions.state_position"] as StatePositionEnum,
                    state_process: c["vehiclepositions_positions.state_process"] as StateProcessEnum,
                    tcp_event: c["vehiclepositions_positions.tcp_event"],
                    this_stop_sequence: c["vehiclepositions_positions.this_stop_sequence"],
                    this_stop_id: c["vehiclepositions_positions.this_stop_id"],
                    last_stop_sequence: c["vehiclepositions_positions.last_stop_sequence"],
                    last_stop_id: c["vehiclepositions_positions.last_stop_id"],
                    last_stop_arrival_time: c["vehiclepositions_positions.last_stop_arrival_time"],
                    last_stop_departure_time: c["vehiclepositions_positions.last_stop_departure_time"],
                } as IVPTripsPositionAttributes);
                return p;
            }, [] as ITripPositions[]);
        } catch (err) {
            throw new GeneralError(
                `Error while transforming data from db for batch. Trip count ${results.length}.`,
                this.name,
                err
            );
        }
    };

    public bulkUpdate = async (data: any[]): Promise<any> => {
        const connection = PostgresConnector.getConnection();
        const promises = data.map(async (d) => {
            const transaction = await connection.transaction();
            const [res]: Array<{ exists: boolean }> = await connection.query(
                // eslint-disable-next-line max-len
                `SELECT EXISTS(SELECT 1 FROM "${PG_SCHEMA}".${this.tableName} WHERE id='${d.id}') AS "exists";`,
                {
                    transaction,
                    type: QueryTypes.SELECT,
                    raw: true,
                }
            );

            try {
                const [record] = await this.sequelizeModel.upsert(d, { transaction });
                await transaction.commit();

                return { id: record.id, upd: !!res?.exists };
            } catch (err) {
                log.error(err);
                await transaction.rollback();
                throw new GeneralError("Error while saving to database.", this.name, err);
            }
        });

        const records = await Promise.all(promises);
        return records.reduce(
            (acc, record) => {
                if (record.upd) {
                    acc.updated.push(record.id);
                }
                return acc;
            },
            { updated: [] } as Record<"updated", any[]>
        );
    };

    public upsertCommonRunPosition = async (
        { run, run_message }: IUpdateRunsGtfsTripInput,
        gtfsTrip: IScheduleDto,
        isCurrent = true
    ): Promise<any> => {
        try {
            const [record] = await this.sequelizeModel.upsert(
                PositionsMapper.mapCommonRunToDto(run_message, isCurrent, gtfsTrip, run),
                {
                    returning: true,
                }
            );

            return record.get({ plain: true });
        } catch (err) {
            throw new GeneralError("upsertCommonRunPosition: Error while saving to database.", this.name, err);
        }
    };

    public upsertMetroRunPosition = async (
        runInput: IMetroRunInputForProcessing,
        gtfsTrip: IScheduleDto,
        isCurrent = true
    ): Promise<any> => {
        try {
            const [record] = await this.sequelizeModel.upsert(PositionsMapper.mapMetroRunToDto(isCurrent, gtfsTrip, runInput), {
                returning: true,
            });

            return record.get({ plain: true });
        } catch (err) {
            throw new GeneralError("upsertMetroRunPosition: Error while saving to database.", this.name, err);
        }
    };

    public deleteNHoursOldData = async (hours: number): Promise<number> => {
        try {
            return await this.sequelizeModel.destroy({
                where: {
                    updated_at: {
                        [Op.lt]: Sequelize.literal(`NOW() - INTERVAL '${hours} HOURS'`),
                    },
                },
            });
        } catch (err) {
            throw new GeneralError("Error while purging old data", this.constructor.name, err);
        }
    };

    public createNotPublic = async (positionDto: Partial<IPositionDto>): Promise<PositionDto> => {
        try {
            return await this.sequelizeModel.create(positionDto, {
                returning: true,
            });
        } catch (err) {
            throw new GeneralError("createNotPublic: error while saving data", this.name, err);
        }
    };
}
