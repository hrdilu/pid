import { IMetroRunInputForProcessing } from "#ie/vehicle-positions/workers/runs/interfaces/IMetroRunInputForProcessing";
import { DateTimeUtils } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/DateTimeUtils";
import { ICommonRunsModel } from "#sch/vehicle-positions/models/interfaces/ICommonRunsModel";
import { IPositionDto } from "#sch/vehicle-positions/models/interfaces/IPositionDto";
import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import { PositionTrackingEnum, StatePositionEnum, StateProcessEnum } from "src/const";
import { IRunMessageWithStringTimestamps } from "../../interfaces/IUpdateRunsGtfsTripInput";
import VPTripsIdGenerator from "./TripsIdGenerator";

export default class PositionsMapper {
    public static mapCommonRunToDto(
        runMessage: IRunMessageWithStringTimestamps,
        isCurrent: boolean,
        gtfsTrip: IScheduleDto,
        runInput: ICommonRunsModel
    ): Partial<IPositionDto> {
        return {
            asw_last_stop_id: runMessage.last_stop_asw_id,
            lat: runMessage.lat,
            lng: runMessage.lng,
            origin_time: DateTimeUtils.parseUTCTimeFromISO(runMessage.actual_stop_timestamp_real),
            origin_timestamp: new Date(runMessage.actual_stop_timestamp_real),
            scheduled_timestamp: runMessage.actual_stop_timestamp_scheduled
                ? new Date(runMessage.actual_stop_timestamp_scheduled)
                : null,
            state_position: StatePositionEnum.UNKNOWN,
            state_process: StateProcessEnum.TCP_INPUT,
            tracking: isCurrent ? PositionTrackingEnum.TRACKING : PositionTrackingEnum.NOT_TRACKING,
            trips_id: VPTripsIdGenerator.generateFromCommonRun(gtfsTrip, runInput),
            tcp_event: runMessage.events,
        };
    }

    public static mapCommonRunNotPublicToDto(runMessage: IRunMessageWithStringTimestamps, tripId: string): Partial<IPositionDto> {
        return {
            asw_last_stop_id: runMessage.last_stop_asw_id,
            lat: runMessage.lat,
            lng: runMessage.lng,
            origin_time: DateTimeUtils.parseUTCTimeFromISO(runMessage.actual_stop_timestamp_real),
            origin_timestamp: new Date(runMessage.actual_stop_timestamp_real),
            state_position: StatePositionEnum.NOT_PUBLIC,
            state_process: StateProcessEnum.PROCESSED,
            tracking: PositionTrackingEnum.TRACKING,
            trips_id: tripId,
            tcp_event: runMessage.events,
        };
    }

    public static mapMetroRunToDto(
        isCurrent: boolean,
        gtfsTrip: IScheduleDto,
        runInput: IMetroRunInputForProcessing
    ): Partial<IPositionDto> {
        return {
            lat: runInput.coordinates.lat,
            lng: runInput.coordinates.lon,
            origin_time: DateTimeUtils.parseUTCTimeFromISO(runInput.messageTimestamp),
            origin_timestamp: new Date(runInput.messageTimestamp),
            scheduled_timestamp: new Date(runInput.timestampScheduled),
            state_position: StatePositionEnum.UNKNOWN,
            state_process: StateProcessEnum.TCP_INPUT,
            this_stop_id: runInput.coordinates.gtfs_stop_id,
            tracking: isCurrent ? PositionTrackingEnum.TRACKING : PositionTrackingEnum.NOT_TRACKING,
            trips_id: VPTripsIdGenerator.generateFromMetroRun(gtfsTrip, runInput),
        };
    }
}
