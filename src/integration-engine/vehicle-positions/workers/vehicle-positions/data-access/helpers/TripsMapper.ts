import { IMetroRunInputForProcessing } from "#ie/vehicle-positions/workers/runs/interfaces/IMetroRunInputForProcessing";
import DPPUtils from "#ie/vehicle-positions/workers/vehicle-positions/helpers/DPPUtils";
import { DateTimeUtils } from "#ie/vehicle-positions/workers/vehicle-positions/helpers/DateTimeUtils";
import { ICommonRunsModel } from "#sch/vehicle-positions/models/interfaces/ICommonRunsModel";
import { IVPTripsModel } from "#sch/vehicle-positions/models/interfaces/IVPTripsModel";
import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import { GTFSRouteTypeEnum, MPVRouteTypesEnum } from "src/helpers/RouteTypeEnums";
import { ProviderSourceTypeEnum } from "../../helpers/ProviderSourceTypeEnum";
import VPTripsIdGenerator from "./TripsIdGenerator";

export default class TripsMapper {
    public static mapCommonRunToDto(gtfsTrip: IScheduleDto, runInput: ICommonRunsModel): Partial<IVPTripsModel> {
        return {
            ...this.getGTFSAttributes(gtfsTrip),
            id: VPTripsIdGenerator.generateFromCommonRun(gtfsTrip, runInput),
            origin_route_name: runInput.route_id,
            internal_route_name: runInput.route_id,
            run_number: parseInt(runInput.run_number),
            internal_run_number: parseInt(runInput.run_number),
            vehicle_registration_number: parseInt(runInput.registration_number),
            vehicle_type_id: this.getVehicleTypeId(
                +gtfsTrip.route_type,
                !!+gtfsTrip.is_night,
                !!+gtfsTrip.is_regional,
                !!+gtfsTrip.is_substitute_transport
            ),
            wheelchair_accessible: runInput.wheelchair_accessible,
            provider_source_type: ProviderSourceTypeEnum.TcpCommon,
        };
    }

    public static mapCommonRunNotPublicToDto(
        runInput: ICommonRunsModel,
        tripId: string,
        lastPositionId: string
    ): Partial<IVPTripsModel> {
        return {
            agency_name_real: DPPUtils.DPP_AGENCY_NAME,
            agency_name_scheduled: DPPUtils.DPP_AGENCY_NAME,
            gtfs_route_type: GTFSRouteTypeEnum.TRAM,
            origin_route_name: runInput.route_id,
            internal_route_name: runInput.route_id,
            run_number: parseInt(runInput.run_number),
            internal_run_number: parseInt(runInput.run_number),
            vehicle_registration_number: parseInt(runInput.registration_number),
            vehicle_type_id: MPVRouteTypesEnum.TRAM,
            wheelchair_accessible: runInput.wheelchair_accessible,
            provider_source_type: ProviderSourceTypeEnum.TcpCommon,
            id: tripId,
            last_position_id: parseInt(lastPositionId),
        };
    }

    public static mapMetroRunToDto(gtfsTrip: IScheduleDto, runInput: IMetroRunInputForProcessing): Partial<IVPTripsModel> {
        return {
            ...this.getGTFSAttributes(gtfsTrip),
            id: VPTripsIdGenerator.generateFromMetroRun(gtfsTrip, runInput),
            origin_route_name: runInput.routeId,
            internal_route_name: runInput.routeId,
            run_number: runInput.runNumber,
            internal_run_number: runInput.runNumber,
            vehicle_type_id: MPVRouteTypesEnum.METRO,
            wheelchair_accessible: true, // metro vehicles themselves are always wheelchair accessible
            provider_source_type: ProviderSourceTypeEnum.TcpMetro,
        };
    }

    private static getGTFSAttributes(gtfsTrip: IScheduleDto): Partial<IVPTripsModel> {
        return {
            agency_name_real: DPPUtils.DPP_AGENCY_NAME,
            agency_name_scheduled: DPPUtils.DPP_AGENCY_NAME,
            gtfs_block_id: gtfsTrip.block_id,
            gtfs_route_id: gtfsTrip.route_id,
            gtfs_route_short_name: gtfsTrip.route_short_name,
            gtfs_route_type: gtfsTrip.route_type,
            gtfs_trip_headsign: gtfsTrip.trip_headsign,
            gtfs_trip_id: gtfsTrip.trip_id,
            gtfs_date: gtfsTrip.date,
            start_time: DateTimeUtils.parseUTCTimeFromISO(gtfsTrip.start_timestamp),
            start_timestamp: new Date(gtfsTrip.start_timestamp),
            end_timestamp: new Date(gtfsTrip.end_timestamp),
        };
    }

    private static getVehicleTypeId = (
        routeType: number,
        isNight: boolean,
        isRegional: boolean,
        isSubstitute: boolean
    ): number => {
        let id;
        switch (routeType) {
            case GTFSRouteTypeEnum.TRAM:
                if (isNight) {
                    id = MPVRouteTypesEnum.TRAM_NIGHT;
                } else if (isSubstitute) {
                    id = MPVRouteTypesEnum.TRAM_SUBSTITUTE;
                } else {
                    id = MPVRouteTypesEnum.TRAM;
                }
                break;
            case GTFSRouteTypeEnum.METRO:
                id = MPVRouteTypesEnum.METRO;
                break;
            case GTFSRouteTypeEnum.TRAIN:
                id = MPVRouteTypesEnum.TRAIN;
                break;
            case GTFSRouteTypeEnum.BUS:
                if (isNight && isRegional) {
                    id = MPVRouteTypesEnum.BUS_NIGHT_REGIONAL;
                } else if (isNight) {
                    id = MPVRouteTypesEnum.BUS_NIGHT;
                } else if (isRegional) {
                    id = MPVRouteTypesEnum.BUS_REGIONAL;
                } else if (isSubstitute) {
                    id = MPVRouteTypesEnum.BUS_SUBSTITUTE;
                } else {
                    id = MPVRouteTypesEnum.BUS_CITY;
                }
                break;
            case GTFSRouteTypeEnum.FERRY:
                id = MPVRouteTypesEnum.FERRY;
                break;
            case GTFSRouteTypeEnum.FUNICULAR:
                id = MPVRouteTypesEnum.FUNICULAR;
                break;
            case GTFSRouteTypeEnum.TROLLEYBUS:
                id = MPVRouteTypesEnum.TROLLEYBUS;
                break;
            default:
                throw new Error("Unexpected routeType");
        }
        return id;
    };
}
