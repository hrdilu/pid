import { PG_SCHEMA } from "#sch/const";
import { RopidGTFSPrecomputed } from "#sch/ropid-gtfs/RopidGTFSPrecomputed";
import { VPTripsModel } from "#sch/vehicle-positions/models/VPTripsModel";
import { TripWithLastPositionModel } from "#sch/vehicle-positions/models/views/TripWithLastPositionModel";
import { StatePositionEnum } from "src/const";
import { DateTimeUtils } from "../../helpers/DateTimeUtils";

export class RawQueryProvider {
    public static getCheckExistingTripQuery(tripId: string): string {
        return `SELECT EXISTS(SELECT 1 FROM "${PG_SCHEMA}".${VPTripsModel.TABLE_NAME} WHERE id='${tripId}') AS "exists";`;
    }

    public static getFindAssociatedTripsQuery(): string {
        return `
            SELECT id,
                gtfs_block_id, gtfs_trip_id, gtfs_route_type,
                start_timestamp, end_timestamp,
                run_number, internal_run_number,
                origin_route_name, internal_route_name,
                vehicle_registration_number
            FROM "${PG_SCHEMA}".${VPTripsModel.TABLE_NAME}
            WHERE id LIKE any (('{'||$tripIds||'}')::text[]);
        `;
    }

    public static getFindGtfsCommonTripsQuery(startDateDayName: string, startDateDayBeforeDayName: string): string {
        /* eslint-disable max-len */
        return `
            SELECT DISTINCT
                "${PG_SCHEMA}".ropidgtfs_trips.trip_id as gtfs_trip_id,
                "${PG_SCHEMA}".ropidgtfs_precomputed_trip_schedule.date as gtfs_date,
                "${PG_SCHEMA}".ropidgtfs_trips.trip_headsign as gtfs_trip_headsign,
                "${PG_SCHEMA}".ropidgtfs_routes.route_id as gtfs_route_id,
                "${PG_SCHEMA}".ropidgtfs_routes.route_type as gtfs_route_type,
                "${PG_SCHEMA}".ropidgtfs_routes.route_short_name as gtfs_route_short_name,
                "${PG_SCHEMA}".ropidgtfs_precomputed_minmax_stop_sequences.min_stop_time,
                "${PG_SCHEMA}".ropidgtfs_precomputed_minmax_stop_sequences.max_stop_time,
                "${PG_SCHEMA}".ropidgtfs_precomputed_trip_schedule.run_number as internal_run_number,
                "${PG_SCHEMA}".ropidgtfs_precomputed_trip_schedule.origin_route_name as internal_route_name
            FROM "${PG_SCHEMA}".ropidgtfs_trips
            INNER JOIN "${PG_SCHEMA}".ropidgtfs_routes ON "${PG_SCHEMA}".ropidgtfs_trips.route_id=ropidgtfs_routes.route_id
            INNER JOIN "${PG_SCHEMA}".ropidgtfs_stop_times ON "${PG_SCHEMA}".ropidgtfs_trips.trip_id=ropidgtfs_stop_times.trip_id
            LEFT JOIN "${PG_SCHEMA}".ropidgtfs_precomputed_trip_schedule
                ON "${PG_SCHEMA}".ropidgtfs_trips.trip_id=ropidgtfs_precomputed_trip_schedule.trip_id
                    AND ropidgtfs_precomputed_trip_schedule.start_timestamp::date = current_date
            LEFT JOIN "${PG_SCHEMA}".ropidgtfs_precomputed_minmax_stop_sequences ON "${PG_SCHEMA}".ropidgtfs_trips.trip_id=ropidgtfs_precomputed_minmax_stop_sequences.trip_id
            WHERE
            ( "${PG_SCHEMA}".ropidgtfs_routes.route_short_name LIKE $cisLineShortName
              OR CASE WHEN ($cisLineShortName = 'IKEA')
                THEN "${PG_SCHEMA}".ropidgtfs_routes.route_short_name LIKE 'IKEA ČM'
                ELSE 'FALSE' END
            )
            AND "${PG_SCHEMA}".ropidgtfs_stop_times.stop_id IN
            ( SELECT stop_id FROM "${PG_SCHEMA}".ropidgtfs_stops
            WHERE stop_id LIKE
              (SELECT CONCAT('U',CAST(node AS TEXT),'Z%') FROM "${PG_SCHEMA}".ropidgtfs_cis_stop_groups
              WHERE cis IN
                (SELECT cis FROM "${PG_SCHEMA}".ropidgtfs_cis_stops
                    WHERE cis = $startCisStopId OR id = $startAswStopId))
              AND
                (platform_code LIKE $startCisStopPlatformCode
                OR CASE WHEN (LENGTH(platform_code)<2) THEN platform_code LIKE
                  (CAST((ASCII($startCisStopPlatformCode)-64) AS CHAR)) END)
            )
            AND stop_sequence = 1
            AND CONCAT(
                MOD(SUBSTRING(LPAD(ropidgtfs_stop_times.departure_time, 8, '0'),1,2)::int,24),
                SUBSTRING(LPAD(ropidgtfs_stop_times.departure_time, 8, '0'),3,6)
              )
                =  TO_CHAR(($startDateFormated at time zone 'Europe/Prague'), 'FMHH24:MI:SS')
            AND ( CASE WHEN SUBSTRING(LPAD(ropidgtfs_stop_times.departure_time, 8, '0'),1,2)::int < 24 THEN
                    "${PG_SCHEMA}".ropidgtfs_trips.service_id IN (
                    SELECT service_id FROM "${PG_SCHEMA}".ropidgtfs_calendar
                    WHERE ${startDateDayName} = 1
                    AND to_date(start_date, 'YYYYMMDD') <= $startDateYMD
                    AND to_date(end_date, 'YYYYMMDD') >= $startDateYMD
                    UNION SELECT service_id FROM "${PG_SCHEMA}".ropidgtfs_calendar_dates
                    WHERE exception_type = 1
                    AND to_date(date, 'YYYYMMDD') = $startDateYMD
                    EXCEPT SELECT service_id FROM "${PG_SCHEMA}".ropidgtfs_calendar_dates
                    WHERE exception_type = 2
                    AND to_date(date, 'YYYYMMDD') = $startDateYMD
                    )
                ELSE
                    "${PG_SCHEMA}".ropidgtfs_trips.service_id IN (
                    SELECT service_id FROM "${PG_SCHEMA}".ropidgtfs_calendar
                    WHERE ${startDateDayBeforeDayName} = 1
                    AND to_date(start_date, 'YYYYMMDD') <= $startDateDayBeforeYMD
                    AND to_date(end_date, 'YYYYMMDD') >= $startDateDayBeforeYMD
                    UNION SELECT service_id FROM "${PG_SCHEMA}".ropidgtfs_calendar_dates
                    WHERE exception_type = 1
                    AND to_date(date, 'YYYYMMDD') = $startDateDayBeforeYMD
                    EXCEPT SELECT service_id FROM "${PG_SCHEMA}".ropidgtfs_calendar_dates
                    WHERE exception_type = 2
                    AND to_date(date, 'YYYYMMDD') = $startDateDayBeforeYMD
                    )
                END
            )
            ORDER BY gtfs_trip_id ASC;
        `;
        /* eslint-enable max-len */
    }

    public static getFindGtfsTrainTripsQuery(): string {
        /* eslint-disable max-len */
        return `
            WITH wanted_trip AS (SELECT cis_line_short_name,
                cis_trip_number,
                start_cis_stop_id,
                start_asw_stop_id,
                start_timestamp,
                DATE_TRUNC('day', start_timestamp) AS start_date,
                extract(isodow from DATE_TRUNC('day', start_timestamp)) AS start_date_name,
                DATE_TRUNC('day', start_timestamp) - INTERVAL '1 day' AS start_date_day_before,
                extract(isodow from DATE_TRUNC('day', start_timestamp) - INTERVAL '1 day')
                    AS start_date_day_before_name
            FROM "${PG_SCHEMA}".vehiclepositions_trips
            WHERE id = $tripId
            LIMIT 1)
            SELECT DISTINCT
                "${PG_SCHEMA}".ropidgtfs_trips.trip_id as gtfs_trip_id,
                "${PG_SCHEMA}".ropidgtfs_precomputed_trip_schedule.date as gtfs_date,
                "${PG_SCHEMA}".ropidgtfs_trips.block_id as gtfs_block_id,
                "${PG_SCHEMA}".ropidgtfs_trips.trip_headsign as gtfs_trip_headsign,
                "${PG_SCHEMA}".ropidgtfs_trips.trip_short_name as gtfs_trip_short_name,
                "${PG_SCHEMA}".ropidgtfs_routes.route_id as gtfs_route_id,
                "${PG_SCHEMA}".ropidgtfs_routes.route_type as gtfs_route_type,
                "${PG_SCHEMA}".ropidgtfs_routes.route_short_name as gtfs_route_short_name,
                wanted_trip.cis_trip_number,
                "${PG_SCHEMA}".ropidgtfs_precomputed_minmax_stop_sequences.min_stop_time,
                "${PG_SCHEMA}".ropidgtfs_precomputed_minmax_stop_sequences.max_stop_time,
                "${PG_SCHEMA}".ropidgtfs_precomputed_trip_schedule.run_number as internal_run_number,
                "${PG_SCHEMA}".ropidgtfs_precomputed_trip_schedule.origin_route_name as internal_route_name
            FROM "${PG_SCHEMA}".ropidgtfs_trips
            INNER JOIN wanted_trip ON 1 = 1
            INNER JOIN "${PG_SCHEMA}".ropidgtfs_routes ON "${PG_SCHEMA}".ropidgtfs_trips.route_id=ropidgtfs_routes.route_id
            INNER JOIN "${PG_SCHEMA}".ropidgtfs_stop_times ON "${PG_SCHEMA}".ropidgtfs_trips.trip_id=ropidgtfs_stop_times.trip_id
            LEFT JOIN "${PG_SCHEMA}".ropidgtfs_precomputed_trip_schedule
                ON "${PG_SCHEMA}".ropidgtfs_trips.trip_id=ropidgtfs_precomputed_trip_schedule.trip_id
                    AND ropidgtfs_precomputed_trip_schedule.start_timestamp::date = current_date
            LEFT JOIN "${PG_SCHEMA}".ropidgtfs_precomputed_minmax_stop_sequences ON "${PG_SCHEMA}".ropidgtfs_trips.trip_id="${PG_SCHEMA}".ropidgtfs_precomputed_minmax_stop_sequences.trip_id
            WHERE
                "${PG_SCHEMA}".ropidgtfs_trips.trip_id ~ CONCAT('\\d+_', wanted_trip.cis_trip_number, '_\\d+')
                AND "${PG_SCHEMA}".ropidgtfs_routes.route_type = '2'
                AND stop_sequence = 1
                AND ( CASE WHEN SUBSTRING(LPAD(ropidgtfs_stop_times.departure_time, 8, '0'),1,2)::int < 24 THEN
                        "${PG_SCHEMA}".ropidgtfs_trips.service_id IN (
                        SELECT service_id FROM "${PG_SCHEMA}".ropidgtfs_calendar
                        WHERE (
                            CASE WHEN wanted_trip.start_date_name = 1 THEN monday = '1'
                                WHEN wanted_trip.start_date_name = 2 THEN tuesday = '1'
                                WHEN wanted_trip.start_date_name = 3 THEN wednesday = '1'
                                WHEN wanted_trip.start_date_name = 4 THEN thursday = '1'
                                WHEN wanted_trip.start_date_name = 5 THEN friday = '1'
                                WHEN wanted_trip.start_date_name = 6 THEN saturday = '1'
                                WHEN wanted_trip.start_date_name = 7 THEN sunday = '1'
                            ELSE 'FALSE'
                        END
                        )
                        AND to_date(start_date, 'YYYYMMDD') <= wanted_trip.start_date
                        AND to_date(end_date, 'YYYYMMDD') >= wanted_trip.start_date
                        UNION SELECT service_id FROM "${PG_SCHEMA}".ropidgtfs_calendar_dates
                        WHERE exception_type = 1
                        AND to_date(date, 'YYYYMMDD') = wanted_trip.start_date
                        EXCEPT SELECT service_id FROM "${PG_SCHEMA}".ropidgtfs_calendar_dates
                        WHERE exception_type = 2
                        AND to_date(date, 'YYYYMMDD') = wanted_trip.start_date
                        )
                    ELSE
                        "${PG_SCHEMA}".ropidgtfs_trips.service_id IN (
                        SELECT service_id FROM "${PG_SCHEMA}".ropidgtfs_calendar
                        WHERE (
                            CASE WHEN wanted_trip.start_date_day_before_name = 1 THEN monday = '1'
                                WHEN wanted_trip.start_date_day_before_name = 2 THEN tuesday = '1'
                                WHEN wanted_trip.start_date_day_before_name = 3 THEN wednesday = '1'
                                WHEN wanted_trip.start_date_day_before_name = 4 THEN thursday = '1'
                                WHEN wanted_trip.start_date_day_before_name = 5 THEN friday = '1'
                                WHEN wanted_trip.start_date_day_before_name = 6 THEN saturday = '1'
                                WHEN wanted_trip.start_date_day_before_name = 7 THEN sunday = '1'
                            ELSE 'FALSE'
                        END
                        )
                        AND to_date(start_date, 'YYYYMMDD') <= wanted_trip.start_date_day_before
                        AND to_date(end_date, 'YYYYMMDD') >= wanted_trip.start_date_day_before
                        UNION SELECT service_id FROM "${PG_SCHEMA}".ropidgtfs_calendar_dates
                        WHERE exception_type = 1
                        AND to_date(date, 'YYYYMMDD') = wanted_trip.start_date_day_before
                        EXCEPT SELECT service_id FROM "${PG_SCHEMA}".ropidgtfs_calendar_dates
                        WHERE exception_type = 2
                        AND to_date(date, 'YYYYMMDD') = wanted_trip.start_date_day_before
                        )
                    END
                )
            ORDER BY gtfs_trip_id ASC;
        `;
        /* eslint-enable max-len */
    }

    public static getUpdateLastPositionTripsQuery(): string {
        return `
            UPDATE "${PG_SCHEMA}".${VPTripsModel.TABLE_NAME}
            SET
                last_position_id = $lastPositionId,
                last_position_context = jsonb_set(
                    last_position_context::jsonb,
                    '{lastPositionBeforeTrackDelayed}', $jsonLastPositionContext,
                    true
                )::json
            WHERE id = $tripId
        `;
    }

    public static getUpdateGtfsTripDataQuery(): string {
        return `
            update "${PG_SCHEMA}".${VPTripsModel.TABLE_NAME} vp_trip
            set
                gtfs_block_id = trip_with_gtfs_data.gtfs_block_id,
                gtfs_route_id = trip_with_gtfs_data.gtfs_route_id,
                gtfs_route_short_name = trip_with_gtfs_data.gtfs_route_short_name,
                gtfs_route_type = trip_with_gtfs_data.gtfs_route_type,
                gtfs_trip_headsign = trip_with_gtfs_data.gtfs_trip_headsign,
                start_time = trip_with_gtfs_data.start_time,
                start_timestamp = trip_with_gtfs_data.start_timestamp,
                end_timestamp = trip_with_gtfs_data.end_timestamp,
                internal_run_number = trip_with_gtfs_data.internal_run_number,
                internal_route_name = trip_with_gtfs_data.internal_route_name,
                updated_at = now()
            from (
                select
                    vp_trip_with_position.id,
                    gtfs_trip.block_id as gtfs_block_id,
                    gtfs_trip.route_id as gtfs_route_id,
                    gtfs_trip.route_short_name as gtfs_route_short_name,
                    gtfs_trip.route_type as gtfs_route_type,
                    gtfs_trip.trip_headsign as gtfs_trip_headsign,
                    gtfs_trip.start_timestamp::time at time zone '${DateTimeUtils.TIMEZONE}' as start_time,
                    gtfs_trip.start_timestamp,
                    gtfs_trip.end_timestamp,
                    gtfs_trip.run_number as internal_run_number,
                    gtfs_trip.origin_route_name as internal_route_name
                from "${PG_SCHEMA}".${TripWithLastPositionModel.tableName} vp_trip_with_position
                inner join "${PG_SCHEMA}".${RopidGTFSPrecomputed.tripSchedule.pgTableName} gtfs_trip
                    on vp_trip_with_position.gtfs_trip_id = gtfs_trip.trip_id
                    and gtfs_trip.start_timestamp::date = current_date
                where vp_trip_with_position.state_position != '${StatePositionEnum.AFTER_TRACK}'
            ) as trip_with_gtfs_data
            where vp_trip.id = trip_with_gtfs_data.id;
        `;
    }
}
