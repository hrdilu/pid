import { IMetroRunInputForProcessing } from "#ie/vehicle-positions/workers/runs/interfaces/IMetroRunInputForProcessing";
import { ICommonRunsModel } from "#sch/vehicle-positions/models/interfaces/ICommonRunsModel";
import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";

export default class TripsIdGenerator {
    /**
     * @example 2022-11-04T12:31:00+01:00_10_4873_220926_8215
     */
    public static generateFromCommonRun(gtfsTrip: IScheduleDto, runInput: ICommonRunsModel) {
        const { start_timestamp, trip_id } = gtfsTrip;
        const { registration_number } = runInput;
        return `${start_timestamp}_${trip_id}_${registration_number}`;
    }

    /**
     * @example NOT_PUBLIC_67_1_2021-06-28T15:05:00+02:00_5572
     */
    public static generateFromCommonRunNotPublic(runInput: ICommonRunsModel) {
        return (
            `NOT_PUBLIC_${runInput.route_id}_${runInput.run_number}_` +
            `${runInput.msg_start_timestamp}_${runInput.registration_number}`
        );
    }

    /**
     * @example 2022-11-04T12:37:35+01:00_991_416_220901_13_13
     */
    public static generateFromMetroRun(gtfsTrip: IScheduleDto, runInput: IMetroRunInputForProcessing) {
        const { start_timestamp, trip_id } = gtfsTrip;
        const { trainSetNumberScheduled, trainSetNumberReal } = runInput;
        return `${start_timestamp}_${trip_id}_${trainSetNumberScheduled}_${trainSetNumberReal}`;
    }
}
