import { IMetroRunInputForProcessing } from "#ie/vehicle-positions/workers/runs/interfaces/IMetroRunInputForProcessing";
import { DescriptorRepository } from "#ie/vehicle-positions/workers/vehicle-descriptors/data-access/DescriptorRepository";
import { PG_SCHEMA } from "#sch/const";
import { VPTripsModel } from "#sch/vehicle-positions/models";
import { IGtfsRtTripDto } from "#sch/vehicle-positions/models/interfaces/IGtfsRtTripDto";
import { IVPTripsModel } from "#sch/vehicle-positions/models/interfaces/IVPTripsModel";
import { IVPTripsPositionAttributes } from "#sch/vehicle-positions/models/interfaces/IVPTripsPositionAttributes";
import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import moment from "@golemio/core/dist/shared/moment-timezone";
import Sequelize, { Op, QueryTypes } from "@golemio/core/dist/shared/sequelize";
import { StatePositionEnum } from "src/const";
import { IUpdateRunsGtfsTripInput } from "../interfaces/IUpdateRunsGtfsTripInput";
import { IPropagateDelay } from "../interfaces/VPInterfaces";
import { PositionsRepository } from "./PositionsRepository";
import { RawQueryProvider } from "./helpers/RawQueryProvider";
import TripsMapper from "./helpers/TripsMapper";
import { StopTimeDelayPredictionRepository } from "./views/StopTimeDelayPredictionRepository";

export interface IUpdateDelayTripsIdsData {
    id: string;
    gtfs_trip_id?: string;
    gtfs_block_id?: string;
    gtfs_route_type?: number;
    start_timestamp: string;
    end_timestamp?: string;
    run_number?: number | null;
    internal_run_number?: number | null;
    origin_route_name?: string | null;
    internal_route_name?: string | null;
    vehicle_registration_number?: number | null;
}

export interface IUpdateDelayRunTripsData extends IUpdateDelayTripsIdsData {
    gtfs_trip_id: string;
    internal_route_name: string | null;
    internal_run_number: number | null;
    vehicle_registration_number: number;
}

export interface IUpdateGTFSTripIdData extends Omit<Partial<IVPTripsModel>, "start_timestamp"> {
    id: string;
    start_timestamp: string;
    run_number: number | null;
    origin_route_name: string | null;
    vehicle_registration_number: number | null;
}

export interface IFoundGTFSTripData {
    id?: string;
    gtfs_trip_id: string;
    gtfs_trip_headsign: string;
    gtfs_route_id: string;
    gtfs_route_short_name: string;
    min_stop_time: string;
    max_stop_time: string;

    gtfs_block_id?: string;
    gtfs_trip_short_name?: string;
    gtfs_route_type?: number;
    cis_trip_number?: number;
    internal_run_number?: number;
    internal_route_name?: string;
}

export interface IBulkUpsertOutput {
    inserted: IUpdateGTFSTripIdData[];
    updated: Array<{
        id: string;
        origin_route_name: string | null;
        run_number: string | null;
        start_timestamp: Date;
    }>;
}

export interface IPropagatedDelayTripWithPosition {
    position: {
        id: string;
        delay: number;
        state_position: StatePositionEnum;
        origin_timestamp: Date;
    };
    trip: {
        id: string;
    };
}

export class TripsRepository extends PostgresModel implements IModel {
    private positionsRepository: PositionsRepository;
    private stopTimeDelayPredictionRepository: StopTimeDelayPredictionRepository;
    private descriptorRepository: DescriptorRepository;

    constructor() {
        super(
            "VPTripsRepository",
            {
                pgTableName: VPTripsModel.TABLE_NAME,
                pgSchema: PG_SCHEMA,
                outputSequelizeAttributes: VPTripsModel.attributeModel,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("VPTripsRepositoryValidator", VPTripsModel.jsonSchema)
        );

        this.positionsRepository = new PositionsRepository();
        this.descriptorRepository = new DescriptorRepository(log);
        this.stopTimeDelayPredictionRepository = new StopTimeDelayPredictionRepository();

        this.sequelizeModel.belongsTo(this.positionsRepository.sequelizeModel, {
            as: "last_position",
            foreignKey: "last_position_id",
        });
        this.sequelizeModel.hasMany(this.stopTimeDelayPredictionRepository.sequelizeModel, {
            as: "stop_times",
            foreignKey: "trip_id",
            sourceKey: "gtfs_trip_id",
        });
        this.sequelizeModel.belongsTo(this.descriptorRepository.sequelizeModel, {
            as: "vehicle_descriptor",
            foreignKey: "vehicle_registration_number",
            targetKey: "registration_number",
            scope: {
                [Sequelize.Op.and]: [
                    Sequelize.literal(`${VPTripsModel.TABLE_NAME}.gtfs_route_type = vehicle_descriptor.gtfs_route_type`),
                ],
            },
        });
    }

    /**
     * Overrides PostgresModel::save
     * @todo use a single function for upserting (save, bulkUpsert, bulkUpdate)
     */
    public save = async (
        data: Partial<IVPTripsModel> | Array<Partial<IVPTripsModel>>,
        useTmpTable: boolean = false
    ): Promise<any> => {
        // data validation
        if (this.validator) {
            try {
                await this.validator.Validate(data);
            } catch (err) {
                throw new GeneralError("Error while validating data.", this.name, err);
            }
        } else {
            log.warn(this.name + ": Model validator is not set.");
        }

        if (useTmpTable) {
            throw new GeneralError("Saving to tmp table is not implemented for this model.", this.name);
        }

        const connection = PostgresConnector.getConnection();

        const trips = Array.isArray(data) ? data : [data];
        const inserted: Array<Partial<IVPTripsModel>> = [];
        const updated: string[] = [];

        const promises = trips.map(async (trip) => {
            const transaction = await connection.transaction();
            try {
                // TODO there is a chance res is always null
                // See https://sequelize.org/master/class/lib/model.js~Model.html#static-method-upsert
                const [, res] = await this.sequelizeModel.upsert(trip, { transaction });
                await transaction.commit();

                if (res) {
                    inserted.push({
                        cis_line_short_name: trip.cis_line_short_name,
                        id: trip.id,
                        start_asw_stop_id: trip.start_asw_stop_id,
                        start_cis_stop_id: trip.start_cis_stop_id,
                        start_cis_stop_platform_code: trip.start_cis_stop_platform_code,
                        start_timestamp: trip.start_timestamp,
                    });
                } else {
                    updated.push(trip.id as string);
                }
            } catch (err) {
                log.error(err);
                await transaction.rollback();
                throw new GeneralError("Error while saving to database.", this.name, err);
            }
        });

        await Promise.all(promises);
        return { inserted, updated };
    };

    /**
     * @todo use a single function for upserting (save, bulkUpsert, bulkUpdate)
     */
    public bulkUpsert = async (data: any[]): Promise<IBulkUpsertOutput> => {
        // data validation
        if (this.validator) {
            try {
                await this.validator.Validate(data);
            } catch (err) {
                throw new GeneralError("Error while validating data.", this.name, err);
            }
        } else {
            log.warn(this.name + ": Model validator is not set.");
        }

        const connection = PostgresConnector.getConnection();
        const promises = data.map(async (d) => {
            const transaction = await connection.transaction();
            const dataItem = { ...d };
            const [res]: Array<{ exists: boolean }> = await connection.query(RawQueryProvider.getCheckExistingTripQuery(d.id), {
                transaction,
                type: QueryTypes.SELECT,
                raw: true,
            });

            const isToBeUpdated = !!res?.exists;

            // Delete incoming start_timestamp for updated trips to avoid overwriting existing computed timestamp
            // TODO this is a workaround, dedicated column (gtfs_start_timestamp) will be added
            if (isToBeUpdated) {
                delete dataItem.start_timestamp;
            }

            try {
                const [record] = await this.sequelizeModel.upsert(dataItem, { transaction });
                await transaction.commit();
                return { data: record, upd: isToBeUpdated };
            } catch (err) {
                log.error(err);
                await transaction.rollback();
                throw new GeneralError("bulkUpsert: Error while saving to database.", this.name, err);
            }
        });

        const records = await Promise.all(promises);
        return records.reduce(
            (acc, record) => {
                if (record.upd) {
                    acc.updated.push({
                        id: record.data.id,
                        origin_route_name: record.data.origin_route_name,
                        run_number: record.data.run_number,
                        start_timestamp: record.data.start_timestamp,
                    });
                } else {
                    acc.inserted.push({
                        cis_line_short_name: record.data.cis_line_short_name,
                        id: record.data.id,
                        start_asw_stop_id: record.data.start_asw_stop_id,
                        start_cis_stop_id: record.data.start_cis_stop_id,
                        start_cis_stop_platform_code: record.data.start_cis_stop_platform_code,
                        start_timestamp: record.data.start_timestamp,
                        agency_name_real: record.data.agency_name_real,
                        agency_name_scheduled: record.data.agency_name_scheduled,
                        cis_line_id: record.data.cis_line_id,
                        cis_trip_number: record.data.cis_trip_number,
                        origin_route_name: record.data.origin_route_name,
                        run_number: record.data.run_number,
                        start_time: record.data.start_time,
                        vehicle_registration_number: record.data.vehicle_registration_number,
                        vehicle_type_id: record.data.vehicle_type_id,
                        wheelchair_accessible: record.data.wheelchair_accessible,
                    });
                }
                return acc;
            },
            { inserted: [], updated: [] } as IBulkUpsertOutput
        );
    };

    /**
     * @todo use a single function for upserting (save, bulkUpsert, bulkUpdate)
     */
    public bulkUpdate = async (data: any[]): Promise<any> => {
        const connection = PostgresConnector.getConnection();
        const promises = data.map(async (d) => {
            const transaction = await connection.transaction();
            const [res]: Array<{ exists: boolean }> = await connection.query(RawQueryProvider.getCheckExistingTripQuery(d.id), {
                transaction,
                type: QueryTypes.SELECT,
                raw: true,
            });

            try {
                const [record] = await this.sequelizeModel.upsert(d, { transaction });
                await transaction.commit();
                return { id: record.id, upd: !!res?.exists };
            } catch (err) {
                log.error(err);
                await transaction.rollback();
                throw new GeneralError("bulkUpdate: Error while saving to database.", this.name, err);
            }
        });

        const records = await Promise.all(promises);
        return records.reduce(
            (acc, record) => {
                if (record.upd) {
                    acc.updated.push(record.id);
                }
                return acc;
            },
            { updated: [] } as Record<"updated", any[]>
        );
    };

    public findAllAssocTripIds = async (tripIds: string[]): Promise<IUpdateDelayTripsIdsData[]> => {
        const connection = PostgresConnector.getConnection();
        return Array.isArray(tripIds) && tripIds.length > 0
            ? (
                  await connection.query(RawQueryProvider.getFindAssociatedTripsQuery(), {
                      type: Sequelize.QueryTypes.SELECT,
                      bind: { tripIds: tripIds.map((id) => `${id}%`) },
                  })
              ).map((res: any) => ({
                  id: res.id,
                  gtfs_trip_id: res.gtfs_trip_id,
                  gtfs_block_id: res.gtfs_block_id,
                  gtfs_route_type: res.gtfs_route_type,
                  start_timestamp: res.start_timestamp,
                  end_timestamp: res.end_timestamp,
                  run_number: res.run_number,
                  internal_run_number: res.internal_run_number,
                  origin_route_name: res.origin_route_name,
                  internal_route_name: res.internal_route_name,
                  vehicle_registration_number: res.vehicle_registration_number,
              }))
            : [];
    };

    public findGTFSTripId = async (
        trip: IUpdateGTFSTripIdData
    ): Promise<IUpdateDelayTripsIdsData | IUpdateDelayTripsIdsData[] | undefined> => {
        let foundGtfsTrips: IFoundGTFSTripData[] = [];
        if (Number(trip.start_cis_stop_id) >= 5400000 && Number(trip.start_cis_stop_id) < 5500000) {
            // trains
            // array of founded gtfs trips, 0 or 1 or more with same block_id
            foundGtfsTrips = await this.findGTFSTripIdsTrain(trip);
        } else {
            // other
            foundGtfsTrips = await this.findGTFSTripIdBasic(trip);
        }

        if (foundGtfsTrips.length < 1) {
            return;
        }

        // update existing trip with gtfs_trip_id, gtfs_block_id and other gtfs data
        const firstFoundGtfsTrip = foundGtfsTrips.shift()!;
        const { startTimestamp, endTimestamp } = this.deduceTimestamps(
            firstFoundGtfsTrip.min_stop_time,
            firstFoundGtfsTrip.max_stop_time,
            trip.start_timestamp
        );

        await this.update(
            {
                ...firstFoundGtfsTrip,
                internal_run_number: firstFoundGtfsTrip.internal_run_number ?? trip.run_number,
                internal_route_name: firstFoundGtfsTrip.internal_route_name ?? trip.origin_route_name,
                start_timestamp: startTimestamp,
                end_timestamp: endTimestamp,
            },
            {
                where: {
                    id: trip.id,
                },
            }
        );

        const newIds: IUpdateDelayTripsIdsData[] = [
            {
                id: trip.id,
                gtfs_trip_id: firstFoundGtfsTrip.gtfs_trip_id,
                gtfs_block_id: firstFoundGtfsTrip.gtfs_block_id,
                gtfs_route_type: firstFoundGtfsTrip.gtfs_route_type,
                internal_run_number: firstFoundGtfsTrip.internal_run_number ?? trip.run_number,
                internal_route_name: firstFoundGtfsTrip.internal_route_name ?? trip.origin_route_name,
                start_timestamp: startTimestamp.toISOString(),
                end_timestamp: endTimestamp.toISOString(),
            },
        ];

        // for other trips insert new rows with suffixed id
        for (const foundTrip of foundGtfsTrips) {
            const newId = `${trip.id}_gtfs_trip_id_${foundTrip.gtfs_trip_id}`;

            foundTrip.id = newId;

            const { startTimestamp, endTimestamp } = this.deduceTimestamps(
                foundTrip.min_stop_time,
                foundTrip.max_stop_time,
                trip.start_timestamp
            );

            await this.save({
                ...trip,
                ...foundTrip,
                internal_run_number: foundTrip.internal_run_number ?? trip.run_number,
                internal_route_name: foundTrip.internal_route_name ?? trip.origin_route_name,
                start_timestamp: startTimestamp,
                end_timestamp: endTimestamp,
            });

            newIds.push({
                id: newId,
                gtfs_trip_id: foundTrip.gtfs_trip_id,
                gtfs_block_id: foundTrip.gtfs_block_id,
                gtfs_route_type: foundTrip.gtfs_route_type,
                start_timestamp: startTimestamp.toISOString(),
                end_timestamp: endTimestamp.toISOString(),
                run_number: trip.run_number,
                internal_run_number: foundTrip.internal_run_number ?? trip.run_number,
                internal_route_name: foundTrip.internal_route_name ?? trip.origin_route_name,
                origin_route_name: trip.origin_route_name,
                vehicle_registration_number: trip.vehicle_registration_number,
            });
        }
        return newIds;
    };

    public deleteNHoursOldData = async (hours: number): Promise<number> => {
        try {
            return await this.sequelizeModel.destroy({
                where: {
                    updated_at: {
                        [Op.lt]: Sequelize.literal(`NOW() - INTERVAL '${hours} HOURS'`),
                    },
                },
            });
        } catch (err) {
            throw new GeneralError("Error while purging old data", this.constructor.name, err);
        }
    };

    private deduceTimestamps = (
        min_stop_time: string,
        max_stop_time: string,
        origin_start_timestamp: string
    ): {
        startTimestamp: Date;
        endTimestamp: Date;
    } => {
        const minTimeParts = min_stop_time.split(":").map((e: string) => parseInt(e, 10));
        const maxTimeParts = max_stop_time.split(":").map((e: string) => parseInt(e, 10));
        const originStartTimestamp = moment(origin_start_timestamp).tz("Europe/Prague");

        // if trip starts not on origin_start_timestamp but after midnight with times <=12:**:**
        // then we add day
        const startsNextDay = originStartTimestamp.hours() >= minTimeParts[0] + 12;

        const startTimestamp = originStartTimestamp
            .clone()
            .tz("Europe/Prague")
            .startOf("day")
            .add(startsNextDay ? 1 : 0, "days")
            .add(minTimeParts[0] > 23 && originStartTimestamp.hours() > 12 ? 1 : 0, "days")
            .add(minTimeParts[0] % 24, "hours")
            .add(minTimeParts[1], "minutes")
            .add(minTimeParts[2], "seconds")
            .toDate();

        const endTimestamp =
            startTimestamp.getTime() +
            (maxTimeParts[0] * 3600 +
                maxTimeParts[1] * 60 +
                maxTimeParts[2] -
                (minTimeParts[0] * 3600 + minTimeParts[1] * 60 + minTimeParts[2])) *
                1000;

        return {
            startTimestamp: startTimestamp,
            endTimestamp: new Date(endTimestamp),
        };
    };

    public upsertCommonRunTrip = async (
        { run }: IUpdateRunsGtfsTripInput,
        gtfsTrip: IScheduleDto
    ): Promise<IUpdateDelayRunTripsData> => {
        try {
            const [record] = await this.sequelizeModel.upsert(TripsMapper.mapCommonRunToDto(gtfsTrip, run), {
                returning: true,
            });

            return record.get({ plain: true });
        } catch (err) {
            throw new GeneralError("upsertCommonRunTrip: Error while saving to database.", this.name, err);
        }
    };

    public upsertMetroRunTrip = async (
        runInput: IMetroRunInputForProcessing,
        gtfsTrip: IScheduleDto
    ): Promise<IUpdateDelayRunTripsData> => {
        try {
            const [record] = await this.sequelizeModel.upsert(TripsMapper.mapMetroRunToDto(gtfsTrip, runInput), {
                returning: true,
            });

            return record.get({ plain: true });
        } catch (err) {
            throw new GeneralError("upsertMetroRunTrip: Error while saving to database.", this.name, err);
        }
    };

    /**
     * Updates before track positions with propagated delay and also updates last position context for parent trips
     */
    public bulkUpdatePropagatedDelay = async (data: IPropagatedDelayTripWithPosition[]): Promise<void[]> => {
        const connection = PostgresConnector.getConnection();
        const promises = data.map(async (d) => {
            const transaction = await connection.transaction();
            try {
                await this.positionsRepository.update(d.position, {
                    where: {
                        id: d.position.id,
                    },
                    transaction,
                });

                await connection.query(RawQueryProvider.getUpdateLastPositionTripsQuery(), {
                    type: Sequelize.QueryTypes.UPDATE,
                    transaction,
                    bind: {
                        lastPositionId: d.position.id,
                        jsonLastPositionContext: {
                            delay: d.position.delay,
                            originTimestamp: d.position.origin_timestamp,
                        },
                        tripId: d.trip.id,
                    },
                });

                await transaction.commit();

                return;
            } catch (err) {
                log.error(err);
                await transaction.rollback();
                throw new GeneralError("Error while saving to database.", this.name, err);
            }
        });

        return Promise.all(promises);
    };

    public getPropagateDelayTripsWithPositions = async (
        data: IPropagateDelay
    ): Promise<Array<IVPTripsModel & { last_position: IVPTripsPositionAttributes }>> => {
        return this.sequelizeModel.findAll({
            include: [
                {
                    as: "last_position",
                    model: this.positionsRepository.sequelizeModel,
                    where: {
                        state_position: {
                            [Sequelize.Op.in]: [StatePositionEnum.BEFORE_TRACK, StatePositionEnum.INVISIBLE],
                        },
                        origin_timestamp: data.currentOriginTimestamp,
                    },
                },
            ],
            where: {
                gtfs_trip_id: data.nextGtfsTrips.map((element) => element.trip_id),
                vehicle_registration_number: data.currentRegistrationNumber,
            },
            order: [["start_timestamp", "ASC"]],
        });
    };

    public hasOne = (model: any, options: any): any => {
        return this.sequelizeModel.hasOne(model, options);
    };

    public findAll = async (options: Sequelize.FindOptions<any> | undefined): Promise<any> => {
        return this.sequelizeModel.findAll(options);
    };

    private findGTFSTripIdBasic = async (trip: IUpdateGTFSTripIdData): Promise<IFoundGTFSTripData[]> => {
        const connection = PostgresConnector.getConnection();
        const startDate = moment(trip.start_timestamp).tz("Europe/Prague");
        const startDateDayBefore = startDate.clone().subtract(1, "day");

        const startDateYMD = startDate.format("YYYY-MM-DD");
        const startDateDayName = startDate.format("dddd").toLowerCase();
        const startDateDayBeforeYMD = startDateDayBefore.format("YYYY-MM-DD");
        const startDateDayBeforeDayName = startDateDayBefore.format("dddd").toLowerCase();

        const sqlQuery = RawQueryProvider.getFindGtfsCommonTripsQuery(startDateDayName, startDateDayBeforeDayName);
        let result = (await connection.query(sqlQuery, {
            type: Sequelize.QueryTypes.SELECT,
            bind: {
                startDateDayBeforeYMD,
                startDateYMD,
                cisLineShortName: trip.cis_line_short_name,
                startCisStopId: trip.start_cis_stop_id || 0,
                startAswStopId: trip.start_asw_stop_id || "",
                startCisStopPlatformCode: trip.start_cis_stop_platform_code,
                startDateFormated: startDate.utc().format(),
            },
        })) as IFoundGTFSTripData[];

        if (result.length < 1) {
            log.verbose(`${this.constructor.name}: Model data was not found for id '${trip.id}' (basic).`);
        } else if (result.length > 1) {
            log.verbose(`There are too many gtfs trips (${result.length}) linked with id '${trip.id}' (trip).`);
            result = [];
        }

        return result;
    };

    private findGTFSTripIdsTrain = async (trip: IUpdateGTFSTripIdData): Promise<IFoundGTFSTripData[]> => {
        const connection = PostgresConnector.getConnection();

        const sqlQuery = RawQueryProvider.getFindGtfsTrainTripsQuery();
        let results = (await connection.query(sqlQuery, {
            type: Sequelize.QueryTypes.SELECT,
            bind: {
                tripId: trip.id,
            },
        })) as IFoundGTFSTripData[];

        if (results.length < 1) {
            log.verbose(`Model data was not found for id '${trip.id}' (train).`, true, this.constructor.name, 5001);
        } else if (results.length > 10) {
            log.verbose(`There are too many gtfs trips (${results.length}) linked with id '${trip.id}' (train).`);
            results = [];
        }

        return results as any[];
    };

    public upsertNotPublic = async (tripDto: Partial<IVPTripsModel>): Promise<void> => {
        try {
            await this.sequelizeModel.upsert(tripDto);
        } catch (err) {
            throw new GeneralError("upsertNotPublic: error while saving data", this.name, err);
        }
    };

    /**
     * Return all valid trips for GTFS Realtime feed
     *
     * @returns {Promise<any[]>}
     */
    public findAllForGTFSRt = async (): Promise<IGtfsRtTripDto[]> => {
        return this.sequelizeModel.findAll({
            attributes: [
                "id",
                "run_number",
                "start_timestamp",
                "cis_line_id",
                "cis_trip_number",
                "start_time",
                "vehicle_registration_number",
                "gtfs_trip_id",
                "gtfs_route_id",
                "gtfs_route_type",
                "gtfs_route_short_name",
                "wheelchair_accessible",
            ],
            include: [
                {
                    as: "last_position",
                    model: this.positionsRepository.sequelizeModel,
                    attributes: ["is_canceled", "origin_timestamp", "last_stop_sequence", "bearing", "lat", "lng", "speed"],
                    where: {
                        state_position: {
                            [Sequelize.Op.in]: [
                                StatePositionEnum.ON_TRACK,
                                StatePositionEnum.AT_STOP,
                                StatePositionEnum.CANCELED,
                                StatePositionEnum.BEFORE_TRACK,
                                StatePositionEnum.BEFORE_TRACK_DELAYED,
                            ],
                        },
                    },
                },
                {
                    as: "stop_times",
                    model: this.stopTimeDelayPredictionRepository.sequelizeModel,
                    attributes: ["stop_id", "stop_sequence", "arrival_delay_seconds", "departure_delay_seconds"],
                    required: true,
                },
                {
                    as: "vehicle_descriptor",
                    model: this.descriptorRepository.sequelizeModel,
                    attributes: ["is_air_conditioned"],
                },
            ],
            order: [["updated_at", "DESC"], ["id", "ASC"], Sequelize.literal("stop_times.stop_sequence ASC")],
        });
    };

    public refreshGtfsTripData = async (): Promise<void> => {
        const connection = PostgresConnector.getConnection();

        try {
            const [_result, rowCount] = await connection.query(RawQueryProvider.getUpdateGtfsTripDataQuery(), {
                type: QueryTypes.UPDATE,
            });

            log.info(`refreshGtfsTripData: updated ${rowCount} trips`);
        } catch (err) {
            throw new GeneralError("refreshGtfsTripData: error while updating data", this.name, err);
        }
    };
}
