import { config } from "@golemio/core/dist/integration-engine/config";
import { log } from "@golemio/core/dist/integration-engine/helpers/Logger";
import { PositionTrackingEnum } from "src/const";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";
import { IUpdateDelayRunTripsData, IUpdateDelayTripsIdsData } from "../data-access/TripsRepository";
import { IPositionTransformationResult } from "../interfaces/TransformationInterfaces";
import { TripStopsSchedule } from "../interfaces/VPInterfaces";

export class VPUtils {
    public static DELAY_COMPUTATION_CACHE_TTL = 7200;

    /**
     * Determine whether to create new position and its tracking status (trains only for now)
     *
     * @param  {TripStopsSchedule | undefined} tripStopSchedule - Cached trip stops for given block id (see Redis)
     * @param {IUpdateDelayRunTripsData | IUpdateDelayTripsIdsData} trip - New trip
     * @param {IPositionTransformationResult} position - Current position
     */
    public static determineNewPositionTracking = (
        tripStopSchedule: TripStopsSchedule | undefined,
        trip: IUpdateDelayRunTripsData | IUpdateDelayTripsIdsData,
        position: IPositionTransformationResult
    ): PositionTrackingEnum => {
        let trackingStatus = position.tracking!;
        if (!trip.gtfs_block_id || !trip.gtfs_trip_id || trip.gtfs_route_type != GTFSRouteTypeEnum.TRAIN) {
            return trackingStatus;
        }

        trackingStatus = this.determineTrackingStatus(trip.gtfs_trip_id, tripStopSchedule, position);
        return trackingStatus;
    };

    /**
     * Determine tracking status
     *
     * @param {IUpdateDelayRunTripsData | IUpdateDelayTripsIdsData} gtfsTripId - new trip id
     * @param {TripStopsSchedule | undefined} tripStopSchedule - Cached trip stops for given block id (see Redis)
     * @param {IPositionTransformationResult} position - Current position
     */
    public static determineTrackingStatus = (
        gtfsTripId: string,
        tripStopSchedule: TripStopsSchedule | undefined,
        position: IPositionTransformationResult
    ): PositionTrackingEnum => {
        const { trip_stops: tripStops } = tripStopSchedule?.[gtfsTripId] ?? {};
        if (!tripStops) {
            return position.tracking!;
        }

        const cisLastStopIndex = tripStops.findIndex((stop) => stop.cis === position.cis_last_stop_id);
        const shouldBeTracking = cisLastStopIndex > -1 && cisLastStopIndex < tripStops.length - 1;
        return shouldBeTracking ? PositionTrackingEnum.TRACKING : PositionTrackingEnum.NOT_TRACKING;
    };

    /**
     * Get the next PXAT timestamp at which Redis keys should expire
     */
    public static getNextExpireTimestamp = (): number | undefined => {
        const [hour, minute] = config.vehiclePositions.redisExpireTime.split(":");
        const timeObject = {
            hour: Number(hour),
            minute: Number(minute),
        };

        if (Number.isNaN(timeObject.hour) || Number.isNaN(timeObject.minute)) {
            log.error(
                `[VehiclePositionsUtils] Unable to parse '${config.vehiclePositions.redisExpireTime}'. Redis keys won't expire.`
            );
            return;
        }

        const localDateTime = new Date();
        localDateTime.setMinutes(localDateTime.getMinutes() + 2);

        const expireDateTime = new Date();
        expireDateTime.setHours(timeObject.hour, timeObject.minute, 0, 0);

        if (expireDateTime.valueOf() < localDateTime.valueOf()) {
            expireDateTime.setDate(expireDateTime.getDate() + 1);
        }

        return expireDateTime.valueOf();
    };
}
