import { IStopTime } from "#ie/ropid-gtfs";
import { IVPTripsPositionAttributes } from "#sch/vehicle-positions/models/interfaces/IVPTripsPositionAttributes";
import { IVPTripsLastPositionContext } from "#sch/vehicle-positions/models/interfaces/VPTripsLastPositionInterfaces";
import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import * as turf from "@turf/turf";
import { Feature, Point } from "@turf/turf";
import moment from "moment-timezone";
import { PositionTrackingEnum, StatePositionEnum, StateProcessEnum, TCPEventEnum } from "src/const";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";
import {
    IComputationTrip,
    ICurrentPositionProperties,
    IPositionToUpdate,
    IProcessedPositions,
    IShapeAnchorPoint,
    ITripPositionsWithGTFS,
    IUpdatePositionsIteratorOptions,
} from "../interfaces/VPInterfaces";
import DPPUtils from "./DPPUtils";
import PositionCalculator from "./PositionCalculator";
import { PositionHandlerEnum } from "./PositionHandlerEnum";
import ValidToCalculator from "./ValidToCalculator";
import ComputeDelayHelper from "./compute-positions/ComputeDelayHelper";
import { IPositionStateAndStopSequences } from "./interfaces/IPositionStateAndStopSequences";

export default class PositionsManager {
    /**
     * Compute positions and return computed positions
     *
     * @param {ITripPositionsWithGTFS} tripPositions - Trip positions with shape anchors data
     * @returns {Promise<IProcessedPositions>} - Returns computed/updated positions
     */
    public static computePositions = async (
        tripPositions: ITripPositionsWithGTFS,
        schedule: IScheduleDto[] | undefined
    ): Promise<IProcessedPositions> => {
        const startTimestamp = tripPositions.start_timestamp.getTime();
        const endTimestamp = tripPositions.end_timestamp?.getTime() ?? 0;
        const startDayTimestamp = this.getStartDayTimestamp(
            startTimestamp,
            tripPositions.gtfsData.shapes_anchor_points[0].time_scheduled_seconds
        );

        const gtfsRouteType = tripPositions.gtfs_route_type;
        const context = PositionsManager.getCurrentContext(tripPositions);
        const computedPositions: IPositionToUpdate[] = [];

        return PositionsManager.updatePositions(
            { tripPositions, startTimestamp, endTimestamp, startDayTimestamp, context, computedPositions, gtfsRouteType },
            schedule
        );
    };

    /**
     * Takes position one by one, set proper handler for type of position, and do the process of position
     *
     * @param {number} i - Iteration
     * @param {IUpdatePositionsIteratorOptions} options - Initial options
     * @param {number} cb - Callback function of iterator
     * @returns {void} - void
     */
    public static updatePositions = async (options: IUpdatePositionsIteratorOptions, schedule: IScheduleDto[] | undefined) => {
        const { tripPositions, startDayTimestamp, startTimestamp, endTimestamp, context, computedPositions, gtfsRouteType } =
            options;

        for (let i = 0; i < tripPositions.positions.length; i++) {
            const position = tripPositions.positions[i];
            let positionToUpdate: IPositionToUpdate | null = null;

            // situations
            switch (this.setPositionUpdateHandler(position)) {
                case PositionHandlerEnum.TRACKING:
                    const currentPosition = turf.point([position.lng, position.lat], {
                        id: position.id,
                        origin_time: position.origin_time,
                        origin_timestamp: position.origin_timestamp,
                        scheduled_timestamp: position.scheduled_timestamp,
                        this_stop_id: position.this_stop_id,
                        tcp_event: position.tcp_event,
                    } as ICurrentPositionProperties);

                    positionToUpdate = await this.getEstimatedPoint(
                        tripPositions.gtfsData,
                        currentPosition,
                        context,
                        startDayTimestamp,
                        endTimestamp,
                        tripPositions.gtfs_route_type
                    );

                    if (positionToUpdate.state_position !== StatePositionEnum.MISMATCHED) {
                        positionToUpdate = ComputeDelayHelper.updatePositionToUpdate(
                            context,
                            positionToUpdate,
                            position,
                            gtfsRouteType
                        );
                    }

                    break;
                case PositionHandlerEnum.NOT_TRACKING:
                    // if there is no previous positions with tracking status, set position as before_track
                    const firstShapesAnchorPoint: IShapeAnchorPoint = tripPositions.gtfsData.shapes_anchor_points[0];
                    if (context.lastPositionTracking === null) {
                        const firstStopTime: IStopTime = tripPositions.gtfsData.stop_times[0];
                        // if there is propagated delay we can use it for new position
                        const lastPositionDelayed = context.lastPositionBeforeTrackDelayed;
                        // if there is no delay to duplicate and DPP trip is far from start then invisible
                        const setAsInvisible: boolean =
                            !lastPositionDelayed &&
                            DPPUtils.isInvisible(
                                tripPositions.agency_name_scheduled,
                                position.origin_timestamp,
                                startTimestamp,
                                [position.lng, position.lat],
                                firstShapesAnchorPoint.coordinates,
                                schedule,
                                tripPositions.gtfs_trip_id,
                                context.lastPositionTracking,
                                position.tracking
                            );

                        positionToUpdate = {
                            id: position.id,
                            next_stop_arrival_time: new Date(startDayTimestamp + firstStopTime.arrival_time_seconds! * 1000),
                            next_stop_departure_time: new Date(startDayTimestamp + firstStopTime.departure_time_seconds! * 1000),
                            next_stop_id: firstStopTime.stop_id,
                            next_stop_sequence: firstStopTime.stop_sequence,
                            next_stop_name: firstStopTime.stop.stop_name,
                            shape_dist_traveled: firstStopTime.shape_dist_traveled,
                            state_position: setAsInvisible ? StatePositionEnum.INVISIBLE : StatePositionEnum.BEFORE_TRACK,
                            state_process: StateProcessEnum.PROCESSED,
                            ...(firstStopTime.stop_headsign && {
                                last_stop_headsign: firstStopTime.stop_headsign,
                            }),
                        };

                        if (positionToUpdate.state_position === StatePositionEnum.BEFORE_TRACK) {
                            positionToUpdate.delay = this.getDelayBeforeTrack(
                                lastPositionDelayed?.delay,
                                firstStopTime.departure_time_seconds,
                                position.origin_timestamp.getTime(),
                                startDayTimestamp
                            );
                        }
                    } else {
                        // if there is tracking 2 position with same origin_timestamp then this position is duplicate
                        const statePosition =
                            position.origin_timestamp.getTime() === context.lastPositionOriginTimestamp ||
                            tripPositions.positions.findIndex(
                                (positionItem) =>
                                    positionItem.origin_timestamp.getTime() === position.origin_timestamp.getTime() &&
                                    positionItem.tracking === PositionTrackingEnum.TRACKING
                            ) >= 0
                                ? StatePositionEnum.DUPLICATE
                                : StatePositionEnum.AFTER_TRACK;
                        const lastShapesAnchorPoint: IShapeAnchorPoint =
                            tripPositions.gtfsData.shapes_anchor_points[tripPositions.gtfsData.shapes_anchor_points.length - 1];
                        const lastStopTime: IStopTime =
                            tripPositions.gtfsData.stop_times[tripPositions.gtfsData.stop_times.length - 1];
                        // set as invisible if there are some AFTER_TRACK before
                        const setAsInvisible: boolean =
                            DPPUtils.isInvisible(
                                tripPositions.agency_name_scheduled,
                                position.origin_timestamp,
                                startTimestamp,
                                [position.lng, position.lat],
                                firstShapesAnchorPoint.coordinates,
                                schedule,
                                tripPositions.gtfs_trip_id,
                                context.lastPositionTracking,
                                position.tracking
                            ) && statePosition === StatePositionEnum.AFTER_TRACK;
                        positionToUpdate = {
                            id: position.id,
                            last_stop_arrival_time: new Date(
                                startDayTimestamp + lastShapesAnchorPoint.time_scheduled_seconds * 1000
                            ),
                            last_stop_departure_time: new Date(
                                startDayTimestamp + lastShapesAnchorPoint.time_scheduled_seconds * 1000
                            ),
                            last_stop_id: lastStopTime.stop_id,
                            last_stop_sequence: lastShapesAnchorPoint.last_stop_sequence,
                            shape_dist_traveled: lastShapesAnchorPoint.shape_dist_traveled,
                            state_position: setAsInvisible ? StatePositionEnum.INVISIBLE : statePosition,
                            state_process: StateProcessEnum.PROCESSED,
                            ...(lastStopTime.stop_headsign && {
                                last_stop_headsign: lastStopTime.stop_headsign,
                            }),
                        };
                        positionToUpdate = ComputeDelayHelper.updatePositionToUpdate(
                            context,
                            positionToUpdate,
                            position,
                            gtfsRouteType
                        );
                    }
                    break;
                case PositionHandlerEnum.CANCELED:
                    positionToUpdate = {
                        id: position.id,
                        state_position: StatePositionEnum.CANCELED,
                        state_process: StateProcessEnum.PROCESSED,
                    };
                    break;
                case PositionHandlerEnum.DO_NOTHING:
                    break;
                default:
                    break;
            }

            // if not null push to update
            if (positionToUpdate) {
                positionToUpdate.valid_to = ValidToCalculator.getValidToAttribute(
                    positionToUpdate,
                    position,
                    gtfsRouteType,
                    tripPositions.gtfsData,
                    startTimestamp,
                    startDayTimestamp
                );
                computedPositions.push(positionToUpdate);
            }

            // set if last position is AFTER_TRACK
            context.isLastPositionAfterTrack =
                positionToUpdate?.state_position === StatePositionEnum.AFTER_TRACK ||
                position.state_position === StatePositionEnum.AFTER_TRACK;

            // set last known BEFORE_TRACK_DELAYED position
            if (position.state_position === StatePositionEnum.BEFORE_TRACK_DELAYED) {
                context.lastPositionBeforeTrackDelayed = {
                    delay: position.delay!,
                    origin_timestamp: position.origin_timestamp.getTime(),
                };
            }
            // set last position tracking (only for at_stop and on_track)
            if (
                positionToUpdate?.state_position === StatePositionEnum.AT_STOP ||
                positionToUpdate?.state_position === StatePositionEnum.ON_TRACK ||
                position.state_position === StatePositionEnum.AT_STOP ||
                position.state_position === StatePositionEnum.ON_TRACK
            ) {
                context.lastPositionTracking = turf.point([position.lng, position.lat], {
                    ...position,
                    ...positionToUpdate,
                } as IVPTripsPositionAttributes);
            }

            // set new first position at stop streak if this stop seqence is set and it is not same as before
            if (
                positionToUpdate
                    ? positionToUpdate.this_stop_sequence &&
                      context.atStopStreak.stop_sequence !== positionToUpdate.this_stop_sequence
                    : position.this_stop_sequence && context.atStopStreak.stop_sequence !== position.this_stop_sequence
            ) {
                context.atStopStreak.stop_sequence = positionToUpdate
                    ? positionToUpdate.this_stop_sequence!
                    : position.this_stop_sequence;
                context.atStopStreak.firstPositionTimestamp = position.origin_timestamp.getTime();
                context.atStopStreak.firstPositionDelay = positionToUpdate ? positionToUpdate.delay! : position.delay;
            }
            // IF currently valid updated position / position was processed before
            // and it is NOT AT_STOP
            // then disrupt atStopStreak
            if (
                positionToUpdate &&
                positionToUpdate.state_position !== StatePositionEnum.MISMATCHED &&
                !positionToUpdate.this_stop_sequence
            ) {
                context.atStopStreak.stop_sequence = null;
            } else if (!positionToUpdate && !position.this_stop_sequence) {
                context.atStopStreak.stop_sequence = null;
            }

            // duplicated or mismatched position should not be considered at all
            if (
                positionToUpdate?.state_position !== StatePositionEnum.DUPLICATE &&
                positionToUpdate?.state_position !== StatePositionEnum.INVISIBLE &&
                positionToUpdate?.state_position !== StatePositionEnum.MISMATCHED
            ) {
                context.lastPositionId = position.id;
                context.lastPositionCanceled = position.is_canceled;
                context.lastPositionOriginTimestamp = position.origin_timestamp.getTime();

                ComputeDelayHelper.updateContext(context, positionToUpdate, position, gtfsRouteType);
            }
        }

        return {
            context,
            positions: computedPositions,
        };
    };

    private static getCurrentContext = (tripPositions: ITripPositionsWithGTFS): IVPTripsLastPositionContext => {
        const context: IVPTripsLastPositionContext = tripPositions.last_position_context ?? {
            atStopStreak: {
                stop_sequence: null,
                firstPositionTimestamp: null,
                firstPositionDelay: null,
            },
            lastPositionLastStop: {
                id: null,
                sequence: null,
                arrival_time: null,
                arrival_delay: null,
                departure_time: null,
                departure_delay: null,
            },
            lastPositionDelay: null,
            lastPositionId: null,
            lastPositionOriginTimestamp: null,
            lastPositionTracking: null,
            lastPositionCanceled: null,
            lastPositionBeforeTrackDelayed: null,
            isLastPositionAfterTrack: false,
            tripId: tripPositions.id,
        };

        return context;
    };

    /**
     * Decide how to process input position data
     *
     * @param {IVPTripsPositionAttributes} position - Input vehiclepositions_positions row data
     * @returns {PositionHandlerEnum} - Returns action handler enum
     */
    private static setPositionUpdateHandler = (position: IVPTripsPositionAttributes): PositionHandlerEnum => {
        if (position.state_process === StateProcessEnum.PROCESSED) return PositionHandlerEnum.DO_NOTHING;
        if (position.is_canceled) return PositionHandlerEnum.CANCELED;
        if (position.lat && position.lng && position.tracking === PositionTrackingEnum.TRACKING)
            return PositionHandlerEnum.TRACKING;
        if (position.tracking !== PositionTrackingEnum.TRACKING) return PositionHandlerEnum.NOT_TRACKING;

        return PositionHandlerEnum.NOT_TRACKING;
    };

    /**
     * Returns estimate of point on shape, where the trip should be with appropriate delay
     *
     * @param {IShapeAnchorPoint[]} tripShapePoints - Precalculated trip shape equidistant points with scheduled times
     * @param {Feature<Point, ICurrentPositionProperties>} currentPosition - Current position of trip
     * @param {IVPTripsLastPositionContext | null} context - Context state, holds information about previous positions
     * @param {number} startDayTimestamp - Unix timestamp of midnight before trip starts
     * @returns {IPositionToUpdate} - Position object to update
     */
    private static getEstimatedPoint = async (
        tripGtfsData: IComputationTrip,
        currentPosition: Feature<Point, ICurrentPositionProperties>,
        context: IVPTripsLastPositionContext | null,
        startDayTimestamp: number,
        tripEndTimestamp: number,
        gtfsRouteType: GTFSRouteTypeEnum
    ): Promise<IPositionToUpdate> => {
        // init radius around GPS position ( 200 meters radius, 16 points polygon aka circle)
        const radius = 0.2;
        const defaultStatePosition = PositionsManager.isScheduledAfterTermination(currentPosition, tripEndTimestamp)
            ? StatePositionEnum.AFTER_TRACK
            : StatePositionEnum.OFF_TRACK;

        // Initial value
        let estimatedPoint: IPositionToUpdate = {
            id: currentPosition.properties.id,
            state_position: defaultStatePosition,
            state_process: StateProcessEnum.PROCESSED,
            tcp_event: currentPosition.properties.tcp_event,
            valid_to: ValidToCalculator.getDefaultValidToAttribute(currentPosition.properties.origin_timestamp),
            ...(context &&
                context.lastPositionTracking?.properties.last_stop_sequence && {
                    shape_dist_traveled: context.lastPositionTracking?.properties.shape_dist_traveled!,
                    last_stop_arrival_time: context.lastPositionTracking?.properties.last_stop_arrival_time
                        ? new Date(context.lastPositionTracking?.properties.last_stop_arrival_time)
                        : undefined,
                    last_stop_departure_time: context.lastPositionTracking?.properties.last_stop_departure_time
                        ? new Date(context.lastPositionTracking?.properties.last_stop_departure_time)
                        : undefined,
                    last_stop_sequence: context.lastPositionTracking?.properties.last_stop_sequence!,
                    last_stop_id: context.lastPositionTracking?.properties.last_stop_id!,
                }),
        };

        // returns array of segments (row of points) of shape anchor points around gps
        const ptsInRadius: IShapeAnchorPoint[][] = await PositionCalculator.getShapePointsAroundPosition(
            tripGtfsData.shapes_anchor_points,
            currentPosition,
            radius,
            tripGtfsData.trip_id
        );
        if (ptsInRadius.length < 1) {
            return estimatedPoint;
        }
        // picks only closest point for each segment
        const closestPts: IShapeAnchorPoint[] = await PositionCalculator.getClosestPoints(ptsInRadius, currentPosition);
        if (closestPts.length < 1) {
            return estimatedPoint;
        }
        // DECIDE WHICH POINT IS PROBABLY RIGHT
        return this.getClosestPoint(
            currentPosition,
            context,
            startDayTimestamp,
            tripEndTimestamp,
            closestPts,
            tripGtfsData,
            gtfsRouteType
        );
    };

    /**
     * Picks only one closest point for multiple possible points based on delay of last position and stop times
     *
     * @param {Feature<Point, ICurrentPositionProperties>} currentPosition - Feature Point of current position
     * @param {IVPTripsLastPositionContext | null} context - Context state, holds information about previous positions
     * @param {number} startDayTimestamp - Unix timestamp of start of the day
     * @param {IShapeAnchorPoint[]} closestPts - All closest points of possible segments
     * @param {IComputationTrip} tripGtfsData - GTFS data and all set of known positions
     * @returns {IPositionToUpdate} - Result point as position to update in DB
     */
    private static getClosestPoint = async (
        currentPosition: Feature<Point, ICurrentPositionProperties>,
        context: IVPTripsLastPositionContext | null,
        startDayTimestamp: number,
        tripEndTimestamp: number,
        closestPts: IShapeAnchorPoint[],
        tripGtfsData: IComputationTrip,
        gtfsRouteType: GTFSRouteTypeEnum
    ): Promise<IPositionToUpdate> => {
        // want to find minimum difference of our prediction, where the bus should be
        let minTimeRealDiff = Infinity;
        const tripShapePoints = tripGtfsData.shapes_anchor_points;
        const tripStopTimes = tripGtfsData.stop_times;
        const lastTripShapePoint = tripShapePoints[tripShapePoints.length - 1];

        const closestPtsIterator = (
            i: number,
            prevTimeDelay: number,
            estimatedPoint: IPositionToUpdate | null,
            cb: (estimatedPoint: IPositionToUpdate) => void
        ): void => {
            // end of iteration
            if (closestPts.length === i) {
                return cb(estimatedPoint as IPositionToUpdate);
            }

            let thisClosestPoint = closestPts[i];
            const timeScheduledTimestamp = this.getLocalTimeToDateObject(
                startDayTimestamp,
                // take stop arrival time if is point at stop
                thisClosestPoint.this_stop_sequence
                    ? tripStopTimes[thisClosestPoint.this_stop_sequence - 1].arrival_time_seconds
                    : thisClosestPoint.time_scheduled_seconds
            );

            let timeDelay: number | null = moment
                .utc(currentPosition.properties.origin_timestamp)
                .diff(timeScheduledTimestamp, "seconds");

            // time where the bus should on this point
            const timeProposed = thisClosestPoint.time_scheduled_seconds + prevTimeDelay;

            // difference
            const timeRealDiff = thisClosestPoint.time_scheduled_seconds + timeDelay - timeProposed;

            // we look for the best fitting point
            if (Math.abs(timeRealDiff) < Math.abs(minTimeRealDiff)) {
                const isBusOrTram =
                    gtfsRouteType === GTFSRouteTypeEnum.BUS ||
                    gtfsRouteType === GTFSRouteTypeEnum.TROLLEYBUS ||
                    gtfsRouteType === GTFSRouteTypeEnum.TRAM;

                minTimeRealDiff = timeRealDiff;

                // push trip into nearest at_stop shape anchor point if trip "P" event (tram) / "V" event (bus)
                if (
                    isBusOrTram &&
                    currentPosition.properties.tcp_event === TCPEventEnum.ARRIVAL_ANNOUNCED &&
                    thisClosestPoint.this_stop_sequence === null
                ) {
                    let movingShapePointIndex = thisClosestPoint.index;
                    while (thisClosestPoint.this_stop_sequence === null && movingShapePointIndex < lastTripShapePoint.index) {
                        movingShapePointIndex++;
                        thisClosestPoint = tripShapePoints[movingShapePointIndex];
                    }
                }

                // lets correct delay if it is at stop
                if (thisClosestPoint.this_stop_sequence && timeDelay) {
                    const thisStopSequence = thisClosestPoint.this_stop_sequence;
                    timeDelay = this.getCorrectedTimeDelay(timeDelay, context, currentPosition, thisStopSequence, {
                        departureTime: tripStopTimes[thisStopSequence - 1].departure_time_seconds,
                        arrivalTime: tripStopTimes[thisStopSequence - 1].arrival_time_seconds,
                    });
                }

                let { statePosition, thisStopSequence, lastStopSequence, nextStopSequence } =
                    PositionsManager.getStateAndStopSequences(
                        gtfsRouteType,
                        tripStopTimes,
                        thisClosestPoint,
                        currentPosition.properties,
                        context
                    );

                // test if TCP trip with DEPARTURED event should not bet at stop (not applied for last stop)
                if (
                    currentPosition.properties.tcp_event === TCPEventEnum.DEPARTURED &&
                    thisStopSequence !== lastTripShapePoint.this_stop_sequence
                ) {
                    statePosition = StatePositionEnum.ON_TRACK;
                }

                if (
                    isBusOrTram &&
                    PositionsManager.isAfterTrack(currentPosition, lastStopSequence, lastTripShapePoint, tripEndTimestamp)
                ) {
                    statePosition = StatePositionEnum.AFTER_TRACK;
                    thisStopSequence = lastTripShapePoint.this_stop_sequence;
                    lastStopSequence = lastTripShapePoint.last_stop_sequence;
                    nextStopSequence = lastTripShapePoint.next_stop_sequence;
                    timeDelay = 0;
                }

                // save it for result
                estimatedPoint = {
                    id: currentPosition.properties.id,
                    bearing: thisClosestPoint.bearing,
                    shape_dist_traveled: Math.round(thisClosestPoint.shape_dist_traveled * 1000) / 1000,
                    next_stop_id: tripStopTimes[nextStopSequence - 1].stop_id,
                    last_stop_id: tripStopTimes[lastStopSequence - 1].stop_id,
                    next_stop_name: tripStopTimes[nextStopSequence - 1].stop.stop_name,
                    last_stop_name: tripStopTimes[lastStopSequence - 1].stop.stop_name,
                    next_stop_sequence: nextStopSequence,
                    last_stop_sequence: lastStopSequence,
                    next_stop_arrival_time: this.getLocalTimeToDateObject(
                        startDayTimestamp,
                        tripStopTimes[nextStopSequence - 1].arrival_time_seconds
                    ),
                    last_stop_arrival_time: this.getLocalTimeToDateObject(
                        startDayTimestamp,
                        tripStopTimes[lastStopSequence - 1].arrival_time_seconds
                    ),
                    next_stop_departure_time: this.getLocalTimeToDateObject(
                        startDayTimestamp,
                        tripStopTimes[nextStopSequence - 1].departure_time_seconds
                    ),
                    last_stop_departure_time: this.getLocalTimeToDateObject(
                        startDayTimestamp,
                        tripStopTimes[lastStopSequence - 1].departure_time_seconds
                    ),
                    delay: timeDelay,
                    this_stop_id:
                        (statePosition === StatePositionEnum.AT_STOP &&
                            thisStopSequence &&
                            tripStopTimes[thisStopSequence - 1].stop_id) ||
                        undefined,
                    this_stop_name:
                        (statePosition === StatePositionEnum.AT_STOP &&
                            thisStopSequence &&
                            tripStopTimes[thisStopSequence - 1].stop.stop_name) ||
                        undefined,
                    this_stop_sequence: (statePosition === StatePositionEnum.AT_STOP && thisStopSequence) || undefined,
                    last_stop_headsign: tripStopTimes[lastStopSequence - 1].stop_headsign || undefined,
                    state_position: statePosition,
                    state_process: StateProcessEnum.PROCESSED,
                    valid_to: ValidToCalculator.getDefaultValidToAttribute(currentPosition.properties.origin_timestamp),
                };
            }

            // next step
            closestPtsIterator(i + 1, prevTimeDelay, estimatedPoint, cb);
        };

        return new Promise((resolve) => {
            closestPtsIterator(0, context?.lastPositionTracking?.properties.delay ?? 0, null, (estimatedPoint): void => {
                resolve(estimatedPoint);
            });
        });
    };

    /**
     * Returns date object for stop time
     * + given by start of day when trip starts
     * + and GTFS HH:mm:ss format of stop time in seconds
     *
     * @param {number} startDayTimestamp - Unix timestamp of local time midnight before trip starts
     * @param {number} timeSeconds - Number of seconds given by HH:mm:ss format of GTFS stop time
     * @returns {Date} - Returns date object
     */
    private static getLocalTimeToDateObject = (startDayTimestamp: number, timeSeconds: number): Date => {
        return moment(startDayTimestamp)
            .tz("Europe/Prague")
            .startOf("day")
            .hours(Math.floor(timeSeconds / 3600))
            .minutes(Math.floor((timeSeconds % 3600) / 60))
            .seconds(timeSeconds % 60)
            .toDate();
    };

    /**
     * Corrects time delay at stop with dwelling time
     *
     * @param {number} timeDelay - Initial computed delay in seconds, can be negative for trip ahead
     * @param {IVPTripsLastPositionContext | null} context - Context state, holds information about previous positions
     * @param {Feature<Point, ICurrentPositionProperties>} currentPosition - Feature Point of current position
     * @param {IShapeAnchorPoint} thisClosestPoint - Closest point of shape anchors
     * @param { departureTime: number; arrivalTime: number } stopTimes - departure and arrival stop times in seconds
     * @returns {number} - Result delay in seconds, can be negative for trip ahead
     */
    private static getCorrectedTimeDelay = (
        timeDelay: number,
        context: IVPTripsLastPositionContext | null,
        currentPosition: Feature<Point, ICurrentPositionProperties>,
        thisStopSequence: number | null,
        stopTimes: { departureTime: number; arrivalTime: number }
    ): number => {
        // compute dwell time in stop, most common is zero
        const stopDwellTimeSeconds = stopTimes.departureTime - stopTimes.arrivalTime;
        // if dwell time is sheduled as zero, return initial computed delay
        if (stopDwellTimeSeconds <= 0) {
            return timeDelay;
        }

        // if last position was not in this same stop or there is no last position at all
        if (!context || context.atStopStreak.stop_sequence !== thisStopSequence) {
            // timeDelay >= 0 trip is DELAYED
            // we presume it will lower delay by shortening its scheduled dwell time,
            // cant go under zero of course, trip should not go ahead
            // else trip is AHeAD
            // left computed delay as it was
            return timeDelay >= 0 ? Math.max(timeDelay - stopDwellTimeSeconds, 0) : timeDelay;
        }

        // we presume that first position at same stop is real arrival time
        if (context.atStopStreak.firstPositionDelay! >= 0) {
            // trip was DELAYED before
            // we presume it will lower delay by shortening its scheduled dwell time,
            // cant go under zero of course, trip should not go ahead
            return Math.max(timeDelay - stopDwellTimeSeconds, 0);
        }

        // trip was AHEAD before
        // real dwell time so far
        const realDwellTimeSeconds = Math.round(
            (currentPosition.properties.origin_timestamp.getTime() - context.atStopStreak.firstPositionTimestamp!) / 1000
        );

        // if real dwell is longer than scheduled, then add to negative delay time
        return context.atStopStreak.firstPositionDelay! + Math.max(realDwellTimeSeconds - stopDwellTimeSeconds, 0);
    };

    /**
     * Get delay in seconds for positions before track to ensure correct delay propagation
     */
    private static getDelayBeforeTrack(
        lastPositionDelay: number | null | undefined,
        firstStopDepartureTime: number,
        positionOriginUnixTimestamp: number,
        startDayUnixTimestamp: number
    ): number | null {
        if (typeof lastPositionDelay !== "number") {
            return null;
        }

        const departureTimeInSeconds = startDayUnixTimestamp / 1000 + firstStopDepartureTime;
        const positionOriginTimeInSeconds = positionOriginUnixTimestamp / 1000;
        return Math.max(lastPositionDelay, positionOriginTimeInSeconds - departureTimeInSeconds);
    }

    /**
     * Compute UTC timestamp of start of day when trip starts
     *
     * @param {number} startTimestamp - Unix timestamp of start of the trip
     * @param {number} firstStopTimeScheduledSeconds - Number of seconds from midnight of first stop departure
     * @returns {number} - Returns unix timestamp in milliseconds.
     */
    private static getStartDayTimestamp = (startTimestamp: number, firstStopTimeScheduledSeconds: number): number => {
        let startDayTimestamp = moment.utc(startTimestamp).tz("Europe/Prague").startOf("day");
        const stopTimeDayOverflow = Math.floor(firstStopTimeScheduledSeconds / (60 * 60 * 24));
        // if trip has 24+ stop times set real startDay to yesterday
        if (stopTimeDayOverflow > 0) {
            startDayTimestamp.subtract(1, "day");
        }
        return startDayTimestamp.valueOf();
    };

    private static getStateAndStopSequences(
        gtfsRouteType: GTFSRouteTypeEnum,
        tripStopTimes: IStopTime[],
        thisClosestPoint: IShapeAnchorPoint,
        positionProperties: ICurrentPositionProperties,
        context: Pick<IVPTripsLastPositionContext, "lastPositionTracking"> | null
    ): IPositionStateAndStopSequences {
        let statePosition = thisClosestPoint.this_stop_sequence ? StatePositionEnum.AT_STOP : StatePositionEnum.ON_TRACK;
        let thisStopSequence = thisClosestPoint.this_stop_sequence;
        let lastStopSequence = thisClosestPoint.last_stop_sequence;
        let nextStopSequence = thisClosestPoint.next_stop_sequence;

        if (gtfsRouteType === GTFSRouteTypeEnum.METRO) {
            thisStopSequence = null;

            if (positionProperties.this_stop_id) {
                const thisStopTime = tripStopTimes.find((stopTime) => stopTime.stop_id === positionProperties.this_stop_id);
                if (thisStopTime) {
                    statePosition = StatePositionEnum.AT_STOP;
                    thisStopSequence = thisStopTime.stop_sequence;
                    lastStopSequence = thisStopSequence;
                    nextStopSequence = Math.min(tripStopTimes.length, thisStopSequence + 1);
                }
            }

            if (thisStopSequence === null) {
                statePosition = StatePositionEnum.ON_TRACK;

                if (context?.lastPositionTracking) {
                    lastStopSequence = context.lastPositionTracking.properties.last_stop_sequence ?? lastStopSequence;
                    nextStopSequence = context.lastPositionTracking.properties.next_stop_sequence ?? nextStopSequence;
                }
            }
        }
        // if current positions's TCP event is 'T' and previous position is tracked
        else if (positionProperties.tcp_event === TCPEventEnum.TIME && context?.lastPositionTracking) {
            const previousPositionTcpEvent = context.lastPositionTracking.properties.tcp_event;
            const previousLastStopSequence = context.lastPositionTracking.properties.last_stop_sequence ?? 0;

            // if previous position was 'P' and current last stop sequence is lower than the previous last stop sequence
            // or if previous position was 'O', current position is at stop
            //    and current last stop sequence is lower than the previous last stop sequence
            // then state is MISMATCHED
            // else if previous position was 'O', current position is at stop
            //    and current last stop sequence is equal to the previous last stop sequence
            // then state is ON_TRACK
            if (previousPositionTcpEvent === TCPEventEnum.ARRIVAL_ANNOUNCED && lastStopSequence < previousLastStopSequence) {
                statePosition = StatePositionEnum.MISMATCHED;
            } else if (previousPositionTcpEvent === TCPEventEnum.DEPARTURED && thisStopSequence) {
                if (thisStopSequence === previousLastStopSequence) {
                    statePosition = StatePositionEnum.ON_TRACK;
                } else if (thisStopSequence < previousLastStopSequence) {
                    statePosition = StatePositionEnum.MISMATCHED;
                }
            }

            if (statePosition === StatePositionEnum.MISMATCHED || statePosition === StatePositionEnum.ON_TRACK) {
                thisStopSequence = null;
            }
        }

        return {
            statePosition,
            thisStopSequence,
            lastStopSequence,
            nextStopSequence,
        };
    }

    private static isAfterTrack(
        currentPosition: turf.helpers.Feature<turf.helpers.Point, ICurrentPositionProperties>,
        lastStopSequence: number,
        lastTripShapePoint: IShapeAnchorPoint,
        tripEndTimestamp: number
    ): boolean {
        if (!currentPosition.properties.tcp_event) {
            return false;
        }

        // Trip is terminated OR the vehicle is close to/in the terminus
        if (
            currentPosition.properties.tcp_event === TCPEventEnum.TERMINATED ||
            lastStopSequence >= lastTripShapePoint.last_stop_sequence
        ) {
            return true;
        }

        return PositionsManager.isScheduledAfterTermination(currentPosition, tripEndTimestamp);
    }

    /**
     *  Vehicle sent a TCP message that was scheduled to be sent after termination
     */
    private static isScheduledAfterTermination(
        { properties }: turf.helpers.Feature<turf.helpers.Point, ICurrentPositionProperties>,
        tripEndTimestamp: number
    ): boolean {
        return !!(properties.scheduled_timestamp && properties.scheduled_timestamp.getTime() >= tripEndTimestamp);
    }
}
