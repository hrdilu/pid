import { log } from "@golemio/core/dist/integration-engine";
import * as turf from "@turf/turf";
import { Feature, Point } from "@turf/turf";
import CheapRuler from "cheap-ruler";
import { ICurrentPositionProperties, IShapeAnchorPoint } from "../interfaces/VPInterfaces";

const ruler = CheapRuler(50);

export default class PositionCalculator {
    // FIND ALL SHAPE POINTS IN POLYGON CIRCLE, GET SEGMENTS WITH CONSEQUENTING POINTS
    public static getShapePointsAroundPosition = (
        tripShapePoints: IShapeAnchorPoint[],
        currentPosition: Feature<Point, ICurrentPositionProperties>,
        radius: number,
        tripId: string
    ): IShapeAnchorPoint[][] => {
        try {
            const ptsInRadius: IShapeAnchorPoint[][] = [];
            let segmentIndex = 0;
            let lastWasIn = true;

            // FIND ALL SHAPE POINTS IN POLYGON CIRCLE, GET SEGMENTS WITH CONSEQUENTING POINTS
            for (let i = 0; i < tripShapePoints.length; i++) {
                if (
                    ruler.distance(tripShapePoints[i].coordinates, currentPosition.geometry.coordinates as CheapRuler.Point) <=
                    radius
                ) {
                    if (!lastWasIn) {
                        if (ptsInRadius[segmentIndex] !== undefined) {
                            segmentIndex++;
                        }
                        lastWasIn = true;
                    }
                    if (ptsInRadius[segmentIndex] === undefined) {
                        ptsInRadius[segmentIndex] = [];
                    }
                    ptsInRadius[segmentIndex].push(tripShapePoints[i]);
                } else {
                    lastWasIn = false;
                }
            }

            return ptsInRadius;
        } catch (err) {
            log.error(
                // eslint-disable-next-line max-len
                `Unable to calculate getShapePointsAroundPosition for tripId: ${tripId}, tripShapePoints.length: ${tripShapePoints.length}, error message: ${err.message}`
            );
            return [];
        }
    };

    //form [longitude, latitude]
    public static getDistanceInKilometers(coords1: [number, number], coords2: [number, number]): number {
        return ruler.distance(coords1, coords2);
    }

    /**
     * Picks only one closest point for each segment/array of points of shape
     *
     * @param {IShapeAnchorPoint[][]} ptsInRadius - Feature Point of current position
     * @param {Feature<Point, ICurrentPositionProperties>} currentPosition - Feature Point of current position
     * @returns {IShapeAnchorPoint[]} - Result closest point for each segment
     */
    public static getClosestPoints = async (
        ptsInRadius: IShapeAnchorPoint[][],
        currentPosition: turf.helpers.Feature<turf.helpers.Point, ICurrentPositionProperties>
    ): Promise<IShapeAnchorPoint[]> => {
        const closestPts: IShapeAnchorPoint[] = [];

        const innerPtsInRadiusIterator = (
            i: number,
            options: {
                k: number;
                nPt: any;
            },
            cb: (res: any) => void
        ): void => {
            // end of iteration
            if (ptsInRadius[i].length === options.k) {
                return cb(options.nPt);
            }

            const distance = ruler.distance(
                currentPosition.geometry.coordinates as CheapRuler.Point,
                ptsInRadius[i][options.k].coordinates
            );
            if (distance < options.nPt.distance) {
                options.nPt = ptsInRadius[i][options.k];
                options.nPt.distance = distance;
            }

            // next step
            options.k++;
            innerPtsInRadiusIterator(i, options, cb);
        };
        const ptsInRadiusIterator = (i: number, cb: () => void): void => {
            // end of iteration
            if (ptsInRadius.length === i) {
                return cb();
            }

            const nPt = { distance: Infinity };
            innerPtsInRadiusIterator(
                i,
                {
                    k: 0,
                    nPt,
                },
                (res) => {
                    // so now you have all possible nearest points on shape
                    // (could be more if shape line is overlaping itself)
                    closestPts.push(res);

                    // next step
                    ptsInRadiusIterator(i + 1, cb);
                }
            );
        };
        return new Promise((resolve) => {
            ptsInRadiusIterator(0, () => {
                resolve(closestPts);
            });
        });
    };
}
