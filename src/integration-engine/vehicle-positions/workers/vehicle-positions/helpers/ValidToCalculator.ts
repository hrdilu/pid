import { IVPTripsPositionAttributes } from "#sch/vehicle-positions/models/interfaces/IVPTripsPositionAttributes";
import { StatePositionEnum } from "src/const";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";
import { IComputationTrip, IPositionToUpdate } from "../interfaces/VPInterfaces";
import PositionCalculator from "./PositionCalculator";

export default class ValidToCalculator {
    private static CANCELLED_ADDITION_TIME_IN_MS = 30 * 60 * 1000;
    /**
     * Calculates valid_to attribute value for updateDelay worker
     *
     */
    public static getValidToAttribute = (
        positionToUpdate: IPositionToUpdate,
        tripPositionAttributes: IVPTripsPositionAttributes,
        gtfsRouteType: GTFSRouteTypeEnum,
        gtfsData: IComputationTrip,
        startTimestamp: number,
        startDayTimestamp: number
    ): Date => {
        if (positionToUpdate.state_position === StatePositionEnum.CANCELED) {
            return new Date(
                startDayTimestamp +
                    gtfsData.stop_times[gtfsData.stop_times.length - 1].departure_time_seconds * 1000 +
                    ValidToCalculator.CANCELLED_ADDITION_TIME_IN_MS
            );
        } else if (gtfsRouteType === GTFSRouteTypeEnum.TRAM) {
            // For TRAM only:
            // For all real stops:
            // return default valid to attribute + the time difference between departure and arrival

            if (
                positionToUpdate.state_position == StatePositionEnum.AT_STOP &&
                positionToUpdate.last_stop_arrival_time != positionToUpdate.last_stop_departure_time &&
                positionToUpdate.last_stop_arrival_time &&
                positionToUpdate.last_stop_departure_time
            ) {
                return new Date(
                    this.getDefaultValidToAttribute(tripPositionAttributes.origin_timestamp).getTime() +
                        (positionToUpdate.last_stop_departure_time.getTime() - positionToUpdate.last_stop_arrival_time.getTime())
                );
            }

            // For final positions:
            // return default valid to attribute + the time difference between (next trip departure) and arrival

            if (positionToUpdate.state_position == StatePositionEnum.BEFORE_TRACK) {
                const distance = PositionCalculator.getDistanceInKilometers(
                    [tripPositionAttributes.lng, tripPositionAttributes.lat],
                    [gtfsData.stop_times[0].stop.stop_lon, gtfsData.stop_times[0].stop.stop_lat]
                );

                if (distance <= 1) {
                    return this.getDefaultValidToAttribute(
                        new Date(Math.max(tripPositionAttributes.origin_timestamp.getTime(), startTimestamp))
                    );
                }
            }
        }

        // For all other positions:
        return this.getDefaultValidToAttribute(tripPositionAttributes.origin_timestamp);
    };

    /**
     * Calculate default valid_to attribute
     * return 5min + `origin_timestamp`
     */
    public static getDefaultValidToAttribute = (origin_timestamp: Date): Date => {
        return new Date(origin_timestamp.getTime() + 5 * 60 * 1000);
    };
}
