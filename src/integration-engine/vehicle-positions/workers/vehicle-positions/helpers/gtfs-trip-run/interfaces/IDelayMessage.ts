import { IUpdateDelayRunTripsData } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/TripsRepository";
import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";

export interface IDelayMessage {
    updatedTrips: IUpdateDelayRunTripsData[];
    positions: any[];
    schedule?: IScheduleDto[];
}
