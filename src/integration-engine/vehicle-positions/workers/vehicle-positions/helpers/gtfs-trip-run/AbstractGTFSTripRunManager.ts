import { RunTripsRedisRepository } from "#ie/ropid-gtfs/data-access/cache";
import { CommonRunsRepository } from "#ie/vehicle-positions/workers/runs/data-access/CommonRunsRepository";
import { IMetroRunInputForProcessing } from "#ie/vehicle-positions/workers/runs/interfaces/IMetroRunInputForProcessing";
import { PositionsRepository } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/PositionsRepository";
import { TripsRepository } from "#ie/vehicle-positions/workers/vehicle-positions/data-access/TripsRepository";
import { IVehiclePositionsSchedule } from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/VPInterfaces";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { config } from "@golemio/core/dist/integration-engine/config";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc";
import { Moment } from "@golemio/core/dist/shared/moment-timezone";
import { IUpdateRunsGtfsTripInput } from "../../interfaces/IUpdateRunsGtfsTripInput";
import { DateTimeUtils } from "../DateTimeUtils";
import { VPUtils } from "../VPUtils";
import { IDelayMessage } from "./interfaces/IDelayMessage";
import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";

export abstract class AbstractGTFSTripRunManager {
    protected config: ISimpleConfig;
    private turnaroundEstimateInSeconds: number;

    constructor(
        protected readonly positionsRepository: PositionsRepository,
        protected readonly tripsRepository: TripsRepository,
        protected readonly runsRepository: CommonRunsRepository,
        protected readonly runTripsRedisRepository: RunTripsRedisRepository,
        protected readonly testMode: boolean = false
    ) {
        this.config = IntegrationEngineContainer.resolve<ISimpleConfig>(CoreToken.SimpleConfig);
        this.turnaroundEstimateInSeconds = config.vehiclePositions.turnaroundTimeInSeconds ?? 30;
    }

    public abstract generateDelayMsg(
        schedule: IScheduleDto[],
        input: IUpdateRunsGtfsTripInput | IMetroRunInputForProcessing
    ): Promise<IDelayMessage>;

    public setAndReturnScheduledTrips = async (scheduleData: IVehiclePositionsSchedule): Promise<IScheduleDto[]> => {
        const { route_id, run_number, msg_last_timestamp } = scheduleData;
        const scheduledTripIds = await this.runTripsRedisRepository.get(`${route_id}_${run_number}`);
        if (scheduledTripIds?.schedule) {
            return scheduledTripIds.schedule;
        }

        const scheduledTrips = await this.runsRepository.getScheduledTrips(route_id, run_number, msg_last_timestamp);

        for (let index = 0; index < scheduledTrips.length; index++) {
            const trip = scheduledTrips[index];
            trip.start_timestamp = DateTimeUtils.formatSQLTimestamp(trip.start_timestamp);
            trip.end_timestamp = DateTimeUtils.formatSQLTimestamp(trip.end_timestamp);

            trip.requiredTurnaroundSeconds = this.isSameStop(trip, scheduledTrips, index) ? 0 : this.turnaroundEstimateInSeconds;
        }

        await this.runTripsRedisRepository.set(
            `${route_id}_${run_number}`,
            { schedule: scheduledTrips },
            undefined,
            VPUtils.getNextExpireTimestamp()
        );

        return scheduledTrips;
    };

    protected getCurrentTripForRun(schedule: IScheduleDto[], timestampScheduled: Moment): IScheduleDto | undefined {
        let currentTrip: IScheduleDto | undefined = undefined;
        for (const tripCache of schedule) {
            const startTimestamp = new Date(tripCache.start_timestamp);
            const endTimestamp = new Date(tripCache.end_timestamp);

            if (!timestampScheduled.isBetween(startTimestamp, endTimestamp, "second", "[]")) {
                continue;
            }

            if (!currentTrip || startTimestamp.getTime() > new Date(currentTrip.start_timestamp).getTime()) {
                currentTrip = tripCache;
            }
        }

        return currentTrip;
    }

    /**
     * Get scheduled trips adjacent to run message timestamp (scheduled)
     *   originally based on route id and run number
     */
    protected getAdjacentTrips(schedule: IScheduleDto[], timestampScheduled: Moment, currentTripId: string | undefined) {
        return schedule.filter(
            (scheduledTrip) =>
                scheduledTrip.trip_id !== currentTripId && this.isTripAdjacent(scheduledTrip, timestampScheduled.valueOf())
        );
    }

    protected abstract getAdjacentTripTimeWindow(): ITimeWindow;

    private isTripAdjacent(scheduledTrip: IScheduleDto, messageScheduledTimestamp: number) {
        const { startTimeWindowInMinutes, endTimeWindowInMinutes } = this.getAdjacentTripTimeWindow();
        const startTimestamp = new Date(scheduledTrip.start_timestamp).getTime();
        const endTimestamp = new Date(scheduledTrip.end_timestamp).getTime();
        const leftBound = messageScheduledTimestamp - endTimeWindowInMinutes * 60 * 1000;
        const rightBound = messageScheduledTimestamp + startTimeWindowInMinutes * 60 * 1000;

        return (
            (leftBound <= endTimestamp && endTimestamp <= messageScheduledTimestamp) ||
            (rightBound >= startTimestamp && startTimestamp >= messageScheduledTimestamp)
        );
    }

    private isSameStop = (trip: IScheduleDto, scheduledTrips: IScheduleDto[], index: number) => {
        if (index === 0) {
            return true;
        }

        return trip.first_stop_id === scheduledTrips[index - 1].last_stop_id;
    };
}
