import { IMetroRunInputForProcessing } from "#ie/vehicle-positions/workers/runs/interfaces/IMetroRunInputForProcessing";
import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import { log } from "@golemio/core/dist/integration-engine";
import moment, { Moment } from "@golemio/core/dist/shared/moment-timezone";
import { AbstractGTFSTripRunManager } from "./AbstractGTFSTripRunManager";
import { IDelayMessage } from "./interfaces/IDelayMessage";

export class GTFSTripMetroRunManager extends AbstractGTFSTripRunManager {
    public async generateDelayMsg(schedule: IScheduleDto[], input: IMetroRunInputForProcessing) {
        const timestampScheduled = moment(input.timestampScheduled);
        const currentTrip = this.getCurrentTripForRun(schedule, timestampScheduled);
        let delayMsg: IDelayMessage = {
            updatedTrips: [],
            positions: [],
            schedule,
        };

        if (currentTrip?.trip_id) {
            delayMsg = await this.generateDelayMsgFromCurrentTrip(delayMsg, currentTrip, input);
        } else {
            log.verbose(`generateDelayMsg: could not find current gtfsTrip for metro run: ${JSON.stringify(input)}`);
        }

        // last/next gtfs trip for run
        return this.updateLastTripsAndPositions(schedule, timestampScheduled, currentTrip?.trip_id, input, delayMsg);
    }

    protected getAdjacentTripTimeWindow(): ITimeWindow {
        return {
            startTimeWindowInMinutes: this.config.getValue<number>(
                "old.datasources.pid.vehicle-positions.adjacentTrips.metro.startTimeWindowInMinutes",
                15
            ) as number,
            endTimeWindowInMinutes: this.config.getValue<number>(
                "old.datasources.pid.vehicle-positions.adjacentTrips.metro.endTimeWindowInMinutes",
                2
            ) as number,
        };
    }

    private async generateDelayMsgFromCurrentTrip(
        delayMsg: IDelayMessage,
        gtfsCurrentTrip: IScheduleDto,
        input: IMetroRunInputForProcessing
    ) {
        const isCurrentTripTracking = true;
        const { trip, position } = await this.updateTrip(input, gtfsCurrentTrip, isCurrentTripTracking);

        delayMsg.updatedTrips.push(trip);
        if (this.testMode) delayMsg.positions.push(position);

        return delayMsg;
    }

    private async updateLastTripsAndPositions(
        schedule: IScheduleDto[],
        timestampScheduled: Moment,
        currentTripId: string | undefined,
        input: IMetroRunInputForProcessing,
        delayMsg: IDelayMessage
    ) {
        const gtfsAdjacentTrips = this.getAdjacentTrips(schedule, timestampScheduled, currentTripId);

        for (const gtfsTrip of gtfsAdjacentTrips) {
            const { trip, position } = await this.updateTrip(input, gtfsTrip, false);

            delayMsg.updatedTrips.push(trip);
            if (this.testMode) delayMsg.positions.push(position);
        }

        return delayMsg;
    }

    private async updateTrip(input: IMetroRunInputForProcessing, gtfsTrip: IScheduleDto, isTripTracking: boolean) {
        const trip = await this.tripsRepository.upsertMetroRunTrip(input, gtfsTrip);
        const position = await this.positionsRepository.upsertMetroRunPosition(input, gtfsTrip, isTripTracking);

        return { trip, position };
    }
}
