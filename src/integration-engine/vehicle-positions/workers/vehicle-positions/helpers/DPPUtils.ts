import { IVPTripsPositionAttributes } from "#sch/vehicle-positions/models/interfaces/IVPTripsPositionAttributes";
import { IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import { Feature, Point } from "@turf/turf";
import { PositionTrackingEnum } from "src/const";
import PositionCalculator from "./PositionCalculator";

export default class DPPUtils {
    public static DPP_AGENCY_NAME = "DP PRAHA";
    public static TIME_THRESHOLD_IN_MS = 1 * 60 * 1000;
    public static DISTANCE_THRESHOLD_IN_KM = 0.5;

    // Decide DPP trips to set invisible, in cases when vehicle is on the way from or to garage.
    public static isInvisible = (
        agencyNameScheduled: string,
        originTimestamp: Date,
        startTimestamp: number,
        currentCoordinates: [number, number],
        firstStopCoordinates: [number, number],
        schedule: IScheduleDto[] | undefined,
        gtfsTripId: string,
        lastPositionTracking: Feature<Point, IVPTripsPositionAttributes> | null,
        tracking: number
    ): boolean => {
        // For DPP PRAHA we need filter all buses which are not close to first/last stop (.5km)
        // and still not tracking (except those with time in range of theirs trip stop times)
        if (agencyNameScheduled !== DPPUtils.DPP_AGENCY_NAME) return false;
        if (!schedule) return false;
        const tripDate = schedule.find((el) => new Date(el.start_timestamp).valueOf() == startTimestamp)?.date;
        const tripsForDay = schedule.filter((el) => el.date == tripDate);
        if (tripsForDay.length === 0) return false;
        const firstSegment = tripsForDay[0];
        const lastSegment = tripsForDay[tripsForDay.length - 1];

        return (
            DPPUtils.isOnTheWayFromGarage(originTimestamp, firstSegment, currentCoordinates, firstStopCoordinates) ||
            DPPUtils.isOnTheWayToGarage(lastSegment, gtfsTripId, tracking, lastPositionTracking)
        );
    };

    private static isOnTheWayFromGarage(
        originTimestamp: Date,
        firstSegment: IScheduleDto,
        currentCoordinates: [number, number],
        firstStopCoordinates: [number, number]
    ) {
        if (originTimestamp.getTime() < new Date(firstSegment.start_timestamp).valueOf() - this.TIME_THRESHOLD_IN_MS) {
            const distanceFromFirstStop = PositionCalculator.getDistanceInKilometers(currentCoordinates, firstStopCoordinates);

            return distanceFromFirstStop > this.DISTANCE_THRESHOLD_IN_KM;
        }

        return false;
    }

    private static isOnTheWayToGarage(
        lastSegment: IScheduleDto,
        gtfsTripId: string,
        tracking: number,
        lastPositionTracking: Feature<Point, IVPTripsPositionAttributes> | null
    ) {
        // if it is last line on the track (gtfstripid is same as last trip id in a schedule for a given day)
        // and the position is not tracked and lastPositioTracking var is not null meaning vehicle was already on track
        return (
            lastSegment.trip_id === gtfsTripId && tracking === PositionTrackingEnum.NOT_TRACKING && lastPositionTracking != null
        );
    }
}
