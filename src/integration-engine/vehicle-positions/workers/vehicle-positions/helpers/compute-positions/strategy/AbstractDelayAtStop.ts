import { IPositionToUpdate } from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/VPInterfaces";
import { IVPTripsPositionAttributes } from "#sch/vehicle-positions/models/interfaces/IVPTripsPositionAttributes";
import { IVPTripsLastPositionContext } from "#sch/vehicle-positions/models/interfaces/VPTripsLastPositionInterfaces";
import { StatePositionEnum } from "src/const";

/*  In some cases both position and positionToUpdate are checked for values.
 *  If position state is 'input' then expect value in positionToUpdate instance.
 *  If position was already processed positionToUpdate is null so position instance should be checked.
 */
export default abstract class AbstractDelayAtStop {
    abstract updateContext(
        context: IVPTripsLastPositionContext,
        positionToUpdate: IPositionToUpdate | null,
        position: IVPTripsPositionAttributes
    ): IVPTripsLastPositionContext;

    abstract updatePosition(
        context: IVPTripsLastPositionContext,
        positionToUpdate: IPositionToUpdate,
        position: IVPTripsPositionAttributes | undefined
    ): IPositionToUpdate;

    protected calcDelayAtStop(actualTimestamp: number, scheduledTimestamp: Date | undefined): number | null {
        if (!scheduledTimestamp) {
            return null;
        }

        return (actualTimestamp - scheduledTimestamp.getTime()) / 1000;
    }

    protected resetContext(
        context: IVPTripsLastPositionContext,
        positionToUpdate: IPositionToUpdate | null,
        position: IVPTripsPositionAttributes | undefined
    ): IVPTripsLastPositionContext {
        if (this.isContextChange(context, positionToUpdate, position)) {
            context.lastPositionLastStop.arrival_time = null;
            context.lastPositionLastStop.arrival_delay = null;
            context.lastPositionLastStop.sequence = null;
            context.lastPositionLastStop.departure_time = null;
            context.lastPositionLastStop.departure_delay = null;
        }

        return context;
    }

    protected isContextChange(
        context: IVPTripsLastPositionContext,
        positionToUpdate: IPositionToUpdate | null,
        position: IVPTripsPositionAttributes | undefined
    ) {
        return context.lastPositionLastStop.sequence !== (positionToUpdate?.last_stop_sequence ?? position?.last_stop_sequence);
    }

    protected updatePositionBasedOnContext(
        context: IVPTripsLastPositionContext,
        positionToUpdate: IPositionToUpdate
    ): IPositionToUpdate {
        if (context.lastPositionLastStop.arrival_time && positionToUpdate.last_stop_arrival_time) {
            positionToUpdate.delay_stop_arrival = this.calcDelayAtStop(
                context.lastPositionLastStop.arrival_time,
                positionToUpdate.last_stop_arrival_time
            );
        }
        if (
            context.lastPositionLastStop.departure_time &&
            positionToUpdate.last_stop_departure_time &&
            positionToUpdate.state_position !== StatePositionEnum.AT_STOP &&
            positionToUpdate.state_position !== StatePositionEnum.AFTER_TRACK
        ) {
            positionToUpdate.delay_stop_departure = this.calcDelayAtStop(
                context.lastPositionLastStop.departure_time,
                positionToUpdate.last_stop_departure_time
            );
        }

        return positionToUpdate;
    }
}
