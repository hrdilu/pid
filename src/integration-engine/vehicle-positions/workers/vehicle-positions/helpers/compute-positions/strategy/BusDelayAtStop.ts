import { IPositionToUpdate } from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/VPInterfaces";
import { IVPTripsPositionAttributes } from "#sch/vehicle-positions/models/interfaces/IVPTripsPositionAttributes";
import { IVPTripsLastPositionContext } from "#sch/vehicle-positions/models/interfaces/VPTripsLastPositionInterfaces";
import { StatePositionEnum, PositionTrackingEnum, TCPEventEnum } from "src/const";
import AbstractDelayAtStop from "./AbstractDelayAtStop";

export default class BusDelayAtStop extends AbstractDelayAtStop {
    public updateContext(
        context: IVPTripsLastPositionContext,
        positionToUpdate: IPositionToUpdate | null,
        position: IVPTripsPositionAttributes
    ): IVPTripsLastPositionContext {
        if (!this.isTcpBus(position) || position.tracking !== PositionTrackingEnum.TRACKING) {
            return context;
        }

        context = this.resetContext(context, positionToUpdate, position);

        if (!context.lastPositionLastStop.sequence) {
            context.lastPositionLastStop.sequence = positionToUpdate?.last_stop_sequence ?? position?.last_stop_sequence ?? null;
        }

        if (this.didBusDepart(position)) {
            context.lastPositionLastStop.departure_delay = context.lastPositionDelay;
        }

        return context;
    }

    public updatePosition(
        context: IVPTripsLastPositionContext,
        positionToUpdate: IPositionToUpdate,
        position: IVPTripsPositionAttributes | undefined
    ): IPositionToUpdate {
        if (!this.isTcpBus(position) || position.tracking !== PositionTrackingEnum.TRACKING) {
            return positionToUpdate;
        }

        context = this.resetContext(context, positionToUpdate, position);

        if (this.didBusDepart(position) && positionToUpdate.state_position !== StatePositionEnum.AFTER_TRACK) {
            positionToUpdate.delay_stop_departure = positionToUpdate.delay ?? position.delay;
        } else if (context.lastPositionLastStop.departure_delay !== null) {
            positionToUpdate.delay_stop_departure = context.lastPositionLastStop.departure_delay;
        }

        return positionToUpdate;
    }

    private isTcpBus(position: IVPTripsPositionAttributes | undefined): position is IVPTripsPositionAttributes {
        return !!position?.tcp_event;
    }

    private didBusDepart(position: IVPTripsPositionAttributes) {
        return position.tcp_event === TCPEventEnum.DEPARTURED;
    }
}
