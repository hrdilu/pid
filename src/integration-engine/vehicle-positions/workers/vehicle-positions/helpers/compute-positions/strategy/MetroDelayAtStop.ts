import { IPositionToUpdate } from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/VPInterfaces";
import { IVPTripsPositionAttributes } from "#sch/vehicle-positions/models/interfaces/IVPTripsPositionAttributes";
import { IVPTripsLastPositionContext } from "#sch/vehicle-positions/models/interfaces/VPTripsLastPositionInterfaces";
import { config } from "@golemio/core/dist/integration-engine/config";
import { PositionTrackingEnum, StatePositionEnum } from "src/const";
import AbstractDelayAtStop from "./AbstractDelayAtStop";

export default class MetroDelayAtStop extends AbstractDelayAtStop {
    private readonly metroArrivalDelayAddition: number;
    private readonly metroDepartureDelayAddition: number;

    constructor() {
        super();
        this.metroArrivalDelayAddition = config.vehiclePositions.metroArrivalDelayAddition;
        this.metroDepartureDelayAddition = config.vehiclePositions.metroDepartureDelayAddition;
    }

    public updateContext(
        context: IVPTripsLastPositionContext,
        positionToUpdate: IPositionToUpdate | null,
        position: IVPTripsPositionAttributes
    ): IVPTripsLastPositionContext {
        if (position.tracking !== PositionTrackingEnum.TRACKING) {
            return context;
        }

        context = this.resetContext(context, positionToUpdate, position);

        if (!context.lastPositionLastStop.sequence) {
            context.lastPositionLastStop.sequence = positionToUpdate?.last_stop_sequence ?? position?.last_stop_sequence ?? null;
        }

        if (
            positionToUpdate?.state_position === StatePositionEnum.AT_STOP ||
            positionToUpdate?.state_position === StatePositionEnum.ON_TRACK
        ) {
            context.lastPositionLastStop.arrival_delay = positionToUpdate.delay_stop_arrival ?? null;
            context.lastPositionLastStop.departure_delay = positionToUpdate.delay_stop_departure ?? null;
        }

        return context;
    }

    public updatePosition(
        context: IVPTripsLastPositionContext,
        positionToUpdate: IPositionToUpdate,
        position: IVPTripsPositionAttributes | undefined
    ): IPositionToUpdate {
        if (position?.tracking !== PositionTrackingEnum.TRACKING) {
            return positionToUpdate;
        }

        context = this.resetContext(context, positionToUpdate, position);
        positionToUpdate.delay_stop_arrival = context.lastPositionLastStop.arrival_delay;
        positionToUpdate.delay_stop_departure = context.lastPositionLastStop.departure_delay;

        // Compute delay of the current metro position
        // as the difference between the scheduled and the origin timestamp of the original message
        let delayInSeconds: number | null = null;
        if (position.origin_timestamp !== null && position.scheduled_timestamp !== null) {
            delayInSeconds = (position.origin_timestamp.getTime() - position.scheduled_timestamp.getTime()) / 1000;
        }

        if (positionToUpdate.state_position === StatePositionEnum.AT_STOP && context.atStopStreak.stop_sequence === null) {
            positionToUpdate.delay_stop_arrival =
                delayInSeconds === null ? null : delayInSeconds + this.metroArrivalDelayAddition;
        } else if (positionToUpdate.state_position === StatePositionEnum.ON_TRACK && context.atStopStreak.stop_sequence) {
            positionToUpdate.delay_stop_departure =
                delayInSeconds === null ? null : delayInSeconds + this.metroDepartureDelayAddition;
        }

        return positionToUpdate;
    }
}
