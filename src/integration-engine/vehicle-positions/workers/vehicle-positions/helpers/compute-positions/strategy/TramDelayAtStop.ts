import { IPositionToUpdate } from "#ie/vehicle-positions/workers/vehicle-positions/interfaces/VPInterfaces";
import { IVPTripsPositionAttributes } from "#sch/vehicle-positions/models/interfaces/IVPTripsPositionAttributes";
import { IVPTripsLastPositionContext } from "#sch/vehicle-positions/models/interfaces/VPTripsLastPositionInterfaces";
import { PositionTrackingEnum, TCPEventEnum } from "src/const";
import AbstractDelayAtStop from "./AbstractDelayAtStop";

export default class TramDelayAtStop extends AbstractDelayAtStop {
    public updateContext(
        context: IVPTripsLastPositionContext,
        positionToUpdate: IPositionToUpdate | null,
        position: IVPTripsPositionAttributes
    ): IVPTripsLastPositionContext {
        context = this.resetContext(context, positionToUpdate, position);
        if (this.didTramArrive(position)) {
            context.lastPositionLastStop.arrival_time = position.origin_timestamp.getTime();
            context.lastPositionLastStop.sequence = positionToUpdate?.this_stop_sequence ?? position?.this_stop_sequence ?? null;
            context.lastPositionLastStop.departure_time = null;
        } else if (this.didTramDepart(position, positionToUpdate, context)) {
            context.lastPositionLastStop.departure_time = position.origin_timestamp.getTime();
        }

        return context;
    }

    public updatePosition(
        context: IVPTripsLastPositionContext,
        positionToUpdate: IPositionToUpdate,
        position: IVPTripsPositionAttributes | undefined
    ): IPositionToUpdate {
        context = this.resetContext(context, positionToUpdate, position);
        positionToUpdate = this.updatePositionBasedOnContext(context, positionToUpdate);

        if (position && this.didTramArrive(position)) {
            positionToUpdate.delay_stop_arrival = this.calcDelayAtStop(
                position.origin_timestamp.getTime(),
                positionToUpdate.last_stop_arrival_time
            );
        } else if (
            position &&
            this.didTramDepart(position, positionToUpdate, context) &&
            (positionToUpdate.delay_stop_departure === undefined || positionToUpdate.delay_stop_departure === null)
        ) {
            positionToUpdate.delay_stop_departure = this.calcDelayAtStop(
                position.origin_timestamp.getTime(),
                positionToUpdate.last_stop_departure_time
            );
        }

        return positionToUpdate;
    }

    private didTramArrive(position: IVPTripsPositionAttributes | undefined) {
        return position?.tcp_event === TCPEventEnum.ARRIVAL_ANNOUNCED && position.tracking === PositionTrackingEnum.TRACKING;
    }

    private didTramDepart(
        position: IVPTripsPositionAttributes | undefined,
        positionToUpdate: IPositionToUpdate | null,
        context: IVPTripsLastPositionContext
    ) {
        return (
            position?.tcp_event === TCPEventEnum.DEPARTURED &&
            context.lastPositionLastStop.sequence === (position?.last_stop_sequence ?? positionToUpdate?.last_stop_sequence) &&
            position.tracking === PositionTrackingEnum.TRACKING
        );
    }
}
