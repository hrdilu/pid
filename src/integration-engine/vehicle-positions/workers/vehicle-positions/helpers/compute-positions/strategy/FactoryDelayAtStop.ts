import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";
import AbstractDelayAtStop from "./AbstractDelayAtStop";
import BusDelayAtStop from "./BusDelayAtStop";
import MetroDelayAtStop from "./MetroDelayAtStop";
import TramDelayAtStop from "./TramDelayAtStop";

export default class FactoryDelayAtStop {
    private static _instance: FactoryDelayAtStop;
    private dictionary: Map<GTFSRouteTypeEnum, AbstractDelayAtStop>;

    public static getInstance() {
        if (!this._instance) {
            this._instance = new FactoryDelayAtStop();
        }

        return this._instance;
    }

    private constructor() {
        this.dictionary = new Map<GTFSRouteTypeEnum, AbstractDelayAtStop>();
        this.dictionary.set(GTFSRouteTypeEnum.TRAM, new TramDelayAtStop());
        this.dictionary.set(GTFSRouteTypeEnum.BUS, new BusDelayAtStop());
        this.dictionary.set(GTFSRouteTypeEnum.TROLLEYBUS, new BusDelayAtStop());
        this.dictionary.set(GTFSRouteTypeEnum.METRO, new MetroDelayAtStop());
    }

    public getStrategy(gtfsRouteType: GTFSRouteTypeEnum): AbstractDelayAtStop | null {
        if (this.dictionary.has(gtfsRouteType)) {
            return this.dictionary.get(gtfsRouteType)!;
        }

        return null;
    }
}
