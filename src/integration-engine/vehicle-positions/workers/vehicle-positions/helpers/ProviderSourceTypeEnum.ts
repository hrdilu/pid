export enum ProviderSourceTypeEnum {
    Unknown = "0",
    Http = "1",
    TcpCommon = "2",
    TcpMetro = "3",
}
