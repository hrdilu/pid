import { DateTime } from "@golemio/core/dist/shared/luxon";

export class DateTimeUtils {
    public static TIMEZONE = "Europe/Prague";
    // ISO 8601, no fractional seconds (same as moment.defaultFormat)
    private static FORMAT_DATE_TIME = "yyyy-LL-dd'T'HH:mm:ssZZ";
    private static FORMAT_TIME = "HH:mm:ss";

    /**
     * @example 2022-10-26 08:55:10+01 -> 2022-10-26T09:55:10+02:00
     */
    public static formatSQLTimestamp(sqlTimestamp: string) {
        return DateTime.fromSQL(sqlTimestamp).setZone(this.TIMEZONE).toFormat(this.FORMAT_DATE_TIME);
    }

    /**
     * @example 2022-10-26T08:55:10+01:00 -> 09:55:10
     */
    public static parseUTCTimeFromISO(isoTimestamp = new Date().toISOString()) {
        return DateTime.fromISO(isoTimestamp).setZone(this.TIMEZONE).toFormat(this.FORMAT_TIME);
    }
}
