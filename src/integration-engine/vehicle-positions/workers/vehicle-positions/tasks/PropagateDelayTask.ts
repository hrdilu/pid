import { RunTripsRedisRepository } from "#ie/ropid-gtfs/data-access/cache";
import { IVPTripsPositionAttributes } from "#sch/vehicle-positions/models/interfaces/IVPTripsPositionAttributes";
import { IGtfsRunTripCacheDto, IScheduleDto } from "#sch/vehicle-positions/redis/interfaces/IGtfsRunTripCacheDto";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import { Feature, Point } from "@turf/turf";
import { StatePositionEnum } from "src/const";
import { IUpdateDelayRunTripsData, IUpdateDelayTripsIdsData, TripsRepository } from "../data-access/TripsRepository";
import { IPropagateDelayInput } from "../interfaces/IPropagateDelayInput";
import { IProcessedPositions, IPropagateDelay } from "../interfaces/VPInterfaces";
import { PropagateDelayValidationSchema } from "../schema/PropagateDelaySchema";

export class PropagateDelayTask extends AbstractTask<IPropagateDelayInput> {
    public readonly queueName = "propagateDelay";
    public readonly queueTtl = 5 * 60 * 1000; // 5 minutes
    public readonly schema = PropagateDelayValidationSchema;

    private readonly tripsRepository: TripsRepository;
    private readonly runTripsRedisRepository: RunTripsRedisRepository;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.tripsRepository = new TripsRepository();
        this.runTripsRedisRepository = new RunTripsRedisRepository();
    }

    protected async execute(data: IPropagateDelayInput) {
        const delayedPositionsWithRunTrips = this.mapDelayedPositionsToRunTrips(data.processedPositions, data.trips);
        for (const delayedPositions of delayedPositionsWithRunTrips) {
            const { internal_route_name, internal_run_number, gtfs_trip_id, vehicle_registration_number, start_timestamp } =
                delayedPositions.trip;

            const scheduledTrips: IGtfsRunTripCacheDto | undefined = await this.runTripsRedisRepository.get(
                `${internal_route_name}_${internal_run_number}`
            );

            const positionTripWeekDay = new Date(start_timestamp).getDay();
            const gtfsCurrentTrip = scheduledTrips?.schedule.find((trip) => {
                return this.isCurrentGtfsTrip(trip, gtfs_trip_id, positionTripWeekDay);
            });

            if (!gtfsCurrentTrip) {
                log.warn(
                    `${this.queueName}: could not determine GTFS trip from run trips data: ` +
                        JSON.stringify(delayedPositions.trip).substring(0, 2000)
                );
                continue;
            }

            const gtfsNextTrips = scheduledTrips!.schedule.filter((trip) => {
                return this.isConnectingLinkIn60Minutes(gtfsCurrentTrip, trip);
            });

            try {
                await this.propagateDelayForGtfsTrip({
                    currentGtfsTripId: gtfs_trip_id,
                    currentOriginTimestamp: delayedPositions.position.properties.origin_timestamp,
                    currentDelay: delayedPositions.position.properties.delay!,
                    currentRegistrationNumber: vehicle_registration_number,
                    currentStartTime: gtfsCurrentTrip.start_timestamp,
                    currentEndTime: gtfsCurrentTrip.end_timestamp,
                    nextGtfsTrips: gtfsNextTrips.map((trip) => {
                        return {
                            trip_id: trip.trip_id,
                            requiredTurnaroundSeconds: trip.requiredTurnaroundSeconds,
                        };
                    }),
                });
            } catch (err) {
                log.warn(`${this.queueName}: could not propagate delay for GTFS trip: ${gtfs_trip_id} - ${err.message}`);
            }
        }
    }

    private mapDelayedPositionsToRunTrips = (
        processedPositions: IProcessedPositions[],
        updatedTrips: Array<IUpdateDelayTripsIdsData | IUpdateDelayRunTripsData>
    ): Array<{ position: Feature<Point, IVPTripsPositionAttributes>; trip: IUpdateDelayRunTripsData }> => {
        const tripPositionMap: Array<{ position: Feature<Point, IVPTripsPositionAttributes>; trip: IUpdateDelayRunTripsData }> =
            [];
        for (const batch of processedPositions) {
            if (batch.context.lastPositionTracking === null) {
                continue;
            }
            const {
                properties: { state_position },
            } = batch.context.lastPositionTracking;
            if (state_position !== StatePositionEnum.ON_TRACK && state_position !== StatePositionEnum.AT_STOP) {
                continue;
            }
            const trip = updatedTrips.find((el) => el.id === batch.context.tripId);
            if (trip && this.isUpdateDelayRunTripsIdsData(trip)) {
                tripPositionMap.push({
                    position: batch.context.lastPositionTracking,
                    trip: trip as IUpdateDelayRunTripsData,
                });
            }
        }
        return tripPositionMap;
    };

    private isUpdateDelayRunTripsIdsData = (
        data: IUpdateDelayTripsIdsData | IUpdateDelayRunTripsData
    ): data is IUpdateDelayRunTripsData => {
        const { internal_route_name, internal_run_number, gtfs_trip_id, vehicle_registration_number } =
            data as IUpdateDelayRunTripsData;
        return !!(
            internal_route_name &&
            internal_run_number &&
            gtfs_trip_id &&
            typeof vehicle_registration_number !== "undefined"
        );
    };

    /**
     * Takes all newly computed positions search for next trips in near future and propagate delay for them
     */
    private propagateDelayForGtfsTrip = async (data: IPropagateDelay) => {
        const results = await this.tripsRepository.getPropagateDelayTripsWithPositions(data);
        if (!results.length) return;

        let turnTimeAcc = (+results[0].start_timestamp! - new Date(data.currentEndTime).getTime()) / 1000;
        const rowsToUpdate = results.map((item, i) => {
            const requiredTurnaroundSeconds =
                data.nextGtfsTrips.find((element) => element.trip_id === results[i].gtfs_trip_id)?.requiredTurnaroundSeconds ?? 0;

            turnTimeAcc +=
                i > 0
                    ? (+results[i].start_timestamp! - +results[i - 1].end_timestamp!) / 1000 - requiredTurnaroundSeconds
                    : -requiredTurnaroundSeconds;

            return {
                position: {
                    id: item.last_position.id,
                    delay: Math.max(0, data.currentDelay - turnTimeAcc),
                    state_position: StatePositionEnum.BEFORE_TRACK_DELAYED,
                    origin_timestamp: item.last_position.origin_timestamp,
                },
                trip: {
                    id: item.id,
                },
            };
        });
        await this.tripsRepository.bulkUpdatePropagatedDelay(rowsToUpdate);
    };

    private isConnectingLinkIn60Minutes(gtfsCurrentTrip: IScheduleDto, trip: IScheduleDto) {
        const currentTripEndTime = new Date(gtfsCurrentTrip.end_timestamp);
        const nextTripStartTime = new Date(trip.start_timestamp);
        const currentTripEndTimePlus60Minutes = new Date(currentTripEndTime.getTime() + 60 * 60 * 1000);

        return currentTripEndTimePlus60Minutes >= nextTripStartTime && currentTripEndTime <= nextTripStartTime;
    }

    private isCurrentGtfsTrip(tripSchedule: IScheduleDto, propagatedGtfsTripId: string, propagatedTripWeekDay: number) {
        const tripWeekDay = new Date(tripSchedule.start_timestamp).getDay();
        return tripSchedule.trip_id === propagatedGtfsTripId && tripWeekDay === propagatedTripWeekDay;
    }
}
