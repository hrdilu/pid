import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine/workers";
import { TripsRepository } from "../data-access/TripsRepository";

export class RefreshGtfsTripDataTask extends AbstractEmptyTask {
    public readonly queueName = "refreshGTFSTripData";
    public readonly queueTtl = 10 * 60 * 1000; // 10 minutes

    private readonly tripsRepository: TripsRepository;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.tripsRepository = new TripsRepository();
    }

    protected async execute() {
        await this.tripsRepository.refreshGtfsTripData();
    }
}
