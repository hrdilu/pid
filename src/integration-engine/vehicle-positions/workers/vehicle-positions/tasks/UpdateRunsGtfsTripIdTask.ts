import { RunTripsRedisRepository } from "#ie/ropid-gtfs/data-access/cache";
import { CommonRunsRepository } from "#ie/vehicle-positions/workers/runs/data-access/CommonRunsRepository";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import { ArrayNotPublicRegistrationNumbers } from "src/const";
import PositionsMapper from "../data-access/helpers/PositionsMapper";
import TripsIdGenerator from "../data-access/helpers/TripsIdGenerator";
import TripsMapper from "../data-access/helpers/TripsMapper";
import { PositionsRepository } from "../data-access/PositionsRepository";
import { TripsRepository } from "../data-access/TripsRepository";
import { AbstractGTFSTripRunManager } from "../helpers/gtfs-trip-run/AbstractGTFSTripRunManager";
import { GTFSTripRunManagerFactory, GTFSTripRunType } from "../helpers/gtfs-trip-run/GTFSTripRunManagerFactory";
import { IUpdateRunsGtfsTripInput } from "../interfaces/IUpdateRunsGtfsTripInput";
import { UpdateRunsGtfsTripIdValidationSchema } from "../schema/UpdateRunsGtfsTripIdSchema";

export class UpdateRunsGtfsTripIdTask extends AbstractTask<IUpdateRunsGtfsTripInput> {
    public readonly queueName = "updateRunsGTFSTripId";
    public readonly queueTtl = 5 * 60 * 1000; // 5 minutes
    public readonly schema = UpdateRunsGtfsTripIdValidationSchema;

    private readonly tripsRepository: TripsRepository;
    private readonly positionsRepository: PositionsRepository;
    private readonly commonRunsRepository: CommonRunsRepository;
    private readonly runTripsRedisRepository: RunTripsRedisRepository;
    private readonly gtfsTripRunManager: AbstractGTFSTripRunManager;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.tripsRepository = new TripsRepository();
        this.positionsRepository = new PositionsRepository();
        this.commonRunsRepository = new CommonRunsRepository();
        this.runTripsRedisRepository = new RunTripsRedisRepository();

        this.gtfsTripRunManager = GTFSTripRunManagerFactory.create(
            GTFSTripRunType.Common,
            this.positionsRepository,
            this.tripsRepository,
            this.commonRunsRepository,
            this.runTripsRedisRepository
        );
    }

    /**
     * Process trips from runs data
     */
    protected async execute(data: IUpdateRunsGtfsTripInput) {
        const schedule = await this.gtfsTripRunManager.setAndReturnScheduledTrips(data.run);

        if (schedule.length === 0) {
            if (ArrayNotPublicRegistrationNumbers.includes(data.run.registration_number)) {
                await this.updateNotPublicRunTrip(data);
            } else {
                log.verbose(`updateRunsGTFSTripId: no schedule for run: ${JSON.stringify(data.run)}`);
            }
            return;
        }

        if (!data.run_message.actual_stop_timestamp_scheduled) {
            log.warn(
                `updateRunsGTFSTripId: actualStopTimestampScheduled is not provided for current run: ${JSON.stringify(data.run)}`
            );
            return;
        }

        const delayMsg = await this.gtfsTripRunManager.generateDelayMsg(schedule, data);

        // send to updateDelay only if there are some trips updated
        if (delayMsg.updatedTrips.length > 0) {
            await QueueManager.sendMessageToExchange(this.queuePrefix, "updateDelay", delayMsg);
        }
    }

    /**
     * Process not public trips from runs data
     */
    private updateNotPublicRunTrip = async (input: IUpdateRunsGtfsTripInput) => {
        const tripId = TripsIdGenerator.generateFromCommonRunNotPublic(input.run);

        //#region position
        const positionDto = PositionsMapper.mapCommonRunNotPublicToDto(input.run_message, tripId);
        const createdPosition = await this.positionsRepository.createNotPublic(positionDto);
        //#endregion

        //#region trip
        const tripDto = TripsMapper.mapCommonRunNotPublicToDto(input.run, tripId, createdPosition.id);
        await this.tripsRepository.upsertNotPublic(tripDto);
        //#endregion
    };
}
