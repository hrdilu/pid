import { RopidGTFSStopTimesModel } from "#ie/ropid-gtfs/RopidGTFSStopTimesModel";
import { RopidGTFSTripsModel } from "#ie/ropid-gtfs/RopidGTFSTripsModel";
import { DelayComputationRedisRepository, BlockStopsRedisRepository } from "#ie/ropid-gtfs/data-access/cache";
import { IntegrationErrorHandler, log } from "@golemio/core/dist/integration-engine/helpers";
import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { ITripPositions, PositionsRepository } from "../data-access/PositionsRepository";
import { TripsRepository } from "../data-access/TripsRepository";
import { DelayComputationManager } from "../helpers/DelayComputationManager";
import PositionsManager from "../helpers/PositionsManager";
import { TripStopsManager } from "../helpers/TripStopsManager";
import { VPUtils } from "../helpers/VPUtils";
import { IUpdateDelayInput } from "../interfaces/IUpdateDelayInput";
import { IPositionTransformationResult } from "../interfaces/TransformationInterfaces";
import {
    IComputationTrip,
    IProcessedPositions,
    ITripPositionsWithGTFS,
    ITripPositionsWithOrWithoutGTFS,
    TripStopsSchedule,
} from "../interfaces/VPInterfaces";
import { UpdateDelayValidationSchema } from "../schema/UpdateDelaySchema";

export class UpdateDelayTask extends AbstractTask<IUpdateDelayInput> {
    public readonly queueName = "updateDelay";
    public readonly queueTtl = 5 * 60 * 1000; // 5 minutes
    public readonly schema = UpdateDelayValidationSchema;

    private readonly tripsRepository: TripsRepository;
    private readonly positionsRepository: PositionsRepository;
    private readonly delayComputationRedisRepository: DelayComputationRedisRepository;
    private readonly tripStopsManager: TripStopsManager;
    private readonly delayComputationManager: DelayComputationManager;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.tripsRepository = new TripsRepository();
        this.positionsRepository = new PositionsRepository();
        this.delayComputationRedisRepository = new DelayComputationRedisRepository();

        const gtfsStopTimesRepository = new RopidGTFSStopTimesModel();
        const gtfsTripsRepository = new RopidGTFSTripsModel();
        const blockStopsRedisRepository = new BlockStopsRedisRepository();

        this.tripStopsManager = new TripStopsManager(gtfsStopTimesRepository, blockStopsRedisRepository);
        this.delayComputationManager = new DelayComputationManager(gtfsTripsRepository);
    }

    protected async execute(data: IUpdateDelayInput) {
        const tripIds: string[] = data.updatedTrips.map((trip) => trip.id);

        if (data.positions?.length > 0) {
            // unique ids in input data in key-value (id-positions) structure
            let tripIdToPositionDict: Record<string, IPositionTransformationResult> = {};
            for (const position of data.positions) {
                tripIdToPositionDict[position.trips_id] = position;
            }

            // unique block_ids and its parent input trips_id in key-value structure
            let tripsIdsByBlockIds: Record<string, string> = {};

            // unique block_ids and its parent gtfs_trip_id in key-value structure
            let blockIdToTripIdsDict: Record<string, Set<string>> = {};

            for (const trip of data.updatedTrips) {
                if (!trip.gtfs_block_id) {
                    continue;
                }

                if (!tripsIdsByBlockIds[trip.gtfs_block_id]) {
                    tripsIdsByBlockIds[trip.gtfs_block_id] = trip.id;
                } else {
                    // save only the shorter trip_id (it is the raw input id with no suffixes)
                    tripsIdsByBlockIds[trip.gtfs_block_id] =
                        tripsIdsByBlockIds[trip.gtfs_block_id] > trip.id ? trip.id : tripsIdsByBlockIds[trip.gtfs_block_id];
                }

                if (!blockIdToTripIdsDict[trip.gtfs_block_id]) {
                    blockIdToTripIdsDict[trip.gtfs_block_id] = new Set();
                }

                if (trip.gtfs_trip_id) {
                    blockIdToTripIdsDict[trip.gtfs_block_id].add(trip.gtfs_trip_id);
                }
            }

            // Get cached schedule for each unique block_id
            const blockIdToTripStopScheduleDict = await Object.entries(blockIdToTripIdsDict).reduce(
                async (acc, [blockId, gtfsTripIds]) => {
                    const tripStops = await acc;
                    try {
                        const schedule = await this.tripStopsManager.setAndReturnTripStopsSchedule(blockId, [...gtfsTripIds]);
                        tripStops[blockId] = schedule;
                    } catch (err) {
                        IntegrationErrorHandler.handle(err);
                    }

                    return acc;
                },
                Promise.resolve({} as Record<string, TripStopsSchedule>)
            );

            // duplicate positions for trips with block_ids
            for (const trip of data.updatedTrips) {
                if (tripIdToPositionDict[trip.id]) {
                    if (trip.gtfs_block_id) {
                        // Determine current position tracking status
                        tripIdToPositionDict[trip.id].tracking = VPUtils.determineTrackingStatus(
                            trip.gtfs_trip_id!,
                            blockIdToTripStopScheduleDict[trip.gtfs_block_id],
                            tripIdToPositionDict[trip.id]
                        );
                    }

                    continue;
                }

                const currentPosition = data.positions.find((position) => {
                    return position.trips_id === tripsIdsByBlockIds[trip.gtfs_block_id!];
                });

                if (!currentPosition) {
                    continue;
                }

                // Determine position duplication and new position tracking status
                const newPositionTrackingStatus = VPUtils.determineNewPositionTracking(
                    blockIdToTripStopScheduleDict[trip.gtfs_block_id!],
                    trip,
                    currentPosition
                );

                // duplicate position with new created trip for second trip
                const newPosition = { ...currentPosition };
                newPosition.trips_id = trip.id;
                newPosition.tracking = newPositionTrackingStatus;
                data.positions.push(newPosition);
            }

            // save all new positions
            await this.positionsRepository.save(data.positions);
        }

        if (data.updatedTrips?.length > 0) {
            try {
                // Get all positions for each trip
                const tripsPositionsToUpdate = await this.positionsRepository.getPositionsForUpdateDelay(tripIds);

                // Append gtfs data to each trip
                const tripsPositionsWithOrWithoutGTFSDataToUpdate: ITripPositionsWithOrWithoutGTFS[] = await Promise.all(
                    tripsPositionsToUpdate.map(async (trip: ITripPositions) => {
                        const gtfsData = (await this.delayComputationRedisRepository.get(
                            trip.gtfs_trip_id
                        )) as IComputationTrip | null;
                        return {
                            ...trip,
                            gtfsData,
                        };
                    })
                );

                // Process every position
                const processedPositions = await Promise.all(
                    tripsPositionsWithOrWithoutGTFSDataToUpdate.map(async (trip) => {
                        // if gtfs shape points and schedules data is not ready, calculate it first
                        if (trip.gtfsData === null) {
                            log.debug("Delay Computation data (Redis) was not found. (gtfsTripId = " + trip.gtfs_trip_id + ")");
                            try {
                                const gtfsComputationData = await this.delayComputationManager.getComputationObject(
                                    trip.gtfs_trip_id
                                );

                                await this.delayComputationRedisRepository.set(
                                    "trip_id",
                                    gtfsComputationData,
                                    VPUtils.DELAY_COMPUTATION_CACHE_TTL
                                );

                                trip.gtfsData = gtfsComputationData;
                            } catch (err) {
                                log.error(err);
                                return;
                            }
                        }
                        return PositionsManager.computePositions(trip as ITripPositionsWithGTFS, data.schedule);
                    })
                );

                const validProcessedPositions = processedPositions.filter((e): e is IProcessedPositions => !!e);
                await this.updateComputedPositionsAndTrips(validProcessedPositions);

                await QueueManager.sendMessageToExchange(this.queuePrefix, "propagateDelay", {
                    processedPositions: validProcessedPositions,
                    trips: data.updatedTrips,
                });
            } catch (err) {
                throw new GeneralError(
                    `Error while updating delay. For trip ids:${tripIds.slice(0, 10).join(",")}`,
                    this.constructor.name,
                    err
                );
            }
        }
    }

    /**
     * Take all newly computed positions and save it, then save the last ones to the trips
     *
     * @param {IProcessedPositions[]} computedResult - Arrays of all newly computed positions
     * @returns {Object[]} - Arrays of inserted/updated positions and array of (inserted)/updated trips
     */
    private updateComputedPositionsAndTrips = async (computedResult: IProcessedPositions[]) => {
        const positionsUpdated = await this.positionsRepository.bulkUpdate(computedResult.flatMap((e) => e.positions));
        const tripsUpdated = await this.tripsRepository.bulkUpdate(
            computedResult.map((e) => ({
                id: e.context.tripId,
                last_position_id: e.context.lastPositionId,
                last_position_context: e.context,
                is_canceled: e.context.lastPositionCanceled,
            }))
        );

        return {
            positionsUpdated,
            tripsUpdated,
        };
    };
}
