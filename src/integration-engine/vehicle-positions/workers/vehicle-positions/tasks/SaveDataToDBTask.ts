import { QueueManager } from "@golemio/core/dist/integration-engine/queueprocessors";
import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import { TripsRepository } from "../data-access/TripsRepository";
import { IMpvMessageInput } from "../interfaces/MpvMessageInterfaces";
import { SaveDataToDBValidationSchema } from "../schema/SaveDataToDBSchema";
import { PositionsTransformation } from "../transformations/PositionsTransformation";

export class SaveDataToDBTask extends AbstractTask<IMpvMessageInput> {
    public readonly queueName = "saveDataToDB";
    public readonly queueTtl = 5 * 60 * 1000; // 5 minutes
    public readonly schema = SaveDataToDBValidationSchema;

    private readonly tripsRepository: TripsRepository;
    private readonly positionsTransformation: PositionsTransformation;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.tripsRepository = new TripsRepository();
        this.positionsTransformation = new PositionsTransformation();
    }

    protected async execute(data: IMpvMessageInput) {
        const transformedData = await this.positionsTransformation.transform(data.m.spoj);
        const rows = await this.tripsRepository.bulkUpsert(transformedData.trips);

        // send message for update GTFSTripIds
        for (let i = 0, chunkSize = 50; i < rows.inserted.length; i += chunkSize) {
            const inserted = rows.inserted.slice(i, i + chunkSize);
            const insertedIds = inserted.map((ins) => {
                return ins.id;
            });

            await QueueManager.sendMessageToExchange(this.queuePrefix, "updateGTFSTripId", {
                trips: inserted,
                positions: transformedData.positions.filter((position) => insertedIds.includes(position.trips_id)),
            });
        }

        // send message for update delay
        // aggregate runs by origin_route_name & run_number to keep related messages in same bulk
        const runTripMap: Record<string, string[]> = rows.updated.length ? { unknown: [] } : {};

        for (const trip of rows.updated) {
            if (!trip.origin_route_name || !trip.run_number) {
                runTripMap.unknown.push(trip.id);
            } else {
                const key = `${trip.origin_route_name}_${trip.run_number}`;
                runTripMap[key] ? runTripMap[key].push(trip.id) : (runTripMap[key] = [trip.id]);
            }
        }

        const chunks: string[][] = [];
        let idx = 0;
        for (const key in runTripMap) {
            if (!chunks[idx]) {
                chunks[idx] = [];
            }
            chunks[idx] = chunks[idx].concat(runTripMap[key]);
            if (chunks[idx].length > 200) {
                idx++;
            }
        }

        for (const key in chunks) {
            const positions = transformedData.positions.filter((position) => chunks[key].includes(position.trips_id));
            const updatedTrips = await this.tripsRepository.findAllAssocTripIds(chunks[key]);

            await QueueManager.sendMessageToExchange(this.queuePrefix, "updateDelay", {
                positions,
                updatedTrips,
            });
        }
    }
}
