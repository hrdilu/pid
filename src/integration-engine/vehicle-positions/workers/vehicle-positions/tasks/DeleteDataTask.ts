import { IDataDeletionParams } from "#ie/shared/interfaces/IDataDeletionParams";
import { DataDeletionValidationSchema } from "#ie/shared/schema/DataDeletionSchema";
import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import { PositionsHistoryRepository } from "../data-access/PositionsHistoryRepository";
import { PositionsRepository } from "../data-access/PositionsRepository";
import { TripsHistoryRepository } from "../data-access/TripsHistoryRepository";
import { TripsRepository } from "../data-access/TripsRepository";

export class DeleteDataTask extends AbstractTask<IDataDeletionParams> {
    public readonly queueName = "deleteData";
    public readonly queueTtl = 29 * 60 * 1000; // 29 minutes
    public readonly schema = DataDeletionValidationSchema;

    private readonly tripsRepository: TripsRepository;
    private readonly tripsHistoryRepository: TripsHistoryRepository;
    private readonly positionsRepository: PositionsRepository;
    private readonly positionsHistoryRepository: PositionsHistoryRepository;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.tripsRepository = new TripsRepository();
        this.tripsHistoryRepository = new TripsHistoryRepository();
        this.positionsRepository = new PositionsRepository();
        this.positionsHistoryRepository = new PositionsHistoryRepository();
    }

    protected async execute(data: IDataDeletionParams) {
        await this.tripsRepository.deleteNHoursOldData(data.targetHours);
        await this.positionsRepository.deleteNHoursOldData(data.targetHours);
        await this.tripsHistoryRepository.deleteNHoursOldData(data.targetHours);
        await this.positionsHistoryRepository.deleteNHoursOldData(data.targetHours);
    }
}
