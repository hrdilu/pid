const VPContainerToken = {
    //#region Vehicle Descriptors
    SeznamAutobusuDataSourceProvider: Symbol(),
    DescriptorDataSourceFactory: Symbol(),
    DescriptorRepository: Symbol(),
    DescriptorTransformation: Symbol(),
    RefreshDescriptorsTask: Symbol(),
    DeleteDataTask: Symbol(),
    //#endregion
};

export { VPContainerToken };
