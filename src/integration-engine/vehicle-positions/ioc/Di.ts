import { PidContainer } from "#ie/ioc/Di";
import { DependencyContainer } from "@golemio/core/dist/shared/tsyringe";
import { DescriptorRepository } from "../workers/vehicle-descriptors/data-access/DescriptorRepository";
import { DescriptorDataSourceFactory } from "../workers/vehicle-descriptors/datasources/DescriptorDataSourceFactory";
import { SeznamAutobusuDataSourceProvider } from "../workers/vehicle-descriptors/datasources/seznam-autobusu/SeznamAutobusuDataSourceProvider";
import { DeleteDataTask } from "../workers/vehicle-descriptors/tasks/DeleteDataTask";
import { RefreshDescriptorsTask } from "../workers/vehicle-descriptors/tasks/RefreshDescriptorsTask";
import { DescriptorTransformation } from "../workers/vehicle-descriptors/transformations/DescriptorTransformation";
import { VPContainerToken } from "./VPContainerToken";

//#region Initialization
const VPContainer: DependencyContainer = PidContainer.createChildContainer();
//#endregion

//#region Datasources
VPContainer.register(VPContainerToken.SeznamAutobusuDataSourceProvider, SeznamAutobusuDataSourceProvider);
VPContainer.registerSingleton(VPContainerToken.DescriptorDataSourceFactory, DescriptorDataSourceFactory);
//#endregion

//#region Repositories
VPContainer.register(VPContainerToken.DescriptorRepository, DescriptorRepository);
//#endregion

//#region Transformations
VPContainer.register(VPContainerToken.DescriptorTransformation, DescriptorTransformation);
//#endregion

//#region Tasks
VPContainer.registerSingleton(VPContainerToken.RefreshDescriptorsTask, RefreshDescriptorsTask);
VPContainer.registerSingleton(VPContainerToken.DeleteDataTask, DeleteDataTask);
//#endregion

export { VPContainer };
