import { AbstractWorker } from "@golemio/core/dist/integration-engine/workers";
import { CheckForNewDataTask } from "./tasks/CheckForNewDataTask";
import { CheckSavedRowsAndReplaceTablesTask } from "./tasks/CheckSavedRowsAndReplaceTablesTask";
import { DownloadDatasetsTask } from "./tasks/DownloadDatasetsTask";
import { RefreshPrecomputedTablesTask } from "./tasks/RefreshPrecomputedTablesTask";
import { TransformAndSaveDataTask } from "./tasks/TransformAndSaveDataTask";

export class TimetableWorker extends AbstractWorker {
    protected name = "ropidgtfs";

    constructor() {
        super();

        // Register tasks
        this.registerTask(new CheckForNewDataTask(this.getQueuePrefix()));
        this.registerTask(new CheckSavedRowsAndReplaceTablesTask(this.getQueuePrefix()));
        this.registerTask(new DownloadDatasetsTask(this.getQueuePrefix()));
        this.registerTask(new RefreshPrecomputedTablesTask(this.getQueuePrefix()));
        this.registerTask(new TransformAndSaveDataTask(this.getQueuePrefix()));
    }
}
