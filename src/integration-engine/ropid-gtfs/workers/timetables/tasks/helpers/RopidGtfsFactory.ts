import { DatasetEnum } from "#ie/ropid-gtfs/RopidGTFSMetadataModel";
import { RopidGTFSCisStopsTransformation } from "#ie/ropid-gtfs/transformations/RopidGTFSCisStopsTransformation";
import { RopidGTFSOisMappingTransformation } from "#ie/ropid-gtfs/transformations/RopidGTFSOisTransformation";
import { RopidGTFSTransformation } from "#ie/ropid-gtfs/transformations/RopidGTFSTransformation";
import { RopidGTFS } from "#sch/ropid-gtfs";
import {
    FTPProtocolStrategy,
    FTPReturnType,
    FTPTargetType,
    IDataSource,
    JSONDataTypeStrategy,
    config,
} from "@golemio/core/dist/integration-engine";
import { RetryDataSource } from "@golemio/core/dist/integration-engine/datasources/RetryDataSource";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { CacheContentType, DatasetMap } from "./HelperTypes";

@injectable()
export class RopidGtfsFactory {
    private dataSourceGtfs: IDataSource;
    private dataSourceCisStops: IDataSource;
    private dataSourceOisMapping: IDataSource;
    private dataSourceRunNumbers: IDataSource;

    private transformationGtfs: RopidGTFSTransformation;
    private transformationCisStops: RopidGTFSCisStopsTransformation;
    private transformationOisMapping: RopidGTFSOisMappingTransformation;

    private datasetMap: DatasetMap;

    constructor() {
        // Ropid GTFS
        this.dataSourceGtfs = new RetryDataSource(
            RopidGTFS.name + "DataSource",
            new FTPProtocolStrategy({
                filename: config.datasources.RopidGTFSFilename,
                targetType: FTPTargetType.COMPRESSED,
                returnType: FTPReturnType.Stream,
                path: config.datasources.RopidGTFSPath,
                url: config.datasources.RopidFTP,
                whitelistedFiles: [
                    "agency.txt",
                    "calendar.txt",
                    "calendar_dates.txt",
                    "shapes.txt",
                    "stop_times.txt",
                    "stops.txt",
                    "routes.txt",
                    "trips.txt",
                ],
                encoding: "utf8",
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            null as any
        );
        this.transformationGtfs = new RopidGTFSTransformation();

        // Ropid GTFS Run Numbers Datasource
        this.dataSourceRunNumbers = new RetryDataSource(
            RopidGTFS.run_numbers.name + "DataSource",
            new FTPProtocolStrategy({
                filename: config.datasources.RopidGTFSRunNumbersFilename,
                returnType: FTPReturnType.Stream,
                path: config.datasources.RopidGTFSRunNumbersPath,
                url: config.datasources.RopidFTP,
                encoding: "utf8",
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            null as any
        );

        // Ropid GTFS CIS STOPS Datasource
        const cisStopsTypeStrategy = new JSONDataTypeStrategy({ resultsPath: "stopGroups" });
        cisStopsTypeStrategy.setFilter((item) => item.cis !== 0);
        this.dataSourceCisStops = new RetryDataSource(
            RopidGTFS.name + "CisStops",
            new FTPProtocolStrategy({
                filename: config.datasources.RopidGTFSCisStopsFilename,
                path: config.datasources.RopidGTFSCisStopsPath,
                url: config.datasources.RopidFTP,
                encoding: "utf8",
            }),
            cisStopsTypeStrategy,
            null as any
        );
        this.transformationCisStops = new RopidGTFSCisStopsTransformation();

        // Ropid GTFS Ois Datasource
        this.dataSourceOisMapping = new RetryDataSource(
            RopidGTFS.name + "Ois",
            new FTPProtocolStrategy({
                filename: config.datasources.RopidGTFSOisFilename,
                path: config.datasources.RopidGTFSOisPath,
                url: config.datasources.RopidFTP,
                encoding: "utf8",
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(RopidGTFS.ois.name + "DataSource", RopidGTFS.ois.datasourceJsonSchema)
        );
        this.transformationOisMapping = new RopidGTFSOisMappingTransformation();

        this.datasetMap = {
            [DatasetEnum.PID_GTFS]: {
                datasource: this.dataSourceGtfs,
                transformation: this.transformationGtfs,
                cacheContentType: CacheContentType.CSV,
            },
            [DatasetEnum.RUN_NUMBERS]: {
                datasource: this.dataSourceRunNumbers,
                transformation: this.transformationGtfs,
                cacheContentType: CacheContentType.CSV,
            },
            [DatasetEnum.CIS_STOPS]: {
                datasource: this.dataSourceCisStops,
                transformation: this.transformationCisStops,
                cacheContentType: CacheContentType.JSON,
            },
            [DatasetEnum.OIS_MAPPING]: {
                datasource: this.dataSourceOisMapping,
                transformation: this.transformationOisMapping,
                cacheContentType: CacheContentType.JSON,
            },
        };
    }

    public getDatasetMap(): DatasetMap {
        return this.datasetMap;
    }
}
