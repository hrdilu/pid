import { DeparturesRepository } from "#ie/ropid-gtfs/data-access/precomputed/DeparturesRepository";
import { MinMaxStopSequencesRepository } from "#ie/ropid-gtfs/data-access/precomputed/MinMaxStopSequencesRepository";
import { ServicesCalendarRepository } from "#ie/ropid-gtfs/data-access/precomputed/ServicesCalendarRepository";
import { TripScheduleRepository } from "#ie/ropid-gtfs/data-access/precomputed/TripScheduleRepository";
import { SourceTableSuffixEnum } from "#ie/ropid-gtfs/helpers/SourceTableSuffixEnum";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class PrecomputedTablesFacade {
    /** Precomputed data repositories */
    private departuresRepository: DeparturesRepository;
    private minMaxStopSequencesRepository: MinMaxStopSequencesRepository;
    private tripScheduleRepository: TripScheduleRepository;
    private servicesCalendarRepository: ServicesCalendarRepository;

    constructor(@inject(CoreToken.Logger) private readonly logger: ILogger) {
        /** Precomputed data repositories */
        this.departuresRepository = new DeparturesRepository();
        this.minMaxStopSequencesRepository = new MinMaxStopSequencesRepository();
        this.tripScheduleRepository = new TripScheduleRepository();
        this.servicesCalendarRepository = new ServicesCalendarRepository();
    }

    public createAndPopulatePrecomputedTmpTables = async (sourceTableSuffix: SourceTableSuffixEnum): Promise<void> => {
        this.logger.info(
            `Datasets: creating temp table ${this.servicesCalendarRepository["tableName"]}${SourceTableSuffixEnum.Tmp}`
        );
        await this.servicesCalendarRepository.createAndPopulate(sourceTableSuffix);

        this.logger.info(
            `Datasets: creating indexes on ${this.servicesCalendarRepository["tableName"]}${SourceTableSuffixEnum.Tmp}`
        );
        await this.servicesCalendarRepository.createIndexes();

        this.logger.info(
            `Datasets: creating temp table ${this.minMaxStopSequencesRepository["tableName"]}${SourceTableSuffixEnum.Tmp}`
        );
        await this.minMaxStopSequencesRepository.createAndPopulate(sourceTableSuffix);

        this.logger.info(
            `Datasets: creating indexes on ${this.minMaxStopSequencesRepository["tableName"]}${SourceTableSuffixEnum.Tmp}`
        );
        await this.minMaxStopSequencesRepository.createIndexes();

        this.logger.info(`Datasets: creating temp table ${this.tripScheduleRepository["tableName"]}${SourceTableSuffixEnum.Tmp}`);
        await this.tripScheduleRepository.createAndPopulate(sourceTableSuffix);

        this.logger.info(`Datasets: creating indexes on ${this.tripScheduleRepository["tableName"]}${SourceTableSuffixEnum.Tmp}`);
        await this.tripScheduleRepository.createIndexes();

        this.logger.info(`Datasets: creating temp table ${this.departuresRepository["tableName"]}${SourceTableSuffixEnum.Tmp}`);
        await this.departuresRepository.createAndPopulate(sourceTableSuffix);

        this.logger.info(`Datasets: creating indexes on ${this.departuresRepository["tableName"]}${SourceTableSuffixEnum.Tmp}`);
        await this.departuresRepository.createIndexes();

        this.logger.info(`Datasets: running analyze on  ${this.departuresRepository["tableName"]}${SourceTableSuffixEnum.Tmp}`);
        await this.departuresRepository.analyze();
    };
}
