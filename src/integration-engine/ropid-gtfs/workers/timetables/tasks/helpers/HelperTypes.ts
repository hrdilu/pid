import { DatasetEnum } from "#ie/ropid-gtfs/RopidGTFSMetadataModel";
import { DatasetModelName, ScheduleModelName } from "#sch/ropid-gtfs";
import { IDataSource } from "@golemio/core/dist/integration-engine/datasources/IDataSource";
import { IFile } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/IProtocolStrategy";
import { ITransformation } from "@golemio/core/dist/integration-engine/transformations/ITransformation";

export const ORIGIN_HOSTNAME_HEADER = "x-origin-hostname";

export type ICachedFileMeta = Omit<IFile, "data">;

export type ICachedDataset = ICachedFileMeta & {
    dataset: GTFSDatasets;
};

export enum CacheContentType {
    CSV = "CSV",
    JSON = "JSON",
}

export type GTFSDatasets = Exclude<DatasetEnum, DatasetEnum.DEPARTURES_PRESETS>;

export type DatasetMap = {
    [key in GTFSDatasets]: {
        datasource: IDataSource;
        transformation: ITransformation;
        cacheContentType: CacheContentType;
    };
};

export const isCachedDataset = (obj: any): obj is ICachedDataset => {
    return obj instanceof Object && obj.filepath !== undefined && obj.name !== undefined && obj.dataset !== undefined;
};

export const datasetFileModelMap = (dataset: GTFSDatasets, name: string): DatasetModelName | DatasetModelName[] => {
    let model;
    switch (dataset) {
        case DatasetEnum.PID_GTFS:
            model = name as ScheduleModelName;
            break;
        case DatasetEnum.RUN_NUMBERS:
            model = <const>"run_numbers";
            break;
        case DatasetEnum.OIS_MAPPING:
            model = <const>"ois";
            break;
        case DatasetEnum.CIS_STOPS:
            model = [<const>"cis_stops", <const>"cis_stop_groups"];
            break;
        default:
            throw new Error(`Error mapping dataset ${dataset} and source ${name}`);
    }

    return model;
};
