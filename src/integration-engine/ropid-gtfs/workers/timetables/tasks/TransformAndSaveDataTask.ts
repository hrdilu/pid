import { PidContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { DatasetEnum, RopidGTFSMetadataModel, RopidGTFSTransformation } from "#ie/ropid-gtfs";
import { StaticFileRedisRepository } from "#ie/ropid-gtfs/data-access/cache/StaticFileRedisRepository";
import { SourceTableSuffixEnum } from "#ie/ropid-gtfs/helpers/SourceTableSuffixEnum";
import { RopidGtfsContainer } from "#ie/ropid-gtfs/ioc/Di";
import { RopidGtfsContainerToken } from "#ie/ropid-gtfs/ioc/RopidGtfsContainerToken";
import { buildDropIndexQuery } from "#ie/ropid-gtfs/utils";
import { MetaStateEnum } from "#ie/shared/RopidMetadataModel";
import { PG_SCHEMA } from "#sch/const";
import { DatasetModelName, RopidGTFS, ScheduleModelName } from "#sch/ropid-gtfs";
import { RopidGTFSPgProp } from "#sch/ropid-gtfs/RopidGtfsSchedule";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { RedisPubSubChannel } from "@golemio/core/dist/integration-engine/data-access/pubsub";
import { PostgresModel } from "@golemio/core/dist/integration-engine/models/PostgresModel";
import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { MessageProperties } from "amqplib";
import { from as copyFrom } from "pg-copy-streams";
import {
    CacheContentType,
    GTFSDatasets,
    ICachedDataset,
    ORIGIN_HOSTNAME_HEADER,
    datasetFileModelMap,
} from "./helpers/HelperTypes";
import { RopidGtfsFactory } from "./helpers/RopidGtfsFactory";
import { CachedDatasetSchema } from "./schema/CachedDatasetSchema";

export class TransformAndSaveDataTask extends AbstractTask<ICachedDataset> {
    public readonly queueName = "transformAndSaveData";
    public readonly queueTtl = 23 * 60 * 60 * 1000; // 23 hours
    public readonly schema = CachedDatasetSchema;
    private logger: ILogger;

    private staticFileRedisRepository: StaticFileRedisRepository;
    private transformationGtfs: RopidGTFSTransformation;
    private ropidGtfsFactory: RopidGtfsFactory;
    private gtfsRedisChannel: RedisPubSubChannel;
    private metaModel: RopidGTFSMetadataModel;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.logger = RopidGtfsContainer.resolve<ILogger>(CoreToken.Logger);
        this.metaModel = RopidGtfsContainer.resolve<RopidGTFSMetadataModel>(ModuleContainerToken.RopidGTFSMetadataModel);

        this.staticFileRedisRepository = RopidGtfsContainer.resolve<StaticFileRedisRepository>(
            RopidGtfsContainerToken.StaticFileRedisRepository
        );
        this.transformationGtfs = RopidGtfsContainer.resolve<RopidGTFSTransformation>(
            RopidGtfsContainerToken.RopidGTFSTransformation
        ); // TODO is it posible to use factory instead, which already contain this transformation?
        this.ropidGtfsFactory = RopidGtfsContainer.resolve<RopidGtfsFactory>(RopidGtfsContainerToken.RopidGtfsFactory);
        this.gtfsRedisChannel = RopidGtfsContainer.resolve<RedisPubSubChannel>(RopidGtfsContainerToken.RedisPubSubChannel);
    }

    protected async execute(inputData: ICachedDataset, msgProperties?: MessageProperties) {
        const datasetMap = this.ropidGtfsFactory.getDatasetMap();
        let modelName = datasetFileModelMap(inputData.dataset, inputData.name);
        if (!Array.isArray(modelName)) {
            modelName = [modelName];
        }

        let errorMessage: string | undefined;
        if (inputData.dataset === DatasetEnum.PID_GTFS || inputData.dataset === DatasetEnum.RUN_NUMBERS) {
            try {
                const sourceStream = this.staticFileRedisRepository.getReadableStream(inputData.filepath);
                const transformedDataset = await this.transformationGtfs.transform({
                    sourceStream,
                    name: modelName[0],
                });

                await this.saveTransformedDataset(transformedDataset, inputData.dataset, modelName[0]);
            } catch (err) {
                errorMessage = err.message;
            }
        } else {
            try {
                const data = await this.staticFileRedisRepository.get(inputData.filepath);
                const transformedDataset = await datasetMap[inputData.dataset as GTFSDatasets].transformation.transform({
                    data: datasetMap[inputData.dataset].cacheContentType === CacheContentType.JSON ? JSON.parse(data) : data,
                    name: inputData.name,
                });

                if (inputData.dataset === DatasetEnum.CIS_STOPS) {
                    await this.saveTransformedDataset(transformedDataset.cis_stops, inputData.dataset, modelName[0]);
                    await this.saveTransformedDataset(transformedDataset.cis_stop_groups, inputData.dataset, modelName[1]);
                } else {
                    await this.saveTransformedDataset(transformedDataset, inputData.dataset, modelName[0]);
                }
            } catch (err) {
                errorMessage = err.message;
            }
        }

        if (msgProperties?.headers[ORIGIN_HOSTNAME_HEADER]) {
            await this.gtfsRedisChannel.publishMessage(`${errorMessage ?? "OK"} (${modelName})`, {
                channelSuffix: msgProperties.headers[ORIGIN_HOSTNAME_HEADER],
            });
        }

        if (errorMessage) {
            throw new GeneralError(errorMessage, this.constructor.name);
        }
    }

    private saveTransformedDataset = async (
        transformedData: any,
        dataset: GTFSDatasets,
        modelName: DatasetModelName
    ): Promise<void> => {
        const model = await this.createTmpModel(modelName);

        switch (dataset) {
            case DatasetEnum.PID_GTFS:
            case DatasetEnum.RUN_NUMBERS:
                await this.streamDataToTmp(transformedData, modelName as ScheduleModelName);
                break;
            default:
                await model?.save(transformedData, false);
                this.logger.info(`Datasets: copying to tmp success (${modelName})`);
        }

        const dbLastModified = await this.metaModel.getLastModified(dataset);
        await this.metaModel.updateState(dataset, modelName, MetaStateEnum.SAVED, dbLastModified.version);
    };

    private createTmpModel = async (name: string): Promise<PostgresModel> => {
        const model = await this.getTmpModelByName(name);
        if (RopidGTFS[name as RopidGTFSPgProp].modelIndexOptions) {
            await model.query(buildDropIndexQuery(RopidGTFS[name as RopidGTFSPgProp].modelIndexOptions));
        }
        await model.sync({ force: true, schema: PG_SCHEMA });
        return model;
    };

    private streamDataToTmp = async (data: any, modelName: ScheduleModelName) => {
        // get connection and db client
        const connection = PidContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector).getConnection();
        const client = (await connection.connectionManager.getConnection({ type: "write" })) as any;

        const tmpTableName = RopidGTFS[modelName as RopidGTFSPgProp].pgTableName + SourceTableSuffixEnum.Tmp;

        // copy transformed data to tmp table by stream
        await new Promise<void>((resolve, reject) => {
            const stream = client
                .query(copyFrom(`COPY "${PG_SCHEMA}".${tmpTableName} FROM STDIN DELIMITER ',' CSV HEADER;`))
                .on("error", async (err: any) => {
                    this.logger.error(`Datasets: copying to tmp error (${modelName}): ${err.toString()}`);
                    await connection.connectionManager.releaseConnection(client);
                    return reject(err);
                })
                .on("finish", async () => {
                    this.logger.info(`Datasets: copying to tmp success (${modelName})`);
                    await connection.connectionManager.releaseConnection(client);
                    return resolve();
                });
            data.pipe(stream);
        });
    };

    private getTmpModelByName = async (name: string): Promise<PostgresModel> => {
        const modelDef = RopidGTFS[name as RopidGTFSPgProp];

        if (!modelDef || !modelDef.name || !modelDef.pgTableName || !modelDef.outputSequelizeAttributes) {
            throw new Error("Model not found.");
        }

        const model = new PostgresModel(
            modelDef.name + "Tmp" + "Model",
            {
                hasTmpTable: false,
                outputSequelizeAttributes: modelDef.outputSequelizeAttributes,
                pgSchema: PG_SCHEMA,
                pgTableName: modelDef.pgTableName + SourceTableSuffixEnum.Tmp,
                savingType: "insertOnly",
                sequelizeAdditionalSettings: {
                    timestamps: false,
                    ...(modelDef.modelIndexOptions &&
                        modelDef.modelIndexOptions.length && {
                            indexes: modelDef.modelIndexOptions.map((indexDef: any) => {
                                return {
                                    ...indexDef,
                                    name: indexDef.name + SourceTableSuffixEnum.Tmp,
                                };
                            }),
                        }),
                },
            },
            new JSONSchemaValidator(modelDef.name + "Tmp" + "ModelValidator", modelDef.outputMongooseSchemaObject)
        );

        return model;
    };
}
