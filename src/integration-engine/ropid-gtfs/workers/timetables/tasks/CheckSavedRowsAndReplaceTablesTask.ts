import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { RopidGTFSMetadataModel } from "#ie/ropid-gtfs/RopidGTFSMetadataModel";
import { DataCacheManager } from "#ie/ropid-gtfs/helpers/DataCacheManager";
import { SourceTableSuffixEnum } from "#ie/ropid-gtfs/helpers/SourceTableSuffixEnum";
import { RopidGtfsContainer } from "#ie/ropid-gtfs/ioc/Di";
import { RopidGtfsContainerToken } from "#ie/ropid-gtfs/ioc/RopidGtfsContainerToken";
import { WORKER_NAME as VP_WORKER_NAME } from "#ie/vehicle-positions/workers/vehicle-positions/constants";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { IntegrationErrorHandler, QueueManager } from "@golemio/core/dist/integration-engine";
import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { PrecomputedTablesFacade } from "./helpers/PrecomputedTablesFacade";
import { IDatasetsInput } from "./interfaces/IDatasetsInput";
import { DatasetsInputSchema } from "./schema/DownloadDataInputSchema";

export class CheckSavedRowsAndReplaceTablesTask extends AbstractTask<IDatasetsInput> {
    public readonly queueName = "checkSavedRowsAndReplaceTables";
    public readonly queueTtl = 23 * 60 * 60 * 1000; // 23 hours
    public readonly schema = DatasetsInputSchema;
    private logger: ILogger;

    private metaModel: RopidGTFSMetadataModel;
    private dataCacheManager: DataCacheManager;
    private precomputeTablesFacade: PrecomputedTablesFacade;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.logger = RopidGtfsContainer.resolve<ILogger>(CoreToken.Logger);
        // MetaData Model
        this.metaModel = RopidGtfsContainer.resolve<RopidGTFSMetadataModel>(ModuleContainerToken.RopidGTFSMetadataModel);
        this.dataCacheManager = DataCacheManager.getInstance();
        this.precomputeTablesFacade = RopidGtfsContainer.resolve<PrecomputedTablesFacade>(
            RopidGtfsContainerToken.PrecomputedTablesFacade
        );
    }

    protected async execute(data: IDatasetsInput) {
        const datasetDeployInfo = await Promise.all(
            data.datasets.map(async (dataset) => {
                const lastModified = await this.metaModel.getLastModified(dataset);
                const isDeployed = await this.metaModel.checkIfNewVersionIsAlreadyDeployed(dataset, lastModified.version);
                return { dataset, lastModified, isDeployed };
            })
        );

        this.logger.debug(`Datasets status: ${JSON.stringify(datasetDeployInfo)}`);

        const notDeployed = datasetDeployInfo.filter((info) => !info.isDeployed);

        if (data.datasets.length !== notDeployed.length) {
            const names = notDeployed.map((el) => el.dataset);
            this.logger.error(`Error while synchronized download of datasets: [${names}] of [${data.datasets}] deployed already`);
        }

        if (!notDeployed.length) {
            this.logger.debug(`Datasets status: already deployed`);
            return;
        }

        const notDeployedVersions = notDeployed.map((el) => ({ dataset: el.dataset, version: el.lastModified.version }));
        const allSaved = await this.metaModel.checkAllTablesHasSavedState(notDeployedVersions);

        try {
            if (!allSaved) {
                throw new GeneralError("Some GTFS datasets did not properly save", this.constructor.name);
            }

            await Promise.all(
                datasetDeployInfo.map(async (el) => {
                    await this.metaModel.checkSavedTmpTables(el.dataset, el.lastModified.version);
                })
            );

            await this.precomputeTablesFacade.createAndPopulatePrecomputedTmpTables(SourceTableSuffixEnum.Tmp);
            await this.metaModel.replaceTables(notDeployedVersions);
            await this.dataCacheManager.cleanCache();
            await QueueManager.sendMessageToExchange(VP_WORKER_NAME.toLowerCase(), "refreshGTFSTripData", {});

            return;
        } catch (err) {
            // log failed saving process
            await Promise.all(
                notDeployedVersions.map(async (el) => {
                    await this.metaModel.rollbackFailedSaving(el.dataset, el.version);
                })
            );
            // check number of tries
            const retries = await Promise.all(
                notDeployedVersions.map(async (el) => {
                    return await this.metaModel.getNumberOfDownloadRetries(el.dataset, el.version);
                })
            );

            if (Math.max(...retries) <= 5) {
                // send new downloadFiles, log it and finish
                QueueManager.sendMessageToExchange(this.queuePrefix, "downloadDatasets", data);

                IntegrationErrorHandler.handle(
                    new GeneralError(
                        `Error while checking RopidGTFS saved rows. Attempt number ${Math.min(...retries)} was resent.`,
                        this.constructor.name,
                        err
                    )
                );
                return;
            } else {
                // at least refresh precomputed tables and finish with error
                QueueManager.sendMessageToExchange(this.queuePrefix, "refreshPrecomputedTables", {});

                throw new GeneralError("Error while checking RopidGTFS saved rows.", this.constructor.name, err);
            }
        }
    }
}
