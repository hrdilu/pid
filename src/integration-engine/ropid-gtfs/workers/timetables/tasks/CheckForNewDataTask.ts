import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { RopidGTFSMetadataModel } from "#ie/ropid-gtfs/RopidGTFSMetadataModel";
import { RopidGtfsContainer } from "#ie/ropid-gtfs/ioc/Di";
import { RopidGtfsContainerToken } from "#ie/ropid-gtfs/ioc/RopidGtfsContainerToken";
import { ICheckForNewDataInput } from "#ie/ropid-gtfs/workers/timetables/tasks/interfaces/ICheckForNewDataInput";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { QueueManager } from "@golemio/core/dist/integration-engine";
import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import { GTFSDatasets } from "./helpers/HelperTypes";
import { RopidGtfsFactory } from "./helpers/RopidGtfsFactory";
import { IDatasetsInput } from "./interfaces/IDatasetsInput";
import { CheckForNewDataMessageValidation } from "./schema/CheckForNewDataMessageValidation";

export class CheckForNewDataTask extends AbstractTask<ICheckForNewDataInput> {
    public readonly queueName = "checkForNewData";
    public readonly queueTtl = 19 * 60 * 1000; // 19 minutes
    public readonly schema = CheckForNewDataMessageValidation;
    private logger: ILogger;

    private metaModel: RopidGTFSMetadataModel;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.logger = RopidGtfsContainer.resolve<ILogger>(CoreToken.Logger);
        // MetaData Model
        this.metaModel = RopidGtfsContainer.resolve<RopidGTFSMetadataModel>(ModuleContainerToken.RopidGTFSMetadataModel);
    }

    protected async execute(data: ICheckForNewDataInput) {
        let forceRefresh = !!data.forceRefresh;
        const datasets = [];
        const ropidGtfsFactory = RopidGtfsContainer.resolve<RopidGtfsFactory>(RopidGtfsContainerToken.RopidGtfsFactory);
        const datasetMap = ropidGtfsFactory.getDatasetMap();

        for (const dataset of Object.keys(datasetMap) as GTFSDatasets[]) {
            const serverLastModified = await datasetMap[dataset].datasource.getLastModified();
            const dbLastModified = await this.metaModel.getLastModified(dataset);
            const isDeployed = await this.metaModel.isDeployed(dataset);

            if (serverLastModified !== dbLastModified.lastModified || !isDeployed) {
                datasets.push(dataset);
            }
        }

        if (datasets.length && datasets.length !== Object.keys(datasetMap).length) {
            this.logger.error(`RopidGTFS datasets are out of sync`);
        }

        if (datasets.length) {
            await QueueManager.sendMessageToExchange(this.queuePrefix, "downloadDatasets", {
                datasets: datasets,
            } as IDatasetsInput);
            return;
        }

        this.logger.info("RopidGTFS datasets are up to date");
        if (forceRefresh) {
            await QueueManager.sendMessageToExchange(this.queuePrefix, "refreshPrecomputedTables", {});
        }
    }
}
