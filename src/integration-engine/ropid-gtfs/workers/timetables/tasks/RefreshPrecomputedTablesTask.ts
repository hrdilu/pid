import { PidContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { RopidGTFSMetadataModel } from "#ie/ropid-gtfs/RopidGTFSMetadataModel";
import { DataCacheManager } from "#ie/ropid-gtfs/helpers/DataCacheManager";
import { SourceTableSuffixEnum } from "#ie/ropid-gtfs/helpers/SourceTableSuffixEnum";
import { RopidGtfsContainer } from "#ie/ropid-gtfs/ioc/Di";
import { RopidGtfsContainerToken } from "#ie/ropid-gtfs/ioc/RopidGtfsContainerToken";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine/workers";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { PrecomputedTablesFacade } from "./helpers/PrecomputedTablesFacade";

export class RefreshPrecomputedTablesTask extends AbstractEmptyTask {
    public readonly queueName = "refreshPrecomputedTables";
    public readonly queueTtl = 59 * 60 * 1000; // 59 minutes
    private logger: ILogger;

    private metaModel: RopidGTFSMetadataModel;
    private dataCacheManager: DataCacheManager;
    private precomputeTablesFacade: PrecomputedTablesFacade;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.logger = RopidGtfsContainer.resolve<ILogger>(CoreToken.Logger);
        this.metaModel = RopidGtfsContainer.resolve<RopidGTFSMetadataModel>(ModuleContainerToken.RopidGTFSMetadataModel);
        this.dataCacheManager = DataCacheManager.getInstance();
        this.precomputeTablesFacade = RopidGtfsContainer.resolve<PrecomputedTablesFacade>(
            RopidGtfsContainerToken.PrecomputedTablesFacade
        );
    }

    protected async execute() {
        try {
            await this.precomputeTablesFacade.createAndPopulatePrecomputedTmpTables(SourceTableSuffixEnum.Actual);
        } catch (err) {
            this.logger.error(err);
            throw new GeneralError("Error while creating RopidGTFS precomputed tables", this.constructor.name);
        }

        const connection = PidContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector).getConnection();
        const transaction = await connection.transaction();

        try {
            await this.metaModel.replacePrecomputedTables(transaction);
            await transaction.commit();
        } catch (err) {
            await transaction.rollback();

            this.logger.error(err);
            throw new GeneralError("Error while replacing RopidGTFS precomputed tables", this.constructor.name);
        }

        await this.dataCacheManager.cleanCache();
    }
}
