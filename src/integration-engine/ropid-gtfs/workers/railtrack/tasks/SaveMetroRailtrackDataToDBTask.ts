import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import { MetroRailtrackGPSRepository } from "./data-access/MetroRailtrackGPSRepository";
import { MetroRailtrackDataTransformation } from "./transformations/MetroRailtrackDataTransformation";
import { IMetroRailtrackDataInput } from "./interfaces/MetroRailtrackDataInterfaces";
import { MetroRailtrackDataValidationSchema } from "./schema/MetroRailtrackDataSchema";

export class SaveMetroRailtrackDataToDBTask extends AbstractTask<IMetroRailtrackDataInput> {
    public readonly queueName = "saveMetroRailtrackDataToDB";
    public readonly queueTtl = 5 * 60 * 1000; // 5 minutes
    public readonly schema = MetroRailtrackDataValidationSchema;

    private readonly railtrackDataTransformation: MetroRailtrackDataTransformation;
    private readonly railtrackGPSRepository: MetroRailtrackGPSRepository;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.railtrackDataTransformation = new MetroRailtrackDataTransformation();
        this.railtrackGPSRepository = new MetroRailtrackGPSRepository();
    }

    protected async execute(data: IMetroRailtrackDataInput) {
        const railtrackGPSData = await this.railtrackDataTransformation.transform(data);
        await this.railtrackGPSRepository.saveData(railtrackGPSData);
    }
}
