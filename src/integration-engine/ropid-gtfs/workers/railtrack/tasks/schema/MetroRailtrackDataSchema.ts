import {
    ArrayMinSize,
    ValidateNested,
    IsString,
    IsByteLength,
    IsObject,
    IsArray,
    IsLatitude,
    IsLongitude,
} from "@golemio/core/dist/shared/class-validator";
import { Type } from "@golemio/core/dist/shared/class-transformer";
import { IMetroRailtrack, IMetroRailtrackDataInput } from "../interfaces/MetroRailtrackDataInterfaces";

class MetroRailtrackValidationSchema implements IMetroRailtrack {
    @IsString()
    track_section!: string;

    @IsString()
    @IsByteLength(1, 1)
    line!: string;

    @IsString()
    @IsLatitude()
    gps_lat!: string;

    @IsString()
    @IsLongitude()
    gps_lon!: string;

    @IsString()
    stop_id!: string;
}

export class MetroRailtrackDataValidationSchema implements IMetroRailtrackDataInput {
    @IsArray()
    @ArrayMinSize(1)
    @IsObject({ each: true })
    @ValidateNested()
    @Type(() => MetroRailtrackValidationSchema)
    data!: IMetroRailtrack[];
}
