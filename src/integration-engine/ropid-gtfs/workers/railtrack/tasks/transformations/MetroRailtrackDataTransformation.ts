import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { MetroRailtrackGPSModel } from "#sch/ropid-gtfs/models/railtrack/MetroRailtrackGPSModel";
import {
    IMetroRailtrackDataInput,
    IMetroRailtrack,
} from "#ie/ropid-gtfs/workers/railtrack/tasks/interfaces/MetroRailtrackDataInterfaces";

export class MetroRailtrackDataTransformation extends BaseTransformation implements ITransformation {
    public name = "MetroRailtrackData";

    public transform = ({ data }: IMetroRailtrackDataInput): Promise<MetroRailtrackGPSModel[]> => {
        const dto: MetroRailtrackGPSModel[] = [];
        for (const dataItem of data) {
            const dtoItem = this.transformElement(dataItem);
            dto.push(dtoItem);
        }

        return Promise.resolve(dto);
    };

    protected transformElement = (element: IMetroRailtrack): MetroRailtrackGPSModel => {
        return {
            track_id: element.track_section,
            route_name: element.line,
            coordinates: {
                type: "Point",
                coordinates: [Number.parseFloat(element.gps_lon), Number.parseFloat(element.gps_lat)],
            },
            gtfs_stop_id: element.stop_id === "" ? null : element.stop_id,
        } as MetroRailtrackGPSModel;
    };
}
