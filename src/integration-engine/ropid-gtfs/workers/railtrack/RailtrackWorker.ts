import { AbstractWorker } from "@golemio/core/dist/integration-engine/workers";
import { RopidGTFS } from "#sch/ropid-gtfs";
import { SaveMetroRailtrackDataToDBTask } from "./tasks/SaveMetroRailtrackDataToDBTask";

export class RailtrackWorker extends AbstractWorker {
    protected name = RopidGTFS.name + "Railtrack";

    constructor() {
        super();

        // Register tasks
        this.registerTask(new SaveMetroRailtrackDataToDBTask(this.getQueuePrefix()));
    }
}
