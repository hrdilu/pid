import { PresetLogModel } from "#sch/ropid-departures-preset-logs/models";
import { IPresetLogOutputDto } from "#sch/ropid-departures-preset-logs/models/interfaces";
import { IModel } from "@golemio/core/dist/integration-engine/models";
import { ModelStatic } from "@golemio/core/dist/shared/sequelize";

export interface IPresetLogRepository extends IModel {
    sequelizeModel: ModelStatic<PresetLogModel>;

    findUnprocessed: (limit: number) => Promise<PresetLogModel[]>;
    saveData: (data: IPresetLogOutputDto[]) => Promise<void>;
    markAsProcessed: (logIds: string[]) => Promise<void>;
    deleteNHoursOldData: (hours: number) => Promise<number>;
}
