import { IDataSourceProvider } from "#ie/shared/datasources/IDataSourceProvider";
import { ILogQueryData } from "#sch/ropid-departures-preset-logs/datasources/interfaces";
import { PresetLogProvider } from "../helpers/PresetLogProviderEnum";

export type DataSourceProviderDict = {
    [PresetLogProvider.GrafanaLoki]: IDataSourceProvider<ILogQueryData[]>;
};

export type DataSourceReturnType<T extends PresetLogProvider> = ReturnType<DataSourceProviderDict[T]["getDataSource"]>;

export interface IPresetLogDataSourceFactory {
    getDataSource<T extends PresetLogProvider>(
        presetLogsProvider: T,
        ...params: Parameters<DataSourceProviderDict[T]["getDataSource"]>
    ): DataSourceReturnType<T>;
}
