import { IDataSourceProvider } from "#ie/shared/datasources/IDataSourceProvider";
import { grafanaLokiJsonSchema } from "#sch/ropid-departures-preset-logs/datasources";
import { ILogQueryData } from "#sch/ropid-departures-preset-logs/datasources/interfaces";
import { DataSource, HTTPProtocolStrategy, IDataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { IConfiguration } from "@golemio/core/dist/integration-engine/config/abstract";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc/ContainerToken";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

interface IQueryParams {
    start: number;
    end: number;
    query: string;
}

const DEFAULT_API_LIMIT = 5000;

@injectable()
export class GrafanaLokiDataSourceProvider implements IDataSourceProvider<ILogQueryData[]> {
    private static DATASOURCE_NAME = "GrafanaLokiDataSource";

    constructor(@inject(ContainerToken.Config) private config: IConfiguration) {}

    public getDataSource(params: IQueryParams): IDataSource<ILogQueryData[]> {
        return new DataSource<ILogQueryData[]>(
            GrafanaLokiDataSourceProvider.DATASOURCE_NAME,
            this.getProtocolStrategy(params),
            new JSONDataTypeStrategy({ resultsPath: "data.result" }),
            new JSONSchemaValidator(GrafanaLokiDataSourceProvider.DATASOURCE_NAME, grafanaLokiJsonSchema)
        );
    }

    private getProtocolStrategy(params: IQueryParams): HTTPProtocolStrategy {
        const url = new URL("/loki/api/v1/query_range", this.config.datasources.GrafanaLokiApiUrl);
        url.searchParams.set("start", params.start.toString());
        url.searchParams.set("end", params.end.toString());
        url.searchParams.set("query", params.query);
        url.searchParams.set("limit", this.config.datasources.GrafanaLokiApiLimit ?? DEFAULT_API_LIMIT);

        return new HTTPProtocolStrategy({
            method: "GET",
            url: url.toString(),
            headers: this.config.datasources.GrafanaLokiApiHeaders,
            timeout: 20000, // 20 seconds
        });
    }
}
