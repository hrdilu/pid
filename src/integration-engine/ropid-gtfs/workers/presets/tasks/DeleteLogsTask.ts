import { PidContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { IDataDeletionParams } from "#ie/shared/interfaces/IDataDeletionParams";
import { DataDeletionValidationSchema } from "#ie/shared/schema/DataDeletionSchema";
import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import { IPresetLogRepository } from "../data-access/interfaces/IPresetLogRepository";

export class DeleteLogsTask extends AbstractTask<IDataDeletionParams> {
    public readonly queueName = "deleteLogs";
    public readonly queueTtl = 1 * 60 * 60 * 1000; // 1 hour
    public readonly schema = DataDeletionValidationSchema;

    private logRepository: IPresetLogRepository;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.logRepository = PidContainer.resolve<IPresetLogRepository>(ModuleContainerToken.PresetLogRepository);
    }

    protected async execute(data: IDataDeletionParams) {
        await this.logRepository.deleteNHoursOldData(data.targetHours);
    }
}
