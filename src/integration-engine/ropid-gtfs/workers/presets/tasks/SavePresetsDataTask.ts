import { PidContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { DeparturePresetsFacade } from "#ie/ropid-gtfs/facade/DeparturePresetsFacade";
import { IRopidDeparturesPreset, RopidDeparturesPresets } from "#sch/ropid-departures-presets";
import { AbstractTaskJsonSchema } from "@golemio/core/dist/integration-engine/workers/AbstractTaskJsonSchema";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class SavePresetsDataTask extends AbstractTaskJsonSchema<IRopidDeparturesPreset[]> {
    public readonly queueName = "savePresets";
    public readonly queueTtl = 5 * 60 * 1000; // 5 minutes
    protected schema = new JSONSchemaValidator(
        RopidDeparturesPresets.name + "Controller",
        RopidDeparturesPresets.datasourceJsonSchema
    );

    constructor(queuePrefix: string) {
        super(queuePrefix);
    }

    protected async execute(data: IRopidDeparturesPreset[]) {
        const facade = PidContainer.resolve<DeparturePresetsFacade>(ModuleContainerToken.DeparturePresetsFacade);
        await facade.handleNewDeparturePresets(data, new Date().toISOString());
    }
}
