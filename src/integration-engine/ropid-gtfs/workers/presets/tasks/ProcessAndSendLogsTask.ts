import { PidContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { PresetLogModel } from "#sch/ropid-departures-preset-logs/models";
import { IPresetLogProcessedDto } from "#sch/ropid-departures-preset-logs/models/interfaces/IPresetLogProcessedDto";
import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine/workers";
import { IPresetLogRepository } from "../data-access/interfaces/IPresetLogRepository";
import { LogDateTimeUtils } from "../helpers/LogDateTimeUtils";
import { IRopidMonitoringService } from "../helpers/interfaces/IRopidMonitoringService";

const DEFAULT_LOG_LIMIT = 10000;
const DEFAULT_BATCH_SIZE = 1000;

export class ProcessAndSendLogsTask extends AbstractEmptyTask {
    public readonly queueName = "processAndSendLogs";
    public readonly queueTtl = 59 * 1000; // 59 seconds

    private logRepository: IPresetLogRepository;
    private monitoringService: IRopidMonitoringService;
    private logger: ILogger;
    private logLimit: number;
    private batchSize: number;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        const config = PidContainer.resolve<ISimpleConfig>(CoreToken.SimpleConfig);

        this.logRepository = PidContainer.resolve<IPresetLogRepository>(ModuleContainerToken.PresetLogRepository);
        this.monitoringService = PidContainer.resolve<IRopidMonitoringService>(ModuleContainerToken.RopidMonitoringService);
        this.logger = PidContainer.resolve<ILogger>(CoreToken.Logger);

        const logLimit = config.getValue("env.ROPID_PRESET_LOG_LIMIT", DEFAULT_LOG_LIMIT.toString());
        this.logLimit = Number.parseInt(logLimit);

        const batchSize = config.getValue("env.ROPID_PRESET_LOG_BATCH_SIZE", DEFAULT_BATCH_SIZE.toString());
        this.batchSize = Number.parseInt(batchSize);
    }

    protected async execute() {
        const logEntries = await this.logRepository.findUnprocessed(this.logLimit);
        if (logEntries.length === 0) {
            this.logger.warn(`${this.constructor.name}: no logs found to send to the ROPID monitoring system`);
            return;
        }

        // Process logs in batches to avoid exceeding the maximum allowed size of the request
        for (let i = 0; i < logEntries.length; i += this.batchSize) {
            const batchedLogs = logEntries.slice(i, i + this.batchSize);
            await this.processBatch(batchedLogs);
        }
    }

    private async processBatch(logEntries: PresetLogModel[]) {
        let logIds: string[] = [];
        let logData: IPresetLogProcessedDto[] = [];

        for (const logEntry of logEntries) {
            const { device_alias, received_at } = logEntry.toJSON();

            logIds.push(logEntry.get("id") as string);
            logData.push({
                device_alias,
                received_at: LogDateTimeUtils.parseISONoFractionalFromDate(received_at),
            });
        }

        try {
            await this.monitoringService.sendLogsToMonitoringSystem(logData);
            this.logger.info(`${this.constructor.name}: ${logData.length} logs sent to the ROPID monitoring system`);
        } catch (err) {
            // err should always be an instance of GeneralError
            throw err;
        } finally {
            await this.logRepository.markAsProcessed(logIds);
        }
    }
}
