import { IPresetLogProcessedDto } from "#sch/ropid-departures-preset-logs/models/interfaces/IPresetLogProcessedDto";
import { IConfiguration } from "@golemio/core/dist/integration-engine/config";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc/ContainerToken";
import axios, { AxiosRequestConfig } from "@golemio/core/dist/shared/axios";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { IRopidMonitoringService } from "./interfaces/IRopidMonitoringService";

@injectable()
export class RopidMonitoringService implements IRopidMonitoringService {
    constructor(@inject(ContainerToken.Config) private config: IConfiguration) {}

    public async sendLogsToMonitoringSystem(logEntries: IPresetLogProcessedDto[]): Promise<void> {
        const requestConfig = this.getRequestConfig(logEntries);

        try {
            const response = await axios.request(requestConfig);
            if (response.status < 200 || response.status >= 300) {
                throw new GeneralError(
                    `Error while sending logs to the ROPID monitoring system. Status code: ${response.status}`,
                    this.constructor.name
                );
            }
        } catch (err) {
            if (err instanceof GeneralError) {
                throw err;
            }

            throw new GeneralError("Error while sending logs to the ROPID monitoring system", this.constructor.name, err);
        }
    }

    private getRequestConfig(logEntries: IPresetLogProcessedDto[]): AxiosRequestConfig {
        return {
            method: "POST",
            url: this.config.datasources.RopidMonitoringPushUrl,
            headers: this.config.datasources.RopidMonitoringPushHeaders,
            data: logEntries,
            timeout: 20000, // 20 seconds
        };
    }
}
