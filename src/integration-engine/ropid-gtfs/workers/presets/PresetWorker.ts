import { AbstractWorker } from "@golemio/core/dist/integration-engine/workers";
import { CollectAndSaveLogsTask } from "./tasks/CollectAndSaveLogsTask";
import { DeleteLogsTask } from "./tasks/DeleteLogsTask";
import { ProcessAndSendLogsTask } from "./tasks/ProcessAndSendLogsTask";
import { SavePresetsDataTask } from "./tasks/SavePresetsDataTask";

export class PresetWorker extends AbstractWorker {
    protected name = "RopidPresets";

    constructor() {
        super();

        // Register tasks
        this.registerTask(new SavePresetsDataTask(this.getQueuePrefix()));
        this.registerTask(new CollectAndSaveLogsTask(this.getQueuePrefix()));
        this.registerTask(new ProcessAndSendLogsTask(this.getQueuePrefix()));
        this.registerTask(new DeleteLogsTask(this.getQueuePrefix()));
    }
}
