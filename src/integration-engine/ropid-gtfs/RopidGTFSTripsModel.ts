import { PG_SCHEMA } from "#sch/const";
import { RopidGTFS } from "#sch/ropid-gtfs";
import { TripDto } from "#sch/ropid-gtfs/models/TripDto";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { IValidator, JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import Sequelize from "@golemio/core/dist/shared/sequelize";
import { RopidGTFSShapesModel, RopidGTFSStopTimesModel, RopidGTFSStopsModel } from ".";

export interface ITripsShapes {
    bikes_allowed: number;
    block_id: string;
    direction_id: number;
    exceptional: number;
    route_id: string;
    service_id: string;
    shape_id: string;
    trip_headsign: string;
    trip_id: string;
    trip_short_name: string;
    wheelchair_accessible: number;

    shapes: IShape[];
}
export interface IShape {
    shape_dist_traveled: number;
    shape_id: string;
    shape_pt_lat: number;
    shape_pt_lon: number;
    shape_pt_sequence: number;
}

export interface IStop {
    stop_id: string;
    stop_lat: number;
    stop_lon: number;
    stop_name: string;
}

export interface IStopTime {
    arrival_time_seconds: number;
    departure_time_seconds: number;
    shape_dist_traveled: number;
    stop_id: string;
    stop_sequence: number;
    stop_headsign: string;

    stop: IStop;
}
export interface ITripsStopTimesStops {
    bikes_allowed: number;
    block_id: string;
    direction_id: number;
    exceptional: number;
    route_id: string;
    service_id: string;
    shape_id: string;
    trip_headsign: string;
    trip_id: string;
    trip_short_name: string;
    wheelchair_accessible: number;

    stop_times: IStopTime[];
}
export class RopidGTFSTripsModel extends PostgresModel implements IModel {
    /** Model name */
    public name!: string;
    /** The Sequelize Model */
    protected sequelizeModel!: Sequelize.ModelCtor<any>;
    /** The Sequelize Model for temporary table */
    protected tmpSequelizeModel!: Sequelize.ModelCtor<any> | null;
    /** Validation helper */
    protected validator!: IValidator;
    /** Type/Strategy of saving the data */
    protected savingType!: "insertOnly" | "insertOrUpdate";

    private modelGTFSShapes: RopidGTFSShapesModel;
    private modelGTFSStopTimes: RopidGTFSStopTimesModel;
    private modelGTFSStops: RopidGTFSStopsModel;

    constructor() {
        super(
            RopidGTFS.trips.name + "Model",
            {
                outputSequelizeAttributes: RopidGTFS.trips.outputSequelizeAttributes,
                pgTableName: RopidGTFS.trips.pgTableName,
                pgSchema: PG_SCHEMA,
                savingType: "insertOnly",
            },
            new JSONSchemaValidator(RopidGTFS.trips.name + "ModelValidator", TripDto.jsonSchema)
        );
        this.modelGTFSStopTimes = new RopidGTFSStopTimesModel();
        this.modelGTFSStops = new RopidGTFSStopsModel();
        this.modelGTFSShapes = new RopidGTFSShapesModel();
        this.sequelizeModel.hasMany(this.modelGTFSStopTimes.sequelizeModel, {
            as: "stop_times",
            foreignKey: "trip_id",
            sourceKey: "trip_id",
            constraints: false,
        });
        this.modelGTFSStopTimes.sequelizeModel.hasOne(this.modelGTFSStops.sequelizeModel, {
            as: "stop",
            foreignKey: "stop_id",
            sourceKey: "stop_id",
            constraints: false,
        });
        this.sequelizeModel.hasMany(this.modelGTFSShapes.sequelizeModel, {
            as: "shapes",
            foreignKey: "shape_id",
            sourceKey: "shape_id",
            constraints: false,
        });
    }

    public findByIdWithStopTimes = async (tripId: string): Promise<ITripsStopTimesStops | undefined> => {
        return (
            await this.sequelizeModel.findOne({
                attributes: [
                    "bikes_allowed",
                    "block_id",
                    "direction_id",
                    "exceptional",
                    "route_id",
                    "service_id",
                    "shape_id",
                    "trip_headsign",
                    "trip_id",
                    "trip_short_name",
                    "wheelchair_accessible",
                ],
                include: {
                    attributes: [
                        "arrival_time",
                        "departure_time",
                        "shape_dist_traveled",
                        "stop_headsign",
                        "stop_id",
                        "stop_sequence",
                        [Sequelize.literal(`EXTRACT(EPOCH FROM "arrival_time"::INTERVAL)::int`), "arrival_time_seconds"],
                        [Sequelize.literal(`EXTRACT(EPOCH FROM "departure_time"::INTERVAL)::int`), "departure_time_seconds"],
                    ],
                    as: "stop_times",
                    model: this.modelGTFSStopTimes.sequelizeModel,
                    include: [
                        {
                            attributes: ["stop_id", "stop_lat", "stop_lon", "stop_name"],
                            as: "stop",
                            model: this.modelGTFSStops.sequelizeModel,
                            required: true,
                        },
                    ],
                },
                order: [["stop_times", "stop_sequence", "ASC"]],
                where: { trip_id: tripId },
            })
        )?.toJSON();
    };

    public findByIdWithShapes = async (tripId: string): Promise<ITripsShapes | undefined> => {
        return (
            await this.sequelizeModel.findOne({
                attributes: [
                    "bikes_allowed",
                    "block_id",
                    "direction_id",
                    "exceptional",
                    "route_id",
                    "service_id",
                    "shape_id",
                    "trip_headsign",
                    "trip_id",
                    "trip_short_name",
                    "wheelchair_accessible",
                ],
                include: {
                    as: "shapes",
                    model: this.modelGTFSShapes.sequelizeModel,
                    attributes: ["shape_dist_traveled", "shape_id", "shape_pt_lat", "shape_pt_lon", "shape_pt_sequence"],
                },
                order: [["shapes", "shape_pt_sequence", "ASC"]],
                where: { trip_id: tripId },
            })
        )?.toJSON();
    };
}
