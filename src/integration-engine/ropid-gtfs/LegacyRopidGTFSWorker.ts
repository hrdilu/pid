import { PidContainer } from "#ie/ioc/Di";
import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { IRopidDeparturesPreset, RopidDeparturesPresets } from "#sch/ropid-departures-presets";
import { RopidGTFS } from "#sch/ropid-gtfs";
import { config } from "@golemio/core/dist/integration-engine/config";
import { DataSource, FTPProtocolStrategy, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine/datasources";
import { BaseWorker } from "@golemio/core/dist/integration-engine/workers";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { DatasetEnum, RopidGTFSMetadataModel } from ".";
import { DeparturePresetsFacade } from "./facade/DeparturePresetsFacade";
import { RopidGtfsContainer } from "./ioc/Di";

export interface IDeparturesPresetsData {
    data: IRopidDeparturesPreset[];
}

export class LegacyRopidGTFSWorker extends BaseWorker {
    private dataSourceDeparturesPresets: DataSource;
    private metaModel: RopidGTFSMetadataModel;
    private readonly queuePrefix: string;

    constructor() {
        super();

        // MetaData Model
        this.metaModel = RopidGtfsContainer.resolve<RopidGTFSMetadataModel>(ModuleContainerToken.RopidGTFSMetadataModel);

        // Ropid Departures Presets
        this.dataSourceDeparturesPresets = new DataSource(
            RopidDeparturesPresets.name + "DataSource",
            new FTPProtocolStrategy({
                filename: config.datasources.RopidDeparturesPresetsFilename,
                path: config.datasources.RopidDeparturesPresetsPath,
                url: config.datasources.RopidFTP,
                encoding: "utf8",
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(RopidDeparturesPresets.name + "DataSource", RopidDeparturesPresets.datasourceJsonSchema)
        );

        this.queuePrefix = config.RABBIT_EXCHANGE_NAME + "." + RopidGTFS.name.toLowerCase();
    }

    /**
     * Queue listener method - triggers for new presets only
     */
    public checkForNewDeparturesPresets = async (): Promise<void> => {
        // checking DEPARTURES_PRESETS dataset
        const departuresPresetsServerLastModified = await this.dataSourceDeparturesPresets.getLastModified();
        const departuresPresetsDbLastModified = await this.metaModel.getLastModified(DatasetEnum.DEPARTURES_PRESETS);
        if (departuresPresetsServerLastModified !== departuresPresetsDbLastModified.lastModified) {
            await this.sendMessageToExchange("workers." + this.queuePrefix + ".downloadDeparturesPresets", "Just do it!");
        }
    };

    /**
     * Queue listener method - triggers processing of DEPARTURES_PRESETS dataset
     */
    public downloadDeparturesPresets = async (): Promise<void> => {
        const file: IDeparturesPresetsData = await this.dataSourceDeparturesPresets.getAll();
        const lastModified = await this.dataSourceDeparturesPresets.getLastModified();

        const facade = PidContainer.resolve<DeparturePresetsFacade>(ModuleContainerToken.DeparturePresetsFacade);
        await facade.handleNewDeparturePresets(file.data, lastModified);
    };
}
