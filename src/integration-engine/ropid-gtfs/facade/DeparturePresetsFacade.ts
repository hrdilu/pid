import { ModuleContainerToken } from "#ie/ioc/ModuleContainerToken";
import { MetaDatasetInfoKeyEnum, MetaStateEnum, MetaTypeEnum } from "#ie/shared/RopidMetadataModel";
import { IRopidDeparturesPreset } from "#sch/ropid-departures-presets";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { ContainerToken } from "@golemio/core/dist/integration-engine/ioc/ContainerToken";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { DatasetEnum, RopidGTFSMetadataModel } from "../RopidGTFSMetadataModel";
import { DeparturePresetsRepository } from "../data-access/DeparturePresetsRepository";
import { RopidDeparturesPresetsTransformation } from "../transformations/RopidDeparturesPresetsTransformation";

@injectable()
export class DeparturePresetsFacade {
    constructor(
        @inject(ModuleContainerToken.RopidGTFSMetadataModel) private metaModel: RopidGTFSMetadataModel,
        @inject(ModuleContainerToken.DeparturePresetsRepository) private departurePresetsRepository: DeparturePresetsRepository,
        @inject(ModuleContainerToken.RopidDeparturesPresetsTransformation)
        private transformationDeparturesPresets: RopidDeparturesPresetsTransformation,
        @inject(ContainerToken.Logger) private logger: ILogger
    ) {}

    public async handleNewDeparturePresets(data: IRopidDeparturesPreset[], lastModified: string) {
        const dbLastModified = await this.metaModel.getLastModified(DatasetEnum.DEPARTURES_PRESETS);
        await this.metaModel.save({
            dataset: DatasetEnum.DEPARTURES_PRESETS,
            key: MetaDatasetInfoKeyEnum.LAST_MODIFIED,
            type: MetaTypeEnum.DATASET_INFO,
            value: lastModified,
            version: dbLastModified.version + 1,
        });

        const transformedData = await this.transformationDeparturesPresets.transform(data);

        await this.metaModel.save({
            dataset: DatasetEnum.DEPARTURES_PRESETS,
            key: "departuresPresets",
            type: MetaTypeEnum.TABLE_TOTAL_COUNT,
            value: transformedData.length,
            version: dbLastModified.version + 1,
        });

        try {
            await this.departurePresetsRepository.truncate(true);
            await this.departurePresetsRepository.save(transformedData, true);

            await this.metaModel.save({
                dataset: DatasetEnum.DEPARTURES_PRESETS,
                key: "departuresPresets",
                type: MetaTypeEnum.STATE,
                value: MetaStateEnum.SAVED,
                version: dbLastModified.version + 1,
            });
            await this.metaModel.checkSavedRows(DatasetEnum.DEPARTURES_PRESETS, dbLastModified.version + 1);
            await this.metaModel.replaceTmpTables(DatasetEnum.DEPARTURES_PRESETS, dbLastModified.version + 1);
        } catch (err) {
            this.logger.error(err);
            await this.metaModel.rollbackFailedSaving(DatasetEnum.DEPARTURES_PRESETS, dbLastModified.version + 1);
        }
    }
}
