import { PidContainer } from "#ie/ioc/Di";
import { RopidGTFS } from "#sch/ropid-gtfs";
import { RedisPubSubChannel } from "@golemio/core/dist/integration-engine/data-access/pubsub";
import { DependencyContainer, Lifecycle } from "@golemio/core/dist/shared/tsyringe";
import { StaticFileRedisRepository } from "../data-access/cache/StaticFileRedisRepository";
import { RopidGTFSTransformation } from "../transformations/RopidGTFSTransformation";
import { PrecomputedTablesFacade } from "../workers/timetables/tasks/helpers/PrecomputedTablesFacade";
import { RopidGtfsFactory } from "../workers/timetables/tasks/helpers/RopidGtfsFactory";
import { RopidGtfsContainerToken } from "./RopidGtfsContainerToken";

//#region Initialization
const RopidGtfsContainer: DependencyContainer = PidContainer.createChildContainer();
//#endregion

//#region Datasources

//#endregion

//#region Repositories
RopidGtfsContainer.registerSingleton(RopidGtfsContainerToken.StaticFileRedisRepository, StaticFileRedisRepository);
//#endregion

//#region Transformations
RopidGtfsContainer.registerSingleton(RopidGtfsContainerToken.RopidGTFSTransformation, RopidGTFSTransformation);
//#endregion

//#region Facade
RopidGtfsContainer.registerSingleton(RopidGtfsContainerToken.PrecomputedTablesFacade, PrecomputedTablesFacade)
    .register(RopidGtfsContainerToken.RopidGtfsFactory, RopidGtfsFactory, { lifecycle: Lifecycle.Transient })
    .registerInstance(RopidGtfsContainerToken.RedisPubSubChannel, new RedisPubSubChannel(RopidGTFS.name));
//#endregion

export { RopidGtfsContainer };
