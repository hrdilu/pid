const RopidGtfsContainerToken = {
    PrecomputedTablesFacade: Symbol(),
    StaticFileRedisRepository: Symbol(),
    RopidGTFSTransformation: Symbol(),
    RopidGtfsFactory: Symbol(),
    RedisPubSubChannel: Symbol(),
};

export { RopidGtfsContainerToken };
