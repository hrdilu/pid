import { buildAlterIndexQuery, buildCreateIndexQuery } from "#ie/ropid-gtfs/utils";
import { PG_SCHEMA } from "#sch/const";
import { RopidGTFS } from "#sch/ropid-gtfs";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { Transaction } from "@golemio/core/dist/shared/sequelize";
import { SourceTableSuffixEnum } from "../../helpers/SourceTableSuffixEnum";

export class MinMaxStopSequencesRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "MinMaxStopSequencesRepository",
            {
                outputSequelizeAttributes: RopidGTFS.minMaxStopSequences.outputSequelizeAttributes,
                pgTableName: RopidGTFS.minMaxStopSequences.pgTableName,
                pgSchema: PG_SCHEMA,
                savingType: "insertOnly",
                attributesToRemove: ["id", "created_at", "updated_at"],
            },
            new JSONSchemaValidator("MinMaxStopSequencesRepositoryValidator", RopidGTFS.minMaxStopSequences.outputJsonSchema)
        );
    }

    public createAndPopulate = async (sourceTableSuffix: SourceTableSuffixEnum): Promise<void> => {
        const sql = `
            SELECT
                trip_id,
                max(stop_sequence) AS max_stop_sequence,
                min(stop_sequence) AS min_stop_sequence,
                GREATEST(
                    max(arrival_time::interval),
                    max(departure_time::interval)
                ) AS max_stop_time,
                LEAST(
                    min(arrival_time::interval),
                    min(departure_time::interval)
                ) AS min_stop_time
            FROM "${PG_SCHEMA}".ropidgtfs_stop_times${sourceTableSuffix}
            GROUP BY trip_id
        `;

        try {
            const tmpTable = RopidGTFS.minMaxStopSequences.pgTableName + SourceTableSuffixEnum.Tmp;

            await this.sequelizeModel.sequelize!.query(
                `DROP TABLE IF EXISTS "${PG_SCHEMA}"."${tmpTable}";
                CREATE TABLE "${PG_SCHEMA}"."${tmpTable}" (
                    LIKE "${PG_SCHEMA}"."${RopidGTFS.minMaxStopSequences.pgTableName}"
                );
                INSERT INTO "${PG_SCHEMA}"."${tmpTable}" ${sql};`
            );
        } catch (err) {
            log.error(err);
            throw err;
        }
    };

    public replaceByTmp = async (transaction: Transaction): Promise<void> => {
        try {
            const pgTableName = RopidGTFS.minMaxStopSequences.pgTableName;
            const tableNameTmp = pgTableName + SourceTableSuffixEnum.Tmp;
            const tableNameActual = pgTableName + SourceTableSuffixEnum.Actual;

            const indexQuery = buildAlterIndexQuery(RopidGTFS.minMaxStopSequences.modelIndexOptions);
            await this.sequelizeModel.sequelize!.query(
                `
                LOCK "${PG_SCHEMA}".${pgTableName} IN EXCLUSIVE MODE;
                ALTER TABLE "${PG_SCHEMA}".${tableNameActual} NO INHERIT "${PG_SCHEMA}".${pgTableName};
                DROP TABLE IF EXISTS "${PG_SCHEMA}"."${tableNameActual}";
                ${indexQuery}
                ALTER TABLE "${PG_SCHEMA}"."${tableNameTmp}" RENAME TO ${tableNameActual};
                ALTER TABLE "${PG_SCHEMA}"."${tableNameActual}" INHERIT "${PG_SCHEMA}"."${pgTableName}";
                `,
                { transaction }
            );
        } catch (err) {
            log.error(err);
            throw err;
        }
    };

    public createIndexes = async (): Promise<void> => {
        try {
            const indexes = RopidGTFS.minMaxStopSequences.modelIndexOptions;
            const indexQuery = buildCreateIndexQuery(RopidGTFS.minMaxStopSequences.pgTableName, indexes);

            await this.sequelizeModel.sequelize!.query(indexQuery);
        } catch (err) {
            log.error(err);
            throw err;
        }
    };
}
