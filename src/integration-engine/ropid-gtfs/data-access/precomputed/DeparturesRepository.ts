import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { buildAlterIndexQuery, buildCreateIndexQuery } from "#ie/ropid-gtfs/utils";
import { PG_SCHEMA } from "#sch/const";
import { DeparturesModel } from "#sch/ropid-gtfs/models/precomputed";
import { config } from "@golemio/core/dist/integration-engine/config";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { Transaction } from "@golemio/core/dist/shared/sequelize";
import { SourceTableSuffixEnum } from "../../helpers/SourceTableSuffixEnum";

export class DeparturesRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "DeparturesRepository",
            {
                pgTableName: DeparturesModel.TABLE_NAME,
                pgSchema: PG_SCHEMA,
                outputSequelizeAttributes: DeparturesModel.attributeModel,
                attributesToRemove: ["id", "created_at", "updated_at"],
                savingType: "insertOnly",
            },
            new JSONSchemaValidator("DeparturesRepository", DeparturesModel.jsonSchema)
        );
    }

    public createAndPopulate = async (sourceTableSuffix: SourceTableSuffixEnum): Promise<void> => {
        const timeConstraintLiteral =
            config.NODE_ENV !== "test" ? `WHERE gtfs_timestamp(t.arrival_time, t4.date) > now() - interval '6 hours'` : "";

        /* eslint-disable max-len */
        const sql = `
            SELECT
                t.stop_sequence,
                t.stop_headsign,
                t.pickup_type,
                t.drop_off_type,
                t.arrival_time,
                gtfs_timestamp(t.arrival_time, t4.date) AS arrival_datetime,
                t.departure_time,
                gtfs_timestamp(t.departure_time, t4.date) AS departure_datetime,
                t0.stop_id,
                t0.stop_name,
                t0.platform_code,
                t0.wheelchair_boarding,
                t1.min_stop_sequence,
                t1.max_stop_sequence,
                t2.trip_id,
                t2.trip_headsign,
                t2.trip_short_name,
                t2.wheelchair_accessible,
                t3.service_id,
                t4.date,
                t5.route_short_name,
                coalesce(t5.route_type, :defaultRouteType) AS route_type,
                t5.route_id,
                t5.is_night,
                t5.is_regional,
                t5.is_substitute_transport,
                t6.stop_sequence AS next_stop_sequence,
                t6.stop_id AS next_stop_id,
                t7.stop_sequence AS last_stop_sequence,
                t7.stop_id AS last_stop_id
            FROM ropidgtfs_stop_times${sourceTableSuffix} t
                LEFT JOIN ropidgtfs_stops${sourceTableSuffix} t0 ON t.stop_id = t0.stop_id
                LEFT JOIN ropidgtfs_trips${sourceTableSuffix} t2 ON t.trip_id = t2.trip_id
                INNER JOIN ropidgtfs_precomputed_services_calendar_tmp t4 ON t2.service_id = t4.service_id
                INNER JOIN ropidgtfs_precomputed_minmax_stop_sequences_tmp t1 ON t.trip_id = t1.trip_id
                LEFT JOIN ropidgtfs_calendar${sourceTableSuffix} t3 ON t2.service_id = t3.service_id
                LEFT JOIN ropidgtfs_routes${sourceTableSuffix} t5 ON t2.route_id = t5.route_id
                LEFT JOIN ropidgtfs_stop_times${sourceTableSuffix} t6 ON t.trip_id = t6.trip_id AND t6.stop_sequence = t.stop_sequence + 1
                LEFT JOIN ropidgtfs_stop_times${sourceTableSuffix} t7 ON t.trip_id = t7.trip_id AND t7.stop_sequence = t.stop_sequence - 1
            ${timeConstraintLiteral}`;
        /* eslint-enable max-len */

        try {
            const tableName = DeparturesModel.TABLE_NAME;
            const tmpTableName = tableName + SourceTableSuffixEnum.Tmp;

            await this.sequelizeModel.sequelize!.query(
                `
                SET LOCAL search_path TO ${PG_SCHEMA};
                DROP TABLE IF EXISTS ${tmpTableName};
                CREATE TABLE ${tmpTableName} (LIKE ${tableName} including defaults);
                INSERT INTO ${tmpTableName} ${sql};`,
                {
                    replacements: {
                        defaultRouteType: GTFSRouteTypeEnum.EXT_MISCELLANEOUS,
                    },
                }
            );
        } catch (err) {
            log.error(err);
            throw err;
        }
    };

    public replaceByTmp = async (transaction: Transaction): Promise<void> => {
        try {
            const tableName = DeparturesModel.TABLE_NAME;
            const tmpTableName = tableName + SourceTableSuffixEnum.Tmp;
            const dropTableName = tableName + SourceTableSuffixEnum.Drop;

            const indexQuery = buildAlterIndexQuery(DeparturesModel.indexesOptions);
            await this.sequelizeModel.sequelize!.query(
                `
                SET LOCAL search_path TO ${PG_SCHEMA};
                LOCK ${tableName} IN EXCLUSIVE MODE;
                ALTER TABLE ${tableName} RENAME TO ${dropTableName};
                ALTER TABLE ${tmpTableName} RENAME TO ${tableName};
                DROP TABLE ${dropTableName} CASCADE;
                ${indexQuery}`,
                { transaction }
            );
        } catch (err) {
            log.error(err);
            throw err;
        }
    };

    public analyze = async (): Promise<void> => {
        try {
            await this.sequelizeModel.sequelize!.query(
                `analyze ${PG_SCHEMA}.${DeparturesModel.TABLE_NAME}${SourceTableSuffixEnum.Tmp};`
            );
        } catch (err) {
            log.error(err);
            throw err;
        }
    };

    public createIndexes = async (): Promise<void> => {
        try {
            const indexes = DeparturesModel.indexesOptions;
            const indexQuery = buildCreateIndexQuery(DeparturesModel.TABLE_NAME, indexes);

            await this.sequelizeModel.sequelize!.query(indexQuery);
        } catch (err) {
            log.error(err);
            throw err;
        }
    };
}
