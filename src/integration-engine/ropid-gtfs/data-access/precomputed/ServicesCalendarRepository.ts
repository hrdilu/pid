import { buildAlterIndexQuery, buildCreateIndexQuery } from "#ie/ropid-gtfs/utils";
import { PG_SCHEMA } from "#sch/const";
import { RopidGTFS } from "#sch/ropid-gtfs";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { Transaction } from "@golemio/core/dist/shared/sequelize";
import { SourceTableSuffixEnum } from "../../helpers/SourceTableSuffixEnum";

export class ServicesCalendarRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "ServicesCalendarRepository",
            {
                outputSequelizeAttributes: RopidGTFS.servicesCalendar.outputSequelizeAttributes,
                pgTableName: RopidGTFS.servicesCalendar.pgTableName,
                pgSchema: PG_SCHEMA,
                savingType: "insertOnly",
                attributesToRemove: ["id", "created_at", "updated_at"],
            },
            new JSONSchemaValidator("ServicesCalendarRepositoryValidator", RopidGTFS.servicesCalendar.outputJsonSchema)
        );
    }

    public createAndPopulate = async (sourceTableSuffix: SourceTableSuffixEnum): Promise<void> => {
        const sql = `
            SELECT
                dates.date,
                date_part('day', dates.date - to_char(timezone('Europe/Prague', now()), 'YYYY-MM-DD')::timestamp) AS day_diff,
                service_id
            FROM (
                SELECT
                    date_trunc('day', dd.dd)::date AS date
                FROM generate_series(
                    ( SELECT (CURRENT_TIMESTAMP - interval '1 day') )::date,
                    ( SELECT (CURRENT_TIMESTAMP + interval '2 days') )::date,
                    '1 day'
                ) dd(dd)) dates
            INNER JOIN "${PG_SCHEMA}".ropidgtfs_calendar${sourceTableSuffix} calendar ON ((1 = 1))
            WHERE (
                (
                    dates.date >= "start_date"::date
                    AND dates.date <= end_date::date
                    AND (
                        (
                            btrim(to_char(dates.date::timestamptz, 'day')) = 'monday'
                            AND monday = 1
                        ) OR (
                            btrim(to_char(dates.date::timestamptz, 'day')) = 'tuesday'
                            AND tuesday = 1
                        ) OR (
                            btrim(to_char(dates.date::timestamptz, 'day')) = 'wednesday'
                            AND wednesday = 1
                        ) OR (
                            btrim(to_char(dates.date::timestamptz, 'day')) = 'thursday'
                            AND thursday = 1
                        ) OR (
                            btrim(to_char(dates.date::timestamptz, 'day')) = 'friday'
                            AND friday = 1
                        ) OR (
                            btrim(to_char(dates.date::timestamptz, 'day')) = 'saturday'
                            AND saturday = 1
                        ) OR (
                            btrim(to_char(dates.date::timestamptz, 'day')) = 'sunday'
                            AND sunday = 1
                        )
                    ) AND (
                        NOT service_id IN (
                            SELECT service_id
                            FROM "${PG_SCHEMA}".ropidgtfs_calendar_dates${sourceTableSuffix} calendar_dates
                            WHERE (
                                exception_type = 2
                                AND dates.date = calendar_dates.date::date
                            )
                        )
                    )) OR service_id IN (
                        SELECT service_id
                        FROM "${PG_SCHEMA}".ropidgtfs_calendar_dates${sourceTableSuffix} calendar_dates
                        WHERE
                            calendar_dates.exception_type = 1
                            AND dates.date = calendar_dates.date::date
                    )
                )
            ORDER BY dates.date
        `;

        try {
            const tmpTable = RopidGTFS.servicesCalendar.pgTableName + SourceTableSuffixEnum.Tmp;

            await this.sequelizeModel.sequelize!.query(
                `DROP TABLE IF EXISTS "${PG_SCHEMA}".${tmpTable};
                CREATE TABLE "${PG_SCHEMA}".${tmpTable} (
                    LIKE "${PG_SCHEMA}".${RopidGTFS.servicesCalendar.pgTableName}
                );
                INSERT INTO "${PG_SCHEMA}".${tmpTable} ${sql};`
            );
        } catch (err) {
            log.error(err);
            throw err;
        }
    };

    public replaceByTmp = async (transaction: Transaction): Promise<void> => {
        try {
            const pgTableName = RopidGTFS.servicesCalendar.pgTableName;
            const tableNameTmp = pgTableName + SourceTableSuffixEnum.Tmp;
            const tableNameActual = pgTableName + SourceTableSuffixEnum.Actual;

            const indexQuery = buildAlterIndexQuery(RopidGTFS.servicesCalendar.modelIndexOptions);
            await this.sequelizeModel.sequelize!.query(
                `
                LOCK "${PG_SCHEMA}".${pgTableName} IN EXCLUSIVE MODE;
                ALTER TABLE "${PG_SCHEMA}".${tableNameActual} NO INHERIT "${PG_SCHEMA}".${pgTableName};
                DROP TABLE IF EXISTS "${PG_SCHEMA}".${tableNameActual};
                ${indexQuery}
                ALTER TABLE "${PG_SCHEMA}".${tableNameTmp} RENAME TO ${tableNameActual};
                ALTER TABLE "${PG_SCHEMA}".${tableNameActual} INHERIT "${PG_SCHEMA}".${pgTableName};
                `,
                { transaction }
            );
        } catch (err) {
            log.error(err);
            throw err;
        }
    };

    public createIndexes = async (): Promise<void> => {
        try {
            const indexQuery = buildCreateIndexQuery(
                RopidGTFS.servicesCalendar.pgTableName,
                RopidGTFS.servicesCalendar.modelIndexOptions
            );
            await this.sequelizeModel.sequelize!.query(indexQuery);
        } catch (err) {
            log.error(err);
            throw err;
        }
    };
}
