import { DelayComputationDtoSchema } from "#sch/ropid-gtfs/redis/schemas/DelayComputationDtoSchema";
import { RedisModel } from "@golemio/core/dist/integration-engine/models/RedisModel";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class DelayComputationRedisRepository extends RedisModel {
    public static NAMESPACE_PREFIX = "gtfsTripDelayComputation";

    constructor() {
        super(
            "GTFSDelayComputationRedisRepository",
            {
                decodeDataAfterGet: JSON.parse,
                encodeDataBeforeSave: JSON.stringify,
                isKeyConstructedFromData: true,
                prefix: DelayComputationRedisRepository.NAMESPACE_PREFIX,
            },
            new JSONSchemaValidator("GTFSDelayComputationRedisRepositoryValidator", DelayComputationDtoSchema)
        );
    }
}
