import { RedisModel } from "@golemio/core/dist/integration-engine/models/RedisModel";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class StaticFileRedisRepository extends RedisModel {
    public static NAMESPACE_PREFIX = "files:gtfsStatic";

    constructor() {
        super("GTFSStaticFileRedisRepository", {
            isKeyConstructedFromData: false,
            prefix: StaticFileRedisRepository.NAMESPACE_PREFIX,
        });
    }
}
