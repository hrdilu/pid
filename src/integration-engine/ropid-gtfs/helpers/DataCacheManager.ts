import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import {
    BlockStopsRedisRepository,
    DelayComputationRedisRepository,
    RunTripsRedisRepository,
    StaticFileRedisRepository,
} from "../data-access/cache";
import { IDataCacheManager } from "./interfaces/IDataCacheManager";

export class DataCacheManager implements IDataCacheManager {
    private static _instance: DataCacheManager;
    private readonly staticFileRepository: StaticFileRedisRepository;
    private readonly runTripsRepository: RunTripsRedisRepository;
    private readonly blockStopsRepository: BlockStopsRedisRepository;
    private readonly delayComputationRepository: DelayComputationRedisRepository;

    public static getInstance() {
        if (!this._instance) {
            this._instance = new DataCacheManager();
        }

        return this._instance;
    }

    private constructor() {
        this.staticFileRepository = new StaticFileRedisRepository();
        this.runTripsRepository = new RunTripsRedisRepository();
        this.blockStopsRepository = new BlockStopsRedisRepository();
        this.delayComputationRepository = new DelayComputationRedisRepository();
    }

    public async cleanCache(): Promise<void> {
        try {
            await this.staticFileRepository.truncate(true);
            await this.runTripsRepository.truncate(true);
            await this.blockStopsRepository.truncate(true);
            await this.delayComputationRepository.truncate(true);
        } catch (err) {
            throw new GeneralError("DataCacheManager: Failed to clean cache", this.constructor.name, err);
        }
    }
}
