import { PG_SCHEMA } from "#sch/const";
import { RopidGTFS } from "#sch/ropid-gtfs";
import { StopDto } from "#sch/ropid-gtfs/models/StopDto";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine";
import { IValidator, JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import Sequelize from "@golemio/core/dist/shared/sequelize";

export class RopidGTFSStopsModel extends PostgresModel implements IModel {
    /** Model name */
    public name!: string;
    /** The Sequelize Model */
    public sequelizeModel!: Sequelize.ModelCtor<any>;
    /** The Sequelize Model for temporary table */
    protected tmpSequelizeModel!: Sequelize.ModelCtor<any> | null;
    /** Validation helper */
    protected validator!: IValidator;
    /** Type/Strategy of saving the data */
    protected savingType!: "insertOnly" | "insertOrUpdate";

    public constructor() {
        super(
            RopidGTFS.stops.name + "Model",
            {
                outputSequelizeAttributes: RopidGTFS.stops.outputSequelizeAttributes,
                pgTableName: RopidGTFS.stops.pgTableName,
                pgSchema: PG_SCHEMA,
                savingType: "insertOnly",
            },
            new JSONSchemaValidator(RopidGTFS.stops.name + "ModelValidator", StopDto.jsonSchema)
        );
    }
}
