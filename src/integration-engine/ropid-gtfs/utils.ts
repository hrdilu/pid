import { PG_SCHEMA } from "#sch/const";
import { ModelIndexesOptions } from "@golemio/core/dist/shared/sequelize";
import { SourceTableSuffixEnum } from "./helpers/SourceTableSuffixEnum";

export const buildCreateIndexQuery = (tableName: string, indexes: ModelIndexesOptions[]): string => {
    const rawQuery = indexes.map((el) => {
        return `CREATE ${el.unique ? "UNIQUE" : ""} INDEX IF NOT EXISTS ${el.name}${SourceTableSuffixEnum.Tmp}
                ON "${PG_SCHEMA}".${tableName}${SourceTableSuffixEnum.Tmp} ${
            el.using ? "USING " + el.using : ""
        } (${el.fields!.toString()});`;
    });

    return rawQuery.join(" ");
};

export const buildAlterIndexQuery = (indexes: ModelIndexesOptions[]): string => {
    const rawQuery = indexes.map((el) => {
        return `ALTER INDEX IF EXISTS "${PG_SCHEMA}".${el.name}${SourceTableSuffixEnum.Tmp} RENAME TO ${el.name};`;
    });

    return rawQuery.join(" ");
};

export const buildDropIndexQuery = (indexes: ModelIndexesOptions[]): string => {
    const rawQuery = indexes.map((el) => {
        return `DROP INDEX IF EXISTS "${PG_SCHEMA}".${el.name}${SourceTableSuffixEnum.Tmp};`;
    });
    return rawQuery.join(" ");
};
