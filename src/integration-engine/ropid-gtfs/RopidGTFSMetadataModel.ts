import {
    DeparturesRepository,
    MinMaxStopSequencesRepository,
    ServicesCalendarRepository,
    TripScheduleRepository,
} from "#ie/ropid-gtfs/data-access/precomputed";
import { buildAlterIndexQuery } from "#ie/ropid-gtfs/utils";
import { MetaDatasetInfoKeyEnum, MetaStateEnum, MetaTypeEnum, RopidMetadataModel } from "#ie/shared";
import { PG_SCHEMA } from "#sch/const";
import { RopidGTFS } from "#sch/ropid-gtfs";
import { RopidGTFSPgProp } from "#sch/ropid-gtfs/RopidGtfsSchedule";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import Sequelize, { Model, Transaction } from "@golemio/core/dist/shared/sequelize";
import { SourceTableSuffixEnum } from "./helpers/SourceTableSuffixEnum";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

interface IMetadataTable {
    key: string;
    tn: string;
}

export enum DatasetEnum {
    PID_GTFS = "PID_GTFS",
    CIS_STOPS = "CIS_STOPS",
    OIS_MAPPING = "OIS_MAPPING",
    RUN_NUMBERS = "RUN_NUMBERS",
    DEPARTURES_PRESETS = "DEPARTURES_PRESETS",
}

@injectable()
export class RopidGTFSMetadataModel extends RopidMetadataModel {
    /** Precomputed data repositories */
    private departuresRepository: DeparturesRepository;
    private minMaxStopSequencesRepository: MinMaxStopSequencesRepository;
    private tripScheduleRepository: TripScheduleRepository;
    private servicesCalendarRepository: ServicesCalendarRepository;

    constructor() {
        super(RopidGTFS);
        this.departuresRepository = new DeparturesRepository();
        this.minMaxStopSequencesRepository = new MinMaxStopSequencesRepository();
        this.tripScheduleRepository = new TripScheduleRepository();
        this.servicesCalendarRepository = new ServicesCalendarRepository();
    }

    public checkSavedTmpTables = async (dataset: string, version: number): Promise<void> => {
        const tables = await this.sequelizeModel.findAll({
            attributes: [["key", "tn"]],
            where: {
                dataset,
                type: MetaTypeEnum.STATE,
                value: MetaStateEnum.SAVED,
                version,
            },
        });

        const tablesArray: any[] = [];
        tables.map((table) => {
            tablesArray.push(`'${RopidGTFS[table.dataValues.tn as RopidGTFSPgProp].pgTableName}${SourceTableSuffixEnum.Tmp}'`);
        });

        const result: Array<{ name: string; total: number }> = await this.sequelizeModel.sequelize!.query(
            `
            SELECT table_name AS name, count_rows(table_schema, table_name) AS total
            FROM information_schema.tables
            WHERE table_schema NOT IN ('pg_catalog', 'information_schema')
                AND table_type = 'BASE TABLE'
                AND table_name in (${tablesArray.join(",")})
                AND table_schema = '${PG_SCHEMA}';
            `,
            { type: Sequelize.QueryTypes.SELECT }
        );

        log.info(`Datasets: total rows for ${dataset}: ${JSON.stringify(result)}`);

        if (!result || !result.length || result.some((table) => table.total === 0)) {
            throw new GeneralError("Empty table found", this.constructor.name);
        }
    };

    public checkAllTablesHasSavedState = async (datasets: Array<{ dataset: string; version: number }>): Promise<boolean> => {
        const notSaved = await this.sequelizeModel.count({
            where: {
                [Sequelize.Op.or]: datasets.map((item) => ({
                    dataset: item.dataset,
                    version: item.version,
                    type: MetaTypeEnum.STATE,
                    value: { [Sequelize.Op.ne]: MetaStateEnum.SAVED },
                })),
            },
        });
        return notSaved === 0;
    };

    public checkIfNewVersionIsAlreadyDeployed = async (dataset: string, version: number): Promise<boolean> => {
        const alreadyDeployed = await this.sequelizeModel.count({
            where: {
                dataset,
                key: MetaDatasetInfoKeyEnum.DEPLOYED,
                type: MetaTypeEnum.DATASET_INFO,
                value: "true",
                version,
            },
        });
        return alreadyDeployed !== 0;
    };

    public replaceTables = async (datasetInfo: Array<{ dataset: string; version: number }>): Promise<boolean> => {
        const connection = PostgresConnector.getConnection();
        const t = await connection.transaction();
        try {
            const tables = await this.sequelizeModel.findAll({
                attributes: [["key", "tn"]],
                transaction: t,
                where: {
                    [Sequelize.Op.or]: datasetInfo.map((item) => ({
                        dataset: item.dataset,
                        type: MetaTypeEnum.STATE,
                        value: MetaStateEnum.SAVED,
                        version: item.version,
                    })),
                },
            });

            await this.replaceStaticTables(tables, t);
            await this.replacePrecomputedTables(t);

            // save meta
            await this.sequelizeModel.bulkCreate(
                datasetInfo.map((el) => ({
                    dataset: el.dataset,
                    key: MetaDatasetInfoKeyEnum.DEPLOYED,
                    type: MetaTypeEnum.DATASET_INFO,
                    value: "true",
                    version: el.version,
                })),
                { transaction: t }
            );

            await this.sequelizeModel.destroy({
                transaction: t,
                where: {
                    [Sequelize.Op.or]: datasetInfo.map((item) => ({
                        dataset: item.dataset,
                        version: {
                            [Sequelize.Op.and]: [
                                // delete all versions older than ten versions back
                                { [Sequelize.Op.lt]: item.version - 10 },
                                { [Sequelize.Op.ne]: -1 },
                            ],
                        },
                    })),
                },
            });

            await t.commit();
            return true;
        } catch (err) {
            log.error(err);
            await t.rollback();
            log.error(`Datasets: replacing failed ${err.toString()}`);
            throw new GeneralError(this.name + ": replaceTables() failed.", undefined, err);
        }
    };

    public updateState = async (dataset: string, name: string, state: MetaStateEnum, version: number): Promise<any> => {
        return this.sequelizeModel.update(
            {
                dataset,
                key: name,
                type: MetaTypeEnum.STATE,
                value: state,
                version,
            },
            {
                where: {
                    dataset,
                    key: name,
                    type: MetaTypeEnum.STATE,
                    version,
                },
            }
        );
    };

    public getNumberOfDownloadRetries = async (dataset: string, version: number): Promise<number> => {
        const numberOfRetries = await this.sequelizeModel.findOne({
            attributes: [["value", "retries"]],
            where: {
                dataset,
                key: MetaDatasetInfoKeyEnum.NUMBER_OF_RETRIES,
                type: MetaTypeEnum.DATASET_INFO,
                version: version - 1,
            },
        });

        if (!numberOfRetries) {
            return 0;
        }

        return +numberOfRetries.dataValues.retries;
    };

    public replaceStaticTables = async (tables: Array<Model<IMetadataTable>>, t: Transaction): Promise<void> => {
        const connection = PostgresConnector.getConnection();
        for (const table of tables) {
            const pgTableName = RopidGTFS[table.dataValues.tn as RopidGTFSPgProp].pgTableName;
            log.info(`Datasets: replacing table ${pgTableName}`);

            const tableNameActual = pgTableName + SourceTableSuffixEnum.Actual;
            const tableNameTmp = pgTableName + SourceTableSuffixEnum.Tmp;
            const tableNameDrop = pgTableName + SourceTableSuffixEnum.Drop;

            const indexQuery = RopidGTFS[table.dataValues.tn as RopidGTFSPgProp].modelIndexOptions
                ? buildAlterIndexQuery(RopidGTFS[table.dataValues.tn as RopidGTFSPgProp].modelIndexOptions)
                : "";

            await connection.query(
                `
                LOCK "${PG_SCHEMA}".${pgTableName} IN EXCLUSIVE MODE;
                ALTER TABLE "${PG_SCHEMA}".${tableNameActual} NO INHERIT "${PG_SCHEMA}".${pgTableName};
                ${indexQuery}
                ALTER TABLE "${PG_SCHEMA}".${tableNameTmp} ADD create_batch_id int8 NULL;
                ALTER TABLE "${PG_SCHEMA}".${tableNameTmp} ADD COLUMN created_at timestamptz NULL DEFAULT now();
                ALTER TABLE "${PG_SCHEMA}".${tableNameTmp} ADD created_by varchar(150) NULL;
                ALTER TABLE "${PG_SCHEMA}".${tableNameTmp} ADD update_batch_id int8 NULL;
                ALTER TABLE "${PG_SCHEMA}".${tableNameTmp} ADD COLUMN updated_at timestamptz NULL DEFAULT now();
                ALTER TABLE "${PG_SCHEMA}".${tableNameTmp} ADD updated_by varchar(150) NULL;
                ALTER TABLE "${PG_SCHEMA}".${tableNameActual} RENAME TO ${tableNameDrop};
                ALTER TABLE "${PG_SCHEMA}".${tableNameTmp} RENAME TO ${tableNameActual};
                ALTER TABLE "${PG_SCHEMA}".${tableNameActual} INHERIT "${PG_SCHEMA}".${pgTableName};
                DROP TABLE "${PG_SCHEMA}".${tableNameDrop};
                `,
                { transaction: t }
            );
        }
    };

    public replacePrecomputedTables = async (t: Transaction): Promise<void> => {
        log.info(`Datasets: replacing table ${this.servicesCalendarRepository["tableName"]}`);
        await this.servicesCalendarRepository.replaceByTmp(t);

        log.info(`Datasets: replacing table ${this.minMaxStopSequencesRepository["tableName"]}`);
        await this.minMaxStopSequencesRepository.replaceByTmp(t);

        log.info(`Datasets: replacing table ${this.tripScheduleRepository["tableName"]}`);
        await this.tripScheduleRepository.replaceByTmp(t);

        log.info(`Datasets: replacing table ${this.departuresRepository["tableName"]}`);
        await this.departuresRepository.replaceByTmp(t);
    };
}
