import { LegacyRopidGTFSWorker } from "#ie/ropid-gtfs/LegacyRopidGTFSWorker";
import { RopidGTFS } from "#sch/ropid-gtfs";
import { config } from "@golemio/core/dist/integration-engine/config";
import { IQueueDefinition } from "@golemio/core/dist/integration-engine/queueprocessors";

/**
 * @deprecated Use the new worker definitions
 */
const queueDefinitions: IQueueDefinition[] = [
    // Ropid GTFS Queue Definitions
    {
        name: RopidGTFS.name,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + "." + RopidGTFS.name.toLowerCase(),
        queues: [
            {
                name: "checkForNewDeparturesPresets",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 4 * 60 * 1000, // 4 minutes
                },
                worker: LegacyRopidGTFSWorker,
                workerMethod: "checkForNewDeparturesPresets",
            },
            {
                name: "downloadDeparturesPresets",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 4 * 60 * 1000, // 4 minutes
                },
                worker: LegacyRopidGTFSWorker,
                workerMethod: "downloadDeparturesPresets",
            },
        ],
    },
];

export { queueDefinitions };
