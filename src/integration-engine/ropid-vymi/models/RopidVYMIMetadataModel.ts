import { log } from "@golemio/core/dist/integration-engine/helpers";
import { RopidMetadataModel, MetaDatasetInfoKeyEnum, MetaTypeEnum } from "#ie/shared";
import { RopidVYMI } from "#sch/ropid-vymi";

export enum DatasetEnum {
    EVENTS = "EVENTS",
}

export class RopidVYMIMetadataModel extends RopidMetadataModel {
    constructor() {
        super(RopidVYMI);
    }

    public getDigest = async (dataset: string): Promise<any | null> => {
        try {
            const result = await this.sequelizeModel.findOne({
                order: [["version", "DESC"]],
                where: {
                    dataset,
                    key: MetaDatasetInfoKeyEnum.DIGEST,
                    type: MetaTypeEnum.DATASET_INFO,
                },
            });
            return result?.dataValues?.value || null;
        } catch (err) {
            log.warn(err);
            return null;
        }
    };
}
