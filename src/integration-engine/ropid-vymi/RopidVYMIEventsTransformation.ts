import moment from "@golemio/core/dist/shared/moment-timezone";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { HTMLUtils } from "#ie/shared/HTMLUtils";
import {
    RopidVYMI,
    IRopidVYMIEvent,
    IRopidVYMIEventOutput,
    IRopidVYMIEventRoute,
    IRopidVYMIEventRouteOutput,
    IRopidVYMIEventStop,
    IRopidVYMIEventStopOutput,
} from "#sch/ropid-vymi";

interface ITransformElementOutput {
    event: IRopidVYMIEventOutput;
    routes: IRopidVYMIEventRouteOutput[];
    stops: IRopidVYMIEventStopOutput[];
}

interface ITransformedData extends Omit<ITransformElementOutput, "event"> {
    events: IRopidVYMIEventOutput[];
}

export class RopidVYMIEventsTransformation extends BaseTransformation implements ITransformation {
    private static ROPID_DATE_TIME_FORMAT = "YYYYMMDD HH:mm:ss";
    public name: string;

    constructor() {
        super();
        this.name = RopidVYMI.events.name;
    }

    public override transform = async (data: IRopidVYMIEvent[]): Promise<ITransformedData> => {
        let output: ITransformedData = {
            events: [],
            routes: [],
            stops: [],
        };

        for (const eventData of data) {
            const { event, routes, stops } = this.transformElement(eventData);
            output.events.push(event);
            output.routes.push(...routes);
            output.stops.push(...stops);
        }

        return output;
    };

    protected override transformElement = (event: IRopidVYMIEvent): ITransformElementOutput => {
        return {
            event: {
                vymi_id: parseInt(event["@id"], 10),
                vymi_id_dtb: parseInt(event["@id_dtb"], 10),
                state: parseInt(event["@stav"], 10),
                ropid_created_at: this.formatRopidAuditDateTime(event["@pa_01"]),
                ropid_created_by: event["@pa_02"].trimEnd(),
                ropid_updated_at: this.formatRopidAuditDateTime(event["@pa_03"]),
                ropid_updated_by: event["@pa_04"].trimEnd(),
                record_type: parseInt(event["@typ_zaznamu"], 10),
                event_type: event["@typ_udalost"] ? parseInt(event["@typ_udalost"], 10) : null,
                channels: parseInt(event["@kanaly"], 10),
                title: event["@nazev"],
                time_from: moment.tz(event["@cas_od"], "Europe/Prague").toISOString(),
                time_to_type: parseInt(event["@typ_do"], 10),
                time_to: event["@cas_do"] ? moment.tz(event["@cas_do"], "Europe/Prague").toISOString() : null,
                expiration_date: event["@zobrazit_do"] ? moment.tz(event["@zobrazit_do"], "Europe/Prague").toISOString() : null,
                transportation_type: parseInt(event["@druh_dopr"], 10),
                priority: event["@priorita"] ? parseInt(event["@priorita"], 10) : null,
                cause: event["@duvod"] ?? null,
                link: event["@link"] ?? null,
                ropid_action: event["@opatr_ropid"] ?? null,
                dpp_action: event["@opatr_dpp"] ?? null,
                description: event["@popis"] ?? null,
            },
            routes: this.transformRoutes(parseInt(event["@id"], 10), event["linky-zmena"]),
            stops: this.transformStops(parseInt(event["@id"], 10), event["zastavky-zmena"]),
        };
    };

    private transformRoutes = (eventId: number, routes: IRopidVYMIEventRoute[] = []): IRopidVYMIEventRouteOutput[] => {
        return routes.map((route) => ({
            event_id: eventId,
            gtfs_route_id: null, // Filled by gtfs_trigger before inserting data
            vymi_id: parseInt(route["@id"], 10),
            vymi_id_dtb: parseInt(route["@id_dtb"], 10),
            number: parseInt(route["@cislo"], 10),
            name: route["@nazev"],
            route_type: parseInt(route["@linka_typ"], 10),
            valid_from: route["@plati_od"] ?? null,
            valid_to: route["@plati_do"] ?? null,
            text: HTMLUtils.outputPlainText(route["#text"]),
        }));
    };

    private transformStops = (eventId: number, stops: IRopidVYMIEventStop[] = []): IRopidVYMIEventStopOutput[] => {
        return stops.map((stop) => ({
            event_id: eventId,
            gtfs_stop_id: null, // Filled by gtfs_trigger before inserting data
            vymi_id: parseInt(stop["@id"], 10),
            vymi_id_dtb: parseInt(stop["@id_dtb"], 10),
            node_number: parseInt(stop["@uzelCislo"], 10),
            stop_number: parseInt(stop["@zastCislo"], 10),
            stop_type: parseInt(stop["@zast_typ"], 10),
            valid_from: stop["@plati_od"] ?? null,
            valid_to: stop["@plati_do"] ?? null,
            text: HTMLUtils.outputPlainText(stop["#text"]),
        }));
    };

    private formatRopidAuditDateTime(dateStr: string) {
        return moment.tz(dateStr, RopidVYMIEventsTransformation.ROPID_DATE_TIME_FORMAT, "Europe/Prague").toISOString();
    }
}
