import { DatasetEnum, RopidVYMIMetadataModel } from "#ie/ropid-vymi/models";
import { AbstractEmptyTask, QueueManager } from "@golemio/core/dist/integration-engine";
import RopidVYMIApiHelper from "./helpers/RopidVYMIApiHelper";

export class CheckForNewEventsTask extends AbstractEmptyTask {
    public readonly queueName = "checkForNewEvents";
    public readonly queueTtl = 1 * 60 * 1000; // 1 minute;
    private readonly modelVYMIMeta: RopidVYMIMetadataModel;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.modelVYMIMeta = new RopidVYMIMetadataModel();
    }

    protected async execute() {
        const lastDigest = await this.modelVYMIMeta.getDigest(DatasetEnum.EVENTS);
        const { digest, data } = await RopidVYMIApiHelper.getInstance().getAllEvents();

        if (digest !== lastDigest) {
            await QueueManager.sendMessageToExchange(this.queuePrefix, "fetchAndProcessEvents", { digest, data });
        }
    }
}
