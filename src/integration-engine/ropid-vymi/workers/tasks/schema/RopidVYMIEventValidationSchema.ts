import { IRopidVYMIEvent, IRopidVYMIEventRoute, IRopidVYMIEventStop } from "#sch/ropid-vymi";
import { Type } from "@golemio/core/dist/shared/class-transformer";
import { IsObject, IsOptional, IsString, ValidateNested } from "@golemio/core/dist/shared/class-validator";

export class RopidVYMIEventValidationSchema implements IRopidVYMIEvent {
    @IsString()
    "@id": string;
    @IsString()
    "@id_dtb": string;
    @IsString()
    "@stav": string;
    @IsString()
    "@pa_01": string;
    @IsString()
    "@pa_02": string;
    @IsString()
    "@pa_03": string;
    @IsString()
    "@pa_04": string;
    @IsString()
    "@typ_zaznamu": string;
    @IsString()
    "@typ_udalost"?: string;
    @IsString()
    "@kanaly": string;
    @IsString()
    "@nazev": string;
    @IsString()
    "@cas_od": string;
    @IsString()
    "@typ_do": string;
    @IsOptional()
    @IsString()
    "@cas_do"?: string;
    @IsOptional()
    @IsString()
    "@zobrazit_do"?: string;
    @IsString()
    "@druh_dopr": string;
    @IsOptional()
    @IsString()
    "@priorita"?: string;
    @IsOptional()
    @IsString()
    "@duvod"?: string;
    @IsOptional()
    @IsString()
    "@link"?: string;
    @IsOptional()
    @IsString()
    "@opatr_ropid"?: string;
    @IsOptional()
    @IsString()
    "@opatr_dpp"?: string;
    @IsOptional()
    @IsString()
    "@popis"?: string;
    @IsOptional()
    @IsObject({ each: true })
    @ValidateNested()
    @Type(() => RopidVYMIEventRouteValidationSchema)
    "linky-zmena"?: IRopidVYMIEventRoute[];
    @IsOptional()
    @IsObject({ each: true })
    @ValidateNested()
    @Type(() => RopidVYMIEventStopValidationSchema)
    "zastavky-zmena"?: IRopidVYMIEventStop[];
}

export class RopidVYMIEventRouteValidationSchema implements IRopidVYMIEventRoute {
    @IsString()
    "@id": string;
    @IsString()
    "@id_dtb": string;
    @IsString()
    "@cislo": string;
    @IsString()
    "@nazev": string;
    @IsString()
    "@linka_typ": string;
    @IsOptional()
    @IsString()
    "@plati_od"?: string;
    @IsOptional()
    @IsString()
    "@plati_do"?: string;
    @IsOptional()
    @IsString()
    "#text"?: string;
}

export class RopidVYMIEventStopValidationSchema implements IRopidVYMIEventStop {
    @IsString()
    "@id": string;
    @IsString()
    "@id_dtb": string;
    @IsString()
    "@uzelCislo": string;
    @IsString()
    "@zastCislo": string;
    @IsString()
    "@zast_typ": string;
    @IsOptional()
    @IsString()
    "@plati_od"?: string;
    @IsOptional()
    @IsString()
    "@plati_do"?: string;
    @IsOptional()
    @IsString()
    "#text"?: string;
}
