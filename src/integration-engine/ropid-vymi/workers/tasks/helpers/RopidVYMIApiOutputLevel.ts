export enum RopidVYMIApiOutputLevel {
    Basic = 0,
    Standard = 1,
    Complete = 2,
}
