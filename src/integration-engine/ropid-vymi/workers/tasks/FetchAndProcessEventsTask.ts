import {
    DatasetEnum,
    RopidVYMIEventsModel,
    RopidVYMIEventsRoutesModel,
    RopidVYMIEventsStopsModel,
    RopidVYMIMetadataModel,
} from "#ie/ropid-vymi/models";
import { RopidVYMIEventsTransformation } from "#ie/ropid-vymi/RopidVYMIEventsTransformation";
import { MetaDatasetInfoKeyEnum, MetaStateEnum, MetaTypeEnum } from "#ie/shared";
import { AbstractTask, PostgresModel } from "@golemio/core/dist/integration-engine";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { IEventFetchOutput } from "./interfaces/IEventFetchOutput";
import { EventFetchOutputValidationSchema } from "./schema/EventFetchOutputValidationSchema";

export class FetchAndProcessEventsTask extends AbstractTask<IEventFetchOutput> {
    public readonly queueName = "fetchAndProcessEvents";
    public readonly queueTtl = 1 * 60 * 1000; // 1 minute;
    protected readonly schema = EventFetchOutputValidationSchema;
    private readonly modelVYMIEvents: RopidVYMIEventsModel;
    private readonly modelVYMIEventsRoutes: RopidVYMIEventsRoutesModel;
    private readonly modelVYMIEventsStops: RopidVYMIEventsStopsModel;
    private readonly transformation: RopidVYMIEventsTransformation;
    private readonly modelVYMIMeta: RopidVYMIMetadataModel;

    constructor(queueprefix: string) {
        super(queueprefix);
        this.modelVYMIMeta = new RopidVYMIMetadataModel();
        this.transformation = new RopidVYMIEventsTransformation();
        this.modelVYMIEvents = new RopidVYMIEventsModel(true);
        this.modelVYMIEventsRoutes = new RopidVYMIEventsRoutesModel(true);
        this.modelVYMIEventsStops = new RopidVYMIEventsStopsModel(true);
    }

    protected execute = async (content: IEventFetchOutput): Promise<void> => {
        const dbLastModified = await this.modelVYMIMeta.getLastModified(DatasetEnum.EVENTS);
        const version = dbLastModified.version + 1;

        await this.modelVYMIMeta.save({
            dataset: DatasetEnum.EVENTS,
            key: MetaDatasetInfoKeyEnum.LAST_MODIFIED,
            type: MetaTypeEnum.DATASET_INFO,
            value: new Date().toISOString(),
            version,
        });

        try {
            const { events, routes, stops } = await this.prepareAndTransformEvents(content.data, version);
            await Promise.all([
                this.prepareAndSaveEvents("events", this.modelVYMIEvents, events, version),
                this.prepareAndSaveEvents("eventsRoutes", this.modelVYMIEventsRoutes, routes, version),
                this.prepareAndSaveEvents("eventsStops", this.modelVYMIEventsStops, stops, version),
            ]);

            await this.modelVYMIMeta.checkSavedRows(DatasetEnum.EVENTS, version);
            await this.modelVYMIMeta.replaceTmpTables(DatasetEnum.EVENTS, version);

            // Save current digest
            await this.modelVYMIMeta.save({
                dataset: DatasetEnum.EVENTS,
                key: MetaDatasetInfoKeyEnum.DIGEST,
                type: MetaTypeEnum.DATASET_INFO,
                value: content.digest,
                version,
            });
        } catch (err) {
            log.error(err);
            await this.modelVYMIMeta.rollbackFailedSaving(DatasetEnum.EVENTS, version);
        }
    };

    private prepareAndTransformEvents = async (data: any[], version: number) => {
        const { events, routes, stops } = await this.transformation.transform(data);
        const baseMetaInputData = {
            dataset: DatasetEnum.EVENTS,
            type: MetaTypeEnum.TABLE_TOTAL_COUNT,
            version,
        };

        await Promise.all([
            this.modelVYMIMeta.save({
                key: "events",
                value: events.length,
                ...baseMetaInputData,
            }),
            this.modelVYMIMeta.save({
                key: "eventsRoutes",
                value: routes.length,
                ...baseMetaInputData,
            }),
            this.modelVYMIMeta.save({
                key: "eventsStops",
                value: stops.length,
                ...baseMetaInputData,
            }),
        ]);

        return {
            events,
            routes,
            stops,
        };
    };

    private prepareAndSaveEvents = async (key: string, model: PostgresModel, transformedData: any[], version: number) => {
        await model.truncate(true);
        await model.save(transformedData, true);

        await this.modelVYMIMeta.save({
            dataset: DatasetEnum.EVENTS,
            key,
            type: MetaTypeEnum.STATE,
            value: MetaStateEnum.SAVED,
            version,
        });
    };
}
