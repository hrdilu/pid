export enum StatePositionEnum {
    OFF_TRACK = "off_track",
    AT_STOP = "at_stop",
    ON_TRACK = "on_track",
    BEFORE_TRACK = "before_track",
    AFTER_TRACK = "after_track",
    CANCELED = "canceled",
    INVISIBLE = "invisible",
    UNKNOWN = "unknown",
    BEFORE_TRACK_DELAYED = "before_track_delayed",
    DUPLICATE = "duplicate",
    MISMATCHED = "mismatched",
    NOT_PUBLIC = "not_public",
}

export enum StateProcessEnum {
    TCP_INPUT = "tcp_input",
    UDP_INPUT = "udp_input",
    INPUT = "input",
    PROCESSED = "processed",
}

export enum TCPEventEnum {
    /** Tram only arrival */
    ARRIVAL_ANNOUNCED = "P",
    DEPARTURED = "O",
    TERMINATED = "V",
    TIME = "T",
}

export enum PositionTrackingEnum {
    NOT_TRACKING = 0,
    TRACKING = 2,
}

export const ArrayNotPublicRegistrationNumbers: string[] = ["2210", "5572"];

export const DATA_RETENTION_IN_MINUTES = 30;
