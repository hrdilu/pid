/**
 *
 * Router /WEB LAYER/: maps routes to specific controller functions, passes request parameters and handles responses.
 * Handles web logic (http request, response). Sets response headers, handles error responses.
 */

import { RopidRouterUtils } from "#og/shared";
import { ValidationArrays } from "#og/shared/constants";
import { createChildSpan } from "@golemio/core/dist/monitoring/opentelemetry/trace-provider";
import { config } from "@golemio/core/dist/output-gateway/config";
import { RedisConnector, useCacheMiddleware } from "@golemio/core/dist/output-gateway/redis";
import { parseBooleanQueryParam } from "@golemio/core/dist/output-gateway/Utils";
import { checkErrors, pagination, paginationLimitMiddleware } from "@golemio/core/dist/output-gateway/Validation";
import { NextFunction, Request, RequestHandler, Response, Router } from "@golemio/core/dist/shared/express";
import { param, query } from "@golemio/core/dist/shared/express-validator";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import moment from "@golemio/core/dist/shared/moment-timezone";
import { promisify } from "util";
import { repositories } from "./data-access";
import { ITripWithPositionRepository } from "./data-access/interfaces/ITripWithPositionRepository";

export class VehiclePositionsRouter {
    public router: Router;
    protected tripWithPositionRepository: ITripWithPositionRepository;

    constructor() {
        this.router = Router();
        this.tripWithPositionRepository = repositories.tripWithLastPositionRepository;
        this.initRoutes("5 seconds");
    }

    public GetAll = async (req: Request, res: Response, next: NextFunction) => {
        const preferredTimezone = RopidRouterUtils.getPreferredTimezone(req.query.preferredTimezone);
        const span = createChildSpan("VehiclePositionsRouter.GetAll");
        try {
            const result = await this.tripWithPositionRepository.GetAll({
                cisTripNumber: Number(req.query.cisTripNumber) || undefined,
                includeNotTracking: parseBooleanQueryParam(req.query.includeNotTracking as string),
                includePositions: parseBooleanQueryParam(req.query.includePositions as string),
                includeNotPublic: parseBooleanQueryParam(req.query.includeNotPublic as string),
                limit: Number(req.query.limit) || undefined,
                offset: Number(req.query.offset) || undefined,
                routeId: req.query.routeId as string,
                routeShortName: req.query.routeShortName as string,
                updatedSince: req.query.updatedSince ? new Date(req.query.updatedSince as string) : null,
                preferredTimezone,
            });

            res.set("X-Last-Modified", moment(result.metadata.maxUpdatedAt).toISOString()).status(200).send(result.data);
        } catch (err) {
            next(err);
        } finally {
            span?.end();
        }
    };

    public GetOne = async (req: Request, res: Response, next: NextFunction) => {
        const id: string = req.params.id;
        const preferredTimezone = RopidRouterUtils.getPreferredTimezone(req.query.preferredTimezone);

        try {
            const data = await this.tripWithPositionRepository.GetOneByGTFSTripId(id, {
                includeNotTracking: parseBooleanQueryParam(req.query.includeNotTracking as string),
                includePositions: parseBooleanQueryParam(req.query.includePositions as string),
                preferredTimezone,
            });
            if (!data) {
                throw new GeneralError("not_found", "VehiclePositionsRouter", undefined, 404);
            }
            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };

    private GetGtfsRtFeed = (fileName: string, sendAsJson = false): RequestHandler => {
        return async (_req, res, next) => {
            try {
                const redisClient = RedisConnector.getConnection();
                const redisHGetAsync = promisify(redisClient.hget).bind(redisClient);
                const file = await redisHGetAsync("files:gtfsRt", fileName);
                if (!file) {
                    throw new GeneralError("not_found", "VehiclePositionsRouter", undefined, 404);
                }
                res.setHeader("Content-Type", sendAsJson ? "application/json" : "application/octet-stream");
                res.status(200).send(sendAsJson ? file : Buffer.from(file, "binary"));
            } catch (err) {
                next(err);
            }
        };
    };

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     * @param {number|string} expire TTL for the caching middleware
     */
    private initRoutes = (expire?: number | string): void => {
        this.router.get(
            "/",
            [
                query("cisTripNumber").optional().isNumeric(),
                query("routeId").optional(),
                query("routeShortName").optional(),
                query("includeNotTracking").optional().isBoolean(),
                query("includePositions").optional().isBoolean(),
                query("includeNotPublic").optional().isBoolean(),
                query("updatedSince").optional().isISO8601(),
                query("preferredTimezone").optional().isString().isIn(ValidationArrays.preferredTimezone),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware("VehiclePositionsRouter"),
            useCacheMiddleware(expire),
            this.GetAll
        );
        this.router.get(
            "/:id",
            [
                param("id").exists(),
                query("includeNotTracking").optional().isBoolean(),
                query("includePositions").optional().isBoolean(),
                query("includeNotPublic").optional().isBoolean(),
                query("preferredTimezone").optional().isString(),
            ],
            checkErrors,
            useCacheMiddleware(expire),
            this.GetOne
        );
        this.router.get("/gtfsrt/trip_updates.pb", this.GetGtfsRtFeed("trip_updates.pb"));
        this.router.get("/gtfsrt/trip_updates.json", this.GetGtfsRtFeed("trip_updates.json", true));
        this.router.get("/gtfsrt/vehicle_positions.pb", this.GetGtfsRtFeed("vehicle_positions.pb"));
        this.router.get("/gtfsrt/vehicle_positions.json", this.GetGtfsRtFeed("vehicle_positions.json", true));
        this.router.get("/gtfsrt/pid_feed.pb", this.GetGtfsRtFeed("pid_feed.pb"));
        this.router.get("/gtfsrt/alerts.pb", this.GetGtfsRtFeed("alerts.pb"));
        this.router.get("/gtfsrt/alerts.json", this.GetGtfsRtFeed("alerts.json", true));
    };
}

const vehiclepositionsRouter: Router = new VehiclePositionsRouter().router;
export { vehiclepositionsRouter };
