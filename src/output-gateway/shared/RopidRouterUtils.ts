import { config } from "@golemio/core/dist/output-gateway/config";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import moment from "@golemio/core/dist/shared/moment-timezone";
import { ClassConstructor, plainToInstance } from "@golemio/core/dist/shared/class-transformer";
import { ParsedQs } from "qs";

export class RopidRouterUtils {
    /**
     * Convert plain object to DTO class instance
     */
    public static mapObjectToDTOInstance = <T>(dto: ClassConstructor<T>, data: any) => {
        return plainToInstance(dto, data, { exposeDefaultValues: true });
    };

    /**
     * Format to ISO 8601 (without milliseconds)
     */
    public static formatTimestamp = (dateObject: Date | null, preferredTimezone: string): string | null => {
        if (!dateObject) return null;

        try {
            const datetime = moment(dateObject).tz(preferredTimezone).format();

            if (datetime === "Invalid date") {
                log.error("RopidRouterUtils.formatTimestamp Date conversion error");
                return null;
            }

            return datetime;
        } catch (err) {
            log.error("RopidRouterUtils.formatTimestamp Date conversion error", err);
            return null;
        }
    };

    /**
     * Validate and return timezone (it is possible to use _ symbol instead of URL encoded / symbol)
     */
    public static getPreferredTimezone = (preferredTimezone: ParsedQs[string]): string => {
        const timezone =
            typeof preferredTimezone === "string"
                ? preferredTimezone.replace(/_/s, "/")
                : config.vehiclePositions.defaultTimezone;

        return timezone;
    };
}
