export enum FeatureFlagEnum {
    PIDShowMetroOnMap = "pid-show-metro-on-map",
    PIDEnableAirConditioning = "pid-enable-air-conditioning",
}
