import { TransformArray, TransformInteger, TransformBoolean } from "#og/shared/decorators";
import { IGTFSStopGetAllOutput } from "#og/ropid-gtfs/models/GTFSStopModelInterfaces";
import { IInfotextsStopsWithEventOutputModel } from "#og/pid/models/RopidVYMIEventsStopsModel";
import { IPIDDepartureOutput, DepartureMode, DepartureFilter, DepartureOrder, DepartureSkip } from "#og/pid";
import { IDepartureBoardsQueryDTO } from "./interfaces/IDepartureBoardsQueryDTO";

export class DepartureBoardsQueryDTO implements IDepartureBoardsQueryDTO {
    @TransformArray()
    aswIds: string[] = [];

    @TransformArray()
    cisIds: string[] = [];

    @TransformArray()
    ids: string[] = [];

    @TransformArray()
    names: string[] = [];

    @TransformArray()
    skip: DepartureSkip[] = [];

    @TransformInteger()
    limit: number = 20;

    @TransformInteger()
    minutesAfter: number = 180;

    @TransformInteger()
    minutesBefore: number = 0;

    @TransformBoolean()
    includeMetroTrains: boolean = false;

    @TransformBoolean()
    airCondition: boolean = true;

    mode: DepartureMode = DepartureMode.DEPARTURES;

    order: DepartureOrder = DepartureOrder.REAL;

    filter: DepartureFilter = DepartureFilter.NONE;

    @TransformInteger()
    offset: number = 0;

    @TransformInteger()
    total?: number;

    preferredTimezone?: string;

    timeFrom?: string;
}

export class DepartureBoardsResponseDTO {
    stops: IGTFSStopGetAllOutput[] = [];
    departures: IPIDDepartureOutput[] = [];
    infotexts: IInfotextsStopsWithEventOutputModel[] = [];
}
