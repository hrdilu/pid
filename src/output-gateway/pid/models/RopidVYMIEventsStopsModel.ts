import { RopidVYMIEventsModel, RopidVYMIEventsRoutesModel } from "#og/pid/models";
import { GTFSStopModel } from "#og/ropid-gtfs/models/GTFSStopModel";
import { RopidRouterUtils } from "#og/shared";
import { PG_SCHEMA } from "#sch/const";
import { RopidGTFS } from "#sch/ropid-gtfs";
import { RopidVYMI } from "#sch/ropid-vymi";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import moment, { Moment } from "@golemio/core/dist/shared/moment-timezone";
import Sequelize from "@golemio/core/dist/shared/sequelize";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { ContainerToken, OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc/";

enum InfotextsRecordTypeEnum {
    EXCEPTION = 1,
    PLANNED_RESTRICTION = 2,
    PERMANENT_CHANGE = 3,
}

export enum InfotextsDisplayTypeEnum {
    INLINE = "inline",
    GENERAL = "general",
}

interface IInfotextsStopsWithEventModel {
    valid_from: Date | null;
    valid_to: Date | null;
    text: string;
    stop_type: number;
    related_stops: string[];
}

interface IInfotextsStopsWithRoutesModel extends IInfotextsStopsWithEventModel {
    vymi_id: number;
    vymi_id_dtb: number;
    expiration_date: Date | null;
    last_updated: Date;
    last_updated_user: string;
    related_routes: string[];
}

export interface IInfotextsStopsWithEventOutputModel
    extends Omit<IInfotextsStopsWithEventModel, "stop_type" | "valid_from" | "valid_to"> {
    display_type: InfotextsDisplayTypeEnum;
    text_en: null;
    valid_from: string | null;
    valid_to: string | null;
}

interface IInfotextsStopsWithRoutesOutputModel
    extends Omit<IInfotextsStopsWithRoutesModel, "stop_type" | "expiration_date" | "last_updated" | "valid_from" | "valid_to"> {
    display_type: InfotextsDisplayTypeEnum;
    text_en: null;
    expiration_date: string | null;
    last_updated: string;
    valid_from: string | null;
    valid_to: string | null;
}

/**
 * Custom Postgres model for Ropid VYMI Events
 */
export class RopidVYMIEventsStopsModel extends SequelizeModel {
    private dbConnector: IDatabaseConnector;
    private readonly eventsModel: RopidVYMIEventsModel;
    private readonly eventsRoutesModel: RopidVYMIEventsRoutesModel;
    private readonly gtfsStopsModel: GTFSStopModel;

    GetAll(options?: any): Promise<any> {
        throw new Error("Method not implemented.");
    }
    GetOne(id: any): Promise<any> {
        throw new Error("Method not implemented.");
    }

    constructor() {
        super(
            RopidVYMI.eventsStops.name + "Model",
            RopidVYMI.eventsStops.pgTableName,
            RopidVYMI.eventsStops.outputSequelizeAttributes,
            { schema: PG_SCHEMA }
        );

        this.dbConnector = OutputGatewayContainer.resolve<IDatabaseConnector>(ContainerToken.PostgresDatabase);

        this.eventsModel = new RopidVYMIEventsModel();
        this.eventsRoutesModel = new RopidVYMIEventsRoutesModel();
        this.gtfsStopsModel = new GTFSStopModel();

        this.sequelizeModel.belongsTo(this.eventsModel.sequelizeModel, {
            as: "vymi_event",
            targetKey: "vymi_id",
            foreignKey: "event_id",
        });

        this.sequelizeModel.hasMany(this.eventsRoutesModel.sequelizeModel, {
            as: "vymi_route",
            foreignKey: "event_id",
            sourceKey: "event_id",
        });

        this.sequelizeModel.hasOne(this.gtfsStopsModel.sequelizeModel, {
            as: "gtfs_stop",
            foreignKey: "stop_id",
            sourceKey: "gtfs_stop_id",
        });
    }

    /** Retrieves all infotexts with event, could be inner joined with set of stops
     *
     * @param {string[]} stopsIds - Array of GTFS stop ids to be inner joined with corresponding infotexts
     * @param {Moment} timeFrom - Timestamp to filter out specified expiration_date column greater than this value
     */
    public GetAllDistinctWithEvents = async (
        stopsIds: string[],
        timeFrom: Moment = moment(),
        timezone: string
    ): Promise<IInfotextsStopsWithEventOutputModel[]> => {
        if (stopsIds.length < 1) {
            return [];
        }

        const data = await this.sequelizeModel.findAll({
            attributes: [
                "valid_from",
                "valid_to",
                "text",
                "stop_type",
                [Sequelize.fn("ARRAY_AGG", Sequelize.col("gtfs_stop_id")), "related_stops"],
            ],
            include: {
                attributes: [],
                as: "vymi_event",
                model: this.dbConnector.getConnection().models[RopidVYMI.events.pgTableName],
                where: {
                    time_from: {
                        [Sequelize.Op.lte]: new Date().toISOString(),
                    },
                    expiration_date: {
                        [Sequelize.Op.or]: [null, { [Sequelize.Op.gte]: timeFrom.toISOString() }],
                    },
                },
            },
            where: {
                gtfs_stop_id: {
                    [Sequelize.Op.in]: stopsIds,
                },
                text: {
                    [Sequelize.Op.and]: [{ [Sequelize.Op.ne]: null }, { [Sequelize.Op.ne]: "" }],
                },
            },
            group: ["valid_from", "valid_to", "text", "stop_type", "vymi_event.vymi_id"],
            raw: true,
        });

        return this.mapDataWithEvent(data, timezone);
    };

    /**
     *  Retrieve all infotexts with routes
     */
    public GetAllWithRoutes = async (timezone: string): Promise<IInfotextsStopsWithRoutesOutputModel[]> => {
        const currentDateISO = new Date().toISOString();
        const data = await this.sequelizeModel.findAll({
            attributes: [
                "valid_from",
                "valid_to",
                "text",
                "stop_type",
                [Sequelize.literal("vymi_event.vymi_id"), "vymi_id"],
                [Sequelize.literal("vymi_event.vymi_id_dtb"), "vymi_id_dtb"],
                [Sequelize.literal("vymi_event.expiration_date"), "expiration_date"],
                [Sequelize.literal("vymi_event.ropid_updated_at"), "last_updated"],
                [Sequelize.literal("vymi_event.ropid_updated_by"), "last_updated_user"],
                [
                    Sequelize.literal(
                        // eslint-disable-next-line max-len
                        "ARRAY_AGG(DISTINCT jsonb_build_object('id', gtfs_stop_id, 'name', gtfs_stop.stop_name, 'platform_code', gtfs_stop.platform_code))"
                    ),
                    "related_stops",
                ],
                [Sequelize.literal("ARRAY_REMOVE(ARRAY_AGG(DISTINCT vymi_route.gtfs_route_id), NULL)"), "related_routes"],
            ],
            include: [
                {
                    attributes: [],
                    as: "vymi_event",
                    model: this.dbConnector.getConnection().models[RopidVYMI.events.pgTableName],
                    where: {
                        time_from: {
                            [Sequelize.Op.lte]: currentDateISO,
                        },
                        expiration_date: {
                            [Sequelize.Op.or]: [null, { [Sequelize.Op.gte]: currentDateISO }],
                        },
                    },
                },
                {
                    attributes: [],
                    as: "vymi_route",
                    model: this.dbConnector.getConnection().models[RopidVYMI.eventsRoutes.pgTableName],
                },
                {
                    attributes: [],
                    as: "gtfs_stop",
                    model: this.dbConnector.getConnection().models[RopidGTFS.stops.pgTableName],
                },
            ],
            where: {
                text: {
                    [Sequelize.Op.and]: [{ [Sequelize.Op.ne]: null }, { [Sequelize.Op.ne]: "" }],
                },
            },
            group: [
                "ropidvymi_events_stops.valid_from",
                "ropidvymi_events_stops.valid_to",
                "ropidvymi_events_stops.text",
                "stop_type",
                "vymi_event.vymi_id",
            ],
            raw: true,
        });

        return this.mapDataWithRoutes(data, timezone);
    };

    private mapDataWithEvent = (
        items: IInfotextsStopsWithEventModel[],
        timezone: string
    ): IInfotextsStopsWithEventOutputModel[] => {
        return items.map((item) => {
            const { stop_type: stopType, valid_from, valid_to, ...infotextsStopModel } = item;
            return {
                display_type: this.mapStopTypeToDisplayType(stopType),
                text_en: null,
                ...infotextsStopModel,
                valid_from: RopidRouterUtils.formatTimestamp(valid_from, timezone),
                valid_to: RopidRouterUtils.formatTimestamp(valid_to, timezone),
            };
        });
    };

    private mapDataWithRoutes = (
        items: IInfotextsStopsWithRoutesModel[],
        timezone: string
    ): IInfotextsStopsWithRoutesOutputModel[] => {
        return items.map((item) => {
            const {
                vymi_id,
                vymi_id_dtb,
                stop_type: stopType,
                expiration_date,
                last_updated,
                valid_from,
                valid_to,
                last_updated_user,
                ...infotextsStopModel
            } = item;
            return {
                vymi_id: vymi_id,
                vymi_id_dtb: vymi_id_dtb,
                display_type: this.mapStopTypeToDisplayType(stopType),
                text_en: null,
                ...infotextsStopModel,
                expiration_date: RopidRouterUtils.formatTimestamp(expiration_date, timezone),
                last_updated: RopidRouterUtils.formatTimestamp(last_updated, timezone)!,
                last_updated_user: last_updated_user,
                valid_from: RopidRouterUtils.formatTimestamp(valid_from, timezone),
                valid_to: RopidRouterUtils.formatTimestamp(valid_to, timezone),
            };
        });
    };

    private mapStopTypeToDisplayType = (stopType: number): InfotextsDisplayTypeEnum => {
        // stop type 1 and 9 leads to general infotext type
        return stopType === 1 || stopType === 9 ? InfotextsDisplayTypeEnum.GENERAL : InfotextsDisplayTypeEnum.INLINE;
    };
}
