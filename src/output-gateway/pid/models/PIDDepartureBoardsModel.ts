import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import moment, { Moment } from "@golemio/core/dist/shared/moment-timezone";
import { models } from "#og/ropid-gtfs/models";
import { RopidVYMIEventsStopsModel, RopidDeparturesDirectionsModel } from ".";
import { DepartureFilter, DepartureMode, DepartureOrder, DepartureSkip } from "..";
import { IRopidDeparturesDirectionsOutput } from "#sch/ropid-departures-directions";
import { DeparturesRepository } from "#og/pid/data-access";
import { config } from "@golemio/core/dist/output-gateway/config";
import { createChildSpan } from "@golemio/core/dist/monitoring/opentelemetry/trace-provider";
import { SpanAttributes } from "@opentelemetry/api";
import PIDDeparturesModel from "./helpers/PIDDepartureModel";

class PIDDepartureBoardsModel {
    private stopsMaxCount = 100;
    private ropidVYMIEventsStopsModel: RopidVYMIEventsStopsModel;
    private ropidDeparturesDirectionsModel: RopidDeparturesDirectionsModel;
    private departuresRepository: DeparturesRepository;

    constructor() {
        this.ropidVYMIEventsStopsModel = new RopidVYMIEventsStopsModel();
        this.ropidDeparturesDirectionsModel = new RopidDeparturesDirectionsModel();
        this.departuresRepository = new DeparturesRepository();
    }

    /** Retrieves all gtfs stop times for specific stop, optionaly enhanced with realtime delays
     * @param {string} id Id of the stop
     * @returns Object of the retrieved record or null
     */
    public GetAll = async (options: {
        aswIds?: string[];
        cisIds?: string[];
        gtfsIds?: string[];
        names?: string[];
        total: number;
        limit: number;
        offset: number;
        mode: DepartureMode;
        minutesBefore: number;
        minutesAfter: number;
        timeFrom?: Moment;
        order: DepartureOrder;
        filter: DepartureFilter;
        skip: DepartureSkip[];
        includeMetroTrains?: boolean;
        airCondition?: boolean;
        timezone: string;
    }): Promise<any> => {
        const currentMoment = moment();
        const minutesOffset = options.timeFrom ? options.timeFrom.diff(currentMoment, "minutes") : 0;
        // Combination of ?minutesBefore > 0 and ?skip=atStop is not supported, fall back to 0
        const minutesBefore =
            options.skip.includes(DepartureSkip.AT_STOP) && options.minutesBefore > 0 ? 0 : options.minutesBefore;

        const spanStops = createChildSpan("PIDRouter.DB.stops");
        spanStops?.setAttributes({ ...options, timeFrom: options.timeFrom?.toString() } as SpanAttributes);
        const stopsToInclude = await models.GTFSStopModel.GetAll({
            includeMetroTrains: options.includeMetroTrains,
            appendAswId: true,
            aswIds: options.aswIds,
            cisIds: options.cisIds,
            gtfsIds: options.gtfsIds,
            limit: this.stopsMaxCount + 1,
            locationType: 0,
            names: options.names,
            returnRaw: true,
        });

        if (stopsToInclude.length === 0) {
            throw new GeneralError("No stops found.", "DepartureBoardsRouter", undefined, 404);
        }
        if (stopsToInclude.length > this.stopsMaxCount) {
            throw new GeneralError(
                `Too many stops, try lower number or split requests. The maximum is ${this.stopsMaxCount} stops.`,
                "DepartureBoardsRouter",
                undefined,
                413
            );
        }

        const stopsIds = stopsToInclude.map((e: any) => e.stop_id);
        spanStops?.end();

        const spanVymi = createChildSpan("PIDRouter.DB.vymi");
        let infotextsToInclude;
        try {
            infotextsToInclude = await this.ropidVYMIEventsStopsModel.GetAllDistinctWithEvents(
                stopsIds,
                options.timeFrom ?? currentMoment,
                options.timezone
            );
        } catch (err) {
            throw new GeneralError("Database error", "DepartureBoardsModel", err, 500);
        }
        spanVymi?.end();

        const spanDirections = createChildSpan("PIDRouter.DB.directions");
        let departuresDirections: IRopidDeparturesDirectionsOutput[];
        try {
            departuresDirections = await this.ropidDeparturesDirectionsModel.GetAll(stopsIds);
        } catch (err) {
            throw new GeneralError("Database error", "DepartureBoardsModel", err, 500);
        }
        spanDirections?.end();

        try {
            const results = await this.departuresRepository.GetAll({
                currentMoment,
                minutesAfter: options.minutesAfter,
                minutesBefore,
                minutesOffset,
                stopsIds,
                mode: options.mode,
                isAirCondition: !!options.airCondition,
            });
            const spanDepartures = createChildSpan("PIDRouter.DB.departures");
            const res = {
                departures: new PIDDeparturesModel(results, {
                    limit: options.limit,
                    offset: options.offset,
                    total: options.total,
                    mode: options.mode,
                    order: options.order,
                    filter: options.filter,
                    skip: options.skip,
                    departuresDirections,
                    timezone: options.timezone,
                }).toArray(),
                infotexts: infotextsToInclude,
                stops: stopsToInclude,
            };
            spanDepartures?.end();
            return res;
        } catch (err) {
            throw new GeneralError("Database error", "DepartureBoardsModel", err, 500);
        }
    };
}

export { PIDDepartureBoardsModel };
