import { PG_SCHEMA } from "#sch/const";
import { RopidVYMI } from "#sch/ropid-vymi";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";

/**
 * Custom Postgres model for Ropid VYMI Events
 */
export class RopidVYMIEventsModel extends SequelizeModel {
    GetAll(options?: any): Promise<any> {
        throw new Error("Method not implemented.");
    }
    GetOne(id: any): Promise<any> {
        throw new Error("Method not implemented.");
    }

    constructor() {
        super(RopidVYMI.events.name + "Model", RopidVYMI.events.pgTableName, RopidVYMI.events.outputSequelizeAttributes, {
            schema: PG_SCHEMA,
        });
    }
}
