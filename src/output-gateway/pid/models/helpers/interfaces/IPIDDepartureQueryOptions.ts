import { DepartureFilter, DepartureMode, DepartureOrder, DepartureSkip } from "#og/pid";
import { IRopidDeparturesDirectionsOutput } from "#sch/ropid-departures-directions";

export default interface IPIDDepartureQueryOptions {
    limit: number;
    offset: number;
    total: number;
    mode: DepartureMode;
    order: DepartureOrder;
    filter: DepartureFilter;
    skip: DepartureSkip[];
    departuresDirections: IRopidDeparturesDirectionsOutput[];
    timezone: string;
}
