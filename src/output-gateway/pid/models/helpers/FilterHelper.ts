import { DepartureFilter, IPIDDeparture } from "#og/pid";

/** SplitDepartures type
 * Departures in once array are the ones we want to show on Departure board.
 * Fill array is only used for some filters allowing to fill final result with
 * departures with secondary importance/ that were filter out by filter condition
 *
 */
export type SplitDepartures = { once: IPIDDeparture[]; fill: IPIDDeparture[] };
export type AccumulatorCondition = (accumulator: SplitDepartures, currentDeparture: IPIDDeparture) => boolean;

export class FilterHelper {
    public static routeOnceHeadingCondition: AccumulatorCondition = (
        accumulator: SplitDepartures,
        currentDeparture: IPIDDeparture
    ) => {
        // only the first departure to respone for trips with same route_id and heading
        return !this.isInOnceBucketWithHeading(accumulator, currentDeparture);
    };

    public static routeOnceHeadingNoGapCondition: AccumulatorCondition = (
        accumulator: SplitDepartures,
        currentDeparture: IPIDDeparture
    ) => {
        // extension of routeOnceHeading filter, when we skip some route_id afterwards it doesnt show up even different headings
        return (
            !this.isInOnceBucketWithHeadingWithStop(accumulator, currentDeparture) &&
            !this.isInFillBucket(accumulator, currentDeparture)
        );
    };

    public static routeOnceCondition: AccumulatorCondition = (accumulator: SplitDepartures, currentDeparture: IPIDDeparture) => {
        // only the first departure to respone for trips with same route_id
        return !this.isInOnceBucket(accumulator, currentDeparture);
    };

    public static isFillFilterType(filter: DepartureFilter) {
        return (
            filter === DepartureFilter.ROUTE_ONCE_FILL ||
            filter === DepartureFilter.ROUTE_HEADING_ONCE_FILL ||
            filter === DepartureFilter.ROUTE_HEADING_ONCE_NOGAP_FILL
        );
    }

    private static isInFillBucket: AccumulatorCondition = (accumulator: SplitDepartures, currentDeparture: IPIDDeparture) => {
        return (
            accumulator.fill.findIndex(
                (e) => e.route_id === currentDeparture.route_id && e.stop_id === currentDeparture.stop_id
            ) >= 0
        );
    };

    private static isInOnceBucketWithHeading: AccumulatorCondition = (
        accumulator: SplitDepartures,
        currentDeparture: IPIDDeparture
    ) => {
        return (
            accumulator.once.findIndex(
                (e) =>
                    e.route_id === currentDeparture.route_id &&
                    (currentDeparture.stop_headsign
                        ? e.stop_headsign === currentDeparture.stop_headsign
                        : e.trip_headsign === currentDeparture.trip_headsign)
            ) >= 0
        );
    };

    private static isInOnceBucketWithHeadingWithStop: AccumulatorCondition = (
        accumulator: SplitDepartures,
        currentDeparture: IPIDDeparture
    ) => {
        return (
            accumulator.once.findIndex(
                (e) =>
                    e.route_id === currentDeparture.route_id &&
                    (currentDeparture.stop_headsign
                        ? e.stop_headsign === currentDeparture.stop_headsign
                        : e.trip_headsign === currentDeparture.trip_headsign) &&
                    e.stop_id === currentDeparture.stop_id
            ) >= 0
        );
    };

    private static isInOnceBucket: AccumulatorCondition = (accumulator: SplitDepartures, currentDeparture: IPIDDeparture) => {
        return accumulator.once.findIndex((e) => e.route_id === currentDeparture.route_id) >= 0;
    };
}
