import { DepartureFilter, DepartureMode, DepartureOrder, DepartureSkip, IPIDDeparture } from "#og/pid";
import DepartureBoardMapper from "#og/pid/helpers/DepartureBoardMapper";
import { AccumulatorCondition, FilterHelper } from "./FilterHelper";
import IPIDDepartureQueryOptions from "./interfaces/IPIDDepartureQueryOptions";

export default class PIDDeparturesModel {
    private departures: IPIDDeparture[];
    private options: IPIDDepartureQueryOptions;

    private static FilterConditionMap: Record<DepartureFilter, AccumulatorCondition | undefined> = {
        routeOnce: FilterHelper.routeOnceCondition,
        routeOnceFill: FilterHelper.routeOnceCondition,
        routeHeadingOnce: FilterHelper.routeOnceHeadingCondition,
        routeHeadingOnceFill: FilterHelper.routeOnceHeadingCondition,
        routeHeadingOnceNoGap: FilterHelper.routeOnceHeadingNoGapCondition,
        routeHeadingOnceNoGapFill: FilterHelper.routeOnceHeadingNoGapCondition,
        none: undefined,
    };

    constructor(departures: IPIDDeparture[], options: IPIDDepartureQueryOptions) {
        this.departures = departures;
        this.options = options;
    }

    public toArray = () => {
        this.toArrayInternal();

        return DepartureBoardMapper.mapDepartures(this, this.options.timezone, this.options.mode);
    };

    private toArrayInternal = (): IPIDDeparture[] => {
        this.skip();
        this.sort();
        this.filterAndLimit();
        this.sort();
        this.addDirections();

        return this.departures;
    };

    /** Orders departures in special way by given options
     */
    private filterAndLimit = (): void => {
        const finalDepartures = this.filterDepartures();

        if (FilterHelper.isFillFilterType(this.options.filter)) {
            // return all once in limit and if still under limit fill it with others
            this.departures = finalDepartures.once
                .slice(0, this.options.total)
                .concat(
                    finalDepartures.fill.slice(0, this.options.total - Math.min(finalDepartures.once.length, this.options.total))
                )
                .slice(this.options.offset, this.options.offset + this.options.limit);
        } else {
            this.departures = finalDepartures.once
                .slice(0, this.options.total)
                .slice(this.options.offset, this.options.offset + this.options.limit);
        }
    };

    /** Skips departures by given options
     */
    private skip = (): void => {
        this.departures = this.departures.filter((departure) => {
            const isAtStop = departure.stop_sequence === departure["trip.last_position.this_stop_sequence"];
            const isPassStop =
                departure["trip.last_position.last_stop_sequence"] &&
                departure.stop_sequence <= departure["trip.last_position.last_stop_sequence"];

            return !(
                (this.options.skip.includes(DepartureSkip.UNTRACKED) && !departure.is_delay_available) ||
                (this.options.skip.includes(DepartureSkip.CANCELED) && departure.is_canceled) ||
                (this.options.skip.includes(DepartureSkip.AT_STOP) && (isAtStop || isPassStop))
            );
        });
    };

    /** Sorts by proper datetime by given options
     */
    private sort = (): void => {
        const datetimeToOrder =
            this.options.mode === DepartureMode.ARRIVALS
                ? this.options.order === DepartureOrder.REAL
                    ? "arrival_datetime_real"
                    : "arrival_datetime"
                : this.options.order === DepartureOrder.REAL
                ? "departure_datetime_real"
                : "departure_datetime";

        this.departures = this.departures.sort((a: any, b: any) => a[datetimeToOrder].getTime() - b[datetimeToOrder].getTime());
    };

    /** Fills direction with proper value based on departure direction ruls
     */
    private addDirections = (): void => {
        this.departures.forEach((departure) => {
            departure.direction = departure.direction || null;
            const thisStopDeparturesDirections = this.options.departuresDirections.filter(
                (departuresDirection) => departuresDirection.departure_stop_id === departure.stop_id
            );
            if (thisStopDeparturesDirections.length > 0) {
                for (const thisStopDeparturesDirection of thisStopDeparturesDirections) {
                    if (
                        departure.stop_id === thisStopDeparturesDirection.departure_stop_id &&
                        departure.next_stop_id?.match(thisStopDeparturesDirection.next_stop_id_regexp)
                    ) {
                        departure.direction = thisStopDeparturesDirection.direction;
                    }
                }
            }
        });
    };

    private filterDepartures() {
        const filterCondition = PIDDeparturesModel.FilterConditionMap[this.options.filter];

        return filterCondition ? this.filterByCondition(filterCondition) : { once: this.departures, fill: [] as IPIDDeparture[] };
    }

    private filterByCondition(conditionFunction: AccumulatorCondition) {
        return this.departures.reduce(
            (acc, currentDeparture) => {
                if (conditionFunction(acc, currentDeparture)) {
                    acc.once.push(currentDeparture);
                } else {
                    acc.fill.push(currentDeparture);
                }
                return acc;
            },
            { once: [] as IPIDDeparture[], fill: [] as IPIDDeparture[] }
        );
    }
}
