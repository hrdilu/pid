import { FeatureFlagEnum } from "#og/shared/data-access/FeatureFlagEnum";
import { PositionRepository } from "#og/vehicle-positions/data-access/PositionRepository";
import { TripRepository } from "#og/vehicle-positions/data-access/TripRepository";
import { VehicleDescriptorRepository } from "#og/vehicle-positions/data-access/VehicleDescriptorRepository";
import { PG_SCHEMA } from "#sch/const";
import { DeparturesModel } from "#sch/ropid-gtfs/models/precomputed";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { featureFlagService } from "@golemio/core/dist/output-gateway/data-access";
import { Moment } from "@golemio/core/dist/shared/moment-timezone";
import Sequelize, { Includeable } from "@golemio/core/dist/shared/sequelize";
import moment from "moment";
import { StatePositionEnum } from "src/const";
import { DepartureMode, IPIDDeparture } from "..";

export interface IDeparturesViewOptions {
    stopsIds: string[];
    currentMoment: Moment;
    minutesBefore: number;
    minutesAfter: number;
    minutesOffset: number;
    mode: DepartureMode;
    isAirCondition: boolean;
}

export class DeparturesRepository extends SequelizeModel {
    private tripRepository: TripRepository;
    private positionRepository: PositionRepository;
    private vehicleDescriptorRepository: VehicleDescriptorRepository;

    constructor() {
        super("DeparturesRepository", DeparturesModel.TABLE_NAME, DeparturesModel.attributeModel, {
            schema: PG_SCHEMA,
        });
        this.sequelizeModel.removeAttribute("id");

        this.tripRepository = new TripRepository();
        this.positionRepository = new PositionRepository();
        this.vehicleDescriptorRepository = new VehicleDescriptorRepository();

        this.sequelizeModel.hasOne(this.tripRepository.sequelizeModel, {
            foreignKey: "gtfs_trip_id",
            sourceKey: "trip_id",
            scope: {
                [Sequelize.Op.and]: [Sequelize.literal(`trip.gtfs_date = ${DeparturesModel.TABLE_NAME}.date`)],
            },
            as: "trip",
        });

        this.tripRepository.sequelizeModel.hasOne(this.positionRepository.sequelizeModel, {
            foreignKey: "id",
            sourceKey: "last_position_id",
            as: "last_position",
        });

        this.tripRepository.sequelizeModel.hasOne(this.vehicleDescriptorRepository.sequelizeModel, {
            as: "vehicle_descriptor",
            foreignKey: "registration_number",
            sourceKey: "vehicle_registration_number",
            scope: {
                [Sequelize.Op.and]: [Sequelize.literal(`trip.gtfs_route_type = "trip->vehicle_descriptor".gtfs_route_type`)],
            },
        });
    }

    GetOne(id: any): Promise<any> {
        throw new Error("Method not implemented.");
    }

    /** Retrieves all departures
     *
     * @param {IDeparturesViewOptions} options - All query options
     * @returns {Promise<IPIDDeparture[]>} Array of the retrieved records
     */
    async GetAll(options: IDeparturesViewOptions): Promise<IPIDDeparture[]> {
        const isAirConditioningFeatureEnabled =
            (await featureFlagService.isFeatureEnabled(FeatureFlagEnum.PIDEnableAirConditioning, true)) && options.isAirCondition;

        // eslint-disable-next-line max-len
        const delaySecondsLiteral = `CASE WHEN "trip->last_position"."delay" IS NULL THEN 0 ELSE "trip->last_position"."delay" END`;
        const delayMinutesLiteral = `TRUNC("trip->last_position"."delay"::DECIMAL/60, 0)::INT`;
        const waitAtStopIntervalLiteral = `("departure_datetime" - "arrival_datetime")::INTERVAL`;
        const isDelayAvailableLiteral = `CASE WHEN "trip->last_position"."delay" IS NULL THEN FALSE ELSE TRUE END`;

        // Always add delay to the arrival time (even when ahead of time)
        const arrivalDatetimeRealLiteral = `"arrival_datetime" + MAKE_INTERVAL(secs => (${delaySecondsLiteral}))`;

        // Always add delay to the departure time (even when ahead of time)
        // When delayed and waiting at stop, substract the waiting time from the delay
        //   as long as the result delay is greater than zero
        const departureDatetimeRealLiteral = `
            "departure_datetime" +
            MAKE_INTERVAL(secs => (${delaySecondsLiteral})) -
            CASE
                WHEN (MAKE_INTERVAL(secs => (${delaySecondsLiteral})) > MAKE_INTERVAL())
                THEN LEAST(
                    MAKE_INTERVAL(secs => (${delaySecondsLiteral})),
                    ${waitAtStopIntervalLiteral}
                )
                ELSE MAKE_INTERVAL()
            END
        `;

        let conditionMode: any = {
            [Sequelize.Op.and]: [
                {
                    pickup_type: {
                        [Sequelize.Op.ne]: "1",
                    },
                },
                {
                    stop_sequence: {
                        [Sequelize.Op.ne]: Sequelize.col("max_stop_sequence"),
                    },
                },
            ],
        };
        switch (options.mode) {
            case DepartureMode.ARRIVALS:
                conditionMode = {
                    [Sequelize.Op.and]: [
                        {
                            drop_off_type: {
                                [Sequelize.Op.ne]: "1",
                            },
                        },
                        {
                            stop_sequence: {
                                [Sequelize.Op.ne]: Sequelize.col("min_stop_sequence"),
                            },
                        },
                    ],
                };
                break;
            case DepartureMode.MIXED:
                conditionMode = {
                    pickup_type: {
                        [Sequelize.Op.ne]: "1",
                    },
                };
                break;
        }

        const conditionTripNotYetDepartured = {
            [Sequelize.Op.or]: [
                {
                    stop_sequence: {
                        [Sequelize.Op.gte]: Sequelize.literal(`"trip->last_position"."last_stop_sequence"`),
                    },
                },
                Sequelize.literal(`"trip->last_position"."last_stop_sequence" IS NULL`),
            ],
        };
        const conditionDepartureBetween = Sequelize.where(
            Sequelize.literal(
                options.mode === DepartureMode.ARRIVALS ? arrivalDatetimeRealLiteral : departureDatetimeRealLiteral
            ),
            {
                [Sequelize.Op.between]: [
                    options.currentMoment
                        .clone()
                        .add(options.minutesOffset - options.minutesBefore, "minutes")
                        .toDate(),
                    options.currentMoment
                        .clone()
                        .add(options.minutesOffset + options.minutesAfter, "minutes")
                        .toDate(),
                ],
            }
        );
        const conditionCanceledTripDepartureSustain = {
            [Sequelize.Op.and]: [
                Sequelize.literal(`"trip"."is_canceled" = TRUE`),
                Sequelize.where(
                    Sequelize.literal(
                        options.mode === DepartureMode.ARRIVALS ? arrivalDatetimeRealLiteral : departureDatetimeRealLiteral
                    ),
                    {
                        [Sequelize.Op.between]: [
                            moment
                                .min([
                                    options.currentMoment.clone().add(options.minutesOffset - options.minutesBefore, "minutes"),
                                    options.currentMoment.clone().add(options.minutesOffset - 3, "minutes"),
                                ])
                                .toDate(),
                            moment
                                .max(
                                    options.currentMoment.clone().add(options.minutesOffset + options.minutesAfter, "minutes"),
                                    options.currentMoment.clone().add(options.minutesOffset + 3, "minutes")
                                )
                                .toDate(),
                        ],
                    }
                ),
            ],
        };

        const whereCondition = {
            [Sequelize.Op.and]: [
                {
                    [Sequelize.Op.or]: [
                        {
                            [Sequelize.Op.and]: [
                                conditionDepartureBetween,
                                options.minutesBefore <= 0 && conditionTripNotYetDepartured,
                            ],
                        },
                        conditionCanceledTripDepartureSustain,
                    ],
                },
                conditionMode,
            ],
        };

        return this.sequelizeModel.findAll({
            attributes: {
                include: [
                    [Sequelize.literal(`"trip->last_position"."delay"`), "delay_seconds"],
                    [Sequelize.literal(`"trip"."wheelchair_accessible"`), "real_wheelchair_accessible"],
                    [Sequelize.literal(`"trip->last_position"."is_canceled"`), "is_canceled"],
                    [Sequelize.literal(arrivalDatetimeRealLiteral), "arrival_datetime_real"],
                    [Sequelize.literal(departureDatetimeRealLiteral), "departure_datetime_real"],
                    [Sequelize.literal(delayMinutesLiteral), "delay_minutes"],
                    [Sequelize.literal(isDelayAvailableLiteral), "is_delay_available"],
                ],
            },
            where: {
                [Sequelize.Op.and]: [
                    {
                        stop_id: {
                            [Sequelize.Op.in]: options.stopsIds,
                        },
                    },
                    whereCondition,
                ],
            },
            include: {
                model: this.tripRepository.sequelizeModel,
                as: "trip",
                required: false,
                include: this.composeIncludeForTrip(isAirConditioningFeatureEnabled),
            },
            raw: true,
        }) as Promise<IPIDDeparture[]>;
    }

    private composeIncludeForTrip(isAirConditioningFeatureEnabled: boolean): Includeable[] {
        const include: Includeable[] = [
            {
                as: "last_position",
                model: this.positionRepository.sequelizeModel,
                required: true,
                where: {
                    valid_to: {
                        [Sequelize.Op.or]: [null, { [Sequelize.Op.gte]: new Date() }],
                    },
                    state_position: {
                        [Sequelize.Op.in]: [
                            // StatePositionEnum.AFTER_TRACK, // excluding those vehicles after their trip
                            StatePositionEnum.AT_STOP,
                            StatePositionEnum.BEFORE_TRACK,
                            StatePositionEnum.BEFORE_TRACK_DELAYED,
                            StatePositionEnum.CANCELED,
                            // StatePositionEnum.INVISIBLE, // excluding information about trips to be hidden
                            StatePositionEnum.OFF_TRACK,
                            StatePositionEnum.ON_TRACK,
                        ],
                    },
                },
            },
        ];

        if (isAirConditioningFeatureEnabled) {
            include.push({
                as: "vehicle_descriptor",
                model: this.vehicleDescriptorRepository.sequelizeModel,
                attributes: ["is_air_conditioned"],
                required: false,
            });
        }

        return include;
    }
}
