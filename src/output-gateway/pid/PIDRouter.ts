/**
 * Router /WEB LAYER/: maps routes to specific controller functions, passes request parameters and handles responses.
 * Handles web logic (http request, response). Sets response headers, handles error responses.
 */

import { useCacheMiddleware } from "@golemio/core/dist/output-gateway/redis";
import { BaseRouter } from "@golemio/core/dist/output-gateway/routes/BaseRouter";
import { checkErrors, pagination, paginationLimitMiddleware } from "@golemio/core/dist/output-gateway/Validation";
import { Router } from "@golemio/core/dist/shared/express";
import { oneOf, query } from "@golemio/core/dist/shared/express-validator";
import { DATA_RETENTION_IN_MINUTES } from "src/const";
import { ValidationArrays } from "../shared/constants";
import { DepartureBoardsController, InfotextsController } from "./controllers";
import { ParamValidatorManager } from "./helpers/ParamValidatorManager";

export class PIDRouter extends BaseRouter {
    public readonly router = Router();
    private readonly departureBoardsController: DepartureBoardsController;
    private readonly infotextsController: InfotextsController;

    constructor() {
        super();
        this.departureBoardsController = new DepartureBoardsController();
        this.infotextsController = new InfotextsController();

        // Register routes
        this.registerDepartureBoardsRoutes();
        this.registerInfotextsRoutes();
    }

    private registerDepartureBoardsRoutes = () => {
        const {
            preferredTimezone,
            departureBoards: { departureMode, order, filter, skip },
        } = ValidationArrays;
        const validation = [
            oneOf([
                query("aswIds").exists().custom(ParamValidatorManager.getAswIdsValidator()),
                query("cisIds").exists().isArray(),
                query("cisIds").exists().isInt(),
                query("ids").exists().isArray(),
                query("ids").exists().isString(),
                query("names").exists().isArray(),
                query("names").exists().isString(),
            ]),
            query("cisIds.*").optional().isInt(),
            query("minutesBefore")
                .optional()
                .isInt(process.env.NODE_ENV === "test" ? {} : { lt: DATA_RETENTION_IN_MINUTES + 1 }),
            query("minutesAfter").optional().isInt(),
            query("preferredTimezone").optional().isString().isIn(preferredTimezone),
            query("mode").optional().isIn(departureMode),
            query("order").optional().isString().isIn(order),
            query("filter").optional().isString().isIn(filter),
            query("skip").optional().isIn(skip),
            query("timeFrom").optional().isString().isISO8601().custom(ParamValidatorManager.getTimeFromValidator()),
            query("includeMetroTrains").optional().isBoolean(),
            query("airCondition").optional().isBoolean(),
            query("total").optional().isInt(),
        ];

        this.router.get(
            "/departureboards",
            validation,
            ...this.commonMiddleware("PIDDepartureBoards"),
            this.departureBoardsController.getDepartureBoard
        );
    };

    private registerInfotextsRoutes = () => {
        this.router.get("/infotexts", ...this.commonMiddleware("PIDInfotexts"), this.infotextsController.getInfotexts);
    };

    private commonMiddleware = (name: string) => [
        pagination,
        checkErrors,
        paginationLimitMiddleware(name),
        useCacheMiddleware("5 seconds"),
    ];
}

const pidRouter: Router = new PIDRouter().router;
export { pidRouter };
