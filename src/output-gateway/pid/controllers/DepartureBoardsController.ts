import moment, { Moment } from "@golemio/core/dist/shared/moment-timezone";
import { RequestHandler } from "@golemio/core/dist/shared/express";
import { createChildSpan } from "@golemio/core/dist/monitoring/opentelemetry/trace-provider";
import { config } from "@golemio/core/dist/output-gateway/config";
import { RopidRouterUtils } from "#og/shared";
import { PIDDepartureBoardsModel } from "#og/pid/models";
import { DepartureBoardsQueryDTO, DepartureBoardsResponseDTO } from "#og/pid/dto";

export class DepartureBoardsController {
    private departureBoardsModel: PIDDepartureBoardsModel;

    constructor() {
        this.departureBoardsModel = new PIDDepartureBoardsModel();
    }

    public getDepartureBoard: RequestHandler = async (req, res, next) => {
        const query = RopidRouterUtils.mapObjectToDTOInstance(DepartureBoardsQueryDTO, req.query);
        const preferredTimezone = RopidRouterUtils.getPreferredTimezone(query.preferredTimezone);

        // datetime is valid ISO 8601 (by express validator)
        // it matches:
        // 2020-10-10T10:00:00Z
        // 2020-10-10 10:00:00.00Z
        // 2020-10-10T10:00:00+02
        // 2020-10-10T10:00:00+02:00
        const timezoneDefinedRegexp: RegExp = /[T ][\d:\.]+([zZ]|([\+-])([01]\d|2[0-3]):?([0-5]\d)?)/;
        // use timezone from ?preferredTimezone if not set in ?timeFrom
        const timeFrom: Moment | undefined = query.timeFrom
            ? query.timeFrom.match(timezoneDefinedRegexp)
                ? moment(query.timeFrom)
                : moment.tz(query.timeFrom, preferredTimezone)
            : undefined;

        const span = createChildSpan("DepartureBoardsController.getDepartureBoard");
        try {
            const data = await this.departureBoardsModel.GetAll({
                aswIds: query.aswIds,
                cisIds: query.cisIds,
                gtfsIds: query.ids,
                names: query.names,
                minutesAfter: query.minutesAfter,
                minutesBefore: query.minutesBefore,
                timeFrom,
                mode: query.mode,
                order: query.order,
                filter: query.filter,
                skip: query.skip,
                limit: query.limit,
                offset: query.offset,
                total: query.total ?? query.limit,
                timezone: preferredTimezone,
                includeMetroTrains: query.includeMetroTrains,
                airCondition: query.airCondition,
            });
            // allow access response Date header
            res.setHeader("access-control-expose-headers", "date");
            res.status(200).send(RopidRouterUtils.mapObjectToDTOInstance(DepartureBoardsResponseDTO, data));
        } catch (err) {
            next(err);
        } finally {
            span?.end();
        }
    };
}
