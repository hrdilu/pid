import { GTFSRouteTypeEnum } from "#helpers/RouteTypeEnums";
import { DeparturesDirectionsEnum } from "#sch/ropid-departures-directions";
import { IDeparturesModel } from "#sch/ropid-gtfs/models/precomputed/interfaces/IDeparturesModel";
import { StatePositionEnum, StateProcessEnum } from "src/const";

export * from "./PIDRouter";
export * from "./models";

export interface IPIDDeparture extends IDeparturesModel {
    direction: DeparturesDirectionsEnum | null;
    delay_seconds: number | null;
    delay_minutes: number | null;
    is_delay_available: boolean;
    arrival_datetime_real: Date | null;
    departure_datetime_real: Date | null;
    state_position: string;
    real_wheelchair_accessible: boolean | null;
    is_canceled: boolean;
    "trip.agency_name_real": string | null;
    "trip.agency_name_scheduled": string | null;
    "trip.cis_line_id": string | null;
    "trip.cis_line_short_name": string | null;
    "trip.cis_trip_number": string | null;
    "trip.gtfs_block_id": string | null;
    "trip.gtfs_route_id": string | null;
    "trip.gtfs_route_short_name": string | null;
    "trip.gtfs_route_type": GTFSRouteTypeEnum | null;
    "trip.gtfs_trip_headsign": string | null;
    "trip.gtfs_trip_id": string | null;
    "trip.id": string | null;
    "trip.is_canceled": string | null;
    "trip.last_position_id": string | null;
    "trip.origin_route_name": string | null;
    "trip.run_number": string | null;
    "trip.start_asw_stop_id": string | null;
    "trip.start_cis_stop_id": string | null;
    "trip.start_cis_stop_platform_code": string | null;
    "trip.start_time": string | null;
    "trip.start_timestamp": string | null;
    "trip.end_timestamp": string | null;
    "trip.vehicle_registration_number": string | null;
    "trip.vehicle_type_id": string | null;
    "trip.wheelchair_accessible": string | null;
    "trip.last_position.asw_last_stop_id": string | null;
    "trip.last_position.bearing": number | null;
    "trip.last_position.cis_last_stop_id": string | null;
    "trip.last_position.cis_last_stop_sequence": number | null;
    "trip.last_position.delay": number | null;
    "trip.last_position.delay_stop_arrival": string | null;
    "trip.last_position.delay_stop_departure": string | null;
    "trip.last_position.id": string | null;
    "trip.last_position.is_canceled": string | null;
    "trip.last_position.last_stop_arrival_time": string | null;
    "trip.last_position.last_stop_departure_time": string | null;
    "trip.last_position.last_stop_id": string | null;
    "trip.last_position.last_stop_sequence": number | null;
    "trip.last_position.last_stop_name": string | null;
    "trip.last_position.lat": string | null;
    "trip.last_position.lng": string | null;
    "trip.last_position.next_stop_arrival_time": string | null;
    "trip.last_position.next_stop_departure_time": string | null;
    "trip.last_position.next_stop_id": string | null;
    "trip.last_position.next_stop_sequence": number | null;
    "trip.last_position.next_stop_name": string | null;
    "trip.last_position.origin_time": string | null;
    "trip.last_position.origin_timestamp": string | null;
    "trip.last_position.shape_dist_traveled": string | null;
    "trip.last_position.speed": number | null;
    "trip.last_position.state_position": StatePositionEnum | null;
    "trip.last_position.state_process": StateProcessEnum | null;
    "trip.last_position.this_stop_id": string | null;
    "trip.last_position.this_stop_sequence": number | null;
    "trip.last_position.this_stop_name": string | null;
    "trip.last_position.tracking": number | null;
    "trip.last_position.trips_id": string | null;
    "trip.last_position.tcp_event": string | null;
    "trip.last_position.last_stop_headsign": string | null;
    "trip.vehicle_descriptor.is_air_conditioned"?: boolean | null;
}

export interface IPIDDepartureOutput {
    arrival_timestamp: IDepartureTime;
    delay: { is_available: boolean; minutes: number | null; seconds: number | null };
    departure_timestamp: IDepartureTime;
    last_stop: { id: string | null; name: string | null };
    route: {
        short_name: string | null;
        type: number | null;
        is_night: boolean;
        is_regional: boolean;
        is_substitute_transport: boolean;
    };
    stop: { id: string; platform_code: string | null };
    trip: {
        direction: DeparturesDirectionsEnum | null;
        headsign: string;
        id: string;
        is_at_stop: boolean;
        is_canceled: boolean;
        is_wheelchair_accessible: boolean;
        is_air_conditioned: boolean | null;
        short_name: string | null;
    };
}

export interface IStop {
    stop_id: string;
    stop_name: string;
    platform_code: string;
}

export interface IDepartureBoard {
    departures: IPIDDeparture[];
    infotexts: any[];
    stops: IStop[];
}

export interface IDepartureTime {
    predicted: string | null;
    scheduled: string | null;
    minutes?: string | null;
}

export enum DepartureMode {
    DEPARTURES = "departures",
    ARRIVALS = "arrivals",
    MIXED = "mixed",
}

export enum DepartureOrder {
    REAL = "real",
    TIMETABLE = "timetable",
}

export enum DepartureFilter {
    NONE = "none",
    ROUTE_ONCE = "routeOnce",
    ROUTE_HEADING_ONCE = "routeHeadingOnce",
    ROUTE_ONCE_FILL = "routeOnceFill",
    ROUTE_HEADING_ONCE_FILL = "routeHeadingOnceFill",
    ROUTE_HEADING_ONCE_NOGAP = "routeHeadingOnceNoGap",
    ROUTE_HEADING_ONCE_NOGAP_FILL = "routeHeadingOnceNoGapFill",
}

export enum DepartureSkip {
    UNTRACKED = "untracked",
    CANCELED = "canceled",
    AT_STOP = "atStop",
    // ON_INFOTEXT = "onInfotext",
}
