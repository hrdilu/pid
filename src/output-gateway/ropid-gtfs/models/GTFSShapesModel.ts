import { PG_SCHEMA } from "#sch/const";
import { RopidGTFS } from "#sch/ropid-gtfs";
import { ShapeDto } from "#sch/ropid-gtfs/models/ShapeDto";
import { SequelizeModel, buildGeojsonFeatureCollection } from "@golemio/core/dist/output-gateway";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { IGTFSModels } from ".";

export class GTFSShapesModel extends SequelizeModel {
    protected outputAttributes: string[] = [];

    public constructor() {
        super(RopidGTFS.shapes.name, RopidGTFS.shapes.pgTableName, ShapeDto.attributeModel, {
            schema: PG_SCHEMA,
        });

        this.outputAttributes = Object.keys(ShapeDto.attributeModel);
    }

    public Associate = (models: IGTFSModels) => {
        this.sequelizeModel.hasMany(models.GTFSTripsModel.sequelizeModel, {
            foreignKey: "trip_id",
        });
    };

    /** Retrieves all gtfs shapes
     * @param {object} [options] Options object with params
     * @param {number} [options.limit] Limit
     * @param {number} [options.offset] Offset
     * @returns Array of the retrieved records
     */
    public GetAll = async (
        options: {
            id?: string;
            limit?: number;
            offset?: number;
        } = {}
    ): Promise<any> => {
        const { limit, offset, id } = options;
        try {
            const order: any = [];

            order.push([["shape_id", "asc"]]);
            const data = await this.sequelizeModel.findAll({
                limit,
                offset,
                order,
                where: { shape_id: id },
            });
            if (data.length === 0) {
                return;
            }
            return buildGeojsonFeatureCollection(data, "shape_pt_lon", "shape_pt_lat", true);
        } catch (err) {
            throw new GeneralError("Database error", "GTFSShapesModel", err, 500);
        }
    };

    public GetOne = async (id: number): Promise<object | null> => {
        return null;
    };
}
