/**
 * app/routers/GTFSRouter.ts
 *
 * Router /WEB LAYER/: maps routes to specific controller functions, passes request parameters and handles responses.
 * Handles web logic (http request, response). Sets response headers, handles error responses.
 */

import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import { oneOf, param, query } from "@golemio/core/dist/shared/express-validator";
import moment from "@golemio/core/dist/shared/moment-timezone";
import { parseCoordinates } from "@golemio/core/dist/output-gateway/Geo";
import { useCacheMiddleware } from "@golemio/core/dist/output-gateway/redis";
import { BaseRouter } from "@golemio/core/dist/output-gateway/routes/BaseRouter";
import { checkErrors, paginationLimitMiddleware, pagination } from "@golemio/core/dist/output-gateway/Validation";
import { models } from "./models";
import { GTFSCalendarModel } from "./models/GTFSCalendarModel";
import { GTFSRoutesModel } from "./models/GTFSRoutesModel";
import { GTFSShapesModel } from "./models/GTFSShapesModel";
import { GTFSStopModel } from "./models/GTFSStopModel";
import { GTFSStopTimesModel } from "./models/GTFSStopTimesModel";
import { GTFSTripsModel } from "./models/GTFSTripsModel";
import { ParamValidatorManager } from "#og/pid/helpers/ParamValidatorManager";

export class GTFSRouter extends BaseRouter {
    // Assign router to the express.Router() instance
    public router: Router = Router();

    protected tripModel: GTFSTripsModel;
    protected serviceModel: GTFSCalendarModel;
    protected stopModel: GTFSStopModel;
    protected routeModel: GTFSRoutesModel;
    protected shapeModel: GTFSShapesModel;
    protected stopTimeModel: GTFSStopTimesModel;

    // Reg-ex to match a valid daytime format (ex. 21:43:56)
    private timeRegex = /(([0-1][0-9])|(2[0-3])):[0-5][0-9]:[0-5][0-9]/;

    private tripInclusions = [
        query("includeShapes").optional().isBoolean(),
        query("includeStops").optional().isBoolean(),
        query("includeStopTimes").optional().isBoolean(),
        query("includeService").optional().isBoolean(),
        query("includeRoute").optional().isBoolean(),
        query("date").optional().isString().isISO8601().custom(ParamValidatorManager.getDateValidator()),
    ];

    private stopTimesHandlers = [
        param("stopId").exists(),
        query("from").optional().matches(this.timeRegex),
        query("to").optional().matches(this.timeRegex),
        query("date").optional().isString().isISO8601().custom(ParamValidatorManager.getDateValidator()),
        query("includeStop").optional().isBoolean(),
    ];

    public constructor() {
        super();
        this.tripModel = models.GTFSTripsModel;
        this.stopModel = models.GTFSStopModel;
        this.stopTimeModel = models.GTFSStopTimesModel;
        this.routeModel = models.GTFSRoutesModel;
        this.shapeModel = models.GTFSShapesModel;
        this.serviceModel = models.GTFSCalendarModel;
        this.initRoutes();
    }

    public GetAllServices = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data = await this.serviceModel.GetAll({
                date: (req.query.date as string) || undefined,
                limit: Number(req.query.limit) || undefined,
                offset: Number(req.query.offset) || undefined,
            });

            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };

    public GetAllStopTimes = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data = await this.stopTimeModel.GetAll({
                date: (req.query.date as string) || undefined,
                from: (req.query.from as string) || undefined,
                limit: Number(req.query.limit) || undefined,
                offset: Number(req.query.offset) || undefined,
                stop: !!req.query.includeStop,
                stopId: req.params.stopId,
                to: (req.query.to as string) || undefined,
            });

            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };

    public GetAllTrips = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data = await this.tripModel.GetAll({
                date: (req.query.date as string) || undefined,
                limit: Number(req.query.limit) || undefined,
                offset: Number(req.query.offset) || undefined,
                stopId: req.query.stopId as string,
            });

            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };

    public GetOneTrip = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const id: string = req.params.id;
            const data = await this.tripModel.GetOne(id, {
                date: (req.query.date as string) || undefined,
                route: !!req.query.includeRoute,
                service: !!req.query.includeService,
                shapes: !!req.query.includeShapes,
                stopTimes: !!req.query.includeStopTimes,
                stops: !!req.query.includeStops,
            });

            if (!data) {
                throw new GeneralError("not_found", "GTFSRouter", undefined, 404);
            }
            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };

    public GetAllStops = async (req: Request, res: Response, next: NextFunction) => {
        const names: string[] = this.ConvertToArray(req.query.names || []);
        const aswIds: string[] = this.ConvertToArray(req.query.aswIds || []);
        const cisIds: string[] = this.ConvertToArray(req.query.cisIds || []);
        const gtfsIds: string[] = this.ConvertToArray(req.query.ids || []);

        try {
            const coords = await parseCoordinates(req.query.latlng as string, req.query.range as string);
            const data = await this.stopModel.GetAll({
                aswIds,
                cisIds,
                gtfsIds,
                lat: coords.lat,
                limit: Number(req.query.limit) || undefined,
                lng: coords.lng,
                names,
                offset: Number(req.query.offset) || undefined,
                range: coords.range,
            });

            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };

    public GetOneStop = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const id: string = req.params.id;
            const data = await this.stopModel.GetOne(id);
            if (!data) {
                throw new GeneralError("not_found", "GTFSRouter", undefined, 404);
            }
            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };

    public GetAllRoutes = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data = await this.routeModel.GetAll({
                limit: Number(req.query.limit) || undefined,
                offset: Number(req.query.offset) || undefined,
            });

            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };

    public GetOneRoute = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const id: string = req.params.id;
            const data = await this.routeModel.GetOne(id);
            if (!data) {
                throw new GeneralError("not_found", "GTFSRouter", undefined, 404);
            }
            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };

    public GetAllShapes = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data = await this.shapeModel.GetAll({
                id: req.params.id,
                limit: Number(req.query.limit) || undefined,
                offset: Number(req.query.offset) || undefined,
            });
            if (!data) {
                throw new GeneralError("not_found", "GTFSRouter", undefined, 404);
            }
            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    private initRoutes = (): void => {
        this.initTripsEndpoints("1 minute");
        this.initStopsEndpoints("1 minute");
        this.initStopTimesEndpoints("1 minute");
        this.initRoutesEndpoints("1 minute");
        this.initShapesEndpoints("1 minute");
        this.initServicesEndpoints("1 minute");
    };

    private initRoutesEndpoints = (expire?: number | string): void => {
        this.router.get(
            "/routes",
            pagination,
            checkErrors,
            paginationLimitMiddleware("GTFSRouter"),
            useCacheMiddleware(expire),
            this.GetAllRoutes
        );
        this.router.get("/routes/:id", param("id").exists(), useCacheMiddleware(expire), this.GetOneRoute);
    };

    private initServicesEndpoints = (expire?: number | string): void => {
        this.router.get(
            "/services",
            query("date").optional().isString().isISO8601().custom(ParamValidatorManager.getDateValidator()),
            pagination,
            checkErrors,
            paginationLimitMiddleware("GTFSRouter"),
            useCacheMiddleware(expire),
            this.GetAllServices
        );
    };

    private initShapesEndpoints = (expire?: number | string): void => {
        this.router.get(
            "/shapes/:id",
            pagination,
            checkErrors,
            paginationLimitMiddleware("GTFSRouter"),
            param("id").exists(),
            useCacheMiddleware(expire),
            this.GetAllShapes
        );
    };

    private initTripsEndpoints = (expire?: number | string): void => {
        this.router.get(
            "/trips",
            query("stopId").optional(),
            this.tripInclusions,
            pagination,
            checkErrors,
            paginationLimitMiddleware("GTFSRouter"),
            useCacheMiddleware(expire),
            this.GetAllTrips
        );
        this.router.get(
            "/trips/:id",
            param("id").exists(),
            this.tripInclusions,
            checkErrors,
            useCacheMiddleware(expire),
            this.GetOneTrip
        );
    };

    private initStopsEndpoints = (expire?: number | string): void => {
        this.router.get(
            "/stops",
            [
                query("latlng").optional().isLatLong(),
                query("range").optional().isNumeric(),
                oneOf([query("names").optional().isArray(), query("names").optional().isString()]),
                oneOf([query("aswIds").optional().isArray(), query("aswIds").optional().isString()]),
                oneOf([query("cisIds").optional().isArray(), query("cisIds").optional().isString()]),
                oneOf([query("ids").optional().isArray(), query("ids").optional().isString()]),
                query("names.*").optional().isString(),
                query("aswIds.*").optional().isString(),
                query("cisIds.*").optional().isInt(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware("GTFSRouter"),
            useCacheMiddleware(expire),
            this.GetAllStops
        );
        this.router.get("/stops/:id", [param("id").exists()], checkErrors, useCacheMiddleware(expire), this.GetOneStop);
    };

    private initStopTimesEndpoints = (expire?: number | string): void => {
        this.router.get(
            "/stoptimes/:stopId",
            this.stopTimesHandlers,
            pagination,
            paginationLimitMiddleware("GTFSRouter"),
            checkErrors,
            (req: Request, res: Response, next: NextFunction) => {
                if (
                    req.query.from &&
                    req.query.to &&
                    moment(req.query.from as string, "H:mm:ss").isAfter(moment(req.query.to as string, "H:mm:ss"))
                ) {
                    throw new GeneralError("Validation error", "GTFSRouter", new Error("'to' cannot be later than 'from'"), 400);
                }
                return next();
            },
            useCacheMiddleware(expire),
            this.GetAllStopTimes
        );
    };
}

const gtfsRouter: Router = new GTFSRouter().router;

export { gtfsRouter };
