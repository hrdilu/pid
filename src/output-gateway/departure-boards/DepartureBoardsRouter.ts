/**
 * app/routers/DepartureBoardsRouter.ts
 *
 * DEPRECATED!!! use PIDDepartureBoardsRoutes.ts instead
 *
 * Router /WEB LAYER/: maps routes to specific controller functions, passes request parameters and handles responses.
 * Handles web logic (http request, response). Sets response headers, handles error responses.
 */

import { DepartureFilter, DepartureMode, DepartureOrder, IPIDDepartureOutput, PIDDepartureBoardsModel } from "#og/pid";
import { ValidationArrays } from "#og/shared/constants";
import { RopidRouterUtils } from "#og/shared";
import { useCacheMiddleware } from "@golemio/core/dist/output-gateway/redis";
import { BaseRouter } from "@golemio/core/dist/output-gateway/routes/BaseRouter";
import { checkErrors, pagination, paginationLimitMiddleware } from "@golemio/core/dist/output-gateway/Validation";
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import { oneOf, query } from "@golemio/core/dist/shared/express-validator";
import moment, { Moment } from "@golemio/core/dist/shared/moment-timezone";
import { DATA_RETENTION_IN_MINUTES } from "src/const";

/**
 * @deprecated use /pid/departureboards instead
 */
export class DepartureBoardsRouter extends BaseRouter {
    // Assign router to the express.Router() instance
    public router: Router = Router();

    private defaultQueryParameters: any = {
        total: 20,
        limit: 20,
        offset: 0,
        minutesAfter: 180,
        minutesBefore: 0,
        timeFromValidRange: {
            hoursBefore: 6,
            hoursAfter: 2 * 24,
        },
    };

    protected departureBoardsModel: PIDDepartureBoardsModel;

    public constructor() {
        super();
        this.departureBoardsModel = new PIDDepartureBoardsModel();
        this.initRoutes();
    }

    public GetDepartureBoard = async (req: Request, res: Response, next: NextFunction) => {
        const aswIds: string[] = this.ConvertToArray(req.query.aswIds || []);
        const cisIds: string[] = this.ConvertToArray(req.query.cisIds || []);
        const gtfsIds: string[] = this.ConvertToArray(req.query.ids || []);
        const names: string[] = this.ConvertToArray(req.query.names || []);
        const limit: number = parseInt(req.query.limit || this.defaultQueryParameters.limit, 10);
        const preferredTimezone = RopidRouterUtils.getPreferredTimezone(req.query.preferredTimezone);

        const timezoneDefinedRegexp: RegExp = /[T ][\d:\.]+([zZ]|([\+-])([01]\d|2[0-3]):?([0-5]\d)?)/;
        const timeFrom: Moment | undefined = req.query.timeFrom
            ? (req.query.timeFrom as string).match(timezoneDefinedRegexp)
                ? moment(req.query.timeFrom as string)
                : moment.tz(req.query.timeFrom, preferredTimezone)
            : undefined;

        try {
            const data = await this.departureBoardsModel.GetAll({
                aswIds,
                cisIds,
                gtfsIds,
                names,
                minutesAfter: parseInt(req.query.minutesAfter || this.defaultQueryParameters.minutesAfter, 10),
                minutesBefore: parseInt(req.query.minutesBefore || this.defaultQueryParameters.minutesBefore, 10),
                timeFrom,
                order: req.query.orderBySchedule === "true" ? DepartureOrder.TIMETABLE : DepartureOrder.REAL,
                filter: req.query.showAllRoutesFirst === "true" ? DepartureFilter.ROUTE_ONCE_FILL : DepartureFilter.NONE,
                limit,
                skip: [],
                mode: DepartureMode.DEPARTURES,
                offset: Number(req.query.offset) || 0,
                total: limit,
                timezone: preferredTimezone,
            });

            res.status(200).send(
                data.departures.map((d: IPIDDepartureOutput) => this.transformResponseData(d, preferredTimezone))
            );
        } catch (err) {
            next(err);
        }
    };

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    private transformResponseData = (x: IPIDDepartureOutput, preferredTimezone: string): any => {
        return {
            arrival_timestamp: x.arrival_timestamp,
            delay: x.delay,
            departure_timestamp: {
                predicted: x.departure_timestamp.predicted,
                scheduled: x.departure_timestamp.scheduled,
            },
            route: {
                short_name: x.route.short_name,
                type: x.route.type,
            },
            stop: {
                id: x.stop.id,
                name: "", // placeholder for deprecated API endpoint
                platform_code: x.stop.platform_code,
                wheelchair_boarding: 0, // placeholder for deprecated API endpoint
            },
            trip: {
                headsign: x.trip.headsign,
                id: x.trip.id,
                is_canceled: x.trip.is_canceled || false,
                is_wheelchair_accessible: x.trip.is_wheelchair_accessible ? 1 : 2, // enum 0,1,2 see GTFS specs
            },
        };
    };

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     */
    private initRoutes = (): void => {
        this.initDepartureBoardsEndpoints("5 seconds");
    };

    private initDepartureBoardsEndpoints = (expire?: number | string): void => {
        this.router.get(
            "/",
            [
                oneOf([
                    query("ids").exists().isArray(),
                    query("aswIds").exists().isArray(),
                    query("cisIds").exists().isArray(),
                    query("ids").exists().isString(),
                    query("aswIds").exists().isString(),
                    query("cisIds").exists().isString(),
                    query("names").exists().isArray(),
                    query("names").exists().isString(),
                ]),
                query("ids.*").optional().isString(),
                query("aswIds.*").optional().isString(),
                query("cisIds.*").optional().isInt(),
                query("names.*").optional().isString(),
                query("minutesBefore")
                    .optional()
                    .isInt(process.env.NODE_ENV === "test" ? {} : { lt: DATA_RETENTION_IN_MINUTES + 1 }),
                query("minutesAfter").optional().isInt(),
                query("orderBySchedule").optional().isBoolean(),
                query("preferredTimezone").optional().isString().isIn(ValidationArrays.preferredTimezone),
                query("showAllRoutesFirst").optional().isBoolean(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware("DepartureBoardsRouter"),
            useCacheMiddleware(expire),
            this.GetDepartureBoard
        );
    };
}

const departureBoardsRouter: Router = new DepartureBoardsRouter().router;

export { departureBoardsRouter };
