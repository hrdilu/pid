export enum VehicleDescriptorStateEnum {
    Dilny = "dilny",
    Doco = "doco",
    Nzr = "nzr",
    Zar = "zar",
}

export enum VehicleDescriptorTractionEnum {
    Tram = "tramvaj",
    Bus = "autobus",
    ElectricBus = "elektrobus",
    TrolleyBus = "trolejbus",
}
