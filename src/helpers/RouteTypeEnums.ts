export enum GTFSRouteTypeEnum {
    TRAM = 0,
    METRO = 1,
    TRAIN = 2,
    BUS = 3,
    FERRY = 4,
    FUNICULAR = 7,
    TROLLEYBUS = 11,
    // Extended types
    // https://developers.google.com/transit/gtfs/reference/extended-route-types
    EXT_MISCELLANEOUS = 1700,
}

export enum MPVRouteTypesEnum {
    METRO = 1,
    TRAM = 2,
    BUS_CITY = 3,
    BUS_REGIONAL = 4,
    BUS_NIGHT = 5,
    TRAM_NIGHT = 6,
    BUS_SUBSTITUTE = 7,
    FUNICULAR = 8,
    SCHOOL = 9,
    DISABILITY = 10,
    CONTRACTUAL = 11,
    FERRY = 12,
    TRAIN = 13,
    TRAIN_SUBSTITUTE = 14,
    TRAM_SUBSTITUTE = 15,
    BUS_NIGHT_REGIONAL = 16,
    OTHER = 17,
    TROLLEYBUS = 18,
}
