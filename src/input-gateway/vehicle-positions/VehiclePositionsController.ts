import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { VehiclePositions } from "#sch/vehicle-positions";
import { BaseController } from "@golemio/core/dist/input-gateway/controllers";

export class VehiclePositionsController extends BaseController {
    public name!: string;
    protected validator!: JSONSchemaValidator;
    protected queuePrefix!: string;

    constructor() {
        super(
            VehiclePositions.name,
            new JSONSchemaValidator(VehiclePositions.name + "Controller", VehiclePositions.datasourceJsonSchema)
        );
    }

    /**
     * Data processing
     */
    public processData = async (inputData: any): Promise<void> => {
        try {
            await this.validator.Validate(inputData); // throws an error
            await this.sendMessageToExchange("input." + this.queuePrefix + ".saveDataToDB", JSON.stringify(inputData), {
                persistent: true,
            });
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            } else {
                throw new GeneralError("Error while validating input data.", this.name, err, 422);
            }
        }
    };
}
