import { checkContentTypeMiddleware } from "@golemio/core/dist/input-gateway/helpers/CheckContentTypeMiddleware";
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import { RopidGtfsController } from "./RopidGtfsController";

export class RopidGtfsRouter {
    public router: Router;
    private controller: RopidGtfsController;

    constructor() {
        this.router = Router();
        this.controller = new RopidGtfsController();
        this.initRoutes();
    }

    private initRoutes() {
        this.router.post("/presets", checkContentTypeMiddleware(["application/json"]), this.post);
    }

    private post = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            await this.controller.processData(req.body);

            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };
}
