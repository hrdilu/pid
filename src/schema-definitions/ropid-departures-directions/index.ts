import "@golemio/core/dist/shared/sequelize";
import { outputDeparturesDirectionsJsonSchema } from "./RopidDeparturesDirectionsOutputSchemas";
import { IRopidDeparturesDirectionsSDMAOutput, outputDeparturesDirectionsSDMA } from "./RopidDeparturesDirectionsSDMA";

enum DeparturesDirectionsEnum {
    TOP = "top",
    TOP_LEFT = "top-left",
    LEFT = "left",
    BOTTOM_LEFT = "bottom-left",
    BOTTOM = "bottom",
    BOTTOM_RIGHT = "bottom-right",
    RIGHT = "right",
    TOP_RIGHT = "top-right",
}

interface IRopidDeparturesDirectionsOutput {
    departure_stop_id: string;
    next_stop_id_regexp: RegExp;
    direction: DeparturesDirectionsEnum;
    rule_order: number;
}

const forExport = {
    name: "RopidDeparturesDirections",
    departureDirections: {
        name: "RopidDeparturesDirections",
        outputJsonSchema: outputDeparturesDirectionsJsonSchema,
        outputSequelizeAttributes: outputDeparturesDirectionsSDMA,
        pgTableName: "ropid_departures_directions",
    },
};

export { forExport as RopidDeparturesDirections, DeparturesDirectionsEnum };
export type { IRopidDeparturesDirectionsSDMAOutput, IRopidDeparturesDirectionsOutput };
