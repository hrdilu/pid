import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";
import { VehicleDescriptorStateEnum } from "src/helpers/VehicleDescriptorEnums";

export interface IDescriptorOutputDto {
    id: number;
    state: VehicleDescriptorStateEnum;
    registration_number: number;
    registration_number_index: number;
    license_plate: string | null;
    operator: string;
    manufacturer: string;
    type: string;
    traction: string;
    gtfs_route_type: GTFSRouteTypeEnum;
    is_air_conditioned: boolean;
    has_usb_chargers: boolean;
    paint: string | null;
    thumbnail_url: string | null;
    photo_url: string | null;
}

// For type safety in the repository
export interface IDescriptorAuditDto {
    updated_at?: Date;
}
