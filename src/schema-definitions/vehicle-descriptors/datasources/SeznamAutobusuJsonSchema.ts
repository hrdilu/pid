import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IVehicleListInfo } from "./interfaces/IVehicleListInfo";

export const seznamAutobusuJsonSchema: JSONSchemaType<IVehicleListInfo[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            id: { type: "integer" },
            state: { type: "string" },
            number: { type: "string", pattern: "^[0-9]+$" },
            numberIndex: { type: "integer" },
            plate: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            operator: { type: "string" },
            manufacturer: { type: "string" },
            type: { type: "string" },
            traction: { type: "string" },
            airCondition: { type: "integer", enum: [0, 1] },
            usbChargers: { type: "integer", enum: [0, 1] },
            paint: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            thumbnailUrl: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            photoUrl: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
        },
        required: [
            "id",
            "state",
            "number",
            "numberIndex",
            "plate",
            "operator",
            "manufacturer",
            "type",
            "traction",
            "airCondition",
            "usbChargers",
            "paint",
            "thumbnailUrl",
            "photoUrl",
        ],
    },
};
