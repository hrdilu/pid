export interface IVehicleListInfo {
    id: number;
    state: string;
    number: string;
    numberIndex: number;
    plate: string | null;
    operator: string;
    manufacturer: string;
    type: string;
    traction: string;
    airCondition: number;
    usbChargers: number;
    paint: string | null;
    thumbnailUrl: string | null;
    photoUrl: string | null;
}
