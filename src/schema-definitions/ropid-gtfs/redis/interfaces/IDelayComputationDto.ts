export interface IShapePoint {
    index: number;
    at_stop: boolean;
    bearing: number;
    coordinates: number[];
    distance_from_last_stop: number;
    last_stop_sequence: number;
    next_stop_sequence: number;
    shape_dist_traveled: number;
    this_stop_sequence: number | null;
    time_scheduled_seconds: number;
}

export interface IStop {
    stop_id: string;
    stop_lat: number;
    stop_lon: number;
    stop_name: string;
}

export interface IStopTime {
    arrival_time: string;
    departure_time: string;
    shape_dist_traveled: number;
    stop_headsign: string | null;
    stop_id: string;
    stop_sequence: number;
    arrival_time_seconds: number;
    departure_time_seconds: number;
    stop: IStop;
}

export interface IDelayComputationDto {
    trip_id: string;
    stop_times: IStopTime[];
    shapes_anchor_points: IShapePoint[];
}
