import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IDelayComputationDto } from "../interfaces/IDelayComputationDto";

const delayComputationDtoSchema: JSONSchemaType<IDelayComputationDto> = {
    $schema: "http://json-schema.org/draft-04/schema#",
    title: "DelayComputationDtoSchema",
    type: "object",
    properties: {
        trip_id: { type: "string" },
        stop_times: {
            type: "array",
            items: {
                type: "object",
                properties: {
                    arrival_time: { type: "string" },
                    departure_time: { type: "string" },
                    shape_dist_traveled: { type: "number" },
                    stop_headsign: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                    stop_id: { type: "string" },
                    stop_sequence: { type: "integer" },
                    arrival_time_seconds: { type: "integer" },
                    departure_time_seconds: { type: "integer" },
                    stop: {
                        type: "object",
                        properties: {
                            stop_id: { type: "string" },
                            stop_lat: { type: "number" },
                            stop_lon: { type: "number" },
                            stop_name: { type: "string" },
                        },
                        required: ["stop_id", "stop_lat", "stop_lon", "stop_name"],
                    },
                },
                required: [
                    "arrival_time",
                    "departure_time",
                    "shape_dist_traveled",
                    "stop_id",
                    "stop_sequence",
                    "arrival_time_seconds",
                    "departure_time_seconds",
                ],
            },
        },
        shapes_anchor_points: {
            type: "array",
            items: {
                type: "object",
                properties: {
                    index: { type: "integer" },
                    at_stop: { type: "boolean" },
                    bearing: { type: "number" },
                    coordinates: { type: "array", items: { type: "number" } },
                    distance_from_last_stop: { type: "number" },
                    last_stop_sequence: { type: "integer" },
                    next_stop_sequence: { type: "integer" },
                    shape_dist_traveled: { type: "number" },
                    this_stop_sequence: { oneOf: [{ type: "integer" }, { type: "null", nullable: true }] },
                    time_scheduled_seconds: { type: "number" },
                },
                required: [
                    "index",
                    "at_stop",
                    "bearing",
                    "coordinates",
                    "distance_from_last_stop",
                    "last_stop_sequence",
                    "next_stop_sequence",
                    "shape_dist_traveled",
                    "time_scheduled_seconds",
                ],
            },
        },
    },
    required: ["trip_id", "stop_times", "shapes_anchor_points"],
};

export { delayComputationDtoSchema as DelayComputationDtoSchema };
