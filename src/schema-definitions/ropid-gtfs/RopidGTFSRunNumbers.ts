import { ModelAttributes, DataTypes } from "@golemio/core/dist/shared/sequelize";

export interface IRopidGTFSRunNumbersInputData {
    route_id: string;
    run_number: string;
    service_id: string;
    trip_id: string;
    vehicle_type: string;
}

export type IRopidGTFSRunNumbersData =
    | IRopidGTFSRunNumbersInputData
    | {
          run_number: number;
          vehicle_type: number;
          [auditFields: string]: unknown;
      };

const datasourceJsonSchema = {
    type: "array",
    items: {
        $ref: "#/$defs/item",
    },
    $defs: {
        item: {
            type: "object",
            properties: {
                route_id: {
                    type: "string",
                },
                run_number: {
                    type: "string",
                },
                service_id: {
                    type: "string",
                },
                trip_id: {
                    type: "string",
                },
                vehicle_type: {
                    type: "string",
                },
            },
            required: ["route_id", "run_number", "service_id", "trip_id", "vehicle_type"],
        },
    },
};

// Output schema for the validator
const outputJsonSchema = {
    ...datasourceJsonSchema,
    $defs: {
        item: {
            type: "object",
            properties: {
                route_id: {
                    type: "string",
                },
                run_number: {
                    type: "number",
                },
                service_id: {
                    type: "string",
                },
                trip_id: {
                    type: "string",
                },
                vehicle_type: {
                    type: "number",
                },
            },
            required: datasourceJsonSchema.$defs.item.required,
        },
    },
};

// Output Sequelize attributes
const outputSequelizeAttributes: ModelAttributes<any, IRopidGTFSRunNumbersData> = {
    route_id: {
        type: DataTypes.STRING(50),
        allowNull: false,
    },
    run_number: {
        primaryKey: true,
        type: DataTypes.INTEGER,
    },
    service_id: {
        primaryKey: true,
        type: DataTypes.STRING(50),
    },
    trip_id: {
        primaryKey: true,
        type: DataTypes.STRING(50),
    },
    vehicle_type: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
};

export const RopidGTFSRunNumbers = {
    name: "RopidGTFSRunNumbers",
    datasourceJsonSchema,
    outputJsonSchema,
    outputSequelizeAttributes,
    pgTableName: "ropidgtfs_run_numbers",
};
