import { AgencyDto } from "./models/AgencyDto";
import { CalendarDatesDto } from "./models/CalendarDatesDto";
import { CalendarDto } from "./models/CalendarDto";
import { RouteDto } from "./models/RouteDto";
import { ShapeDto } from "./models/ShapeDto";
import { StopDto } from "./models/StopDto";
import { StopTimesDto } from "./models/StopTimesDto";
import { TripDto } from "./models/TripDto";
import type {} from "@golemio/core/dist/shared/sequelize";

export const RopidGTFSSchedule = {
    agency: {
        name: "RopidGTFSAgency",
        pgTableName: "ropidgtfs_agency",
        /**
         * @deprecated The property should not be used
         */
        outputSequelizeAttributes: AgencyDto.attributeModel,
    },
    calendar: {
        name: "RopidGTFSCalendar",
        pgTableName: "ropidgtfs_calendar",
        /**
         * @deprecated The property should not be used
         */
        outputSequelizeAttributes: CalendarDto.attributeModel,
    },
    calendar_dates: {
        name: "RopidGTFSCalendarDates",
        pgTableName: "ropidgtfs_calendar_dates",
        /**
         * @deprecated The property should not be used
         */
        outputSequelizeAttributes: CalendarDatesDto.attributeModel,
    },
    shapes: {
        name: "RopidGTFSShapes",
        pgTableName: "ropidgtfs_shapes",
        /**
         * @deprecated The property should not be used
         */
        outputSequelizeAttributes: ShapeDto.attributeModel,
    },
    stop_times: {
        name: "RopidGTFSStopTimes",
        pgTableName: "ropidgtfs_stop_times",
        /**
         * @deprecated The property should not be used
         */
        outputSequelizeAttributes: StopTimesDto.attributeModel,
    },
    stops: {
        name: "RopidGTFSStops",
        pgTableName: "ropidgtfs_stops",
        /**
         * @deprecated The property should not be used
         */
        outputSequelizeAttributes: StopDto.attributeModel,
    },
    routes: {
        name: "RopidGTFSRoutes",
        pgTableName: "ropidgtfs_routes",
        /**
         * @deprecated The property should not be used
         */
        outputSequelizeAttributes: RouteDto.attributeModel,
    },
    trips: {
        name: "RopidGTFSTrips",
        pgTableName: "ropidgtfs_trips",
        /**
         * @deprecated The property should not be used
         */
        outputSequelizeAttributes: TripDto.attributeModel,
    },
};

export type ScheduleModelName = keyof typeof RopidGTFSSchedule;
/**
 * @deprecated The type should not be used
 */
export type RopidGTFSPgProp = keyof Pick<typeof RopidGTFSSchedule, "stop_times">;
