import { ModelAttributes, DataTypes } from "@golemio/core/dist/shared/sequelize";

export interface IRopidGTFSCisStopGroupsInputData {
    avgJtskX: number;
    avgJtskY: number;
    avgLat: number;
    avgLon: number;
    cis: number;
    districtCode: string;
    fullName: string;
    idosCategory: string;
    idosName: string;
    municipality: string;
    name: string;
    node: number;
    uniqueName: string;
}

export interface IRopidGTFSCisStopGroupsData {
    avg_jtsk_x: number;
    avg_jtsk_y: number;
    avg_lat: number;
    avg_lon: number;
    cis: number;
    district_code: string;
    full_name: string;
    idos_category: string;
    idos_name: string;
    municipality: string;
    name: string;
    node: number;
    unique_name: string;

    [auditFields: string]: unknown;
}

const datasourceJsonSchema = {
    type: "array",
    items: {
        $ref: "#/$defs/Item",
    },
    $defs: {
        Item: {
            type: "object",
            properties: {
                avgJtskX: { type: "number" },
                avgJtskY: { type: "number" },
                avgLat: { type: "number" },
                avgLon: { type: "number" },
                cis: { type: "number" },
                districtCode: { type: "string" },
                fullName: { type: "string" },
                idosCategory: { type: "string" },
                idosName: { type: "string" },
                municipality: { type: "string" },
                name: { type: "string" },
                node: { type: "number" },
                uniqueName: { type: "string" },
            },
            required: ["cis"],
        },
    },
};

const outputJsonSchema = {
    type: "array",
    items: {
        $ref: "#/$defs/Item",
        $defs: {
            Item: {
                type: "object",
                properties: {
                    avg_jtsk_x: { type: "number" },
                    avg_jtsk_y: { type: "number" },
                    avg_lat: { type: "number" },
                    avg_lon: { type: "number" },
                    cis: { type: "number" },
                    district_code: { type: "string" },
                    full_name: { type: "string" },
                    idos_category: { type: "string" },
                    idos_name: { type: "string" },
                    municipality: { type: "string" },
                    name: { type: "string" },
                    node: { type: "number" },
                    unique_name: { type: "string" },
                },
                required: ["cis"],
            },
        },
    },
};

const outputSequelizeAttributes: ModelAttributes<any, IRopidGTFSCisStopGroupsData> = {
    avg_jtsk_x: DataTypes.DOUBLE,
    avg_jtsk_y: DataTypes.DOUBLE,
    avg_lat: DataTypes.DOUBLE,
    avg_lon: DataTypes.DOUBLE,
    cis: {
        primaryKey: true,
        type: DataTypes.INTEGER,
    },
    district_code: DataTypes.STRING,
    full_name: DataTypes.STRING,
    idos_category: DataTypes.STRING,
    idos_name: DataTypes.STRING,
    municipality: DataTypes.STRING,
    name: DataTypes.STRING,
    node: DataTypes.INTEGER,
    unique_name: DataTypes.STRING,
};

export const RopidGTFSCisStopGroups = {
    name: "RopidGTFSCisStopGroups",
    datasourceJsonSchema,
    outputJsonSchema,
    outputSequelizeAttributes,
    pgTableName: "ropidgtfs_cis_stop_groups",
};
