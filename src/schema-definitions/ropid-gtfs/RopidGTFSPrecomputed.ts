import { DataTypes, ModelAttributes, ModelIndexesOptions } from "@golemio/core/dist/shared/sequelize";

const outputServicesCalendarSDMA: ModelAttributes<any> = {
    date: DataTypes.DATE,
    day_diff: DataTypes.INTEGER,
    service_id: DataTypes.STRING(255),
};

const servicesCalendarConstraintsMIO: ModelIndexesOptions[] = [
    {
        unique: true,
        using: "BTREE",
        name: "ropidgtfs_precomputed_services_calendar_date_service_id_idx",
        fields: ["date", "service_id"],
    },
];

const outputServicesCalendarJsonSchema = {
    type: "object",
    properties: {
        date: {
            type: "string",
        },
        day_diff: {
            type: "integer",
        },
        service_id: {
            type: "string",
        },
    },
    required: ["date", "day_diff", "service_id"],
};

const outputMinMaxStopSequencesSDMA: ModelAttributes<any> = {
    trip_id: DataTypes.STRING,
    max_stop_sequence: DataTypes.INTEGER, // 9
    min_stop_sequence: DataTypes.INTEGER, // 1
    max_stop_time: DataTypes.STRING, // "11:58:00"
    min_stop_time: DataTypes.STRING, // "11:58:00"
};

const minMaxStopSequencesConstraintsMIO: ModelIndexesOptions[] = [
    {
        unique: true,
        using: "BTREE",
        name: "ropidgtfs_precomputed_minmax_stop_sequences_trip_id_idx",
        fields: ["trip_id"],
    },
];

const outputMinMaxStopSequencesJsonSchema = {
    type: "object",
    properties: {
        trip_id: {
            type: "string",
        },
        max_stop_sequence: {
            type: "integer",
        },
        min_stop_sequence: {
            type: "integer",
        },
        max_stop_time: {
            type: "string",
        },
        min_stop_time: {
            type: "string",
        },
    },
    required: ["trip_id", "max_stop_sequence", "min_stop_sequence", "max_stop_time", "min_stop_time"],
};

const tripScheduleConstraintsMIO: ModelIndexesOptions[] = [
    {
        unique: false,
        using: "BTREE",
        name: "ropidgtfs_precomputed_trip_schedule_origin_route_id_idx",
        fields: ["origin_route_id"],
    },
    {
        unique: false,
        using: "BTREE",
        name: "ropidgtfs_precomputed_trip_schedule_origin_route_name_idx",
        fields: ["origin_route_name"],
    },
    {
        unique: false,
        using: "BTREE",
        name: "ropidgtfs_precomputed_trip_schedule_start_timestamp_idx",
        fields: ["start_timestamp"],
    },
    {
        unique: true,
        using: "BTREE",
        name: "ropidgtfs_precomputed_trip_schedule_unique_id",
        fields: ["trip_id", "service_id", "run_number", "date", "origin_route_id"],
    },
];

const outputTripScheduleSDMA: ModelAttributes<any> = {
    origin_route_id: DataTypes.STRING,
    trip_id: DataTypes.STRING,
    service_id: DataTypes.STRING,
    run_number: DataTypes.INTEGER,
    date: DataTypes.STRING,
    route_id: DataTypes.STRING,
    route_type: {
        type: DataTypes.SMALLINT,
        allowNull: false,
    },
    route_short_name: DataTypes.STRING,
    is_regional: DataTypes.STRING,
    is_substitute_transport: DataTypes.STRING,
    is_night: DataTypes.STRING,
    trip_headsign: DataTypes.STRING,
    trip_short_name: DataTypes.STRING,
    block_id: DataTypes.STRING,
    exceptional: DataTypes.NUMBER,
    min_stop_time: DataTypes.STRING,
    max_stop_time: DataTypes.STRING,
    start_timestamp: DataTypes.STRING,
    end_timestamp: DataTypes.STRING,
    first_stop_id: DataTypes.STRING,
    last_stop_id: DataTypes.STRING,
    origin_route_name: DataTypes.STRING(50),
};

const outputTripScheduleJsonSchema = {
    type: "object",
    properties: {
        origin_route_id: {
            type: "string",
        },
        trip_id: {
            type: "string",
        },
        service_id: {
            type: "string",
        },
        run_number: {
            type: "string",
        },
        date: {
            type: "string",
        },
        route_id: {
            type: "string",
        },
        route_type: {
            type: "integer",
        },
        route_short_name: {
            type: "string",
        },
        is_regional: {
            type: "string",
        },
        is_substitute_transport: {
            type: "string",
        },
        is_night: {
            type: "string",
        },
        trip_headsign: {
            type: "string",
        },
        trip_short_name: {
            type: "string",
        },
        block_id: {
            type: "string",
        },
        exceptional: {
            type: "integer",
        },
        min_stop_time: {
            type: "string",
        },
        max_stop_time: {
            type: "string",
        },
        start_timestamp: {
            type: "string",
        },
        end_timestamp: {
            type: "string",
        },
        first_stop_id: {
            type: "string",
        },
        last_stop_id: {
            type: "string",
        },
        origin_route_name: {
            type: "string",
        },
    },
    required: ["route_type"],
};

export const RopidGTFSPrecomputed = {
    servicesCalendar: {
        outputJsonSchema: outputServicesCalendarJsonSchema,
        outputSequelizeAttributes: outputServicesCalendarSDMA,
        modelIndexOptions: servicesCalendarConstraintsMIO,
        pgTableName: "ropidgtfs_precomputed_services_calendar",
    },
    minMaxStopSequences: {
        outputJsonSchema: outputMinMaxStopSequencesJsonSchema,
        outputSequelizeAttributes: outputMinMaxStopSequencesSDMA,
        modelIndexOptions: minMaxStopSequencesConstraintsMIO,
        pgTableName: "ropidgtfs_precomputed_minmax_stop_sequences",
    },
    tripSchedule: {
        outputJsonSchema: outputTripScheduleJsonSchema,
        outputSequelizeAttributes: outputTripScheduleSDMA,
        modelIndexOptions: tripScheduleConstraintsMIO,
        pgTableName: "ropidgtfs_precomputed_trip_schedule",
    },
};
