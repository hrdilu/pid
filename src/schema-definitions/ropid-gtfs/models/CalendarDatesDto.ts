import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { ICalendarDatesDto } from "../interfaces/ICalendarDatesDto";

export class CalendarDatesDto extends Model<CalendarDatesDto> implements ICalendarDatesDto {
    declare service_id: string;
    declare date: string;
    declare exception_type: number;

    public static attributeModel: ModelAttributes<CalendarDatesDto> = {
        service_id: { type: DataTypes.STRING, primaryKey: true },
        date: { type: DataTypes.STRING, primaryKey: true },
        exception_type: DataTypes.INTEGER,
    };

    public static jsonSchema: JSONSchemaType<ICalendarDatesDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                service_id: { type: "string" },
                date: { type: "string" },
                exception_type: { type: "integer" },
            },
            required: ["service_id", "date"],
        },
    };
}
