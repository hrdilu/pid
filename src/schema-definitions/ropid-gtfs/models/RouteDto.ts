import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";
import { IRouteDto } from "../interfaces/IRouteDto";

export class RouteDto extends Model<IRouteDto> implements IRouteDto {
    declare route_id: string;
    declare agency_id: string;
    declare is_night: string;
    declare is_regional: string;
    declare is_substitute_transport: string;
    declare route_color: string;
    declare route_desc: string;
    declare route_long_name: string;
    declare route_short_name: string;
    declare route_text_color: string;
    declare route_type: GTFSRouteTypeEnum;
    declare route_url: string;

    public static attributeModel: ModelAttributes<RouteDto> = {
        route_id: { type: DataTypes.STRING, primaryKey: true },
        agency_id: DataTypes.STRING,
        is_night: DataTypes.STRING,
        is_regional: DataTypes.STRING,
        is_substitute_transport: DataTypes.STRING,
        route_color: DataTypes.STRING,
        route_desc: DataTypes.STRING,
        route_long_name: DataTypes.STRING,
        route_short_name: DataTypes.STRING,
        route_text_color: DataTypes.STRING,
        route_type: {
            type: DataTypes.SMALLINT,
            allowNull: false,
        },
        route_url: DataTypes.STRING,
    };

    public static jsonSchema: JSONSchemaType<IRouteDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                route_id: { type: "string" },
                agency_id: { type: "string" },
                is_night: { type: "string" },
                is_regional: { type: "string" },
                is_substitute_transport: { type: "string" },
                route_color: { type: "string" },
                route_desc: { type: "string" },
                route_long_name: { type: "string" },
                route_short_name: { type: "string" },
                route_text_color: { type: "string" },
                route_type: { type: "integer" },
                route_url: { type: "string" },
            },
            required: ["route_id", "route_type"],
        },
    };
}
