import { Point } from "@golemio/core/dist/shared/geojson";

export interface IMetroRailtrackGPSModel {
    track_id: string;
    route_name: string;
    coordinates: Point;
    gtfs_stop_id: string | null;
}
