import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IAgencyDto } from "../interfaces/IAgencyDto";

export class AgencyDto extends Model<IAgencyDto> implements IAgencyDto {
    declare agency_email: string;
    declare agency_fare_url: string;
    declare agency_id: string;
    declare agency_lang: string;
    declare agency_name: string;
    declare agency_phone: string;
    declare agency_timezone: string;
    declare agency_url: string;

    public static attributeModel: ModelAttributes<AgencyDto> = {
        agency_email: DataTypes.STRING,
        agency_fare_url: DataTypes.STRING,
        agency_id: { type: DataTypes.STRING, primaryKey: true },
        agency_lang: DataTypes.STRING,
        agency_name: DataTypes.STRING,
        agency_phone: DataTypes.STRING,
        agency_timezone: DataTypes.STRING,
        agency_url: DataTypes.STRING,
    };

    public static jsonSchema: JSONSchemaType<IAgencyDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                agency_email: { type: "string" },
                agency_fare_url: { type: "string" },
                agency_id: { type: "string" },
                agency_lang: { type: "string" },
                agency_name: { type: "string" },
                agency_phone: { type: "string" },
                agency_timezone: { type: "string" },
                agency_url: { type: "string" },
            },
            required: ["agency_id"],
        },
    };
}
