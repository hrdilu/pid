import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes, ModelIndexesOptions } from "@golemio/core/dist/shared/sequelize";
import { IStopDto } from "../interfaces/IStopDto";

export class StopDto extends Model<IStopDto> implements IStopDto {
    declare stop_id: string;
    declare level_id: string;
    declare location_type: number;
    declare parent_station: string;
    declare platform_code: string;
    declare stop_code: string;
    declare stop_desc: string;
    declare stop_lat: number;
    declare stop_lon: number;
    declare stop_name: string;
    declare stop_timezone: string;
    declare stop_url: string;
    declare wheelchair_boarding: number;
    declare zone_id: string;
    declare asw_node_id: number;
    declare asw_stop_id: number;

    public static attributeModel: ModelAttributes<StopDto> = {
        stop_id: { type: DataTypes.STRING, primaryKey: true },
        level_id: DataTypes.STRING,
        location_type: DataTypes.INTEGER,
        parent_station: DataTypes.STRING,
        platform_code: DataTypes.STRING,
        stop_code: DataTypes.STRING,
        stop_desc: DataTypes.STRING,
        stop_lat: DataTypes.DOUBLE,
        stop_lon: DataTypes.DOUBLE,
        stop_name: DataTypes.STRING,
        stop_timezone: DataTypes.STRING,
        stop_url: DataTypes.STRING,
        wheelchair_boarding: DataTypes.INTEGER,
        zone_id: DataTypes.STRING,
        asw_node_id: DataTypes.INTEGER,
        asw_stop_id: DataTypes.INTEGER,
    };

    public static indexesOptions: ModelIndexesOptions[] = [
        {
            unique: false,
            using: "BTREE",
            name: "ropidgtfs_stops_stop_name_idx",
            fields: ["stop_name"],
        },
        {
            unique: false,
            using: "BTREE",
            name: "ropidgtfs_stops_location_type_idx",
            fields: ["location_type"],
        },
        {
            unique: false,
            using: "BTREE",
            name: "ropidgtfs_stops_asw_idx",
            fields: ["asw_node_id", "asw_stop_id"],
        },
    ];

    public static jsonSchema: JSONSchemaType<IStopDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                stop_id: { type: "string" },
                level_id: { type: "string" },
                location_type: { type: "integer" },
                parent_station: { type: "string" },
                platform_code: { type: "string" },
                stop_code: { type: "string" },
                stop_desc: { type: "string" },
                stop_lat: { type: "number" },
                stop_lon: { type: "number" },
                stop_name: { type: "string" },
                stop_timezone: { type: "string" },
                stop_url: { type: "string" },
                wheelchair_boarding: { type: "integer" },
                zone_id: { type: "string" },
                asw_node_id: { type: "integer" },
                asw_stop_id: { type: "integer" },
            },
            required: ["stop_id"],
        },
    };
}
