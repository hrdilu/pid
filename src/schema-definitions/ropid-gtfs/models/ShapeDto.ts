import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IShapeDto } from "../interfaces/IShapeDto";

export class ShapeDto extends Model<IShapeDto> implements IShapeDto {
    declare shape_id: string;
    declare shape_pt_sequence: number;
    declare shape_dist_traveled: number;
    declare shape_pt_lat: number;
    declare shape_pt_lon: number;

    public static attributeModel: ModelAttributes<ShapeDto> = {
        shape_id: { type: DataTypes.STRING, primaryKey: true },
        shape_pt_sequence: { type: DataTypes.INTEGER, primaryKey: true },
        shape_dist_traveled: DataTypes.DOUBLE,
        shape_pt_lat: DataTypes.DOUBLE,
        shape_pt_lon: DataTypes.DOUBLE,
    };

    public static jsonSchema: JSONSchemaType<IShapeDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                shape_id: { type: "string" },
                shape_pt_sequence: { type: "integer" },
                shape_dist_traveled: { type: "number" },
                shape_pt_lat: { type: "number" },
                shape_pt_lon: { type: "number" },
            },
            required: ["shape_id", "shape_pt_sequence"],
        },
    };
}
