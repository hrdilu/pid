import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes, ModelIndexesOptions } from "@golemio/core/dist/shared/sequelize";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";
import { IDeparturesModel } from "./interfaces/IDeparturesModel";

export class DeparturesModel extends Model<DeparturesModel> implements IDeparturesModel {
    public static TABLE_NAME = "ropidgtfs_precomputed_departures";

    declare stop_sequence: number;
    declare stop_headsign: string | null;
    declare pickup_type: string | null;
    declare drop_off_type: string | null;
    declare arrival_time: string | null;
    declare arrival_datetime: Date | null;
    declare departure_time: string | null;
    declare departure_datetime: Date | null;
    declare stop_id: string;
    declare stop_name: string | null;
    declare platform_code: string | null;
    declare wheelchair_boarding: number | null;
    declare min_stop_sequence: number | null;
    declare max_stop_sequence: number | null;
    declare trip_id: string;
    declare trip_headsign: string;
    declare trip_short_name: string | null;
    declare wheelchair_accessible: number | null;
    declare service_id: string | null;
    declare date: string | null;
    declare route_short_name: string | null;
    declare route_type: GTFSRouteTypeEnum;
    declare route_id: string | null;
    declare is_night: string | null;
    declare is_regional: string | null;
    declare is_substitute_transport: string | null;
    declare next_stop_sequence: number | null;
    declare next_stop_id: string | null;
    declare last_stop_sequence: number | null;
    declare last_stop_id: string | null;

    public static attributeModel: ModelAttributes<DeparturesModel> = {
        stop_sequence: {
            type: DataTypes.SMALLINT,
            allowNull: false,
        },
        stop_headsign: DataTypes.STRING(70),
        pickup_type: DataTypes.CHAR(1),
        drop_off_type: DataTypes.CHAR(1),
        arrival_time: DataTypes.STRING(20),
        arrival_datetime: DataTypes.DATE,
        departure_time: DataTypes.STRING(20),
        departure_datetime: DataTypes.DATE,
        stop_id: {
            type: DataTypes.STRING(25),
            allowNull: false,
        },
        stop_name: DataTypes.STRING(100),
        platform_code: DataTypes.STRING(10),
        wheelchair_boarding: DataTypes.SMALLINT,
        min_stop_sequence: DataTypes.SMALLINT,
        max_stop_sequence: DataTypes.SMALLINT,
        trip_id: {
            type: DataTypes.STRING(50),
            allowNull: false,
        },
        trip_headsign: {
            type: DataTypes.STRING(100),
            allowNull: false,
        },
        trip_short_name: DataTypes.STRING(50),
        wheelchair_accessible: DataTypes.SMALLINT,
        service_id: DataTypes.STRING(30),
        date: DataTypes.DATE,
        route_short_name: DataTypes.STRING(50),
        route_type: {
            type: DataTypes.SMALLINT,
            allowNull: false,
            defaultValue: GTFSRouteTypeEnum.EXT_MISCELLANEOUS,
        },
        route_id: DataTypes.STRING(20),
        is_night: DataTypes.CHAR(1),
        is_regional: DataTypes.CHAR(1),
        is_substitute_transport: DataTypes.CHAR(1),
        next_stop_sequence: DataTypes.SMALLINT,
        next_stop_id: DataTypes.STRING(30),
        last_stop_sequence: DataTypes.SMALLINT,
        last_stop_id: DataTypes.STRING(30),
    };

    public static indexesOptions: ModelIndexesOptions[] = [
        {
            unique: false,
            using: "BTREE",
            name: "ropidgtfs_precomputed_departures_stop_id_idx",
            fields: ["stop_id"],
        },
        {
            unique: true,
            using: "BTREE",
            name: "ropidgtfs_precomputed_departures_unique_idx",
            fields: ["date", "trip_id", "stop_sequence"],
        },
    ];

    public static jsonSchema: JSONSchemaType<IDeparturesModel> = {
        type: "object",
        properties: {
            stop_sequence: {
                type: "integer",
            },
            stop_headsign: {
                type: "string",
            },
            pickup_type: {
                type: "string",
            },
            drop_off_type: {
                type: "string",
            },
            arrival_time: {
                type: "string",
            },
            arrival_datetime: {
                type: "object",
                required: ["toISOString"],
            },
            departure_time: {
                type: "string",
            },
            departure_datetime: {
                type: "object",
                required: ["toISOString"],
            },
            stop_id: {
                type: "string",
            },
            stop_name: {
                type: "string",
            },
            platform_code: {
                type: "string",
            },
            wheelchair_boarding: {
                type: "integer",
            },
            min_stop_sequence: {
                type: "integer",
            },
            max_stop_sequence: {
                type: "integer",
            },
            trip_id: {
                type: "string",
            },
            trip_headsign: {
                type: "string",
            },
            trip_short_name: {
                type: "string",
            },
            wheelchair_accessible: {
                type: "integer",
            },
            service_id: {
                type: "string",
            },
            date: {
                type: "string",
            },
            route_short_name: {
                type: "string",
            },
            route_type: {
                type: "integer",
            },
            route_id: {
                type: "string",
            },
            is_night: {
                type: "string",
            },
            is_regional: {
                type: "string",
            },
            is_substitute_transport: {
                type: "string",
            },
            next_stop_sequence: {
                type: "integer",
            },
            next_stop_id: {
                type: "string",
            },
            last_stop_sequence: {
                type: "integer",
            },
            last_stop_id: {
                type: "string",
            },
        },
        required: ["stop_sequence", "stop_id", "trip_id", "trip_headsign"],
    };
}
