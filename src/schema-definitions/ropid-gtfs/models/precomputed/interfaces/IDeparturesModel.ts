import { GtfsStopWheelchairBoardingEnum, GtfsTripWheelchairAccessEnum } from "#helpers/AccessibilityEnums";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";

export interface IDeparturesModel {
    stop_sequence: number;
    stop_headsign: string | null;
    pickup_type: string | null;
    drop_off_type: string | null;
    arrival_time: string | null;
    arrival_datetime: Date | null;
    departure_time: string | null;
    departure_datetime: Date | null;
    stop_id: string;
    stop_name: string | null;
    platform_code: string | null;
    wheelchair_boarding: GtfsStopWheelchairBoardingEnum | null;
    min_stop_sequence: number | null;
    max_stop_sequence: number | null;
    trip_id: string;
    trip_headsign: string;
    trip_short_name: string | null;
    wheelchair_accessible: GtfsTripWheelchairAccessEnum | null;
    service_id: string | null;
    date: string | null;
    route_short_name: string | null;
    route_type: GTFSRouteTypeEnum;
    route_id: string | null;
    is_night: string | null;
    is_regional: string | null;
    is_substitute_transport: string | null;
    next_stop_sequence: number | null;
    next_stop_id: string | null;
    last_stop_sequence: number | null;
    last_stop_id: string | null;
}
