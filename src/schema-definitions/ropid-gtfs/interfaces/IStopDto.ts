export interface IStopDto {
    stop_id: string;
    level_id: string;
    location_type: number;
    parent_station: string;
    platform_code: string;
    stop_code: string;
    stop_desc: string;
    stop_lat: number;
    stop_lon: number;
    stop_name: string;
    stop_timezone: string;
    stop_url: string;
    wheelchair_boarding: number;
    zone_id: string;
    asw_node_id: number;
    asw_stop_id: number;
}
