export interface IPresetLogProcessedDto {
    device_alias: string;
    received_at: string;
}
