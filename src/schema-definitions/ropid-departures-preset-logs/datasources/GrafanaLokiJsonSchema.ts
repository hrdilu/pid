import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { ILogQueryData } from "./interfaces/ILogQueryData";

export const grafanaLokiJsonSchema: JSONSchemaType<ILogQueryData[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            stream: {
                type: "object",
                nullable: true,
                properties: {
                    level: { type: "string" },
                    message: { type: "string" },
                    req_httpVersion: { type: "string" },
                    req_method: { type: "string" },
                    req_remoteAddress: { type: "string" },
                    req_url: { type: "string" },
                    req_userAgent: { type: "string" },
                    res_contentLength: { type: "string", nullable: true },
                    res_status: { type: "string" },
                    responseTime: { type: "string" },
                },
                required: [
                    "level",
                    "message",
                    "req_httpVersion",
                    "req_method",
                    "req_remoteAddress",
                    "req_url",
                    "req_userAgent",
                    "res_status",
                    "responseTime",
                ],
            },
            values: {
                type: "array",
                nullable: true,
                items: {
                    type: "array",
                    items: [{ type: "string" }, { type: "string" }],
                    minItems: 2,
                    maxItems: 2,
                },
            },
        },
    },
};
