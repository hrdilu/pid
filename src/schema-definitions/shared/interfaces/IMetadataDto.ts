export interface IMetadataDto {
    dataset: string;
    key: string;
    type: string;
    value: number | string;
    version: number;
}
