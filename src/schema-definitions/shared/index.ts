// TODO asi později přesunout
export interface IRopidSchemaDefinitions {
    name: string;
    metadata: {
        name: string;
        pgTableName: string;
    };
    [key: string]: any;
}
