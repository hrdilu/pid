const datasourceJsonSchema = {
    // reusable definitions
    definitions: {
        spojObj: {
            type: "object",
            properties: {
                $: {
                    title: "$",
                    type: "object",
                    properties: {
                        alias: { type: "string" },
                        azimut: { type: "string" },
                        cpoz: { type: "string" },
                        dopr: { type: "string" },
                        doprSkut: { type: "string" },
                        info: { type: "string" },
                        kmenl: { type: "string" },
                        lat: { type: "string" },
                        lin: { type: "string" },
                        lng: { type: "string" },
                        np: { type: "string" },
                        po: { type: "string" },
                        rychl: { type: "string" },
                        sled: { type: "string" },
                        spoj: { type: "string" },
                        t: { type: "string" },
                        vuzevc: { type: "string" },
                        zast: { type: "string" },
                        zpoz_prij: { type: "string" },
                        zrus: { type: "string" },
                    },
                    required: [],
                },
                zast: {
                    type: "array",
                    items: {
                        title: "itemOf_zast",
                        type: "object",
                        properties: {
                            $: {
                                title: "$",
                                type: "object",
                                properties: {
                                    odj: { type: "string" },
                                    prij: { type: "string" },
                                    stan: { type: "string" },
                                    zast: { type: "string" },
                                    asw: { type: "string" },
                                    zpoz_odj: { type: "string" },
                                    zpoz_prij: { type: "string" },
                                    zpoz_typ: { type: "string" },
                                    zpoz_typ_odj: { type: "string" },
                                    zpoz_typ_prij: { type: "string" },
                                },
                                required: [],
                            },
                        },
                    },
                },
            },
        },
    },

    // main schema
    type: "object",
    properties: {
        m: {
            title: "m",
            type: "object",
            properties: {
                spoj: {
                    oneOf: [
                        // property `spoj` can be object or array of objects
                        {
                            type: "array",
                            items: { $ref: "#/definitions/spojObj" }, // reference to definition
                        },
                        {
                            $ref: "#/definitions/spojObj", // reference to definition
                        },
                    ],
                },
            },
        },
    },
};

const forExport = {
    datasourceJsonSchema,
    name: "VehiclePositions",
    positions: {
        name: "VehiclePositionsPositions",
        pgTableName: "vehiclepositions_positions",
        pgTableNameHistory: "vehiclepositions_positions_history",
    },
    vehicleTypes: {
        name: "VehiclePositionsVehicleTypes",
        pgTableName: "vehiclepositions_vehicle_types",
    },
};

export { forExport as VehiclePositions };
