import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { ITripStopsCacheDto } from "../interfaces/ITripStopsCacheDto";

// @ts-ignore bcs of missing required statement for pattern properties
const tripStopsCacheDtoSchema: JSONSchemaType<ITripStopsCacheDto> = {
    $schema: "http://json-schema.org/draft-04/schema#",
    title: "IGtfsTripStopsCacheDto",
    type: "object",
    patternProperties: {
        "^.*": {
            type: "object",
            properties: {
                trip_stops: {
                    type: "array",
                    items: {
                        type: "object",
                        properties: {
                            stop_id: {
                                type: "string",
                            },
                            stop_sequence: {
                                type: "number",
                            },
                            cis: {
                                oneOf: [{ type: "number" }, { type: "null", nullable: true }],
                            },
                        },
                        required: ["stop_id", "stop_sequence", "cis"],
                    },
                },
            },
            required: ["trip_stops"],
        },
    },
};

export { tripStopsCacheDtoSchema as TripStopsCacheDtoSchema };
