import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IGtfsRunTripCacheDto } from "../interfaces/IGtfsRunTripCacheDto";

const gtfsRunTripCacheDtoSchema: JSONSchemaType<IGtfsRunTripCacheDto> = {
    $schema: "http://json-schema.org/draft-04/schema#",
    title: "IGtfsRunTripCacheDto",
    type: "object",
    properties: {
        schedule: {
            type: "array",
            items: {
                type: "object",
                properties: {
                    trip_id: { type: "string" },
                    service_id: { type: "string" },
                    date: { type: "string" },
                    route_id: { type: "string" },
                    route_type: { type: "integer" },
                    route_short_name: { type: "string" },
                    is_regional: { type: "string" },
                    is_substitute_transport: { type: "string" },
                    is_night: { type: "string" },
                    trip_headsign: { type: "string" },
                    trip_short_name: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                    block_id: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                    exceptional: { type: "number" },
                    min_stop_time: {
                        type: "object",
                        properties: { hours: { type: "number" }, minutes: { type: "number", nullable: true } },
                        required: ["hours"],
                    },
                    max_stop_time: {
                        type: "object",
                        properties: { hours: { type: "number" }, minutes: { type: "number", nullable: true } },
                        required: ["hours"],
                    },
                    start_timestamp: { type: "string" },
                    end_timestamp: { type: "string" },
                    first_stop_id: { type: "string" },
                    last_stop_id: { type: "string" },
                    requiredTurnaroundSeconds: { type: "number", nullable: true },
                },
                required: [
                    "trip_id",
                    "service_id",
                    "date",
                    "route_id",
                    "route_type",
                    "route_short_name",
                    "is_regional",
                    "is_substitute_transport",
                    "is_night",
                    "trip_headsign",
                    "block_id",
                    "exceptional",
                    "min_stop_time",
                    "max_stop_time",
                    "start_timestamp",
                    "end_timestamp",
                    "first_stop_id",
                    "last_stop_id",
                ],
            },
        },
    },
    required: ["schedule"],
};
export { gtfsRunTripCacheDtoSchema as GtfsRunTripCacheDtoSchema };
