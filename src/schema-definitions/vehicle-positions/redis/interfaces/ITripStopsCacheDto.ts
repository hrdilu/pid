export interface ITripStopsCacheDto {
    [key: string]: {
        trip_stops: Array<{
            stop_id: string;
            stop_sequence: number;
            cis: number | null;
        }>;
    };
}
