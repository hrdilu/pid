export interface IStopTimeDelayPredictionDto {
    trip_id: string;
    stop_sequence: number;
    stop_id: string;
    arrival_delay_seconds: number | null;
    departure_delay_seconds: number | null;
}
