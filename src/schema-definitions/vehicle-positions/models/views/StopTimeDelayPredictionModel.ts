import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IStopTimeDelayPredictionDto } from "./interfaces/IStopTimeDelayPredictionDto";

export class StopTimeDelayPredictionModel extends Model<StopTimeDelayPredictionModel> implements IStopTimeDelayPredictionDto {
    public static tableName = "v_vehiclepositions_stop_time_delay_prediction";

    declare trip_id: string;
    declare stop_sequence: number;
    declare stop_id: string;
    declare arrival_delay_seconds: number | null;
    declare departure_delay_seconds: number | null;

    public static attributeModel: ModelAttributes<StopTimeDelayPredictionModel> = {
        trip_id: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true,
        },
        stop_sequence: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
        },
        stop_id: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        arrival_delay_seconds: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        departure_delay_seconds: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
    };
}
