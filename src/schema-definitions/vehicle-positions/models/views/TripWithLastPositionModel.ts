import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { PositionTrackingEnum, StatePositionEnum } from "src/const";
import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";
import { IDescriptorOutputDto } from "../../../vehicle-descriptors/models/interfaces/IDescriptorOutputDto";
import { IVehicleTypeDto } from "../interfaces/IVehicleTypeDto";
import { IProcessedPositionDto } from "./interfaces/IProcessedPositionDto";
import { ITripWithLastPositionDto } from "./interfaces/ITripWithLastPositionDto";

export class TripWithLastPositionModel extends Model<TripWithLastPositionModel> implements ITripWithLastPositionDto {
    public static tableName = "v_vehiclepositions_all_trips_with_last_position";

    declare id: string;
    declare agency_name_real: string;
    declare agency_name_scheduled: string;
    declare cis_line_id: string;
    declare cis_trip_number: number;
    declare gtfs_route_id: string;
    declare gtfs_route_short_name: string;
    declare gtfs_route_type: GTFSRouteTypeEnum;
    declare gtfs_trip_headsign: string;
    declare gtfs_trip_short_name: string;
    declare gtfs_trip_id: string | null;
    declare origin_route_name: string;
    declare run_number: number;
    declare vehicle_registration_number: number;
    declare vehicle_type_id: number;
    declare wheelchair_accessible: boolean;
    declare updated_at: Date;
    declare start_timestamp_isostring: string;
    declare bearing: number;
    declare delay: number;
    declare delay_stop_arrival: number;
    declare delay_stop_departure: number;
    declare is_canceled: boolean;
    declare last_stop_id: string;
    declare last_stop_sequence: number;
    declare last_stop_headsign: string;
    declare lat: number;
    declare lng: number;
    declare next_stop_id: string;
    declare next_stop_sequence: number;
    declare shape_dist_traveled: number;
    declare speed: number;
    declare state_position: StatePositionEnum;
    declare tracking: PositionTrackingEnum;
    declare last_stop_arrival_time_isostring: string;
    declare last_stop_departure_time_isostring: string;
    declare next_stop_arrival_time_isostring: string;
    declare next_stop_departure_time_isostring: string;
    declare origin_timestamp_isostring: string;

    declare readonly all_positions?: IProcessedPositionDto[];
    declare readonly vehicle_type?: IVehicleTypeDto;
    declare readonly vehicle_descriptor?: Pick<IDescriptorOutputDto, "is_air_conditioned">;

    public static attributeModel: ModelAttributes<TripWithLastPositionModel, ITripWithLastPositionDto> = {
        id: {
            type: DataTypes.STRING,
            primaryKey: true,
        },
        agency_name_real: DataTypes.STRING,
        agency_name_scheduled: DataTypes.STRING,
        cis_line_id: DataTypes.STRING,
        cis_trip_number: DataTypes.INTEGER,
        gtfs_route_id: DataTypes.STRING,
        gtfs_route_short_name: DataTypes.STRING,
        gtfs_route_type: DataTypes.INTEGER,
        gtfs_trip_headsign: DataTypes.STRING,
        gtfs_trip_short_name: DataTypes.STRING,
        gtfs_trip_id: {
            type: DataTypes.STRING,
            primaryKey: true,
        },
        origin_route_name: DataTypes.STRING,
        run_number: DataTypes.INTEGER,
        vehicle_registration_number: DataTypes.INTEGER,
        vehicle_type_id: DataTypes.INTEGER,
        wheelchair_accessible: DataTypes.BOOLEAN,
        updated_at: DataTypes.DATE,
        start_timestamp_isostring: DataTypes.STRING,
        bearing: DataTypes.INTEGER,
        delay: DataTypes.INTEGER,
        delay_stop_arrival: DataTypes.INTEGER,
        delay_stop_departure: DataTypes.INTEGER,
        is_canceled: DataTypes.BOOLEAN,
        last_stop_id: DataTypes.STRING,
        last_stop_sequence: DataTypes.INTEGER,
        last_stop_headsign: DataTypes.STRING,
        lat: DataTypes.FLOAT,
        lng: DataTypes.FLOAT,
        next_stop_id: DataTypes.STRING,
        next_stop_sequence: DataTypes.INTEGER,
        shape_dist_traveled: DataTypes.FLOAT,
        speed: DataTypes.INTEGER,
        state_position: DataTypes.STRING,
        tracking: DataTypes.INTEGER,
        last_stop_arrival_time_isostring: DataTypes.STRING,
        last_stop_departure_time_isostring: DataTypes.STRING,
        next_stop_arrival_time_isostring: DataTypes.STRING,
        next_stop_departure_time_isostring: DataTypes.STRING,
        origin_timestamp_isostring: DataTypes.STRING,
    };
}
