import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { ICommonRunsModel } from "./interfaces/ICommonRunsModel";

export class CommonRunsModel extends Model<CommonRunsModel> implements ICommonRunsModel {
    public static TABLE_NAME = "vehiclepositions_runs";

    declare id: string; // - "route_id"_"run_number"_"registration_number"
    declare route_id: string;
    declare run_number: string;
    declare line_short_name: string;
    declare registration_number: string;
    declare msg_start_timestamp: string;
    declare msg_last_timestamp: string;
    declare wheelchair_accessible: boolean;

    public static attributeModel: ModelAttributes<CommonRunsModel> = {
        id: {
            primaryKey: true,
            type: DataTypes.STRING,
        },
        route_id: { type: DataTypes.STRING },
        run_number: { type: DataTypes.INTEGER },
        line_short_name: DataTypes.STRING,
        registration_number: { type: DataTypes.STRING },
        wheelchair_accessible: { type: DataTypes.BOOLEAN },
        msg_start_timestamp: { type: DataTypes.DATE },
        msg_last_timestamp: { type: DataTypes.DATE },
    };

    public static jsonSchema: JSONSchemaType<ICommonRunsModel> = {
        type: "object",
        properties: {
            id: {
                type: "string",
            },
            route_id: {
                type: "string",
            },
            run_number: {
                type: "string",
            },
            line_short_name: {
                type: "string",
            },
            registration_number: {
                type: "string",
            },
            wheelchair_accessible: {
                type: "boolean",
            },
            msg_start_timestamp: {
                type: "string",
            },
            msg_last_timestamp: {
                type: "string",
            },
        },
        required: [
            "id",
            "route_id",
            "run_number",
            "line_short_name",
            "registration_number",
            "wheelchair_accessible",
            "msg_start_timestamp",
            "msg_last_timestamp",
        ],
    };
}
