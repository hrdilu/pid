import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import Sequelize, { Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IVehicleTypeDto } from "./interfaces/IVehicleTypeDto";

export class VehicleTypeDto extends Model<IVehicleTypeDto> implements IVehicleTypeDto {
    declare description_cs: string;
    declare description_en: string;
    declare id: number;

    public static attributeModel: ModelAttributes<VehicleTypeDto> = {
        description_cs: Sequelize.STRING,
        description_en: Sequelize.STRING,
        id: {
            primaryKey: true,
            type: Sequelize.BIGINT,
        },
    };

    public static jsonSchemaModel: JSONSchemaType<IVehicleTypeDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                description_cs: { type: "string" },
                description_en: { type: "string" },
                id: { type: "number" },
            },
            required: ["description_cs", "description_en", "id"],
        },
    };
}
