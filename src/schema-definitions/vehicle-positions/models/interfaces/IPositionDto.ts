import { PositionTrackingEnum, StatePositionEnum, StateProcessEnum, TCPEventEnum } from "src/const";

export interface IPositionDto {
    id: string;
    asw_last_stop_id: string | null;
    bearing: number | null;
    cis_last_stop_id: number | null;
    cis_last_stop_sequence: number | null;
    delay: number;
    delay_stop_arrival: number | null;
    delay_stop_departure: number | null;
    is_canceled: boolean;
    last_stop_arrival_time: Date;
    last_stop_departure_time: Date;
    last_stop_id: string;
    last_stop_sequence: number;
    lat: number | null;
    lng: number | null;
    next_stop_arrival_time: Date;
    next_stop_departure_time: Date;
    next_stop_id: string;
    next_stop_sequence: number;
    origin_time: string;
    origin_timestamp: Date | string;
    shape_dist_traveled: number;
    speed: number | null;
    state_position: StatePositionEnum;
    state_process: StateProcessEnum;
    this_stop_id: string | null;
    this_stop_sequence: number | null;
    tracking: PositionTrackingEnum | null;
    trips_id: string | null;
    tcp_event: TCPEventEnum | null;
    last_stop_headsign: string | null;
    last_stop_name: string | null;
    valid_to: Date;
    scheduled_timestamp: Date | null;
}
