export interface IVehicleTypeDto {
    description_cs: string;
    description_en: string;
    id: number;
}
