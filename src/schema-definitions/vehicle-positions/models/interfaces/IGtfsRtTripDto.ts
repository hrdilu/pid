import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";

export interface IGtfsRtTripDto {
    last_position: {
        is_canceled: boolean | null;
        origin_timestamp: Date;
        last_stop_sequence: number | null;
        bearing: number | null;
        lat: number;
        lng: number;
        speed: number | null;
    };
    stop_times: Array<{
        stop_id: string;
        stop_sequence: number;
        arrival_delay_seconds: number | null;
        departure_delay_seconds: number | null;
    }>;
    id: string;
    run_number: number | null;
    start_timestamp: string;
    cis_line_id: string | null;
    cis_trip_number: number | null;
    start_time: string | null;
    vehicle_registration_number: string | null;
    gtfs_trip_id: string;
    gtfs_route_id: string | null;
    gtfs_route_type: GTFSRouteTypeEnum;
    gtfs_route_short_name: string | null;
    wheelchair_accessible: boolean | null;
    vehicle_descriptor?: {
        is_air_conditioned: boolean | null;
    };
}
