import { StatePositionEnum, StateProcessEnum, TCPEventEnum } from "src/const";

export interface IVPTripsPositionAttributes {
    bearing: number | null;
    lat: number;
    lng: number;
    origin_time: string;
    origin_timestamp: Date;
    scheduled_timestamp: Date | null;
    delay: number | null;
    tracking: number;
    id: string;
    shape_dist_traveled: number | null;
    is_canceled: boolean;
    state_position: StatePositionEnum;
    state_process: StateProcessEnum;
    tcp_event: TCPEventEnum | null;
    this_stop_sequence: number | null;
    this_stop_id: string | null;
    last_stop_sequence: number | null;
    last_stop_id: string | null;
    last_stop_name: string | null;
    last_stop_arrival_time: Date | null;
    last_stop_departure_time: Date | null;
    next_stop_sequence: number | null;
    next_stop_id: string | null;
    next_stop_name: string | null;
    next_stop_arrival_time: Date | null;
    next_stop_departure_time: Date | null;
    valid_to: Date | null;
}
