import { GTFSRouteTypeEnum } from "src/helpers/RouteTypeEnums";
import { IVPTripsLastPositionContext } from "./VPTripsLastPositionInterfaces";

export interface IVPTripsModel {
    cis_line_id: string | null;
    cis_trip_number: number | null;
    run_number: number | null;
    internal_run_number: number | null;
    cis_line_short_name: string | null;
    gtfs_route_id: string | null;
    gtfs_route_short_name: string | null;
    gtfs_trip_id: string | null;
    gtfs_date: string | null;
    id: string;
    start_cis_stop_id: number | null;
    start_cis_stop_platform_code: string | null;
    start_time: string | null;
    start_timestamp: Date | null;
    vehicle_type_id: number | null;
    wheelchair_accessible: boolean | null;
    agency_name_scheduled: string | null;
    origin_route_name: string | null;
    internal_route_name: string | null;
    agency_name_real: string | null;
    vehicle_registration_number: number | null;
    gtfs_trip_headsign: string | null;
    start_asw_stop_id: string | null;
    gtfs_route_type: GTFSRouteTypeEnum;
    gtfs_block_id: string | null;
    last_position_id: number | null;
    is_canceled: boolean | null;
    end_timestamp: Date | null;
    gtfs_trip_short_name: string | null;
    last_position_context: IVPTripsLastPositionContext | null;
    provider_source_type: string;
}

// Helper type with IVPTripsModel's non-nullable properties
export type VPTripsNonNullableModel = {
    [K in keyof IVPTripsModel]: NonNullable<IVPTripsModel[K]>;
};
