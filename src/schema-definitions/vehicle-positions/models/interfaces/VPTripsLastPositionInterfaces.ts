import { Feature, Point } from "@turf/turf";
import { IVPTripsPositionAttributes } from "./IVPTripsPositionAttributes";

export interface IVPTripsComputedPositionAtStopStreak {
    firstPositionTimestamp: number | null;
    firstPositionDelay: number | null;
    stop_sequence: number | null;
}

export interface IVPTripsLastPositionContext {
    lastPositionId: string | null;
    lastPositionOriginTimestamp: number | null;
    lastPositionTracking: Feature<Point, IVPTripsPositionAttributes> | null;
    lastPositionCanceled: boolean | null;
    lastPositionLastStop: {
        id: string | null;
        sequence: number | null;
        arrival_time: number | null;
        arrival_delay: number | null;
        departure_time: number | null;
        departure_delay: number | null;
    };
    lastPositionDelay: number | null;
    atStopStreak: IVPTripsComputedPositionAtStopStreak;
    lastPositionBeforeTrackDelayed: {
        delay: number;
        origin_timestamp: number;
    } | null;
    isLastPositionAfterTrack: boolean;
    tripId: string;
}
