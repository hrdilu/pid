// Import for side effects only (Sequelize types)
import "@golemio/core/dist/shared/sequelize";

import { outputPresetsJsonSchema } from "./RopidDeparturesPresetsOutputSchemas";
import { IRopidDeparturesPresetsOutput, outputPresetsSDMA } from "./RopidDeparturesPresetsSDMA";

interface IRopidDeparturesPreset {
    routeName: string;
    apiVersion: number;
    route: string;
    query: string;
    note: string;
}

const datasourceJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: <Record<keyof IRopidDeparturesPreset, any>>{
            routeName: {
                type: "string",
                minLength: 1,
                maxLength: 100,
            },
            apiVersion: {
                type: "integer",
                minimum: 1,
                exclusiveMaximum: 100,
            },
            route: {
                minLength: 1,
                maxLength: 100,
            },
            query: {
                type: "string",
                minLength: 4,
                maxLength: 400,
            },
            note: {
                type: "string",
                maxLength: 1000,
            },
        },
    },
};

const forExport = {
    name: "RopidDeparturesPresets",
    datasourceJsonSchema,
    outputJsonSchema: outputPresetsJsonSchema,
    outputSequelizeAttributes: outputPresetsSDMA,
    pgTableName: "ropid_departures_presets",
};

export { forExport as RopidDeparturesPresets };
export type { IRopidDeparturesPreset, IRopidDeparturesPresetsOutput };
