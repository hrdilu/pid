# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

### Changed

-   retry when unable to download Ropid Gtfs data from ftp ([pid#291](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/291))

## [2.8.2] - 2023-07-26

-   Departure boards - metro trips are deemed accessible (`departure.trip.is_wheelchair_accessible`) only if the station is accessible ([pid#325](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/283))

## [2.8.1] - 2023-07-24

### Changed

-   Decrease TTL of the `vehiclepositionsgtfsrt.generateFiles` queue ([pid#276](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/276))
-   Update GTFS-RT openapi docs

## [2.8.0] - 2023-07-24

### Changed

-   Generate additional metro positions within a stricter time limit ([pid#265](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/265))
-   Optimize delay propagation logic

### Fixed

-   Opentelemetry child span initialization on core bump- Opentelemetry child span initialization on core bump

### Removed

-   Duplicate interface for GTFS run trip schedule

## [2.7.12] - 2023-07-20

### Fixed

-   Change column ordering in `ropid_departures_presets` to prevent errors when inserting data from the tmp table

## [2.7.11] - 2023-07-17

### Fixed

-   Recast the SQL function EXTRACT's return type to `integer`

## [2.7.10] - 2023-07-10

### Added

-   Collect preset logs and send them to the ROPID monitoring API ([pid#230](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/230))

## [2.7.9] - 2023-06-26

### Changed

-   Refactoring connectors ([core#17](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/17))

## [2.7.8] - 2023-06-14

### Added

-   Enable processing of trolleybus run messages ([pid#271](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/271))
-   GTFS-RT - combine `trip_updates.pb` and `vehicle_positions.pb` into a single feed ([pid#255](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/255))

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))
-   GTFS-RT - change entity.id of `vehicle_positions.pb` from GTFS trip id to realtime trip id
-   Improve API docs

## [2.7.7] - 2023-06-07

### Added

-   possiblity to upload departure presets trough input gateway([pid#247](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/247))

## [2.7.6] - 2023-06-05

### Fixed

-   Timestamp validation ([pid#263](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/263))

## [2.7.5] - 2023-05-31

### Changed

-   Order unprocessed positions by `created_at` instead of `origin_timestamp` to ensure the correct order of positions ([pid#259](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/259))
-   Refactor metro run timestamps ([pid#263](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/263))
-   Refactor not public trip (greasing tram) mappers
-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))
-   Increase `ropid_departures_presets.url_query_params` column size ([pid#266](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/266))

### Fixed

-   `delay.last_stop_departure` is null when a bus or tram is running on time

## [2.7.4] - 2023-05-29

### Fixed

-   `off_track` tram position processing

## [2.7.3] - 2023-05-24

### Changed

-   Replace bigint timestamps with timestamptz/date objects/iso strings wherever possible ([pid#263](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/263))
-   Improve typings of RT data

## [2.7.2] - 2023-05-22

### Changed

-   Update GTFS trip data in the realtime `vehiclepositions_trips` feed after downloading GTFS static ([pid#82](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/82))
-   Always treat gtfs route type as an integer

## [2.7.1] - 2023-05-17

### Changed

-   GTFS checkForNewData - when forceRefresh is true, always refresh precomputed tables to ensure the most recent data is served to the API ([pid#270](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/270))

### Fixed

-   GTFS downloadDatasets - fix infinite loop when the FTP server is unreachable or the connection is unstable

## [2.7.0] - 2023-05-10

### Added

-   Create and utilize optimized views to serve data to the vehiclepositions output API ([pid#262](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/262))

### Changed

-   Rename the alerting view `v_vehiclepositions_last_position` to `v_vehiclepositions_alerts_last_position`

## [2.6.9] - 2023-05-03

### Changed

-   Adjust IG logger emitter imports

## [2.6.8] - 2023-04-26

### Added

-   Predict stop time delays in GTFS-RT trip_updates.pb ([pid#235](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/235))
-   Add `provider_source_type` column to the `vehiclepositions_trips` table

### Changed

-   Optimize db query for GTFS-RT trip data
-   Set limit for `/vehiclepositions` response with `includePositions=true` parameter ([pid#254](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/254))

### Fixed

-   Download datasets if the last deployment failed ([pid#248](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/248))

## [2.6.7] - 2023-04-19

### Changed

-   Optimize precomputed GTFS tables ([pid#251](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/251))
-   Properly clean cache after replacing GTFS tables instead of flushing all keys in the database

## [2.6.6] - 2023-04-12

### Added

-   Publish airConditioned info to GTFS ([pid#239](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/239))

### Fixed

-   propagate delay over midnight
-   Processing of NOT_PUBLIC run messages (trams without schedule) ([pid#232](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/232))

## [2.6.4] - 2023-04-05

### Changed

-   API docs - improve description of air condition switch ([pid#252](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/252))

## [2.6.3] - 2023-03-27

### Fixed

-   Fix TripHistory delete old data.
-   Fix http busses delay computation.

## [2.6.2] - 2023-03-27

### Added

-   Vehicle descriptor integration of Seznam Autobusu API ([pid#107](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/107))
-   Query param `airCondition` on `/pid/departureboards` endpoint
-   Introduce DI

### Changed

-   Enrich trips with air condition information ([pid#107](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/107))
-   Rename feature toggle for metro
-   Minor refactoring of VP router and repositories

### Fixed

-   `delay_stop_arrival` of `off_track` TCP tram position is `NaN` ([pid#214](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/214))
-   Fix showing delays for night lines. ([pid#240](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/240))
-   Fix presets loading ([pid#242](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/242))

## [2.6.1] - 2023-03-16

### Fixed

-   GTFS departure timestamp DB index typo

## [2.6.0] - 2023-03-15

### Added

-   `delay.last_stop_arrival` and `delay.last_stop_departure` calculation for metro trips ([pid#219](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/219))

### Changed

-   Change validations from Mongoose to JSON Schema
-   Decide this/last/next stops based on metro railtrack data instead of estimated anchor points
-   Correct route id/run number for HTTP trips ([pid#225](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/225))
-   Rename `sequence_id` VP trips column to `run_number`
-   Improve vehicle descriptor, improve metro id format ([pid#226](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/226))

## [2.5.3] - 2023-03-01

### Changed

-   Decide the `after_track` position state for TCP bus/tram runs based on scheduled timestamp (the `tjr` input attribute) ([pid#224](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/224))

### Fixed

-   Metro origin time

## [2.5.2] - 2023-02-27

### Changed

-   Replace some exceptions and warning logs that are of lesser importance with verbose logs ([pid#207](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/207))
-   Update Node.js to v18.14.0 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [2.5.1] - 2023-02-15

### Fixed

-   Fixed validation of preferredTimezone query param ([pid#216](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/216))

## [2.5.0] - 2023-02-13

### Added

-   Before track delay correction ([pid#199](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/199))
-   `delay.last_stop_departure` calculation for TCP (DPP) busses ([pid#186](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/186))

### Changed

-   Rename TCP event enum keys

## [2.4.2] - 2023-01-30

### Changed

-   fixed not copying delay from before_track_delayed positions known before for HTTP data by updating also last_position_context in VPTrip model
-   fixed not propagating delay for HTTP trips ([pid#190](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/190))
-   delay calculation for propagating delay for followup connection. New constant of 30 seconds was introduced which is estimated time to go trough turning point. ([pid#191](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/191))
-   vehicle is changed to state after track, after last stop ([pid#217](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/217))
-   Refactor VP worker methods into separate tasks, add message validation ([pid#176](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/176))

### Added

-   Feature toggle for hiding metro trips
-   Metro changed from udp to tcp

## [2.4.1] - 2023-01-23

### Changed

-   Update openapi docs

## [2.4.0] - 2023-01-23

### Added

-   Migrate to npm

## [2.3.7] - 2023-01-16

### Added

-   analyze is called for `ropidgtfs_departures` during update of schedules ([pid#215](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/215))

## [2.3.6] - 2023-01-04

### Changed

-   Utilize streams for GTFS datasets ([pid#209](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/209))
-   Replace `SET` with `SET LOCAL`, remove `SET search_path` from the `departure_part` SQL function
-   Enable processing of all metro messages ([pid#218](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/218))

## [2.3.5] - 2022-12-13

### Added

-   processing of run messages for trams without tjr ([pid#183](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/183))

### Changed

-   Store last position's context in `vehiclepositions_trips`, fetch only unprocessed positions ([pid#208](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/208))
-   Refactor schema definitions for VP trips, improve typings
-   Find stops directly in `ropigtfs_stops` with ASW ids ([pid#213](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/213))
-   Improve validation of aswIds and cisIds

## [2.3.4] - 2022-11-29

### Added

-   new schema analytic
-   new analytic view for departure boards `v_ropid_departure_boards`
-   Implementation documentation ([pid#96](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/96))

### Changed

-   added stopts for before_track trips into GTFSRt Feed ([pid#202](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/202))
-   added before_track trips into GTFSRt Feed ([pid#202](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/202))
-   Set message TTL on remaining `vehiclepositions.*` queues ([pid#204](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/204))

## [2.3.3] - 2022-11-10

### Added

-   data retention for tables vehiclepositions_positions and vehiclepositons_trips ([pid#189](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/189))
-   Metro line A positions integration ([pid#181](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/181))
-   Helper for generating trips id from runs

### Changed

-   filter `routeHeadingOnceNoGap` for departure boards considers also stop id to show also departures from different directions ([pid#151](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/151))
-   Change common run empty schedule log level to verbose
-   Change column type of `vehiclepositions_metro_runs_messages`.`delay_origin` from smallint to int

### Fixed

-   sorting of `stopTimeUpdate` array in an endpoint `/gtfsrt/trip_updates.pb` ([pid#192](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/192))
-   Fatal error on VYMI datasource - replace streamed datasource to correctly catch parsing and validation errors ([pid#201](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/201))
-   Fix error `Maximum call stack size exceeded` by removing recursion in PositionManager and PositionCalculator helpers. ([pid#200](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/200))

## [2.3.2] - 2022-10-13

### Added

-   New fields added to `/pid/infotexts` endpoint ([pid#152](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/152))
-   Apidocs integration tests ([pid#179](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/179))
-   Create separate worker for railtrack data
-   Receive and save metro railtrack data to `ropidgtfs_metro_railtrack_gps` ([pid#157](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/157))

### Changed

-   Update TypeScript to v4.7.2

### Fixed

-   Format GTFS trip route boolean values
-   GTFS stoptimes time parsing (from, to query parameters) ([pid#184](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/184))

## [2.3.1] - 2022-10-05

### Changed

-   Revert back to the node redis connector implementation in OG ([core#39](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/39))

## [2.3.0] - 2022-10-04

### Added

-   Create separate worker for runs
-   Receive and save metro data to `vehiclepositions_metro_runs_messages` ([pid#166](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/166))
-   Calculation of a delay on arrival and departure from stops for trams ([pid#168](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/168))

### Changed

-   Replace regular expressions in GTFS stop model ([pid#163](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/163))
-   Ropid VYMI worker updated to task based architecture ([pid#170](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/170))
-   Create partitions of departures ([pid#169](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/169))
-   reflect-metadata changed for core shared reflect object ([core#44](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/44))
-   Exchange node redis for ioredis ([core#39](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/39))

### Fixed

-   Show previous departures with the minutesbefore query parameter gt 0 ([pid#178](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/178))
-   Format GTFS trip route boolean values

### Removed

-   Remove query generator helper

## [2.2.11] - 2022-09-01

### Added

-   Add _includeMetroTrains_ query parameter to departureboards ([pid#97](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/97))
-   Save ASW attributes to DB ([pid#162](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/162))

### Changed

-   Skip departures at and pass stops with _?skip=atStop_ ([pid#141](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/141))
-   Time condition is removed for propagating delay ([pid#154](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/154))
-   The Lodash library was replaced with js functions ([core#42](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/42))
-   Only generate delay message for current trips with the uttermost start timestamp ([pid#155](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/155))
-   Ignore tram/bus run messages without the TJR attribute ([pid#155](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/155))
-   Rename external audit columns in `ropidvymi_events`
-   Move audit datetime parsing to the vymi transformation ([pid#156](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/156))

### Removed

-   Remove valid_to property from vehiclepositions last_position ([pid#161](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/161))

## [2.2.10] - 2022-07-27

### Added

-   Openapi docs for IG and OG [OG#213](https://gitlab.com/operator-ict/golemio/code/output-gateway/-/issues/213)
-   Create new route for pid infotexts [pid#133](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/133)
-   Different calculation position valid_to attribute for cancelled lines.[pid#148](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/148)

### Changed

-   New separate calculation of valid_to for trams before track.
-   Improved logic to recognize vehicles on the way from/to garage to mark them as invisible[pid#146](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/146)
-   Move logic from pid router to separate controllers

## [2.2.9] - 2022-06-28

### Changed

-   added possibility of multiple current trips (before one) for situation, when line changes number at a stop ([pid#71](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/71))
-   interpret the position state on the "V" TCP event for both trams and buses as "after track" ([pid#131](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/131))

### Added

-   Wheelchair accessibility to GTFS RT via OVapi extension

### Removed

-   Remove queue for trips model - accidental deadlocks should already be handled in the DB

## [2.2.8] - 2022-06-09

### Changed

-   re-enabled gtfs alerts

## [2.2.7] - 2022-06-09

### Changed

-   Removal of saveStopsToDB worker method and its processing branch including db table.
-   Replacing booleanPointInPolygon with CheapRuler.distance()
-   Added error handling for getShapePointsAroundPosition

### Fixed

-   Fix for /departureboards, add type for PIDDeparturesModel return value

## [2.2.6] - 2022-06-06

### Added

-   Add indexes to vymi tables

### Changed

-   Replace expireat with TTL for delay computation trips cache
-   Replace jsdom fragment functionality with regexes and html-entities
-   Disabletd GTFS Alerts generating

### Fixed

-   Get expire timestamp using the native Date object instead of Luxon DateTime

### Removed

-   Remove jsdom dependency

## [2.2.5] - 2022-05-31

### Added

-   add GTFS RT Alerts

### Changed

-   Expire VP Redis keys ([pid#139](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/139))

## [2.2.4] - 2022-05-26

### Added

-   For departure boards new attribute minutes in departures[].departure_timestamp path was added showing number of minutes till departure. ([pid#132](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/132))
-   Added filters routeHeadingOnceNoGap, routeHeadingOnceNoGapFill for departure boards

## [2.2.3] - 2022-05-20

### Fixed

-   hotfix nullable last_stop_sequence position attribute for canceled trip

## [2.2.2] - 2022-05-18

### Changed

-   instead of 3 map fns only one for cycle
-   instead moment.format() use sql literal
-   removed trip.updated_at attribute, not in API specs
-   added indexes for \_departures, \_trips, \_positions
-   refactoring of OG/PIDDepartureBoardsModel and time optimalization
-   fix ?updatedSince default value
-   fix ambiguous column name

### Fixed

-   Create shared queue for vehicle positions trips model. This should help prevent accidental deadlocks
-   Database error for multiple date parameters of GTFS routes (enforce single string validation) ([pid#126](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/126))
-   Determine train tracking status as NOT_TRACKING if last cis stop is actually the last stop of a trip ([pid#123](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/123))
-   Simplify transactions in trips and positions models to prevent more accidental deadlocks, add more logging

## [2.2.1] - 2022-05-04

### Changed

-   Always add delay to the real arrival time (even when ahead of time)
-   Always add delay to the real departure time (even when ahead of time)
-   Substract waiting time from delay (real repature time) as long as the result delay is greater than zero ([pid#51](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/51))
-   Adapted to MR add valid_to attributes ([pid#35](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/35))
-   Separate processing of tram and bus runs ([pid#66](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/66))
-   Filter internal bus lines from runs ([pid#127](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/127))

## [2.2.0] - 2022-04-13

### Changed

-   Refactoring Shapes Anchor Points algorithm, first taking shape points complementary with stops and than divide trip shape line to get desired precision. ([pid#86](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/86))
-   Minor logging improvements
-   Improve train tracking at stops ([pid#70](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/70))

## [2.1.13] - 2022-04-07

### Fixed

-   Conflicting locks in replaced tables ([pid#120](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/120))

## [2.1.12] - 2022-03-26

### Changed

-   Offset TJR timestamps for TCP busses

## [2.1.11] - 2022-03-01

### Changed

-   Added atribute is_air_conditioned (null value yet) to object trip for endpoints /vehiclepositions and /pid/departureboards
-   Fixed bug for setting next_stop_xy position attributes when trip is in BEFORE_TRACK state.

## [2.1.10] - 2022-02-21

### Added

-   Fixed bug in correction fn for already computed positions. Issue see below.
-   Correction fn for delays at stop where trips dwell for departure. ([pid#55](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/55))

## [2.1.9] - 2022-02-10

### Changed

-   Filter out infotexts based on the time_from field ([pid#92](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/92))

## [2.1.8] - 2022-01-26

### Fixed

-   Missing origin route id

## [2.1.7] - 2022-01-25

### Fixed

-   Consistent timestamp formatting in infotexts

## [2.1.6] - 2022-01-20

### Added

-   Precomputed trips ([pid#80](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/80))

### Changed

-   Change default timezone ([pid#76](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/76))
-   Remove milliseconds from timestamps ([pid#77](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/77))

## [2.1.5] - 2021-12-23

### Changed

-   Refactor and fix infotexts ([pid#74](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/74))

## [2.1.4] - 2021-12-14

### Fixed

-   Proper run scheduled gtfstrip

## [2.1.3] - 2021-12-08

### Added

-   Not public trips

### Fixed

-   Zero stoptimes
-   bikes_allowed property of undefined

### Security

-   Reenabled package auditing

## [2.1.0] - 2021-11-21

### Added

-   Modularized migrations ([pid#53](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/53))

### Changed

-   Optimization of timetables download table switching ([pid#65](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/65))
-   Replace db retention with sequelize destroy
-   Added trip_short_name to vehiclepositions ([pid#29](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/29))
-   Filter DPP after_track ([pid#34](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/34))
-   Modification of last_stop parameters ([pid#42](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/42))
-   Added stop_id to GTFS-Realtime trip update ([pid#54](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/54))

## [2.0.12] - 2021-10-18

### Fixed

-   Empty array of tripsIds in updateDelay
